@extends('layouts.app')
@section('mytitle', 'Add Consumable')
@section('content')
    <div class="bg-top bg-gray p-4">
        <div class="d-flex">
            <h5 class="font-weight-bold text-uppercase mb-0">ADD Consumable</h5>
        </div>
    </div>
    <section class="my-4 p-4 card-main">
        <div class="row">
            <div class="col-md-6 py-4">
                <div class="card shadow h-100">
                    <div class="card-top ">
                        <div class="card-title mb-0">
                            <h5 class="font-weight-bold text-uppercase mb-0"><i class="far fa-building"></i></h5>
                        </div>
                    </div>
                    <div class="card-body pt-0">
                        <form>
                            <div class="form-group">
                                <label class="control-label" for="customer_name">Consumable Code Name <small>*</small></label>
                                <input id="customer_name" type="text" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="customer_belongsto">Unit</label>
                                <select id="customer_belongsto" class="form-control">
                                    <option value="" disabled="" selected=""></option>
                                    <option value="1">Option 1</option>
                                    <option value="2">Option 2</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="category">Belongs To (branch name)</label>
                                <select class="form-control" id="category" name="category">
                                    <option disabled="" selected=""></option>
                                    <option value="piece"> MFD </option>
                                    <option value="copy"> CP</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="noc">No.of Copies</label>
                                <input id="noc" type="text" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="gst">GST(In %)</label>
                                <input id="gst" type="text" class="form-control">
                            </div>
                            <div>
                                <label class="font-weight-bold">MSL</label>
                                <div class="mb-2">
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="spares_true" name="spares" class="custom-control-input">
                                        <label class="custom-control-label" for="spares_true">Yes</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="spares_false" name="spares" class="custom-control-input">
                                        <label class="custom-control-label" for="spares_false">No</label>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-6 py-4">
                <div class="card shadow h-100">
                    <div class="card-top ">
                        <div class="card-title mb-0">
                            <h5 class="font-weight-bold text-uppercase mb-0"><i class="far fa-building"></i></h5>
                        </div>
                    </div>
                    <div class="card-body pt-0">
                        <form>
                            <div class="form-group">
                                <label class="control-label" for="reschedule_date">Consumable Product Name <small>*</small></label>
                                <input id="reschedule_date" type="text" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="customer_belongsto">HSN/SAC Number
                                    <small>*</small></label>
                                <select id="customer_belongsto" class="form-control">
                                    <option value="" disabled="" selected=""></option>
                                    <option value="1">Option 1</option>
                                    <option value="2">Option 2</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="remarks">Unit Price <small>*</small></label>
                                <input id="remarks" type="text" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="brand">Brand Name
                                    <small>*</small></label>
                                <select id="brand" class="form-control">
                                    <option value="" disabled selected></option>
                                    <option value="1">Option 1</option>
                                    <option value="2">Option 2</option>
                                </select>
                            </div>
                            <div class="mt-5 pt-4">
                                <label class="font-weight-bold">Chargeable</label>
                                <div class="mb-2">
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="chargeable_true" name="chargeable" class="custom-control-input">
                                        <label class="custom-control-label" for="chargeable_true">Yes</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="chargeable_false" name="chargeable" class="custom-control-input">
                                        <label class="custom-control-label" for="chargeable_false">No</label>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div>
            <a href="/viewpurchase_consumable" type="submit" class="btn btn-outline-primary btn-lg btn-large mr-2">Cancel</a>
            <button type="submit" class="btn btn-primary btn-lg btn-large">Submit</button>
        </div>
    </section>
@endsection
