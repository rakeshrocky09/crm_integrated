@extends('layouts.app')
@section('mytitle', 'Payment Followup')
@section('head-links')
    <link href="{{ URL::asset('assets/css/datatables.min.css') }}" rel="stylesheet">
@endsection
@section('content')
    <div class="bg-top bg-gray p-4">
        <div class="d-flex">
            <h5 class="font-weight-bold text-uppercase mb-0">Payment Followup</h5>
        </div>
    </div>

    <div class="p-4 card-main">
        <section class="my-5">
            <div class="card shadow">
                <div class="card-top ">
                    <div class="card-title mb-0">
                        <h5 class="font-weight-bold text-uppercase mb-0"><i class="fas fa-user-alt"></i></h5>
                    </div>
                    <div class="btn-right position-relative float-right">
                        <a href="#" class="btn btn-primary text-uppecase">Print</a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table" id="datatable">
                            <thead class="text-uppercase">
                                <tr>
                                    <th scope="col">PAYMENT ID</th>
                                    <th scope="col">CUSTOMER NAME</th>
                                    <th scope="col">INVOICE NO</th>
                                    <th scope="col">INVOICE DATE</th>
                                    <th scope="col">RECEIVED AMOUNT</th>
                                    <th scope="col">AMOUNT RECEIVED DATE</th>
                                    <th scope="col">Follow Up DATE</th>
                                    <th scope="col">PENDING AMOUNT</th>
                                    <th scope="col">Status</th>
                                    <th scope="col" class="nosort">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (count($payment) >= 1)
                                    @foreach ($payment as $payment)
                                        <tr>
                                            <td>{{ $payment->Payment_Id }}</td>
                                            <td>{{ $payment->Customer_Name }}</td>
                                            <td>{{ $payment->Invoice_ID }}</td>
                                            <td>{{ $payment->Invoice_Date }}</td>
                                            <td>{{ $payment->Recieved_Amount }}</td>
                                            <td>{{ $payment->Amount_Received_Date }}</td>
                                            <td>{{ $payment->Followup_Date }}</td>
                                            <td>{{ $payment->Total_Due }}</td>
                                            <td>
                                                @if ($payment->Status == 'open')
                                                    <span class="badge badge-pill badge-success text-12">
                                                        {{ $payment->Status }}
                                                    </span>
                                                @else
                                                    <span class="badge badge-pill badge-secondary text-12">
                                                        {{ $payment->Status }}
                                                    </span>
                                                @endif
                                            </td>
                                            <td class="d-flex flex-nowrap">
                                                <a href="/viewpaymentfollowup/{{ $payment->Payment_Id }}" class="btn btn-outline-info" data-toggle="tooltip"
                                                    data-placement="top" title="View Paymentfollowup">
                                                    <i class="fas fa-user-alt"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="10">Sorry, No records found</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('script')
    <script src="{{ URL::asset('assets/js/datatables.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#datatable').DataTable({
                language: {
                    searchPlaceholder: "Search records",
                    search: "",
                    "lengthMenu": "_MENU_ Entries"
                },
                "lengthMenu": [
                    [8, 16, 32, -1],
                    [8, 16, 32, "All"]
                ],
                "pageLength": 8,
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': ['nosort']
                }]
            });
        });

    </script>
@endsection
