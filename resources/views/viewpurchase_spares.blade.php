@extends('layouts.app')
@section('mytitle', 'View Spares')
@section('content')
    <div class="bg-top bg-gray p-4">
        <div class="d-flex">
            <h5 class="font-weight-bold text-uppercase mb-0">View Spares</h5>
        </div>
    </div>

    <div class="p-4 card-main">
        <section class="my-5">
            <div class="card shadow">
                <div class="card-top ">
                    <div class="card-title mb-0">
                        <h5 class="font-weight-bold text-uppercase mb-0"><i class="fas fa-user-alt"></i></h5>
                    </div>
                    <div class="btn-right position-relative float-right">
                        <a href="#" class="btn btn-primary text-uppecase">Print</a>
                        <a href="/addpurchase_spares" class="btn btn-primary text-uppecase" data-toggle="tooltip"
                            data-placement="top" title="Add Spares">
                            <i class="fas fa-plus"></i> Add
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <div class="row mx-0">
                            <div class="col-sm-12 col-md-6 pl-0 py-3">
                                <div class="dataTables_length" id="example1_length"><select aria-controls="example1"
                                        class="custom-select custom-select-sm form-control form-control-sm">
                                        <option value="10">10</option>
                                        <option value="25">25</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select></div>
                            </div>
                            <div class="col-sm-12 col-md-6 pr-0 py-3">
                                <div id="example1_filter" class="float-right">
                                    <input type="search" class="form-control form-control-sm" placeholder="Search..."
                                        aria-controls="example1">
                                </div>
                            </div>
                        </div>
                        <table class="table">
                            <thead class="text-uppercase">
                                <tr>
                                    <th scope="col">Spare ID</th>
                                    <th scope="col">PART NO.</th>
                                    <th scope="col">Item</th>
                                    <th scope="col">UNIT</th>
                                    <th scope="col">UNIT PRICE</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>SP23</td>
                                    <td>0</td>
                                    <td>Monthly Rental Charges</td>
                                    <td>Month</td>
                                    <td>0</td>
                                    <td class="d-flex flex-nowrap">
                                        <span data-toggle="modal" data-target="#viewspares" class="mr-1">
                                            <button type="button" class="btn btn-outline-info" data-toggle="tooltip"
                                                data-placement="top" title="View Spares">
                                                <i class="fas fa-user-alt"></i>
                                            </button>
                                        </span>
                                        <span data-toggle="modal" data-target="#editspares" class="mr-1">
                                            <button type="button" class="btn btn-outline-success" data-toggle="tooltip"
                                                data-placement="top" title="Edit Spares">
                                                <i class="fas fa-edit"></i>
                                            </button>
                                        </span>
                                        <span data-toggle="modal" data-target="#deletespares">
                                            <button type="button" class="btn btn-outline-danger" data-toggle="tooltip"
                                                data-placement="top" title="Delete Spares">
                                                <i class="fas fa-trash-alt"></i>
                                            </button>
                                        </span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <!-- Modal viewspares -->
                        <div class="modal fade custom-modal" id="viewspares" tabindex="-1" role="dialog"
                            aria-labelledby="viewspares" aria-hidden="true">
                            <div class="modal-dialog modal-xl modal-dialog-centered edit" role="document">
                                <div class="modal-content">
                                    <div class="icon">
                                        <span class="fa-stack fa-2x text-success">
                                            <i class="fas fa-circle fa-stack-2x"></i>
                                            <i class="fas fa-table fa-stack-1x fa-inverse"></i>
                                        </span>
                                    </div>
                                    <div class="modal-header justify-content-center">
                                        <h3 class="modal-title text-capitalize font-weight-bolder mt-3" id="viewspares">
                                            View Spares Details
                                        </h3>
                                    </div>
                                    <div class="modal-body pb-0 text-sm">
                                        <div class="row text-sm mb-4">
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">Spare ID</label>
                                                <p class="mb-0"><span>SP23</span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">HSN/SAC Number</label>
                                                <p class="mb-0"><span>997314</span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">Item</label>
                                                <p class="mb-0"><span>Monthly Rental Charges</span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">Brand Name</label>
                                                <p class="mb-0"><span>Service Charges</span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">Part No</label>
                                                <p class="mb-0"><span>0</span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">Unit</label>
                                                <p class="mb-0"><span>Month</span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">Unit Price</label>
                                                <p class="mb-0"><span>0</span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">Spare Belongs To Branch</label>
                                                <p class="mb-0"><span>KM Enterprises</span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">GST(In %)</label>
                                                <p class="mb-0"><span>18</span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">MSL</label>
                                                <p class="mb-0"><span>yes</span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">Chargeable</label>
                                                <p class="mb-0"><span>yes</span></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer justify-content-center">
                                        <button type="button" class="btn btn-success btn-lg btn-large mt-0"
                                            data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End viewspares -->
                        <!-- Modal Edit spares -->
                        <div class="modal fade custom-modal" id="editspares" tabindex="-1" role="dialog"
                            aria-labelledby="editspares" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered edit modal-xl" role="document">
                                <div class="modal-content">
                                    <div class="icon">
                                        <span class="fa-stack fa-2x text-success">
                                            <i class="fas fa-circle fa-stack-2x"></i>
                                            <i class="far fa-edit fa-stack-1x fa-inverse"></i>
                                        </span>
                                    </div>
                                    <div class="modal-header justify-content-center">
                                        <h3 class="modal-title text-capitalize font-weight-bolder mt-3" id="editspares">
                                            Edit Spares Details
                                        </h3>
                                    </div>
                                    <div class="modal-body pb-0">
                                        <form>
                                            <div class="row">
                                                <div class="form-group col-md-4">
                                                    <label for="date" class="mb-0 control-label active">Spare ID
                                                        <small>*</small></label>
                                                    <input id="date" type="text" class="form-control" value="SP23">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label class="control-label active" for="techician_branch">Brand Name
                                                        <small>*</small>
                                                    </label>
                                                    <select class="form-control" id="techician_branch"
                                                        name="techician_branch">
                                                        <option selected="true" value="1">Service Charges</option>
                                                        <option value="3">Option value</option>
                                                        <option value="3">Option value</option>
                                                    </select>
                                                </div>

                                                <div class="form-group col-md-4">
                                                    <label class="control-label active" for="techician_branch">HSN/SAC
                                                        Number
                                                        <small>*</small>
                                                    </label>
                                                    <select class="form-control" id="techician_branch"
                                                        name="techician_branch">
                                                        <option selected="true" value="1">8443</option>
                                                        <option value="2">Option 1</option>
                                                        <option value="3">Option 2</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="date" class="mb-0 control-label active">Part Number
                                                        <small>*</small></label>
                                                    <input id="date" type="text" class="form-control" value="0">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="date" class="mb-0 control-label active">Item</label>
                                                    <input id="date" type="text" class="form-control" value="0">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="date" class="mb-0 control-label active">Unit Price
                                                        <small>*</small></label>
                                                    <input id="date" type="text" class="form-control" value="0">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label class="control-label active" for="techician_branch">Unit</label>
                                                    <select class="form-control" id="techician_branch"
                                                        name="techician_branch">
                                                        <option selected="true" value="1">Month</option>
                                                        <option value="2">Option 1</option>
                                                        <option value="3">Option 2</option>
                                                    </select>
                                                </div>

                                                <div class="form-group col-md-4">
                                                    <label for="date" class="mb-0 control-label active">Belongs To
                                                        Branch</label>
                                                    <input id="date" type="text" class="form-control"
                                                        value="KM Enterprises">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="date" class="mb-0 control-label active">GST(In %)</label>
                                                    <input id="date" type="text" class="form-control" value="18">
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="font-weight-bold">MSL</label>
                                                    <div class="mb-2">
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input type="radio" id="spares_true" name="spares"
                                                                class="custom-control-input">
                                                            <label class="custom-control-label"
                                                                for="spares_true">Yes</label>
                                                        </div>
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input type="radio" id="spares_false" name="spares"
                                                                class="custom-control-input">
                                                            <label class="custom-control-label"
                                                                for="spares_false">No</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="font-weight-bold">Chargeable</label>
                                                    <div class="mb-2">
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input type="radio" id="chargeable_true" name="chargeable"
                                                                class="custom-control-input">
                                                            <label class="custom-control-label"
                                                                for="chargeable_true">Yes</label>
                                                        </div>
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input type="radio" id="chargeable_false" name="chargeable"
                                                                class="custom-control-input">
                                                            <label class="custom-control-label"
                                                                for="chargeable_false">No</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="modal-footer flex-nowrap">
                                        <button type="button" class="btn btn-outline-success btn-block btn-lg mt-0"
                                            data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-success btn-block btn-lg mt-0">Done</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Edit spares -->
                        <!-- Modal Delete spares -->
                        <div class="modal fade custom-modal" id="deletespares" tabindex="-1" role="dialog"
                            aria-labelledby="deletespares" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="icon">
                                        <span class="fa-stack fa-2x text-primary">
                                            <i class="fas fa-circle fa-stack-2x"></i>
                                            <i class="fas fa-trash-alt fa-stack-1x fa-inverse"></i>
                                        </span>
                                    </div>
                                    <div class="modal-header justify-content-center">
                                        <h3 class="modal-title text-capitalize font-weight-bolder mt-3" id="deletespares">
                                            Delete Spares?
                                        </h3>
                                    </div>
                                    <div class="modal-body pb-0">
                                        <p class="font-weight-bold text-center">
                                            Are you sure you want to delete the Spares?
                                            <br>
                                            You cannot undo this operation!
                                        </p>
                                        <div class="row justify-content-center">
                                            <div class="col-md-11">
                                                <form>
                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <label for="delete_product_id" class="col-form-label">Spare
                                                                ID</label>
                                                            <input type="text" class="form-control" id="delete_product_id"
                                                                value="SP23" readonly>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="delete_product_code" class="col-form-label">Spare
                                                                Code Name</label>
                                                            <input type="text" class="form-control" id="delete_product_code"
                                                                value="997314" readonly>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="delete_product_name" class="col-form-label">Spare
                                                                Name</label>
                                                            <input type="text" class="form-control" id="delete_product_name"
                                                                value="0" readonly>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="delete_brandname" class="col-form-label">Spare
                                                                Price</label>
                                                            <input type="text" class="form-control" id="delete_brandname"
                                                                value="0" readonly>
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <label for="delete_branch" class="col-form-label">Belongs To
                                                                Branch</label>
                                                            <input type="text" class="form-control" id="delete_branch"
                                                                value="KM Enterprises" readonly>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer flex-nowrap">
                                        <button type="button" class="btn btn-outline-primary btn-block btn-lg mt-0"
                                            data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary btn-block btn-lg mt-0">Delete</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Delete spares -->
                    </div>
                    <div class="row my-3">
                        <div class="col-md-5">
                            <div>Showing 1 to 10 of 50 entries</div>
                        </div>
                        <div class="col-md-7">
                            <nav aria-label="Page navigation example">
                                <ul class="pagination justify-content-end mb-0">
                                    <li class="page-item disabled">
                                        <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a>
                                    </li>
                                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                                    <li class="page-item"><a class="page-link" href="#">4</a></li>
                                    <li class="page-item"><a class="page-link" href="#">5</a></li>
                                    <li class="page-item mr-0">
                                        <a class="page-link" href="#">Next</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
