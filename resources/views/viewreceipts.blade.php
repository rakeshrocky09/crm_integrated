@extends('layouts.app')
@section('mytitle', 'View Receipt')
@section('content')
    <div class="p-4">
        <section class="my-5">
            <div class="card shadow">
                <div class="card-top ">
                    <div class="card-title mb-0">
                        <h6 class="font-weight-bold text-capitalize mb-0">View Receipt</h6>
                    </div>
                </div>
                <div class="card-body pt-0">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <h6 class="font-weight-bold mb-0 text-sm">Customer Name</h6>
                                <span class="text-13">{{ $select[0]->customer_name }}</span>
                            </div>
                            <div class="form-group">
                                <h6 class="font-weight-bold mb-0 text-sm">Phone Number</h6>
                                <span class="text-13">{{ $select[0]->cus_phone_no }}</span>
                            </div>
                            <div class="form-group">
                                <h6 class="font-weight-bold mb-0 text-sm">Email</h6>
                                <span class="text-13">{{ $select[0]->cus_email_id }}</span>
                            </div>
                            <div class="form-group">
                                <h6 class="font-weight-bold mb-0 text-sm">Primary Contact Person</h6>
                                <span class="text-13">{{ $select[0]->primary_contact_name }}</span>
                            </div>
                            <div class="form-group">
                                <h6 class="font-weight-bold mb-0 text-sm">Belongs To Branch</h6>
                                <span class="text-13">{{ $select[0]->cus_branch_name }}</span>
                            </div>
                            <div class="form-group">
                                <h6 class="font-weight-bold mb-0 text-sm">Invoice Id</h6>
                                <span class="text-13">{{ $select[0]->Invoice_Id }}</span>
                            </div>
                            <div class="form-group">
                                <h6 class="font-weight-bold mb-0 text-sm">Invoice Amount</h6>
                                <span class="text-13">{{ $select[0]->Invoice_Amount }}</span>
                            </div>
                            <div class="my-3">
                                <a href="{{ url('viewreceipt') }}" class="btn btn-primary">Cancel</a>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="table-responsive">
                                <table class="table table-bordered table-view">
                                    <thead>
                                        <tr>
                                            <th scope="col">Receipt ID</th>
                                            <th scope="col">Mode Of Payment</th>
                                            <th scope="col">Ref. No.</th>
                                            <th scope="col">Amount Paid</th>
                                            <th scope="col">Pending Amount</th>
                                            <th scope="col">Amount Paid Date</th>
                                            <th scope="col">Next Followup Date</th>
                                            <th scope="col">Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($select as $receipt)
                                            <tr>
                                                <td>{{ $receipt->Receipt_Id }}</td>
                                                <td>{{ $receipt->payment_mode }}</td>
                                                <td>{{ $receipt->Ref_no }}</td>
                                                <td>{{ $receipt->Paid_Amount }}</td>
                                                <td>{{ $receipt->Pending_Amount }}</td>
                                                <td>{{ $receipt->Paid_Date }}</td>
                                                <td>{{ $receipt->Next_Followup_Date }}</td>
                                                <td>
                                                    @if ($receipt->Status == 'open')
                                                        <span
                                                            class="badge badge-pill badge-success text-12">{{ $receipt->Status }}</span>
                                                    @else
                                                        <span
                                                            class="badge badge-pill badge-secondary text-12">{{ $receipt->Status }}</span>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
