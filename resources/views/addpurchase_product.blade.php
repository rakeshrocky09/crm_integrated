@extends('layouts.app')
@section('mytitle', 'Add Product')
@section('content')
    <div class="bg-top bg-gray p-4">
        <div class="d-flex">
            <h5 class="font-weight-bold text-uppercase mb-0">ADD Product</h5>
        </div>
    </div>
    <section class="my-4 p-4 card-main">
        <div class="row">
            <div class="col-md-6 py-4">
                <div class="card shadow h-100">
                    <div class="card-top ">
                        <div class="card-title mb-0">
                            <h5 class="font-weight-bold text-uppercase mb-0"><i class="far fa-building"></i></h5>
                        </div>
                    </div>
                    <div class="card-body pt-0">
                        <form>
                            <div class="row">
                                <div class="col-12">
                                    <h6 class="fw-700 text-uppercase pl-1 mb-4 mt-3">
                                        Product Details</h6>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label" for="customer_belongsto">Belongs To (branch name)</label>
                                    <select id="customer_belongsto" class="form-control">
                                        <option value="" disabled="" selected=""></option>
                                        <option value="1">Option 1</option>
                                        <option value="2">Option 2</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label" for="customer_belongsto">HSN/SAC Number
                                        <small>*</small></label>
                                    <select id="customer_belongsto" class="form-control">
                                        <option value="" disabled="" selected=""></option>
                                        <option value="1">Option 1</option>
                                        <option value="2">Option 2</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label" for="customer_belongsto">Brand Name
                                        <small>*</small></label>
                                    <select id="customer_belongsto" class="form-control">
                                        <option value="" disabled="" selected=""></option>
                                        <option value="1">Option 1</option>
                                        <option value="2">Option 2</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label" for="customer_name">Product Name<small>*</small></label>
                                    <input id="customer_name" type="text" class="form-control">
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label" for="customer_name">Model Name <small>*</small></label>
                                    <input id="customer_name" type="text" class="form-control">
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label" for="customer_belongsto">Product Category</label>
                                    <select id="customer_belongsto" class="form-control">
                                        <option value="" disabled="" selected=""></option>
                                        <option value="1">Option 1</option>
                                        <option value="2">Option 2</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label active" for="techician_name">Description
                                        <small>*</small></label>
                                    <textarea class="form-control mt-1" id="exampleFormControlTextarea1"
                                        rows="2"></textarea>
                                </div>
                                <div class="form-group col-md-6 mt-3 pt-1">
                                    <label class="control-label" for="customer_belongsto">GST(In %)
                                        <small>*</small></label>
                                    <select id="customer_belongsto" class="form-control">
                                        <option value="" disabled="" selected=""></option>
                                        <option value="1">Option 1</option>
                                        <option value="2">Option 2</option>
                                    </select>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-6 py-4">
                <div class="card shadow h-100">
                    <div class="card-top ">
                        <div class="card-title mb-0">
                            <h5 class="font-weight-bold text-uppercase mb-0"><i class="far fa-building"></i></h5>
                        </div>
                    </div>
                    <div class="card-body pt-0">
                        <form>
                            <h6 class="fw-700 text-uppercase pl-1 mb-4 mt-3">Product Parameters</h6>
                            <div class="form-group">
                                <label class="control-label" for="brand">Color
                                    <small>*</small></label>
                                <select id="brand" class="form-control">
                                    <option value="" disabled selected></option>
                                    <option value="1">Option 1</option>
                                    <option value="2">Option 2</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="category">Units </label>
                                <select class="form-control" id="category" name="category">
                                    <option disabled="" selected=""></option>
                                    <option value="piece"> MFD </option>
                                    <option value="copy"> CP</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="reschedule_date">Part Number</label>
                                <input id="reschedule_date" type="text" class="form-control">
                            </div>
                            <div class="form-group mt-4 pt-2">
                                <label class="control-label" for="remarks">Product Price <small>*</small></label>
                                <input id="remarks" type="text" class="form-control">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div>
            <a href="/viewpurchase_product" type="submit" class="btn btn-outline-primary btn-lg btn-large mr-2">Cancel</a>
            <button type="submit" class="btn btn-primary btn-lg btn-large">Submit</button>
        </div>
    </section>
@endsection
