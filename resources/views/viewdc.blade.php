@extends('layouts.app')
@section('mytitle', 'View DELIVERY CHALLAN')
@section('head-links')
    <link href="{{ URL::asset('assets/css/datatables.min.css') }}" rel="stylesheet">
@endsection
@section('content')
    <div class="bg-top bg-gray p-4">
        <div class="d-flex">
            <h5 class="font-weight-bold text-uppercase mb-0">View DELIVERY CHALLAN</h5>
        </div>
    </div>
    <div class="p-4 card-main">
        <section class="my-5">
            <div class="card shadow">
                <div class="card-top">
                    <div class="card-title mb-0">
                        <h5 class="font-weight-bold text-uppercase mb-0"><i class="fas fa-file-invoice"></i></h5>
                    </div>
                    <div class="btn-right position-relative float-right">
                        <a href="/adddc" class="btn btn-primary text-uppecase" data-toggle="tooltip" data-placement="top"
                            title="Add Challan">
                            <i class="fas fa-plus"></i> Add
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table display" id="dc_table">
                            <thead class="text-uppercase">
                                <tr>
                                    <th scope="col">DELIVERY ID</th>
                                    <th scope="col">CUSTOMER NAME</th>
                                    <th scope="col">DELIVERY NOTE</th>
                                    <th scope="col">DELIVERY DATE</th>
                                    <th scope="col" class="nosort">ACTION</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($challan as $item)
                                    <tr>
                                        <td>{{ $item->Challan_Const }}{{ $item->Challan_ID }}</td>
                                        <td>{{ $item->Customer_Name }}</td>
                                        <td>{{ $item->Delivery_Note }}</td>
                                        <td>{{ $item->Delivery_Date }}</td>
                                        <td class="d-flex flex-nowrap">
                                            <span id="view_data" data-toggle="modal" data-target="#view_delivery_modal"
                                                data-id="{{ $item->Challan_ID }}">
                                                <button type="button" class="btn btn-outline-info mr-1"
                                                    data-toggle="tooltip" data-placement="top" title="View challan">
                                                    <i class="fas fa-external-link-alt"></i>
                                                </button>
                                            </span>
                                            <a href="{{ url('/edit_dc', $item->Challan_ID) }}"
                                                class="btn btn-outline-success mr-1" data-toggle="tooltip"
                                                data-placement="top" title="Edit challan">
                                                <i class="fas fa-edit">
                                                </i>
                                            </a>
                                            <span data-toggle="modal" data-target="#deletechallan"
                                                data-id="{{ $item->Challan_ID }}" data-name="{{ $item->Customer_Name }}"
                                                data-note="{{ $item->Delivery_Note }}"
                                                data-date="{{ $item->Customer_Name }}">
                                                <button type="button" class="btn btn-outline-danger" data-toggle="tooltip"
                                                    data-placement="top" title="Delete challan">
                                                    <i class="fas fa-trash-alt">
                                                    </i>
                                                </button>
                                            </span>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <!-- Modal View Invoice -->
                        <div class="modal fade custom-modal" id="view_delivery_modal" tabindex="-1" role="dialog"
                            aria-labelledby="view_delivery_modal" aria-hidden="true">
                            <div class="modal-dialog modal-xxl modal-dialog-centered edit" role="document">
                                <div class="modal-content">
                                    <div class="icon">
                                        <span class="fa-stack fa-2x text-success">
                                            <i class="fas fa-circle fa-stack-2x"></i>
                                            <i class="fas fa-table fa-stack-1x fa-inverse"></i>
                                        </span>
                                    </div>
                                    <div class="modal-header justify-content-center">
                                        <h3 class="modal-title text-capitalize font-weight-bolder mt-3"
                                            id="view_delivery_modal">
                                            Delivery Challan Details
                                        </h3>
                                    </div>
                                    <div class="modal-body pb-0">
                                        <div class="invoice_content">
                                            <div id="invoice_content">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="row text-sm">
                                                            <div class="col-12 mb-3">
                                                                <img class="d-inline-block"
                                                                    src="{{ asset('assets/img/km_logo.png') }}">
                                                                <h6 class="d-inline-block fw-700 text-uppercase"
                                                                    id="c_name"></h6>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <address id="v_comp_addr" class="mb-0"></address>
                                                                <p class="mb-0" id="gst_no"></p>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group mb-0">
                                                                    <label class="font-weight-bold mb-0">Delivery
                                                                        Note:</label>
                                                                    <label class="mb-0" id="delivery_note"></label>
                                                                </div>
                                                                <div class="form-group mb-0">
                                                                    <label class="font-weight-bold mb-0">Delivery Note
                                                                        Date:</label>
                                                                    <label class="mb-0" id="delivery_date"></label>
                                                                </div>
                                                                <div class="form-group mb-0">
                                                                    <label class="font-weight-bold mb-0">Other
                                                                        Reference:</label>
                                                                    <label class="mb-0" id="other_reference"></label>
                                                                </div>
                                                                <div class="form-group mb-0">
                                                                    <label class="font-weight-bold mb-0">Supplier
                                                                        Ref Num:</label>
                                                                    <label class="mb-0" id="supplier_reference"></label>
                                                                </div>
                                                                <div class="form-group mb-0">
                                                                    <label class="font-weight-bold mb-0">Buyer
                                                                        Order No:</label>
                                                                    <label class="mb-0" id="buyer_orderno"></label>
                                                                </div>
                                                                <div class="form-group mb-0">
                                                                    <label class="font-weight-bold mb-0">Buyer
                                                                        Order Date:</label>
                                                                    <label class="mb-0" id="buyer_orderdate"></label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group mb-0">
                                                                    <label class="font-weight-bold mb-0">Mode / Term of
                                                                        Payment:</label>
                                                                    <label class="mb-0" id="payment_mod"></label>
                                                                </div>
                                                                <div class="form-group mb-0">
                                                                    <label class="font-weight-bold mb-0">Delivery Document
                                                                        No:</label>
                                                                    <label class="mb-0" id="delivery_doc_no"></label>
                                                                </div>
                                                                <div class="form-group mb-0">
                                                                    <label class="font-weight-bold mb-0">Dispatched
                                                                        Through:</label>
                                                                    <label class="mb-0" id="dispatched_through"></label>
                                                                </div>
                                                                <div class="form-group mb-0">
                                                                    <label class="font-weight-bold mb-0">Terms of
                                                                        Delivery:</label>
                                                                    <label class="mb-0" id="delivery_terms"></label>
                                                                </div>
                                                                <div class="form-group mb-0">
                                                                    <label
                                                                        class="font-weight-bold mb-0">Destination:</label>
                                                                    <label class="mb-0" id="destination"></label>
                                                                </div>
                                                                <div class="form-group mb-0">
                                                                    <label class="font-weight-bold mb-0">Machine Serial
                                                                        No:</label>
                                                                    <label class="mb-0" id="machine_serialno"></label>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-12">
                                                                <div class="row mt-3">
                                                                    <div class="col-md-4">
                                                                        <address class="mb-0">
                                                                            <label class="font-weight-bold">Billed
                                                                                To</label>
                                                                            <p id="cust_name" class="mb-0"></p>
                                                                            <p id="cust_addr" class="mb-0"></p>
                                                                            <p class="mb-0" id="cust_phone"></p>
                                                                        </address>
                                                                        <p class="mb-0" id="cust_gst_no"></p>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <address>
                                                                            <label
                                                                                class="font-weight-bold">Consignee</label>
                                                                            <p id="delivery_name" class="mb-0"></p>
                                                                            <p id="delivery_addr" class="mb-0"></p>
                                                                            <p class="mb-0" id="delivery_phone"></p>
                                                                        </address>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-12">
                                                    <h5 class="fw-700 mt-4 mb-0">Order Summary</h5>
                                                    <hr class="mt-2">
                                                </div>
                                            </div>
                                            <table class="table table-bordered text-center">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">Item Id</th>
                                                        <th scope="col">Item Name</th>
                                                        <th scope="col">HSN/SAC</th>
                                                        <th scope="col">GST</th>
                                                        <th scope="col">Unit Price</th>
                                                        <th scope="col">Quantity</th>
                                                        <th scope="col">Discount</th>
                                                        <th scope="col">Description</th>
                                                        <th scope="col">Item Amount</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="item_table_body"></tbody>
                                                <tfoot id="item_table_footer"></tfoot>
                                            </table>
                                            <h6 class="fw-700 mt-3">Tax details</h6>
                                            <table class="table table-bordered text-center">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">Taxable Amount</th>
                                                        <th scope="col">CGST %</th>
                                                        <th scope="col">CGST Amount</th>
                                                        <th scope="col">SGST %</th>
                                                        <th scope="col">SGST Amount</th>
                                                        <th scope="col">Tax Amount</th>
                                                        <th scope="col">Total Amount</th>
                                                    </tr>
                                                </thead>
                                                <tbody class="font-weight-bold" id="tax_table_body"></tbody>
                                                <tfoot id="tax_table_footer"></tfoot>
                                            </table>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="row text-sm">
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label class="font-weight-bold h6 text-capitalize">Bank
                                                                    Details</label>
                                                                <address class="text-12 mb-0" id="bank_details"></address>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group mb-0">
                                                                <label class="font-weight-bold">Total Bill
                                                                    Amount</label>
                                                                <p class="mb-0">(including GST)</p>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group mb-0 font-weight-bold">
                                                                <label id="invoice_amount"></label>
                                                                <p class="mb-0 text-capitalize" id="invoice_amount_words">
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 my-3">
                                                    <div class="row">
                                                        <div class="col-md-6 font-weight-bold">
                                                            <h6 class="fw-700 text-sm">Declaration</h6>
                                                            <p class="mb-0 font-weight-light text-10">
                                                                We declare that this invoice shows the actual price of
                                                                the
                                                                goods
                                                                described <br>and that all
                                                                the particulars are true and correct.
                                                            </p>
                                                        </div>
                                                        <div class="col-md-3"></div>
                                                        <div class="col-md-3 font-weight-bold float-right">
                                                            <h6 class="text-uppercase mb-4 pb-1">KM ENTERPRISES</h6>
                                                            <p class="mb-0">Authorised signatory</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                    <div id="edit_modal_btn" class="modal-footer flex-nowrap"></div>
                                </div>
                            </div>
                        </div>
                        <!-- End View Invoice -->
                        <!-- Modal Delete Customer -->
                        <div class="modal fade custom-modal" id="deletechallan" tabindex="-1" role="dialog"
                            aria-labelledby="deletechallan" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <form id="delete_form" method="GET">
                                        @csrf
                                        <div class="icon">
                                            <span class="fa-stack fa-2x text-primary">
                                                <i class="fas fa-circle fa-stack-2x"></i>
                                                <i class="fas fa-trash-alt fa-stack-1x fa-inverse"></i>
                                            </span>
                                        </div>
                                        <div class="modal-header justify-content-center">
                                            <h3 class="modal-title text-capitalize font-weight-bolder mt-3"
                                                id="deletechallan">
                                                Delete
                                                Challan?
                                            </h3>
                                        </div>
                                        <div class="modal-body pb-0">
                                            <p class="font-weight-bold text-center">
                                                Are you sure you want to delete the Challan?
                                                <br>
                                                You cannot
                                                undo this operation!
                                            </p>
                                            <div class="row justify-content-center">
                                                <div class="col-md-11">
                                                    <div class="form-group">
                                                        <label for="delete_cus_name" class="col-form-label">Customer
                                                            Name</label>
                                                        <input type="text" class="form-control" id="delete_cus_name"
                                                            readonly>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="delete_note" class="col-form-label">Challan Note</label>
                                                        <input type="text" class="form-control" id="delete_note" readonly>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="delete_date" class="col-form-label">Challan Date</label>
                                                        <input type="text" class="form-control" id="delete_date" readonly>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer flex-nowrap">
                                            <button type="button" class="btn btn-outline-primary btn-block btn-lg mt-0"
                                                data-dismiss="modal">Close</button>
                                            <button type="submit"
                                                class="btn btn-primary btn-block btn-lg mt-0">Delete</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- End Delete Customer -->
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('script')
    <script src="{{ URL::asset('assets/js/datatables.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            //datatable  
            $('#dc_table').DataTable({
                language: {
                    searchPlaceholder: "Search records",
                    search: "",
                    "lengthMenu": "_MENU_ Entries"
                },
                "lengthMenu": [
                    [8, 16, 32, -1],
                    [8, 16, 32, "All"]
                ],
                "pageLength": 8,
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': ['nosort']
                }]
            });
            //end datatable

            $("#view_delivery_modal").on('show.bs.modal', function(event) {
                var challan = $(event.relatedTarget);
                var id = challan.data('id');
                console.log('Grab challan_id:', id, " when view modal is opened");

                challan_details(id).done(function(challan) {
                    challan_items(id).done(function(dcitems) {
                        challan_total(id).done(function(dctotal) {
                            //console.log("challan_details:", challan);
                            load_challan_view(challan, dcitems, dctotal, id);
                        });
                    });
                });
                //challan_details(challan_id);
            });

            $("#deletechallan").on('show.bs.modal', function(event) {
                var challan = $(event.relatedTarget);
                var id = challan.data('id');
                var name = challan.data('name');
                var note = challan.data('note');
                var date = challan.data('date');

                var action = `{{ url('deletedc/${id}') }}`
                $('#delete_form').attr('action', action);
                $("#delete_cus_name").val(name);
                $("#delete_note").val(note);
                $("#delete_date").val(date);
            });
        });



        function load_challan_view(challanObj, dcitemsObj, dctotalObj, id) {
            console.log("Delivery Challan details:", challanObj);
            console.log("Delivery Challan items:", dcitemsObj);
            console.log("Delivery Challan total:", dctotalObj);
            $('#c_name').html(challanObj.org_branch_name);
            $("#v_comp_addr").html(challanObj.org_address + "<br>" + challanObj.org_city + "<br>" + challanObj
                .org_state +
                " - " + challanObj.org_pincode + "<br><span>phone:</span>  " + challanObj.org_phone_no);
            $('#gst_no').html(`<span class="font-weight-bold">GST No:</span>${challanObj.org_gstno}`);

            $('#cust_name').html(challanObj.Customer_Name);
            $("#cust_addr").html(challanObj.cus_address + "<br>" + challanObj.cus_city + "<br>" + challanObj
                .cus_state +
                " - " + challanObj.cus_pincode + "<br><span>phone:</span>  " + challanObj.cus_phone_no);
            $('#cust_gst_no').html(
                `<span class="font-weight-bold">GST No:</span>${challanObj.cus_gst_percentage}`);

            $('#delivery_name').html(challanObj.delivery_name);
            $("#delivery_addr").html(challanObj.delivery_address + "<br>" + challanObj.delivery_city + "<br>" +
                challanObj
                .delivery_state + " - " + challanObj.delivery_pincode + "<br><span>phone:</span>  " +
                challanObj
                .primary_contact_phoneno);

            $('#delivery_note').html(challanObj.Delivery_Note);
            $('#delivery_date').html(challanObj.Delivery_Date);
            $('#other_reference').html(challanObj.Other_Reference);
            $('#supplier_reference').html(challanObj.Supplier_Reference);
            $('#buyer_order_no').html(challanObj.Buyer_Order_No);
            $('#buyer_order_date').html(challanObj.Buyer_Order_Date);

            $('#payment_mod').html(challanObj.Mode_Terms_Payment);
            $('#delivery_doc_no').html(challanObj.delivery_no);
            $('#dispatched_through').html(challanObj.Dispatched_Through);
            $('#delivery_terms').html(challanObj.Terms_Of_Delivery);
            $('#destination').html(challanObj.Destination);
            $('#machine_serialno').html(challanObj.Machine_Serial_No);

            $('#bank_details').html(
                `${challanObj.org_holder_name}<br>${challanObj.org_bank_name}<br>${challanObj.org_account_no}<br>${challanObj.org_bank_branch}<br>${challanObj.org_bank_ifsc}<br>`
            );

            $('#invoice_amount_words').html(challanObj.Tax_Amount_Words);
            $('#invoice_amount').html(challanObj.Total_Bill_Amount);


            let item_row = "";
            let item_row_total = "";
            let tax_row = "";
            let tax_row_total = "";
            $.each(dcitemsObj, function(key, value) {
                let a = parseFloat(value.item_total)
                let b = isNaN(parseFloat(value.item_total)) ? 0 : parseFloat(value.item_total);
                let tax_total = a + b;
                //console.log("tax_total", tax_total);

                item_row += "<tr>";
                tax_row += "<tr>";

                item_row += "<td>" + value.id + "</td>";
                item_row += "<td>" + value.name + "</td>";
                item_row += "<td>" + value.hsn + "</td>";
                item_row += "<td>" + value.gst + "</td>";
                item_row += "<td>" + value.price + "</td>";
                item_row += "<td>" + value.quantity + "</td>";
                item_row += "<td>" + value.discount + "</td>";
                item_row += "<td>" + value.descriptions + "</td>";
                item_row += "<td>" + value.item_total + "</td>";

                tax_row += "<td>" + value.item_total + "</td>";
                tax_row += "<td>" + value.cgst + "</td>";
                tax_row += "<td>" + value.cgst_amount + "</td>";
                tax_row += "<td>" + value.sgst + "</td>";
                tax_row += "<td>" + value.sgst_amount + "</td>";
                tax_row += "<td>" + value.tax_amount + "</td>";
                tax_row += "<td>" + tax_total + "</td>";

                tax_row += "</tr>";
                item_row += "</tr>";
            });

            item_row_total =
                `<tr class="border-top"><td colspan="8" class="text-right">Total Item Amount</td><td>${dctotalObj[0].items_total}</td></tr>`
            tax_row_total =
                `<tr class="border-top"><td colspan="5" class="text-right">Total Item Amount</td><td>${dctotalObj[0].total_tax}</td><td id='total_tax'>asd</td></tr>`
            //push data into invoice modal 
            $("#item_table_body").html(item_row);
            $("#item_table_footer").html(item_row_total);
            $("#tax_table_body").html(tax_row);
            $("#tax_table_footer").html(tax_row_total);
            $("#total_tax").html(challanObj.Total_Bill_Amount);

            $('#edit_modal_btn').html(
                `<button type="button" class="btn btn-outline-success btn-block btn-lg mt-0" data-dismiss="modal">Close</button><a href="{{ url('dcprint/${id}') }}" id="download_invoice" class="btn btn-success btn-block btn-lg mt-0">Download</a>`
            );
        }

        //get challan details
        function challan_details(id) {
            var user = {!! json_encode((array) auth()->user()->username) !!};
            return $.ajax({
                url: "/" + user + "/dcdetails",
                type: "GET",
                dataType: 'json',
                data: {
                    id: id,
                },
                success: function(data) {
                    //console.log("invoice_details ajax:", data);
                },
                error: function(error) {
                    console.log('challan_details error:', error);
                }
            });
        }
        //get challan item details
        function challan_items(id) {
            var user = {!! json_encode((array) auth()->user()->username) !!};
            return $.ajax({
                url: "/" + user + "/challanitems",
                type: "GET",
                dataType: 'json',
                data: {
                    id: id,
                },
                success: function(data) {
                    // console.log("challan_items", data);
                },
                error: function(error) {
                    console.log('challan_items error:', error);
                }
            });
        }
        //get challan total
        function challan_total(id) {
            var user = {!! json_encode((array) auth()->user()->username) !!};
            return $.ajax({
                url: "/" + user + "/challantotal",
                type: "GET",
                dataType: 'json',
                data: {
                    id: id,
                },
                success: function(data) {
                    // console.log("challan_total", data);
                },
                error: function(error) {
                    console.log('challan_total error:', error);
                }
            });
        }

    </script>
@endsection
