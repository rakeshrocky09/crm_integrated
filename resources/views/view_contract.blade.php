@extends('layouts.app')
@section('mytitle', 'View Contract')
@section('content')

    <div class="p-4">
        <section class="my-5">
            <div class="card shadow">
                <div class="card-top ">
                    <div class="card-title mb-0">
                        <h6 class="font-weight-bold text-capitalize mb-0">View Contract</h6>
                    </div>
                </div>
                <div class="card-body pt-0">
                    <div class="row">
                        <div class="col-md-3 text-sm">
                            <h6 class="fw-700 text-uppercase mb-3">Customer info</h6>
                            <div class="form-group">
                                <h6 class="font-weight-bold mb-0 text-sm">Customer Name</h6>
                                <span class="text-13">{{ $contract[0]->customer_name }}</span>
                            </div>
                            <div class="form-group">
                                <h6 class="font-weight-bold mb-0 text-sm">Phone No</h6>
                                <span class="text-13">{{ $contract[0]->cus_phone_no }}</span>
                            </div>

                            <h6 class="fw-700 text-uppercase mb-3 mt-4 pt-2">Contract Details</h6>
                            <div class="form-group">
                                <h6 class="font-weight-bold mb-0 text-sm">Contract Type</h6>
                                <span class="text-13">{{ $contract[0]->Contract_type }}</span>
                            </div>
                            <div class="form-group">
                                <h6 class="font-weight-bold mb-0 text-sm">Start Date</h6>
                                <span class="text-13">{{ $contract[0]->Start_Date }}</span>
                            </div>
                            <div class="form-group">
                                <h6 class="font-weight-bold mb-0 text-sm">Monthly Billing Date</h6>
                                <span class="text-13">{{ $contract[0]->Contract_Billing_Date }}</span>
                            </div>
                            <div class="form-group">
                                <h6 class="font-weight-bold mb-0 text-sm">Product Ownership</h6>
                                <span class="text-13">{{ $contract[0]->Product_Ownership }}</span>
                            </div>
                            <div class="my-3">
                                <a href="{{ url()->previous() }}" class="btn btn-primary">Cancel</a>
                            </div>
                        </div>
                        <div class="col-md-9 pl-md-0">
                            <ul class="nav nav-pills" id="pills-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="pills-contract-tab" data-toggle="pill"
                                        href="#pills-contract" role="tab" aria-controls="pills-contract"
                                        aria-selected="true">Contract Details</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-product-tab" data-toggle="pill" href="#pills-product"
                                        role="tab" aria-controls="pills-product" aria-selected="false">Product</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-spares-tab" data-toggle="pill" href="#pills-spares"
                                        role="tab" aria-controls="pills-spares" aria-selected="false">Spares</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-consumables-tab" data-toggle="pill"
                                        href="#pills-consumables" role="tab" aria-controls="pills-consumables"
                                        aria-selected="false">Consumables</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-schedule-tab" data-toggle="pill" href="#pills-schedule"
                                        role="tab" aria-controls="pills-schedule" aria-selected="false">PM Schedule</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-expenses-tab" data-toggle="pill" href="#pills-expenses"
                                        role="tab" aria-controls="pills-expenses" aria-selected="false">Expenses</a>
                                </li>
                            </ul>
                            <div class="tab-content" id="pills-tabContent">
                                <div class="tab-pane fade show active my-3" id="pills-contract" role="tabpanel"
                                    aria-labelledby="pills-contract-tab">
                                    <div class="mb-3">
                                        <h6 class="fw-700 text-uppercase mb-4">Contract Details</h6>
                                        <p>{{ $contract[0]->Contract_type }}</p>
                                        <div class="row text-sm">
                                            <div class="form-group col-md-4">
                                                <label class="font-weight-bold">Black A3</label>
                                                <p class="mb-0">{{ $contract[0]->c4_blacka3 }}</p>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label class="font-weight-bold">Black A4</label>
                                                <p class="mb-0">{{ $contract[0]->c4_blacka4 }}</p>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label class="font-weight-bold">Black Copies</label>
                                                <p class="mb-0">{{ $contract[0]->c4_black_copies }}</p>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label class="font-weight-bold">color A3</label>
                                                <p class="mb-0">{{ $contract[0]->c4_colora3 }}</p>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label class="font-weight-bold">Color A4</label>
                                                <p class="mb-0">{{ $contract[0]->c4_colora4 }}</p>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label class="font-weight-bold">Color Copies</label>
                                                <p class="mb-0">{{ $contract[0]->c4_color_copies }}</p>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label class="font-weight-bold">Scanner A3</label>
                                                <p class="mb-0">{{ $contract[0]->c4_scannera3 }}</p>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label class="font-weight-bold">Scanner A4</label>
                                                <p class="mb-0">{{ $contract[0]->c4_scannera4 }}</p>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label class="font-weight-bold">Scanner Copies</label>
                                                <p class="mb-0">{{ $contract[0]->c4_scanner_copies }}</p>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label class="font-weight-bold">Credit Period(#Days)</label>
                                                <p class="mb-0">{{ $contract[0]->Rental_Credit_Period }}</p>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label class="font-weight-bold">Rental Chargers</label>
                                                <p class="mb-0">{{ $contract[0]->rental_rentalcharges }}</p>
                                            </div>
                                            <div class="col-md-12">
                                                <h6 class="fw-700 text-uppercase my-4">Service Related Charges</h6>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label class="font-weight-bold">Service Charge(Rs)</label>
                                                <p class="mb-0">{{ $contract[0]->Service_Charge }}</p>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label class="font-weight-bold">Visit Charge(Rs)</label>
                                                <p class="mb-0">{{ $contract[0]->Visit_Charge }}</p>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label class="font-weight-bold">Installation Charge(Rs) </label>
                                                <p class="mb-0">{{ $contract[0]->Installation_Charge }}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane fade show my-3" id="pills-product" role="tabpanel"
                                    aria-labelledby="pills-product-tab">
                                    <div class="d-flex justify-content-between align-items-center mb-2">
                                        <h6 class="fw-700 text-uppercase pl-1">Product Details</h6>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-view">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Machine Serial No.</th>
                                                    <th scope="col">Product Model Name</th>
                                                    <th scope="col">Product Brand Name</th>
                                                    <th scope="col">Installed Date</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($product as $item)
                                                    <tr>
                                                        <td>{{ $item->Machine_Serial_No }}</td>
                                                        <td>{{ $item->product_model_name }}</td>
                                                        <td>{{ $item->product_brand_name }}</td>
                                                        <td>{{ $item->installation_date }}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="tab-pane fade show my-3" id="pills-spares" role="tabpanel"
                                    aria-labelledby="pills-spares-tab">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-view">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Spare Code Name</th>
                                                    <th scope="col">Spare Name</th>
                                                    <th scope="col">Unit Price</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($spares as $item)
                                                    <tr>
                                                        <td>{{ $item->Item_Code_Name }}</td>
                                                        <td>{{ $item->Item_Name }}</td>
                                                        <td>{{ $item->Item_Price }}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="tab-pane fade show my-3" id="pills-consumables" role="tabpanel"
                                    aria-labelledby="pills-consumables-tab">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-view">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Consumable Code Name</th>
                                                    <th scope="col">Consumable Name</th>
                                                    <th scope="col">Unit Price</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($consumables as $item)
                                                    <tr>
                                                        <td>{{ $item->Item_Code_Name }}</td>
                                                        <td>{{ $item->Item_Name }}</td>
                                                        <td>{{ $item->Item_Price }}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="tab-pane fade show my-3" id="pills-schedule" role="tabpanel"
                                    aria-labelledby="pills-schedule-tab">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-view">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Machine Serial No.</th>
                                                    <th scope="col">Scheduled Date</th>
                                                    <th scope="col">Model Name</th>
                                                    <th scope="col">Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($pms as $item)
                                                    <tr>
                                                        <td>{{ $item->Machine_Serial_No }}</td>
                                                        <td>{{ $item->Follow_Up_Date }}</td>
                                                        <td>{{ $item->Item_Model_Name }}</td>
                                                        <td>
                                                            @if ($item->Status == 'open')
                                                                <span class="badge badge-pill badge-success text-12">
                                                                    {{ $item->Status }}
                                                                </span>
                                                            @else
                                                                <span class="badge badge-pill badge-secondary text-12">
                                                                    {{ $item->Status }}
                                                                </span>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="tab-pane fade show my-3" id="pills-expenses" role="tabpanel"
                                    aria-labelledby="pills-expenses-tab">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-view">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Date</th>
                                                    <th scope="col">Item Type</th>
                                                    <th scope="col">Description</th>
                                                    <th scope="col">Amount</th>
                                                    <th scope="col">Invoice</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection
