<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Challan</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <style>
        .text-sm {
            font-size: 10.5px;
        }

        .text-12 {
            font-size: 12px;
        }

        .text-14 {
            font-size: 14px;
        }

        .va-t {
            vertical-align: top;
        }

        .va-m {
            vertical-align: middle;
        }

        table.table th,
        td {
            padding: 4px !important;
        }

        .w-59 {
            width: 59%;
        }

        .w-50 {
            width: 50%;
        }

        .w-40 {
            width: 40%;
        }

        .w-38 {
            width: 38%;
        }

        .w-30 {
            width: 30%;
        }

        .w-29 {
            width: 29%;
        }

        .w-21 {
            width: 21%;
        }

        .w-20 {
            width: 20%;
        }

        label {
            margin-bottom: 0;
        }

        @page {
            margin: 25px;
        }

        footer {
            position: fixed;
            bottom: 0;
            left: 0;
            right: 0;
            height: 4.3cm;
        }

    </style>
</head>

<body>
    <header>
        <div class="mb-2">
            <img class="d-inline-block va-m" src="{{ public_path('assets/img/km_logo.png') }}" width="30">
            <h6 class="d-inline-block va-m"> {{ $challan->org_branch_name }}</h6>
        </div>
        <div class="text-12">
            <div class="d-inline-block va-t w-40">
                <address class="mb-0">
                    {{ $challan->org_address }}<br>
                    {{ $challan->org_city }}<span>, </span>{{ $challan->org_state }}<span> -
                    </span>{{ $challan->org_pincode }}
                </address>
                <p class="mb-0"> <span>phone:</span> {{ $challan->org_phone_no }}</p>
                <p class="mb-0"><span class="font-weight-bold">GST No: </span> {{ $challan->org_gstno }}</p>
            </div>
            <div class="d-inline-block va-t w-29">

                <label class="font-weight-bold d-inline-block">Delivery Note:</label>
                <label class="d-inline-block">{{ $challan->Delivery_Note }}</label>
                <br>
                <label class="font-weight-bold d-inline-block">Delivery Note Date:</label>
                <label class="d-inline-block">{{ $challan->Delivery_Date }}</label>
                <br>
                <label class="font-weight-bold d-inline-block">Other Reference:</label>
                <label class="d-inline-block">{{ $challan->Other_Reference }}</label>
                <br>
                <label class="font-weight-bold d-inline-block">Supplier Ref Num:</label>
                <label class="d-inline-block">{{ $challan->Supplier_Reference }}</label>
                <br>
                <label class="font-weight-bold d-inline-block">Buyer Order No:</label>
                <label class="d-inline-block">{{ $challan->Buyer_Order_No }}</label>
                <br>
                <label class="font-weight-bold d-inline-block">Buyer Order Date:</label>
                <label class="d-inline-block">{{ $challan->Buyer_Order_Date }}</label>
            </div>
            <div class="d-inline-block va-t w-29">
                <label class="font-weight-bold d-inline-block">Mode / Term of Payment:</label>
                <label class="d-inline-block">{{ $challan->Mode_Terms_Payment }}</label>
                <br>
                <label class="font-weight-bold d-inline-block">Delivery Document No:</label>
                <label class="d-inline-block">{{ $challan->delivery_no }}</label>
                <br>
                <label class="font-weight-bold d-inline-block">Dispatched Through:</label>
                <label class="d-inline-block">{{ $challan->Dispatched_Through }}</label>
                <br>
                <label class="font-weight-bold d-inline-block">Terms of Delivery:</label>
                <label class="d-inline-block">{{ $challan->Terms_Of_Delivery }}</label>
                <br>
                <label class="font-weight-bold d-inline-block">Destination:</label>
                <label class="d-inline-block">{{ $challan->Destination }}</label>
                <br>
                <label class="font-weight-bold d-inline-block">Machine Serial No:</label>
                <label class="d-inline-block">{{ $challan->Machine_Serial_No }}</label>
            </div>
        </div>
        <div class="text-12 mt-2">
            <div class="d-inline-block va-t w-40">
                <h6 class="font-weight-bold text-14 mb-2">Billed To</h6>
                <address class="mb-0">
                    <span>{{ $challan->Customer_Name }}/span><br>
                        {{ $challan->cus_address }}<br>
                        {{ $challan->cus_city }}<span>, </span>{{ $challan->cus_state }}<span> -
                        </span>{{ $challan->cus_pincode }}
                </address>
                <p class="mb-0"><span>phone: </span>{{ $challan->cus_phone_no }}</p>
                <p class="mb-0"><span class="font-weight-bold">GST No: </span>{{ $challan->cus_gst_percentage }}</p>
            </div>
            <div class="d-inline-block va-t w-40">
                <h6 class="font-weight-bold text-14 mb-2">Consignee</h6>
                <address class="mb-0">
                    <span>{{ $challan->delivery_name }}/span><br>
                        {{ $challan->delivery_address }}<br>
                        {{ $challan->delivery_city }}<span>, </span>{{ $challan->delivery_state }}<span> -
                        </span>{{ $challan->delivery_pincode }}
                </address>
                <p class="mb-0"><span>phone: </span>{{ $challan->primary_contact_phoneno }}</p>
            </div>
        </div>
    </header>

    <section>
        <h6 class="fw-700 mt-3 text-14 mb-2">Order Summary</h6>
        <table class="table table-bordered text-center small">
            <thead>
                <tr>
                    <th scope="col">Item Id</th>
                    <th scope="col">Item Name</th>
                    <th scope="col">HSN/SAC</th>
                    <th scope="col">GST</th>
                    <th scope="col">Unit Price</th>
                    <th scope="col">Quantity</th>
                    <th scope="col">Discount</th>
                    <th scope="col">Description</th>
                    <th scope="col">Item Amount</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($challan_items as $item)
                    <tr>
                        <td scope="col"> {{ $item->id }}</td>
                        <td scope="col"> {{ $item->name }}</td>
                        <td scope="col"> {{ $item->hsn }}</td>
                        <td scope="col"> {{ $item->gst }}</td>
                        <td scope="col"> {{ $item->price }}</td>
                        <td scope="col"> {{ $item->quantity }}</td>
                        <td scope="col"> {{ $item->discount }}</td>
                        <td scope="col"> {{ $item->descriptions }}</td>
                        <td scope="col"> {{ $item->item_total }}</td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan="8">Total</td>
                    <td>{{ $total[0]->items_total }}</td>
                </tr>
            </tbody>
        </table>
        <h6 class="fw-700 mt-4 text-14 mb-2">Tax details</h6>
        <table class="table table-bordered text-center small">
            <thead>
                <tr>
                    <th scope="col">Taxable Amount</th>
                    <th scope="col">CGST %</th>
                    <th scope="col">CGST Amount</th>
                    <th scope="col">SGST %</th>
                    <th scope="col">SGST Amount</th>
                    <th scope="col">Tax Amount</th>
                    <th scope="col">Total Amount</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($challan_items as $item)
                    <tr>
                        <td scope="col"> {{ $item->item_total }}</td>
                        <td scope="col"> {{ $item->cgst }}</td>
                        <td scope="col"> {{ $item->cgst_amount }}</td>
                        <td scope="col"> {{ $item->sgst }}</td>
                        <td scope="col"> {{ $item->sgst_amount }}</td>
                        <td scope="col"> {{ $item->tax_amount }}</td>

                        @if ($item->tax_amount == 0)
                            <td scope="col">0</td>
                        @else
                            <td scope="col">{{ $item->item_total + $item->tax_amount }}</td>
                        @endif
                    </tr>
                @endforeach
                <tr>
                    <td colspan="5">Total</td>
                    <td>{{ $total[0]->total_tax }}</td>
                    <td>{{ $challan->Total_Bill_Amount }}</td>
                </tr>
            </tbody>
        </table>
    </section>


    <footer>
        <div class="d-block text-12">
            <div class="d-inline-block va-t w-20">
                <h6 class="font-weight-bold text-14 mb-1">Bank Details</h6>
                <p class="mb-0 text-sm">
                    <span>{{ $challan->org_holder_name }}</span>
                    <br>{{ $challan->org_bank_name }}<br>{{ $challan->org_account_no }}<br>{{ $challan->org_bank_branch }}<br>{{ $challan->org_bank_ifsc }}<br>
                </p>
            </div>
            <div class="d-inline-block va-t w-20">
                <label class="font-weight-bold d-block">Total Bill Amount</label>
                <label class="d-block">(including GST)</label>
            </div>
            <div class="d-inline-block va-t w-59">
                <label class="font-weight-bold d-block">{{ $challan->Amount }}</label>
                <label class="d-block">{{ $challan->Tax_Amount_Words }}</label>
            </div>
        </div>
        <div class="d-block text-12 mt-2">
            <div class="d-inline-block va-t w-40">
                <h6 class="font-weight-bold text-sm mb-1">Declaration</h6>
                <p class="mb-0" style="font-size: 8px;">We declare that this invoice shows the actual price of the
                    goods described
                    <br>and that all the particulars are true and correct.
                </p>
            </div>
            <div class="d-inline-block va-t float-right 29">
                <h6 class="text-uppercase mb-4 pb-2 text-14 font-weight-normal">{{ $challan->Company_Name }}</h6>
                <p class="font-weight-bold">Authorised signatory</p>
            </div>
        </div>
    </footer>
</body>

</html>
