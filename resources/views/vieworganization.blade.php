@extends('layouts.app')
@section('mytitle', 'View Organization')
@section('head-links')
    <link href="{{ URL::asset('assets/css/datatables.min.css') }}" rel="stylesheet">
@endsection
@section('content')
    <div class="bg-top bg-gray p-4">
        <div class="d-flex">
            <h5 class="font-weight-bold text-uppercase mb-0">View Organization</h5>
        </div>
    </div>

    <div class="p-4 card-main">
        <section class="my-5">
            <div class="card shadow">
                <div class="card-top ">
                    <div class="card-title mb-0">
                        <h5 class="font-weight-bold text-uppercase mb-0"><i class="fas fa-user-alt"></i></h5>
                    </div>
                    <div class="btn-right position-relative float-right">
                        <a href="#" class="btn btn-primary text-uppecase">Print</a>
                        <a href="/addorganization" class="btn btn-primary text-uppecase" data-toggle="tooltip"
                            data-placement="top" title="Add Organization">
                            <i class="fas fa-plus"></i> Add
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table" id="datatable">
                            <thead class="text-uppercase">
                                <tr>
                                    <th scope="col">ORGANIZATION NAME</th>
                                    <th scope="col">Registered Name</th>
                                    <th scope="col">City</th>
                                    <th scope="col">Emai id</th>
                                    <th scope="col">Phone no</th>
                                    <th scope="col" class="nosort">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (count($orgdetails) >= 1)
                                    @foreach ($orgdetails as $key => $org)
                                        <tr>
                                            <td>{{ $org->org_branch_name }}</td>
                                            <td>{{ $org->org_reg_name }}</td>
                                            <td>{{ $org->org_city }}</td>
                                            <td>{{ $org->org_emailid }}</td>
                                            <td>{{ $org->org_phone_no }}</td>
                                            <td class="d-flex flex-nowrap">
                                                <span data-toggle="modal" data-target="#viewprofile" class="mr-1 view"
                                                    data-branch="{{ $org->org_branch_name }}"
                                                    data-regname="{{ $org->org_reg_name }}"
                                                    data-parentbranch_name="{{ $org->org_parent_name }}"
                                                    data-email="{{ $org->org_emailid }}"
                                                    data-phone_no="{{ $org->org_phone_no }}"
                                                    data-alterphone_no="{{ $org->org_alter_phone }}"
                                                    data-address="{{ $org->org_address }}"
                                                    data-pancard_no="{{ $org->org_pancard }}"
                                                    data-gst_no="{{ $org->org_gstno }}"
                                                    data-bank_name="{{ $org->org_bank_name }}"
                                                    data-bank_branch="{{ $org->org_bank_branch }}"
                                                    data-bank_ifsc="{{ $org->org_bank_ifsc }}"
                                                    data-ac_type="{{ $org->org_acc_type }}"
                                                    data-ac_no="{{ $org->org_account_no }}"
                                                    data-acholder_name="{{ $org->org_holder_name }}"
                                                    data-logo="{{ $org->org_logo }}">
                                                    <button type="button" class="btn btn-outline-info" data-toggle="tooltip"
                                                        data-placement="top" title="View Profile">
                                                        <i class="fas fa-user-alt"></i>
                                                    </button>
                                                </span>
                                                <span data-toggle="modal" data-target="#editorganizaton" class="mr-1"
                                                    data-orgid="{{ $org->org_branch_id }}"
                                                    data-branch="{{ $org->org_branch_name }}"
                                                    data-regname="{{ $org->org_reg_name }}"
                                                    data-parentbranch_name="{{ $org->org_parent_name }}"
                                                    data-email="{{ $org->org_emailid }}"
                                                    data-phone_no="{{ $org->org_phone_no }}"
                                                    data-alterphone_no="{{ $org->org_alter_phone }}"
                                                    data-address="{{ $org->org_address }}"
                                                    data-city="{{ $org->org_city }}"
                                                    data-state="{{ $org->org_state }}"
                                                    data-pincode="{{ $org->org_pincode }}"
                                                    data-pancard_no="{{ $org->org_pancard }}"
                                                    data-gst_no="{{ $org->org_gstno }}"
                                                    data-bank_name="{{ $org->org_bank_name }}"
                                                    data-bank_branch="{{ $org->org_bank_branch }}"
                                                    data-bank_ifsc="{{ $org->org_bank_ifsc }}"
                                                    data-ac_type="{{ $org->org_acc_type }}"
                                                    data-ac_no="{{ $org->org_account_no }}"
                                                    data-acholder_name="{{ $org->org_holder_name }}"
                                                    data-logo="{{ $org->org_logo }}">
                                                    <button type="button" class="btn btn-outline-success"
                                                        data-toggle="tooltip" data-placement="top"
                                                        title="Edit Organization">
                                                        <i class="fas fa-edit"></i>
                                                    </button>
                                                </span>
                                                <span data-toggle="modal" data-target="#deleteorganizaton"
                                                    data-orgid="{{ $org->org_branch_id }}"
                                                    data-branch="{{ $org->org_branch_name }}"
                                                    data-regname="{{ $org->org_reg_name }}"
                                                    data-parentbranch_name="{{ $org->org_parent_name }}"
                                                    data-email="{{ $org->org_emailid }}">
                                                    <button type="button" class="btn btn-outline-danger"
                                                        data-toggle="tooltip" data-placement="top"
                                                        title="Delete Organization">
                                                        <i class="fas fa-trash-alt"></i>
                                                    </button>
                                                </span>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="6">Sorry, No records found</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>

                        <!-- Modal viewprofile -->
                        <div class="modal fade custom-modal" id="viewprofile" tabindex="-1" role="dialog"
                            aria-labelledby="viewprofile" aria-hidden="true">
                            <div class="modal-dialog modal-xl modal-dialog-centered edit" role="document">
                                <div class="modal-content">
                                    <div class="icon">
                                        <span class="fa-stack fa-2x text-success">
                                            <i class="fas fa-circle fa-stack-2x"></i>
                                            <i class="fas fa-table fa-stack-1x fa-inverse"></i>
                                        </span>
                                    </div>
                                    <div class="modal-header justify-content-center">
                                        <h3 class="modal-title text-capitalize font-weight-bolder mt-3" id="viewprofile">
                                            View Branch Details
                                        </h3>
                                    </div>
                                    <div class="modal-body pb-0 text-sm">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="font-weight-bold">Company/Branch Logo</label>
                                                    <div id="logo">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="font-weight-bold">Company Name / Branch Name</label>
                                                    <p class="mb-0"><span id="branchName"></span>
                                                    </p>
                                                </div>
                                                <div class="form-group">
                                                    <label class="font-weight-bold">Parent Branch Name</label>
                                                    <p class="mb-0"><span id="parentbranchName"></span></p>
                                                </div>
                                                <div class="form-group">
                                                    <label class="font-weight-bold">Registered Name</label>
                                                    <p class="mb-0"><span id="regName"></span></p>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="row mb-4">
                                                    <div class="form-group col-md-4">
                                                        <label class="font-weight-bold">Email address</label>
                                                        <p class="mb-0"><span id="email"></span></p>
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label class="font-weight-bold">Phone No</label>
                                                        <p class="mb-0"><span id="phone-no"></span></p>
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label class="font-weight-bold">Alternate Phone Number</label>
                                                        <p class="mb-0"><span id="alterphone-no"></p>
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label class="font-weight-bold">Address</label>
                                                        <p class="mb-0"><span id="address"></span></p>
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label class="font-weight-bold">PAN Card No</label>
                                                        <p class="mb-0"><span id="pancard-no"></span></p>
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label class="font-weight-bold">GST Registered No</label>
                                                        <p class="mb-0"><span id="gst-no"></span></p>
                                                    </div>
                                                </div>
                                                <div class="row text-sm mb-4">
                                                    <div class="col-12">
                                                        <h6 class="fw-700 text-uppercase">Bank Details</h6>
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label class="font-weight-bold">Bank Name</label>
                                                        <p class="mb-0"><span id="bank-name"></span></p>
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label class="font-weight-bold">Bank Branch Name</label>
                                                        <p class="mb-0"><span id="bank-branch"></span></p>
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label class="font-weight-bold">Bank IFSC Code</label>
                                                        <p class="mb-0"><span id="bank-ifsc"></p>
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label class="font-weight-bold">Account Type</label>
                                                        <p class="mb-0"><span id="ac-type"></span></p>
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label class="font-weight-bold">Account No</label>
                                                        <p class="mb-0"><span id="ac-no"></span></p>
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label class="font-weight-bold">Account Holder's Name</label>
                                                        <p class="mb-0"><span id="acholder-name"></span></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer justify-content-center">
                                        <button type="button" class="btn btn-success btn-lg btn-large mt-0"
                                            data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End viewprofile -->
                        <!-- Modal Edit organizaton -->
                        <div class="modal fade custom-modal" id="editorganizaton" tabindex="-1" role="dialog"
                            aria-labelledby="editorganizaton" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered edit modal-xxl" role="document">
                                <form id="edit_form" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <div class="modal-content">
                                        <div class="icon">
                                            <span class="fa-stack fa-2x text-success">
                                                <i class="fas fa-circle fa-stack-2x"></i>
                                                <i class="far fa-edit fa-stack-1x fa-inverse"></i>
                                            </span>
                                        </div>
                                        <div class="modal-header justify-content-center">
                                            <h3 class="modal-title text-capitalize font-weight-bolder mt-3"
                                                id="editorganizaton">
                                                Edit Branch Details
                                            </h3>
                                        </div>
                                        <div class="modal-body pb-0">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="img-upload mb-4">
                                                        <h6 class="fw-700 text-uppercase mb-3">Select Company/Branch Logo
                                                        </h6>
                                                        <div id="edit_logo"></div>
                                                        <div>
                                                            <input id="view_logo" type="file" name="org_img" class="file"
                                                                accept="image/*">
                                                            <div class="input-group mt-2">
                                                                <input type="text" class="form-control" disabled
                                                                    placeholder="Upload File" id="org_img">
                                                                <div class="p-1">
                                                                    <button type="button"
                                                                        class="browse btn btn-primary text-capitalize">Select
                                                                        Image</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-8">
                                                    <div class="row">
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label active"
                                                                for="edit_branchname">Company
                                                                Name/Branch Name<small>*</small></label>
                                                            <input id="edit_branchname" name="edit_branchname" type="text"
                                                                class="form-control">
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label active"
                                                                for="edit_parent_branch">Parent
                                                                Branch Name<small>*</small></label>
                                                            <input id="edit_parent_branch" name="edit_parent_branch"
                                                                type="text" class="form-control">
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label active"
                                                                for="edit_regname">Registered
                                                                Name<small>*</small></label>
                                                            <input id="edit_regname" name="edit_regname" type="text"
                                                                class="form-control">
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label active" for="edit_email">Email
                                                                address<small>*</small></label>
                                                            <input id="edit_email" name="edit_email" type="email"
                                                                class="form-control">
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label active" for="edit_phone">Phone
                                                                No<small>*</small></label>
                                                            <input id="edit_phone" name="edit_phone" type="tel"
                                                                class="form-control">
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label active"
                                                                for="edit_alterphone">Alternate
                                                                Phone No<small>*</small></label>
                                                            <input id="edit_alterphone" name="edit_alterphone" type="text"
                                                                class="form-control">
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label active" for="edit_pancard">PAN Card
                                                                No<small>*</small></label>
                                                            <input id="edit_pancard" name="edit_pancard" type="text"
                                                                class="form-control">
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label active" for="edit_gst">GST
                                                                Registered
                                                                No<small>*</small></label>
                                                            <input id="edit_gst" name="edit_gst" type="text"
                                                                class="form-control">
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label active" for="edit_address">Address
                                                                <small>*</small></label>
                                                            <input id="edit_address" name="edit_address" type="text"
                                                                class="form-control">
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label active" for="edit_city">City
                                                                <small>*</small></label>
                                                            <input id="edit_city" name="edit_city" type="text"
                                                                class="form-control">
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label active" for="edit_state">State
                                                                <small>*</small></label>
                                                            <input id="edit_state" name="edit_state" type="text"
                                                                class="form-control">
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label active"
                                                                for="edit_postal_code">Postal
                                                                Code <small>*</small></label>
                                                            <input id="edit_postal_code" name="edit_postal_code"
                                                                type="number" class="form-control" value="605013">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">

                                                    <div class="row">
                                                        <div class="col-12 mt-2">
                                                            <h6 class="fw-700 text-uppercase mb-3">Bank Details</h6>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label active" for="edit_bank_ifsc">Bank
                                                                IFSC
                                                                Code</label>
                                                            <input id="edit_bank_ifsc" name="edit_bank_ifsc" type="text"
                                                                class="form-control">
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label active" for="edit_bank_name">Bank
                                                                Name
                                                                <small>*</small></label>
                                                            <input id="edit_bank_name" name="edit_bank_name" type="text"
                                                                class="form-control">
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label active" for="edit_ac_type">Account
                                                                Type
                                                                <small>*</small></label>
                                                            <select class="form-control" id="edit_ac_type"
                                                                name="edit_ac_type">
                                                                <option disabled selected></option>
                                                                <option value="SAVINGS ACCOUNT"> SAVINGS ACCOUNT </option>
                                                                <option value="CURRENT ACCOUNT"> CURRENT ACCOUNT </option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label active" for="edit_bank_branch">Bank
                                                                Branch Name<small>*</small></label>
                                                            <input id="edit_bank_branch" name="edit_bank_branch" type="text"
                                                                class="form-control">
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label active" for="edit_ac_no">Account
                                                                No<small>*</small></label>
                                                            <input id="edit_ac_no" name="edit_ac_no" type="number"
                                                                class="form-control">
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label active"
                                                                for="edit_acholder_name">Account
                                                                Holder's
                                                                Name<small>*</small></label>
                                                            <input id="edit_acholder_name" name="edit_acholder_name"
                                                                type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="edit_btn" class="modal-footer flex-nowrap">
                                            <button type="button" class="btn btn-outline-success btn-block btn-lg mt-0"
                                                data-dismiss="modal">Close</button>
                                            <button type="submit"
                                                class="btn btn-success btn-block btn-lg mt-0">Done</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- End Edit profile -->
                        <!-- Modal Delete organizaton -->
                        <div class="modal fade custom-modal" id="deleteorganizaton" tabindex="-1" role="dialog"
                            aria-labelledby="deleteorganizaton" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="icon">
                                        <span class="fa-stack fa-2x text-primary">
                                            <i class="fas fa-circle fa-stack-2x"></i>
                                            <i class="fas fa-trash-alt fa-stack-1x fa-inverse"></i>
                                        </span>
                                    </div>
                                    <div class="modal-header justify-content-center">
                                        <h3 class="modal-title text-capitalize font-weight-bolder mt-3"
                                            id="deleteorganizaton">
                                            Delete Branch?
                                        </h3>
                                    </div>
                                    <div class="modal-body pb-0">
                                        <p class="font-weight-bold text-center">
                                            Are you sure you want to delete the branch?
                                            <br>
                                            You cannot undo this operation!
                                        </p>
                                        <div class="row justify-content-center">
                                            <div class="col-md-11">
                                                <form>
                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <label for="delete_branch" class="col-form-label">Company
                                                                Name / Branch Name</label>
                                                            <input type="text" class="form-control" id="delete_branch"
                                                                readonly>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="delete_parentbranchname"
                                                                class="col-form-label">Parent
                                                                Branch Name</label>
                                                            <input type="text" class="form-control"
                                                                id="delete_parentbranchname" readonly>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="delete_regname" class="col-form-label">Registered
                                                                Name</label>
                                                            <input type="text" class="form-control" id="delete_regname"
                                                                readonly>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="delete_email" class="col-form-label">Email
                                                                address</label>
                                                            <input type="email" class="form-control" id="delete_email"
                                                                readonly>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer flex-nowrap" id="delete-btn"></div>
                                </div>
                            </div>
                        </div>
                        <!-- End Delete organizaton -->
                    </div>
                </div>
            </div>
        </section>
    </div>
    @include('components.alertmodal')
@endsection
@section('script')
    @if (session()->has('message'))
        <script>
            $(document).ready(function() {
                $('#successAlert').modal("show");
            });

        </script>
    @endif
    @if (session()->has('update'))
        <script>
            $(document).ready(function() {
                $('#updateAlert').modal("show");
            });

        </script>
    @endif
    @if (session()->has('delete'))
        <script>
            $(document).ready(function() {
                $('#deleteAlert').modal("show");
            });

        </script>
    @endif
    @if (session()->has('error'))
        <script>
            $(document).ready(function() {
                $('#errorAlert').modal("show");
            });

        </script>
    @endif
    <script src="{{ URL::asset('assets/js/datatables.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#datatable').DataTable({
                language: {
                    searchPlaceholder: "Search records",
                    search: "",
                    "lengthMenu": "_MENU_ Entries"
                },
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': ['nosort']
                }]
            });

            //View Modal
            $("#viewprofile").on('show.bs.modal', function(event) {
                var org = $(event.relatedTarget);
                var branch = org.data('branch');
                var regName = org.data('regname');
                var parentbranchName = org.data('parentbranch_name');
                var email = org.data('email');
                var phoneNo = org.data('phone_no');
                var alterphoneNo = org.data('alterphone_no');
                var address = org.data('address');
                var pancardNo = org.data('pancard_no');
                var gstNo = org.data('gst_no');
                var BankName = org.data('bank_name');
                var bankBranch = org.data('bank_branch');
                var bankIfsc = org.data('bank_ifsc');
                var acType = org.data('ac_type');
                var acNo = org.data('ac_no');
                var acholderName = org.data('acholder_name');
                var logo = org.data('logo');

                //push retrieved data into mentioned id 
                $('#branchName').html(branch);
                $('#regName').html(regName);
                $('#parentbranchName').html(parentbranchName);
                $('#email').html(email);
                $('#phone-no').html(phoneNo);
                $('#alterphone-no').html(alterphoneNo);
                $('#address').html(address);
                $('#pancard-no').html(pancardNo);
                $('#gst-no').html(gstNo);
                $('#bank-name').html(BankName);
                $('#bank-branch').html(bankBranch);
                $('#bank-ifsc').html(bankIfsc);
                $('#ac-type').html(acType);
                $('#ac-no').html(acNo);
                $('#acholder-name').html(acholderName);

                if (logo != '') {
                    //if img exist then 
                    $("#logo").empty();
                    $("#logo").append(
                        `<img alt="user-img" class="rounded lg" src="{{ asset('storage/images/${logo}') }}" width="70">`
                    )
                } else {
                    //if img doesn't exist then 
                    $("#logo").empty();
                    $("#logo").append(
                        `<img alt="user-img" class="rounded lg" src="{{ asset('/assets/img/avatar.png') }}" width="70">`
                    )
                }
            });

            //Edit Modal
            $("#editorganizaton").on('show.bs.modal', function(event) {
                console.log('model opened');
                var org = $(event.relatedTarget);
                var id = org.data('orgid');
                var branch = org.data('branch');
                var regName = org.data('regname');
                var parentbranchName = org.data('parentbranch_name');
                var email = org.data('email');
                var phoneNo = org.data('phone_no');
                var alterphoneNo = org.data('alterphone_no');
                var address = org.data('address');
                var city = org.data('city');
                var state = org.data('state');
                var pincode = org.data('pincode');
                var pancardNo = org.data('pancard_no');
                var gstNo = org.data('gst_no');
                var BankName = org.data('bank_name');
                var bankBranch = org.data('bank_branch');
                var bankIfsc = org.data('bank_ifsc');
                var acType = org.data('ac_type');
                var acNo = org.data('ac_no');
                var acholderName = org.data('acholder_name');
                var logo = org.data('logo');
                console.log('logo', logo);

                //push retrieved data into mentioned id 
                $('#edit_form').attr('action', 'updateorg/' + id);
                //$('#view_logo').attr('value', logo);

                $('#edit_branchname').val(branch);
                $('#edit_regname').val(regName);
                $('#edit_parent_branch').val(parentbranchName);
                $('#edit_email').val(email);
                $('#edit_phone').val(phoneNo);
                $('#edit_alterphone').val(alterphoneNo);
                $('#edit_pancard').val(pancardNo);
                $('#edit_gst').val(gstNo);
                $('#edit_address').val(address);
                $('#edit_city').val(city);
                $('#edit_state').val(state);
                $('#edit_postal_code').val(pincode);
                $('#edit_bank_ifsc').val(bankIfsc);
                $('#edit_bank_name').val(BankName);
                $('#edit_bank_branch').val(bankBranch);
                $('#edit_ac_type').val(acType);
                $('#edit_ac_no').val(acNo);
                $('#edit_acholder_name').val(acholderName);


                if (logo != '') {
                    //if img exist then 
                    $("#edit_logo").empty();
                    $("#edit_logo").append(
                        `<img alt="edit_logo" id="preview" class="rounded lg img-thumbnail border-0" src="{{ asset('storage/images/${logo}') }}" width="170">`
                    )
                } else {
                    //if img doesn't exist then 
                    $("#edit_logo").empty();
                    $("#edit_logo").append(
                        `<img alt="edit_logo" id="preview" class="rounded lg img-thumbnail border-0" src="{{ asset('/assets/img/avatar.png') }}" width="170">`
                    )
                }
            });


            $("#deleteorganizaton").on('show.bs.modal', function(event) {
                var org = $(event.relatedTarget);
                var id = org.data('orgid');
                var email = org.data('email');
                var branch = org.data('branch');
                var regName = org.data('regname');
                var parentbranchName = org.data('parentbranch_name');

                $('#delete_email').val(email);
                $('#delete_branch').val(branch);
                $('#delete_regname').val(regName);
                $('#delete_parentbranchname').val(parentbranchName);

                $('#delete-btn').html(
                    `<button type="button" class="btn btn-outline-primary btn-block btn-lg mt-0" data-dismiss="modal">Close</button><a href='deleteorg/${id}' class="btn btn-primary btn-block btn-lg mt-0">Delete</a>`
                );
            });
        });

    </script>
@endsection
