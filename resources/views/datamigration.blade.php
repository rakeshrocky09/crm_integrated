@extends('layouts.app')
@section('mytitle', 'Data Migration')
@section('head-links')
    <link rel="stylesheet" href="{{ URL::asset('assets/css/bootstrap-select.min.css') }}">
    <script src="{{ URL::asset('assets/js/xlsx.full.min.js') }}"></script>
@endsection
@section('content')
    <div class="bg-top bg-gray p-4">
        <div class="d-flex">
            <h5 class="font-weight-bold text-uppercase mb-0">IMPORT EXISTING DATA</h5>
        </div>
    </div>

    <section class="my-4 p-4 card-main">
        <div class="row">
            <div class="col-md-12 py-4">
                <div class="card shadow">
                    <div class="card-top ">
                        <div class="card-title mb-0">
                            <h5 class="font-weight-bold text-uppercase mb-0"><i class="fas fa-user-alt"></i></h5>
                        </div>
                    </div>
                    <div class="card-body pt-0">
                        <form class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="sdate" class="mb-0 control-label active">Start Date</label>
                                    <input id="sdate" class="flatpickr form-control" placeholder="dd-mm-yy">
                                </div>
                                <div class="form-group">
                                    <label class="d-block font-weight-bold text-sm">Filter</label>
                                    <select class="selectpicker w-100" id="cus_download" name="cus_download">
                                        <option disabled selected>Choose </option>
                                        <option value="customer">Customer</option>
                                        <option value="employee">Employee Details</option>
                                        <option value="products">Products</option>
                                        <option value="spares">Spares</option>
                                        <option value="consumables">Consumables</option>
                                        <option value="customerproduct">CustomerProduct</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">

                                <p>Add the details of customer in the spreadsheet which was downloaded and uploaded the
                                    excel sheet</p>
                                <div class="form-group mt-4 pt-2">
                                    <label class="d-block font-weight-bold text-sm">Import CSV file</label>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="file">
                                        <label class="custom-file-label pl-2" for="file">Choose file</label>
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-2 text-left">
                                <div class="mb-3">
                                    <button type="button" class="btn btn-outline-primary btn-lg btn-large btn-block"
                                        onclick="download()">
                                        <i class="fas fa-download mr-2"></i>Download</button>
                                </div>
                                <span id="preview"></span>
                                <button type="button" class="btn btn-primary btn-lg btn-large btn-block"
                                    onclick="upload()"><i class="fas fa-upload mr-2"></i>Upload</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>
    <script>
        // Add the following code if you want the name of the file appear on select
        $(".custom-file-input").on("change", function() {
            var fileName = $(this).val().split("\\").pop();
            $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
        });

        function download() {
            var type = $("#cus_download").val();
            if ((type == 'customer') || (type == 'employee') || (type == 'products') || (type == 'spares') || (type ==
                    'consumables') || (type == 'customerproduct')) {
                window.location.href = "assets/downloads/" + type + ".xlsx"; //relative-path
            } else {
                alert("Please choose the Filter");
            }

            console.log('download click and type=', type);
            //window.location.href = "assets/downloads/" + type + ".xlsx"; //relative-path
        }

        function upload() {
            var file_data = $('#file').prop('files')[0];
            var form_data = new FormData();
            form_data.append('excelFile', file_data);

            $.ajax({
                url: "/uploadexcel",
                dataType: 'text', // what to expect back from the PHP script
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: "POST",
                success: function(response) {
                    var jsonData = "";
                    /* set up XMLHttpRequest */
                    var url = "excels/" + response;
                    var worksheetNumber = 0;
                    var oReq = new XMLHttpRequest();
                    oReq.open("GET", url, true);
                    oReq.responseType = "arraybuffer";

                    oReq.onload = function(e) {

                        var arraybuffer = oReq.response;
                        /* convert data to binary string */
                        var data = new Uint8Array(arraybuffer);
                        var arr = new Array();
                        for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
                        var bstr = arr.join("");

                        /* Call XLSX */
                        var workbook = XLSX.read(bstr, {
                            type: "binary"
                        });

                        /* DO SOMETHING WITH workbook HERE */
                        var first_sheet_name = workbook.SheetNames[worksheetNumber];
                        /* Get worksheet */
                        var worksheet = workbook.Sheets[first_sheet_name];

                        jsonData = JSON.stringify(XLSX.utils.sheet_to_json(worksheet, {
                            raw: true
                        }), null, 2);


                        var newdata = XLSX.utils.sheet_to_json(worksheet, {
                            raw: true
                        });
                        console.log('newdata:', newdata);



                        /* var convertData = newdata[0];
                        console.log('con', convertData);
                        var arrdata = JSON.parse(jsonData);
                        console.log('arrdata:', arrdata); */

                        var obj = {};
                        $.each(newdata, function(i, item) {
                            obj[i] = item;
                        });
                        console.log('obj:', obj);


                        //API Call
                        var urlData = "";
                        var type = $("#cus_download").val();

                        if (type == 'customer') {
                            urlData = "/datamigration/customer";
                        } else if (type == 'employee') {
                            urlData = "/datamigration/employee";
                        } else if (type == 'products') {
                            urlData = "/datamigration/products";
                        } else if (type == 'spares') {
                            urlData = "/datamigration/spares";
                        } else if (type == 'consumables') {
                            urlData = "/datamigration/consumable";
                        } else if (type == 'customerproduct') {
                            urlData = "/datamigration/customerproduct";
                        } else {
                            alert("Please choose the Filter");
                        }

                        if (urlData === '') {
                            alert("Please Upload a File");
                        } else {
                            $.ajax({
                                url: urlData,
                                type: "POST",
                                data: obj,
                                dataType: 'json',
                                success: function(data) {
                                    console.log('data:', data);
                                    var data_id = data;

                                    if (data_id === "Error") {
                                        alert("Can't added to the server");
                                    } else if (data_id === "Employee Success") {
                                        alert("Employee added successfully");
                                        window.location = "viewuser";
                                    } else if (data_id === "Customer Success") {
                                        alert("Customer added successfully");
                                        window.location = "viewcustomer";
                                    } else if (data_id === "Product Success") {
                                        alert("Products added successfully");
                                        window.location = "viewproduct";
                                    } else if (data_id === "Consumables Success") {
                                        alert("consumables added successfully");
                                        window.location = "viewconsumable";
                                    } else if (data_id === "Spares Success") {
                                        alert("spares added successfully");
                                        window.location = "viewspares";
                                    } else if (data_id === "CustomerProduct Success") {
                                        alert("customerproduct added successfully");
                                        window.location = "viewcustomer";
                                    } else if (data_id === "exist") {
                                        alert("Already exist");
                                    }
                                },
                                error: function(error) {
                                    console.log(error);
                                }
                            });
                        }



                    }
                    oReq.send();
                },
                error: function(error) {
                    console.log('addcusprod error:', error);
                }
            });

        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

    </script>
@endsection
