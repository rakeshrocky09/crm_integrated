@extends('layouts.app')
@section('mytitle', 'View Services')
@section('head-links')
    <link href="{{ URL::asset('assets/css/datatables.min.css') }}" rel="stylesheet">
@endsection
@section('content')
    <div class="bg-top bg-gray p-4">
        <div class="d-flex">
            <h5 class="font-weight-bold text-uppercase mb-0">View Services</h5>
        </div>
    </div>

    <div class="p-4 card-main">
        <section class="my-5">
            <div class="card shadow">
                <div class="card-top ">
                    <div class="card-title mb-0">
                        <h5 class="font-weight-bold text-uppercase mb-0"><i class="fas fa-user-alt"></i></h5>
                    </div>
                    <div class="btn-right position-relative float-right">
                        <a href="#" class="btn btn-primary text-uppecase">Print</a>
                        <a href="/addservices" class="btn btn-primary text-uppecase" data-toggle="tooltip"
                            data-placement="top" title="Add Services">
                            <i class="fas fa-plus"></i> Add
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table" id="datatable">
                            <thead class="text-uppercase">
                                <tr>
                                    <th scope="col">Services ID</th>
                                    <th scope="col">Item</th>
                                    <th scope="col">UNIT</th>
                                    <th scope="col">UNIT PRICE</th>
                                    <th scope="col" class="nosort">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (count($product) >= 1)
                                    @foreach ($product as $service)
                                        <tr>
                                            <td>{{ $service->service_const }}{{ $service->service_id }}</td>
                                            <td>{{ $service->service_item_name }}</td>
                                            <td>{{ $service->service_parts_unit }}</td>
                                            <td>{{ $service->service_unit_price }}</td>
                                            <td class="d-flex flex-nowrap">
                                                <span data-toggle="modal" data-target="#viewservices" class="mr-1"
                                                    data-service_const="{{ $service->service_const }}"
                                                    data-service_id="{{ $service->service_id }}"
                                                    data-hsn="{{ $service->service_hsn }}"
                                                    data-item="{{ $service->service_item_name }}"
                                                    data-brand="{{ $service->service_brand_name }}"
                                                    data-unit="{{ $service->service_parts_unit }}"
                                                    data-price="{{ $service->service_unit_price }}"
                                                    data-branch="{{ $service->service_branch_name }}"
                                                    data-gst="{{ $service->Gst_Percentage }}">
                                                    <button type="button" class="btn btn-outline-info" data-toggle="tooltip"
                                                        data-placement="top" title="View Services">
                                                        <i class="fas fa-user-alt"></i>
                                                    </button>
                                                </span>
                                                <span data-toggle="modal" data-target="#editservices" class="mr-1"
                                                    data-service_const="{{ $service->service_const }}"
                                                    data-service_id="{{ $service->service_id }}"
                                                    data-hsn="{{ $service->service_hsn }}"
                                                    data-item="{{ $service->service_item_name }}"
                                                    data-brand="{{ $service->service_brand_name }}"
                                                    data-unit="{{ $service->service_parts_unit }}"
                                                    data-price="{{ $service->service_unit_price }}"
                                                    data-branch="{{ $service->service_branch_name }}"
                                                    data-type="{{ $service->service_type }}"
                                                    data-gst="{{ $service->Gst_Percentage }}">
                                                    <button type="button" class="btn btn-outline-success"
                                                        data-toggle="tooltip" data-placement="top" title="Edit Services">
                                                        <i class="fas fa-edit"></i>
                                                    </button>
                                                </span>
                                                <span data-toggle="modal" data-target="#deleteservices"
                                                    data-service_id="{{ $service->service_id }}"
                                                    data-hsn="{{ $service->service_hsn }}"
                                                    data-price="{{ $service->service_unit_price }}"
                                                    data-branch="{{ $service->service_branch_name }}">
                                                    <button type="button" class="btn btn-outline-danger"
                                                        data-toggle="tooltip" data-placement="top" title="Delete Services">
                                                        <i class="fas fa-trash-alt"></i>
                                                    </button>
                                                </span>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="5">Sorry, No records found</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                        <!-- Modal view Services -->
                        <div class="modal fade custom-modal" id="viewservices" tabindex="-1" role="dialog"
                            aria-labelledby="viewservices" aria-hidden="true">
                            <div class="modal-dialog modal-xl modal-dialog-centered edit" role="document">
                                <div class="modal-content">
                                    <div class="icon">
                                        <span class="fa-stack fa-2x text-success">
                                            <i class="fas fa-circle fa-stack-2x"></i>
                                            <i class="fas fa-table fa-stack-1x fa-inverse"></i>
                                        </span>
                                    </div>
                                    <div class="modal-header justify-content-center">
                                        <h3 class="modal-title text-capitalize font-weight-bolder mt-3" id="viewservices">
                                            View Service Details
                                        </h3>
                                    </div>
                                    <div class="modal-body pb-0 text-sm">
                                        <div class="row text-sm mb-4">
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">Service ID</label>
                                                <p class="mb-0"><span id="id"></span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">HSN/SAC Number</label>
                                                <p class="mb-0"><span id="hsn"></span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">Item</label>
                                                <p class="mb-0"><span id="item"></span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">Brand Name</label>
                                                <p class="mb-0"><span id="brand"></span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">Unit</label>
                                                <p class="mb-0"><span id="unit"></span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">Unit Price</label>
                                                <p class="mb-0"><span id="price"></span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">Service Belongs To Branch</label>
                                                <p class="mb-0"><span id="branch"></span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">GST(In %)</label>
                                                <p class="mb-0"><span id="gst"></span></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer justify-content-center">
                                        <button type="button" class="btn btn-success btn-lg btn-large mt-0"
                                            data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End view Services -->
                        <!-- Modal Edit Services -->
                        <div class="modal fade custom-modal" id="editservices" tabindex="-1" role="dialog"
                            aria-labelledby="editservices" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered edit modal-xl" role="document">
                                <form id="edit_form" method="post">
                                    @csrf
                                    <div class="modal-content">
                                        <div class="icon">
                                            <span class="fa-stack fa-2x text-success">
                                                <i class="fas fa-circle fa-stack-2x"></i>
                                                <i class="far fa-edit fa-stack-1x fa-inverse"></i>
                                            </span>
                                        </div>
                                        <div class="modal-header justify-content-center">
                                            <h3 class="modal-title text-capitalize font-weight-bolder mt-3"
                                                id="editservices">
                                                Edit Service Details
                                            </h3>
                                        </div>
                                        <div class="modal-body pb-0">
                                            <div class="row">
                                                <div class="form-group col-md-4">
                                                    <label for="edit_id" class="mb-0 control-label active">Service ID
                                                        <small>*</small></label>
                                                    <input id="edit_id" name="edit_id" type="text" class="form-control">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label class="control-label active" for="edit_brand">Brand
                                                        Name<small>*</small>
                                                    </label>
                                                    <select class="form-control" id="edit_brand" name="edit_brand"></select>
                                                </div>

                                                <div class="form-group col-md-4">
                                                    <label class="control-label active" for="edit_hsn">HSN/SAC
                                                        Number`<small>*</small>
                                                    </label>
                                                    <select class="form-control" id="edit_hsn" name="edit_hsn"></select>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="edit_gst" class="mb-0 control-label active">GST(In
                                                        %)</label>
                                                    <input id="edit_gst" name="edit_gst" type="text" class="form-control">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="edit_item" class="mb-0 control-label active">Item</label>
                                                    <input id="edit_item" name="edit_item" type="text" class="form-control">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="edit_price" class="mb-0 control-label active">Unit Price
                                                        <small>*</small></label>
                                                    <input id="edit_price" name="edit_price" type="text"
                                                        class="form-control">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label class="control-label active" for="edit_unit">Unit</label>
                                                    <select class="form-control" id="edit_unit" name="edit_unit"></select>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label class="control-label active" for="edit_type_ofcopies">Type of
                                                        Copies</label>
                                                    <select class="form-control" id="edit_type_ofcopies"
                                                        name="edit_type_ofcopies">
                                                        <option selected="true" disabled=""></option>
                                                        <option value="Rental A3 Black Copies">Rental A3 Black Copies
                                                        </option>
                                                        <option value="Rental A4 Black Copies">Rental A4 Black Copies
                                                        </option>
                                                        <option value="Rental A3 Color Copies">Rental A3 Color Copies
                                                        </option>
                                                        <option value="Rental A4 Color Copies">Rental A4 Color Copies
                                                        </option>
                                                        <option value="4C A3 Black Copies">4C A3 Black Copies</option>
                                                        <option value="4C A4 Black Copies">4C A4 Black Copies</option>
                                                        <option value="4C A3 Color Copies">4C A3 Color Copies</option>
                                                        <option value="4C A4 Color Copies">4C A4 Color Copies</option>
                                                        <option value="Monthly Rental">Monthly Rental</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="edit_branch" class="mb-0 control-label active">Belongs To
                                                        Branch</label>
                                                    <input id="edit_branch" name="edit_branch" type="text"
                                                        class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer flex-nowrap">
                                            <button type="button" class="btn btn-outline-success btn-block btn-lg mt-0"
                                                data-dismiss="modal">Close</button>
                                            <button type="submit"
                                                class="btn btn-success btn-block btn-lg mt-0">Done</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- End Edit Services -->
                        <!-- Modal Delete Services -->
                        <div class="modal fade custom-modal" id="deleteservices" tabindex="-1" role="dialog"
                            aria-labelledby="deleteservices" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="icon">
                                        <span class="fa-stack fa-2x text-primary">
                                            <i class="fas fa-circle fa-stack-2x"></i>
                                            <i class="fas fa-trash-alt fa-stack-1x fa-inverse"></i>
                                        </span>
                                    </div>
                                    <div class="modal-header justify-content-center">
                                        <h3 class="modal-title text-capitalize font-weight-bolder mt-3" id="deleteservices">
                                            Delete Services?
                                        </h3>
                                    </div>
                                    <div class="modal-body pb-0">
                                        <p class="font-weight-bold text-center">
                                            Are you sure you want to delete the Services?
                                            <br>
                                            You cannot undo this operation!
                                        </p>
                                        <div class="row justify-content-center">
                                            <div class="col-md-11">
                                                <form>
                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <label for="delete_id" class="col-form-label">Service
                                                                ID</label>
                                                            <input type="text" class="form-control" id="delete_id" readonly>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="delete_code_name" class="col-form-label">Service
                                                                Code Name</label>
                                                            <input type="text" class="form-control" id="delete_code_name"
                                                                readonly>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="delete_price" class="col-form-label">Service
                                                                Price</label>
                                                            <input type="text" class="form-control" id="delete_price"
                                                                readonly>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="delete_branch" class="col-form-label">Belongs To
                                                                Branch</label>
                                                            <input type="text" class="form-control" id="delete_branch"
                                                                readonly>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer flex-nowrap" id="delete_btn"></div>
                                </div>
                            </div>
                        </div>
                        <!-- End Delete Services -->
                    </div>
                </div>
            </div>
        </section>
    </div>
    @include('components.alertmodal')
@endsection
@section('script')
    @if (session()->has('message'))
        <script>
            $(document).ready(function() {
                $('#successAlert').modal("show");
            });

        </script>
    @endif
    @if (session()->has('update'))
        <script>
            $(document).ready(function() {
                $('#updateAlert').modal("show");
            });

        </script>
    @endif
    @if (session()->has('delete'))
        <script>
            $(document).ready(function() {
                $('#deleteAlert').modal("show");
            });

        </script>
    @endif
    @if (session()->has('error'))
        <script>
            $(document).ready(function() {
                $('#errorAlert').modal("show");
            });

        </script>
    @endif
    <script src="{{ URL::asset('assets/js/datatables.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            loadBrandname();
            getHsn();
            loadUnit();
            $('#datatable').DataTable({
                language: {
                    searchPlaceholder: "Search records",
                    search: "",
                    "lengthMenu": "_MENU_ Entries"
                },
                "lengthMenu": [
                    [8, 16, 32, -1],
                    [8, 16, 32, "All"]
                ],
                "pageLength": 8,
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': ['nosort']
                }]
            });
            //View Modal
            $("#viewservices").on('show.bs.modal', function(event) {
                var services = $(event.relatedTarget);
                var serviceConst = services.data('service_const');
                var serviceId = services.data('service_id');
                var id = `${serviceConst}${serviceId}`;
                var hsn = services.data('hsn');
                var item = services.data('item');
                var brand = services.data('brand');
                var unit = services.data('unit');
                var price = services.data('price');
                var branch = services.data('branch');
                var gst = services.data('gst');

                //push retrieved data into mentioned id 
                $('#id').html(id);
                $('#hsn').html(hsn);
                $('#item').html(item);
                $('#brand').html(brand);
                $('#unit').html(unit);
                $('#price').html(price);
                $('#branch').html(branch);
                $('#gst').html(gst);
            });
            $("#editservices").on('show.bs.modal', function(event) {
                var services = $(event.relatedTarget);
                var serviceConst = services.data('service_const');
                var serviceId = services.data('service_id');
                var id = `${serviceConst}${serviceId}`;
                var hsn = services.data('hsn');
                var item = services.data('item');
                var brand = services.data('brand');
                var unit = services.data('unit');
                var price = services.data('price');
                var branch = services.data('branch');
                var type = services.data('type');
                var gst = services.data('gst');


                //push retrieved data into mentioned id 
                $('#edit_form').attr('action', 'updateservice/' + serviceId);
                $('#edit_id').val(id);
                $('#edit_brand').val(brand);
                $('#edit_hsn').val(hsn);
                $('#edit_gst').val(gst);
                $('#edit_item').val(item);
                $('#edit_price').val(price);
                $('#edit_unit').val(unit);
                $('#edit_type_ofcopies').val(type);
                $('#edit_branch').val(branch);
            });
            $("#deleteservices").on('show.bs.modal', function(event) {
                var services = $(event.relatedTarget);
                var id = services.data('service_id');
                var codename = services.data('hsn');
                var price = services.data('price');
                var branch = services.data('branch');

                $("#delete_id").val(id);
                $("#delete_code_name").val(codename);
                $("#delete_price").val(price);
                $("#delete_branch").val(branch);

                $('#delete_btn').html(
                    `<button type="button" class="btn btn-outline-primary btn-block btn-lg mt-0" data-dismiss="modal">Close</button><a href='deleteservice/${id}' class="btn btn-primary btn-block btn-lg mt-0">Delete</a>`
                );
            });
        });

        function loadBrandname() {
            return $.ajax({
                url: "/brand",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    //console.log("brand_name", data);
                    $('#edit_brand').empty();
                    $('#edit_brand').append('<option selected="true" disabled></option>');
                    $('#edit_brand').prop('selectedIndex', 0);
                    $.each(data, function(key, value) {
                        $('#edit_brand').append($('<option></option>').attr('value', value
                            .name).text(value.name));
                    });
                    $('#edit_brand').append('<option value="others">Others</option>');
                },
                error: function(error) {
                    console.log('brand_name error:', error);
                }
            });
        }

        function getHsn() {
            return $.ajax({
                url: "/servicehsn",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    //console.log("service_hsn", data);
                    $('#edit_hsn').empty();
                    $('#edit_hsn').append('<option selected="true" disabled></option>');
                    $('#edit_hsn').prop('selectedIndex', 0);
                    $.each(data, function(key, value) {
                        $('#edit_hsn').append($('<option></option>').attr(
                            'value',
                            value.Service_HSN_No).text(value.Service_HSN_No));
                    });
                    $('#edit_hsn').append('<option value="others">Others</option>');
                },
                error: function(error) {
                    console.log('service_hsn error:', error);
                }
            });
        }

        function loadUnit() {
            return $.ajax({
                url: "/unit",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    //console.log("unit", data);
                    $('#edit_unit').empty();
                    $('#edit_unit').append('<option selected="true" disabled></option>');
                    $('#edit_unit').prop('selectedIndex', 0);
                    $.each(data, function(key, value) {
                        $('#edit_unit').append($('<option></option>').attr(
                            'value',
                            value.unit).text(value.unit));
                    });
                    $('#edit_unit').append('<option value="others">Others</option>');
                },
                error: function(error) {
                    console.log('unit error:', error);
                }
            });
        }

    </script>
@endsection
