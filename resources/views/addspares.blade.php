@extends('layouts.app')
@section('mytitle', 'Add Spares')
@section('content')
    <div class="bg-top bg-gray p-4">
        <div class="d-flex">
            <h5 class="font-weight-bold text-uppercase mb-0">ADD Spares</h5>
        </div>
    </div>
    <section class="my-4 p-4 card-main">
        <form action="addspare" method="POST">
            @csrf
            <div class="row">
                <div class="col-md-6 py-4">
                    <div class="card shadow h-100">
                        <div class="card-top ">
                            <div class="card-title mb-0">
                                <h5 class="font-weight-bold text-uppercase mb-0"><i class="far fa-building"></i></h5>
                            </div>
                        </div>
                        <div class="card-body pt-0">
                            <div class="form-group">
                                <label class="control-label" for="spare_codename">HSN/SAC Number
                                    <small>*</small></label>
                                <select id="spare_codename" name="spare_codename"
                                    class="form-control @error('spare_codename') is-invalid @enderror"></select>
                                @error('spare_codename')
                                    <span class="text-danger text-12">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="spare_model_name">Item</label>
                                <input id="spare_model_name" name="spare_model_name" type="text"
                                    class="form-control @error('spare_model_name') is-invalid @enderror">
                                @error('spare_model_name')
                                    <span class="text-danger text-12">{{ $message }}</span>
                                @enderror
                            </div>
                            {{-- <div class="form-group">
                                <label class="control-label" for="spare_unit">Unit</label>
                                <select id="spare_unit" name="spare_unit"
                                    class="form-control @error('spare_unit') is-invalid @enderror"></select>
                                @error('spare_unit')
                                    <span class="text-danger text-12">{{ $message }}</span>
                                @enderror
                            </div> --}}
                            <div class="form-group">
                                <label class="control-label" for="spare_gst_percentage">GST(In %)</label>
                                <input id="spare_gst_percentage" name="spare_gst_percentage" type="text"
                                    class="form-control @error('spare_gst_percentage') is-invalid @enderror">
                                @error('spare_gst_percentage')
                                    <span class="text-danger text-12">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="spare_stock_unit">Spare Stock Unit</label>
                                <input id="spare_stock_unit" name="spare_stock_unit" type="number"
                                    class="form-control @error('spare_stock_unit') is-invalid @enderror">
                                @error('spare_stock_unit')
                                    <span class="text-danger text-12">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="spare_unitprice">Unit Price <small>*</small></label>
                                <input id="spare_unitprice" name="spare_unitprice" type="text"
                                    class="form-control @error('spare_unitprice') is-invalid @enderror">
                                @error('spare_unitprice')
                                    <span class="text-danger text-12">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 py-4">
                    <div class="card shadow h-100">
                        <div class="card-top ">
                            <div class="card-title mb-0">
                                <h5 class="font-weight-bold text-uppercase mb-0"><i class="far fa-building"></i></h5>
                            </div>
                        </div>
                        <div class="card-body pt-0">
                            <div class="form-group">
                                <label class="control-label" for="brspare_brand_nameand">Brand Name
                                    <small>*</small></label>
                                <select id="spare_brand_name" name="spare_brand_name"
                                    class="form-control @error('brspare_brand_nameand') is-invalid @enderror"></select>
                                @error('brspare_brand_nameand')
                                    <span class="text-danger text-12">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="spare_partname">Part Number</label>
                                <input id="spare_partname" name="spare_partname" type="text"
                                    class="form-control @error('spare_partname') is-invalid @enderror">
                                @error('spare_partname')
                                    <span class="text-danger text-12">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="spare_belongs_to">Belongs To (branch name)</label>
                                <select class="form-control @error('spare_belongs_to') is-invalid @enderror"
                                    id="spare_belongs_to" name="spare_belongs_to"></select>
                                @error('spare_belongs_to')
                                    <span class="text-danger text-12">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group pt-3">
                                <label class="font-weight-bold">MSL</label>
                                <div class="mb-2 @error('msl') is-invalid @enderror">
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="msl_yes" name="msl" class="custom-control-input"
                                            value="yes">
                                        <label class="custom-control-label" for="msl_yes">Yes</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="msl_no" name="msl" class="custom-control-input" value="no">
                                        <label class="custom-control-label" for="msl_no">No</label>
                                    </div>
                                    @error('msl')
                                        <span class="text-danger text-12">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group pt-1">
                                <label class="font-weight-bold">Chargeable</label>
                                <div class="mb-2 @error('chargeable') is-invalid @enderror">
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="chargeable_yes" name="chargeable"
                                            class="custom-control-input" value="yes">
                                        <label class="custom-control-label" for="chargeable_yes">Yes</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="chargeable_no" name="chargeable"
                                            class="custom-control-input" value="no">
                                        <label class="custom-control-label" for="chargeable_no">No</label>
                                    </div>
                                    @error('chargeable')
                                        <span class="text-danger text-12">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <a href="/viewspares" type="submit" class="btn btn-outline-primary btn-lg btn-large mr-2">Cancel</a>
                <button type="submit" class="btn btn-primary btn-lg btn-large">Submit</button>
            </div>
        </form>
    </section>

    {{-- add_hsn/sac number --}}
    <div class="modal fade custom-modal" id="add_hsn" tabindex="-1" role="dialog" aria-labelledby="add_hsn"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content success">
                <div class="icon">
                    <span class="fa-stack fa-2x text-success">
                        <i class="fas fa-circle fa-stack-2x"></i>
                        <i class="fas fas fa-plus fa-stack-1x fa-inverse"></i>
                    </span>
                </div>
                <div class="modal-header justify-content-center">
                    <h3 class="modal-title text-capitalize font-weight-bolder mt-3">
                        Add HSN Code
                    </h3>
                </div>
                <form action="addsparehsn" method="POST">
                    @csrf
                    <div class="modal-body pb-0">
                        <div class="row justify-content-center">
                            <div class="col-md-11">
                                <div class="form-group">
                                    <label for="add_hsn" class="col-form-label">Enter New HSN Code</label>
                                    <input type="text" class="form-control @error('add_hsn') is-invalid @enderror"
                                        id="add_hsn" name="add_hsn">
                                    @error('add_hsn')
                                        <span class="text-danger text-12">{{ $message }}</span>
                                    @enderror
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer flex-nowrap">
                        <button type="button" class="btn btn-outline-success btn-block btn-lg mt-0"
                            data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success btn-block btn-lg mt-0">Done</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- add_spare_brand --}}
    <div class="modal fade custom-modal" id="add_spare_brand" tabindex="-1" role="dialog" aria-labelledby="add_spare_brand"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content success">
                <div class="icon">
                    <span class="fa-stack fa-2x text-success">
                        <i class="fas fa-circle fa-stack-2x"></i>
                        <i class="fas fas fa-plus fa-stack-1x fa-inverse"></i>
                    </span>
                </div>
                <div class="modal-header justify-content-center">
                    <h3 class="modal-title text-capitalize font-weight-bolder mt-3">
                        Add Product Brand
                    </h3>
                </div>
                <form action="addsparebrand" method="POST">
                    @csrf
                    <div class="modal-body pb-0">
                        <div class="row justify-content-center">
                            <div class="col-md-11">
                                <div class="form-group">
                                    <label for="add_brand_name" class="col-form-label">Enter Brand Name</label>
                                    <input type="text" class="form-control @error('add_brand_name') is-invalid @enderror"
                                        id="add_brand_name" name="add_brand_name">
                                    @error('add_brand_name')
                                        <span class="text-danger text-12">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer flex-nowrap">
                        <button type="button" class="btn btn-outline-success btn-block btn-lg mt-0"
                            data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success btn-block btn-lg mt-0">Done</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- add_units --}}
    {{-- <div class="modal fade custom-modal" id="add_units" tabindex="-1" role="dialog" aria-labelledby="add_units"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content success">
                <div class="icon">
                    <span class="fa-stack fa-2x text-success">
                        <i class="fas fa-circle fa-stack-2x"></i>
                        <i class="fas fas fa-plus fa-stack-1x fa-inverse"></i>
                    </span>
                </div>
                <div class="modal-header justify-content-center">
                    <h3 class="modal-title text-capitalize font-weight-bolder mt-3">
                        Add Unit
                    </h3>
                </div>
                <form action="addunit" method="POST">
                    @csrf
                    <div class="modal-body pb-0">
                        <div class="row justify-content-center">
                            <div class="col-md-11">
                                <div class="form-group">
                                    <label for="add_units" class="col-form-label">Enter New Unit</label>
                                    <input type="text" class="form-control @error('add_units') is-invalid @enderror"
                                        id="add_units" name="add_units">
                                    @error('add_units')
                                        <span class="text-danger text-12">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer flex-nowrap">
                        <button type="button" class="btn btn-outline-success btn-block btn-lg mt-0"
                            data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success btn-block btn-lg mt-0">Done</button>
                    </div>
                </form>
            </div>
        </div>
    </div> --}}
    @include('components.alertmodal')
@endsection
@section('script')
    @if (session()->has('message'))
        <script>
            $(document).ready(function() {
                $('#successAlert').modal("show");
            });

        </script>
    @endif
    @if (session()->has('error'))
        <script>
            $(document).ready(function() {
                $('#errorAlert').modal("show");
            });

        </script>
    @endif
    <script>
        $(document).ready(function() {
            getBranch();
            loadBrandname();
            getHsn();
            //loadUnit();

            $('#spare_codename').change(function() { //jQuery Change Function
                var opval = $(this).val(); //Get value from select element
                if (opval == "others") { //Compare it and if true
                    $('#add_hsn').modal("show"); //Open Modal
                }
            });
            $('#spare_brand_name').change(function() { //jQuery Change Function
                var opval = $(this).val(); //Get value from select element
                if (opval == "others") { //Compare it and if true
                    $('#add_spare_brand').modal("show"); //Open Modal
                }
            });
            /* $('#spare_unit').change(function() { //jQuery Change Function
                var opval = $(this).val(); //Get value from select element
                if (opval == "others") { //Compare it and if true
                    $('#add_units').modal("show"); //Open Modal
                }
            }); */
            $('#spare_belongs_to').change(function() { //jQuery Change Function
                var opval = $(this).val(); //Get value from select element
                if (opval == "others") { //Compare it and if true
                    return window.location.href = "/addorganization";
                }
            });
        });

        function getBranch() {
            return $.ajax({
                url: "/branchname",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    //console.log("branchname", data);
                    $('#spare_belongs_to').empty();
                    $('#spare_belongs_to').append('<option selected="true" disabled></option>');
                    $('#spare_belongs_to').prop('selectedIndex', 0);
                    $.each(data, function(key, value) {
                        $('#spare_belongs_to').append($('<option></option>').attr('value', value
                            .org_branch_name).text(value.org_branch_name));
                    });
                    $('#spare_belongs_to').append('<option value="others">Others</option>');
                },
                error: function(error) {
                    console.log('branchname error:', error);
                }
            });
        }

        function loadBrandname() {
            return $.ajax({
                url: "/sparebrand",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    console.log("branchname", data);
                    $('#spare_brand_name').empty();
                    $('#spare_brand_name').append('<option selected="true" disabled></option>');
                    $('#spare_brand_name').prop('selectedIndex', 0);
                    $.each(data, function(key, value) {
                        $('#spare_brand_name').append($('<option></option>').attr('value', value
                            .name).text(value.name));
                    });
                    $('#spare_brand_name').append('<option value="others">Others</option>');
                },
                error: function(error) {
                    console.log('branchname error:', error);
                }
            });
        }

        function getHsn() {
            return $.ajax({
                url: "/sparehsn",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    //console.log("prdhsn", data);
                    $('#spare_codename').empty();
                    $('#spare_codename').append('<option selected="true" disabled></option>');
                    $('#spare_codename').prop('selectedIndex', 0);
                    $.each(data, function(key, value) {
                        $('#spare_codename').append($('<option></option>').attr(
                            'value',
                            value.Spare_HSN_No).text(value.Spare_HSN_No));
                    });
                    $('#spare_codename').append('<option value="others">Others</option>');
                },
                error: function(error) {
                    console.log('branchname error:', error);
                }
            });
        }

        /*  function loadUnit() {
             return $.ajax({
                 url: "/unit",
                 type: "GET",
                 dataType: 'json',
                 success: function(data) {
                     //console.log("prdhsn", data);
                     $('#spare_unit').empty();
                     $('#spare_unit').append('<option selected="true" disabled></option>');
                     $('#spare_unit').prop('selectedIndex', 0);
                     $.each(data, function(key, value) {
                         $('#spare_unit').append($('<option></option>').attr(
                             'value',
                             value.unit).text(value.unit));
                     });
                     $('#spare_unit').append('<option value="others">Others</option>');
                 },
                 error: function(error) {
                     console.log('branchname error:', error);
                 }
             });
         } */

    </script>
@endsection
