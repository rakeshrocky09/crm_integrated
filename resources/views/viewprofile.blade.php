@extends('layouts.app')
@section('mytitle', 'View Profile')
@section('content')
    <div class="bg-top bg-gray p-4">
        <div class="d-flex">
            <h5 class="font-weight-bold text-uppercase mb-0">View Profile</h5>
        </div>
    </div>
    <section class="my-4 p-4 card-main">
        <div class="card shadow h-100 py-">
            <div class="card-top ">
                <div class="card-title mb-0">
                    <h5 class="font-weight-bold text-uppercase mb-0"><i class="fas fa-user"></i></h5>
                </div>
            </div>
            <div class="card-body pt-0">
                <form>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="img-upload form mb-2">
                                <h6 class="fw-700 text-uppercase mb-3">Company / Branch Logo</h6>
                                <div>
                                    <img src="{{ asset('/assets/img/user1.jpg') }}" id="preview"
                                        class="img-thumbnail border-0" width="200">
                                </div>
                                <div id="msg"></div>
                                <form method="post" id="image-form">
                                    <input type="file" name="img[]" class="file" accept="image/*">
                                    <div class="input-group mt-2">
                                        <input type="text" class="form-control" disabled placeholder="Upload File"
                                            id="file">
                                        <div class="p-1">
                                            <button type="button" class="browse btn btn-primary text-capitalize">Select
                                                Image</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="row">
                                <div class="col-12">                                    
                                    <h6 class="fw-700 text-uppercase mb-4">Basic Information</h6>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="font-weight-bold">Organization Name</label>
                                    <p class="mb-0"><span>Boffo Consulting</span></p>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="font-weight-bold">LoginID/Email</label>
                                    <p class="mb-0"><span>demo@gmail.com</span></p>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="font-weight-bold">Branch Name</label>
                                    <p class="mb-0"><span>MMDA</span></p>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="font-weight-bold">Role</label>
                                    <p class="mb-0"><span>Admin</span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection
