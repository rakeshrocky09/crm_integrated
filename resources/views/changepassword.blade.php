@extends('layouts.app')
@section('mytitle', 'Change Password')
@section('content')
    <div class="bg-top bg-gray p-4">
        <div class="d-flex">
            <h5 class="font-weight-bold text-uppercase mb-0">Change Password</h5>
        </div>
    </div>

    <section class="my-4 p-4 card-main">
        <div class="row">
            <div class="col-md-12 py-4">
                <div class="card shadow">
                    <div class="card-top ">
                        <div class="card-title mb-0">
                            <h5 class="font-weight-bold text-uppercase mb-0"><i class="fas fa-lock"></i></h5>
                        </div>
                    </div>
                    <div class="card-body pt-0">
                        <form class="row">
                            <div class="col-md-4 mb-3">
                                <div class="form-group">
                                    <label class="control-label" for="cpassword">Current Password <small>*</small></label>
                                    <input id="cpassword" type="password" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="password">New Password <small>*</small></label>
                                    <input id="password" type="password" class=" form-control">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="rpassword">Confirm Password <small>*</small></label>
                                    <input id="rpassword" type="password" class=" form-control">
                                </div>
                            </div>
                            <div class="col-md-12">
                                
                                <button type="submit" class="btn btn-outline-primary btn-lg btn-large mr-2">Ok</button>
                                <a href="/dashboard" class="btn btn-primary btn-lg btn-large">Cancel</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
