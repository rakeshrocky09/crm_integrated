@extends('layouts.app')
@section('mytitle', 'View Employee')
@section('head-links')
    <link href="{{ URL::asset('assets/css/datatables.min.css') }}" rel="stylesheet">
@endsection
@section('content')
    <div class="bg-top bg-gray p-4">
        <div class="d-flex">
            <h5 class="font-weight-bold text-uppercase mb-0">View Employee</h5>
        </div>
    </div>

    <div class="p-4 card-main">
        <section class="my-5">
            <div class="card shadow">
                <div class="card-top ">
                    <div class="card-title mb-0">
                        <h5 class="font-weight-bold text-uppercase mb-0"><i class="fas fa-user-alt"></i></h5>
                    </div>
                    <div class="btn-right position-relative float-right">
                        <a href="#" class="btn btn-primary text-uppecase">Print</a>
                        <a href="adduser" class="btn btn-primary text-uppecase" data-toggle="tooltip" data-placement="top"
                            title="Add Employee">
                            <i class="fas fa-plus"></i> Add
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table" id="datatable">
                            <thead class="text-uppercase">
                                <tr>
                                    <th scope="col">EMPLOYEE ID</th>
                                    <th scope="col">First Name</th>
                                    <th scope="col">Last Name</th>
                                    <th scope="col">Role</th>
                                    <th scope="col">Branch name</th>
                                    <th scope="col">Phone no</th>
                                    <th scope="col">Pancard no</th>
                                    <th scope="col" class="nosort">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (count($product) >= 1)
                                    @foreach ($product as $emp)
                                        <tr>
                                            <td>{{ $emp->emp_const }}{{ $emp->emp_id }}</td>
                                            <td>{{ $emp->emp_firstname }}</td>
                                            <td>{{ $emp->emp_lastname }}</td>
                                            <td>{{ $emp->emp_role }}</td>
                                            <td>{{ $emp->emp_branch_name }}</td>
                                            <td>{{ $emp->emp_phone_no }}</td>
                                            <td>{{ $emp->emp_pan_no }}</td>
                                            <td class="d-flex flex-nowrap">
                                                <span data-toggle="modal" data-target="#viewuser" class="mr-1"
                                                    data-id="{{ $emp->emp_id }}" data-fname="{{ $emp->emp_firstname }}"
                                                    data-lname="{{ $emp->emp_lastname }}"
                                                    data-role="{{ $emp->emp_role }}"
                                                    data-branch="{{ $emp->emp_branch_name }}"
                                                    data-address="{{ $emp->emp_address }}"
                                                    data-city="{{ $emp->emp_city }}"
                                                    data-pincode="{{ $emp->emp_pincode }}"
                                                    data-state="{{ $emp->emp_state }}"
                                                    data-aadhar="{{ $emp->emp_aadhar_no }}"
                                                    data-pan_no="{{ $emp->emp_pan_no }}"
                                                    data-join_date="{{ $emp->emp_join_date }}"
                                                    data-phone_no="{{ $emp->emp_phone_no }}"
                                                    data-alterphone_no="{{ $emp->alter_phone_no }}"
                                                    data-resignattion_date="{{ $emp->emp_resign_date }}">
                                                    <button type="button" class="btn btn-outline-info" data-toggle="tooltip"
                                                        data-placement="top" title="View Profile">
                                                        <i class="fas fa-user-alt"></i>
                                                    </button>
                                                </span>
                                                <span data-toggle="modal" data-target="#edituser" class="mr-1"
                                                    data-id="{{ $emp->emp_id }}"
                                                    data-fname="{{ $emp->emp_firstname }}"
                                                    data-lname="{{ $emp->emp_lastname }}"
                                                    data-branch="{{ $emp->emp_branch_name }}"
                                                    data-role="{{ $emp->emp_role }}"
                                                    data-address="{{ $emp->emp_address }}"
                                                    data-city="{{ $emp->emp_city }}"
                                                    data-state="{{ $emp->emp_state }}"
                                                    data-pincode="{{ $emp->emp_pincode }}"
                                                    data-phone="{{ $emp->emp_phone_no }}"
                                                    data-alterphone="{{ $emp->alter_phone_no }}"
                                                    data-aadhar="{{ $emp->emp_aadhar_no }}"
                                                    data-pan="{{ $emp->emp_pan_no }}"
                                                    data-jdate="{{ $emp->emp_join_date }}"
                                                    data-rdate="{{ $emp->emp_resign_date }}">
                                                    <button type="button" class="btn btn-outline-success"
                                                        data-toggle="tooltip" data-placement="top" title="Edit Product">
                                                        <i class="fas fa-edit"></i>
                                                    </button>
                                                </span>
                                                <span data-toggle="modal" data-target="#deleteuser"
                                                    data-id="{{ $emp->emp_id }}"
                                                    data-fname="{{ $emp->emp_firstname }}"
                                                    data-lname="{{ $emp->emp_lastname }}"
                                                    data-phone="{{ $emp->emp_phone_no }}"
                                                    data-pan="{{ $emp->emp_pan_no }}"
                                                    data-branch="{{ $emp->emp_branch_name }}">
                                                    <button type="button" class="btn btn-outline-danger"
                                                        data-toggle="tooltip" data-placement="top" title="Delete Product">
                                                        <i class="fas fa-trash-alt"></i>
                                                    </button>
                                                </span>
                                            </td>
                                            </td>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="8">Sorry No records found</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                        <!-- Modal viewprofile -->
                        <div class="modal fade custom-modal" id="viewuser" tabindex="-1" role="dialog"
                            aria-labelledby="viewuser" aria-hidden="true">
                            <div class="modal-dialog modal-xl modal-dialog-centered edit" role="document">
                                <div class="modal-content">
                                    <div class="icon">
                                        <span class="fa-stack fa-2x text-success">
                                            <i class="fas fa-circle fa-stack-2x"></i>
                                            <i class="fas fa-table fa-stack-1x fa-inverse"></i>
                                        </span>
                                    </div>
                                    <div class="modal-header justify-content-center">
                                        <h3 class="modal-title text-capitalize font-weight-bolder mt-3" id="viewuser">
                                            View Employee Details
                                        </h3>
                                    </div>
                                    <div class="modal-body pb-0">
                                        <h6 class="fw-700 text-uppercase mb-4">User Basic Details</h6>
                                        <div class="row text-sm mb-4">
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">Employee ID</label>
                                                <p class="mb-0"><span id="id"></span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">First Name</label>
                                                <p class="mb-0"><span id="fname"></span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">Last Name</label>
                                                <p class="mb-0"><span id="lname"></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">User Role</label>
                                                <p class="mb-0"><span id="role"></span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">User Belongs To (branch)</label>
                                                <p class="mb-0"><span id="branch"></span></p>
                                            </div>
                                        </div>
                                        <div class="row text-sm mb-4">
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">Address</label>
                                                <p class="mb-0" id="emp-address"></p>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="row">
                                                    <div class="form-group col-md-4">
                                                        <label class="font-weight-bold">Aadhar Card</label>
                                                        <p class="mb-0"><span id="aadhar"></span></p>
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label class="font-weight-bold">Pan Card</label>
                                                        <p class="mb-0"><span id="pan-no"></p>
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label class="font-weight-bold">Joining Date</label>
                                                        <p class="mb-0"><span id="join-date"></span></p>
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label class="font-weight-bold">Phone No</label>
                                                        <p class="mb-0"><span id="phone-no"></span></p>
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label class="font-weight-bold">Alternate Phone No</label>
                                                        <p class="mb-0"><span id="alterphone-no"></span></p>
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label class="font-weight-bold">Resignation Date</label>
                                                        <p class="mb-0"><span id="resignation-date"></span></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer justify-content-center">
                                        <button type="button" class="btn btn-success btn-lg btn-large mt-0"
                                            data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End viewprofile -->
                        <!-- Modal Edit profile -->
                        <div class="modal fade custom-modal" id="edituser" tabindex="-1" role="dialog"
                            aria-labelledby="edituser" aria-hidden="true">
                            <div class="modal-dialog modal-xl modal-dialog-centered edit" role="document">
                                <form action="updateemp" method="post">
                                    @csrf
                                    <div class="modal-content">
                                        <div class="icon">
                                            <span class="fa-stack fa-2x text-success">
                                                <i class="fas fa-circle fa-stack-2x"></i>
                                                <i class="far fa-edit fa-stack-1x fa-inverse"></i>
                                            </span>
                                        </div>
                                        <div class="modal-header justify-content-center">
                                            <h3 class="modal-title text-capitalize font-weight-bolder mt-3" id="edituser">
                                                Edit Employee Details
                                            </h3>
                                        </div>
                                        <div class="modal-body pb-0">
                                            <h6 class="fw-700 text-uppercase">Employee Basic Details</h6>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label active" for="edit_emp_id">Employee
                                                                Id <small>*</small></label>
                                                            <input id="edit_emp_id" name="edit_emp_id" type="text"
                                                                class="form-control" readonly>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label active" for="edit_emp_fname">First
                                                                Name
                                                                <small>*</small></label>
                                                            <input id="edit_emp_fname" name="edit_emp_fname"
                                                                type="text" class="form-control">
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label active" for="edit_emp_lname">Last
                                                                Name
                                                                <small>*</small></label>
                                                            <input id="edit_emp_lname" name="edit_emp_lname" type="text" class="form-control">
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label active"
                                                                for="emp_edit_user_belongs">User
                                                                Belongs To
                                                                (branch)
                                                                <small>*</small></label>
                                                            <select class="form-control" id="emp_edit_user_belongs"
                                                                name="emp_edit_user_belongs">
                                                            </select>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label active"
                                                                for="emp_edit_user_role">User
                                                                Role
                                                                <small>*</small></label>
                                                            <select class="form-control" id="emp_edit_user_role"
                                                                name="emp_edit_user_role">
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <h6 class="fw-700 text-uppercase mt-3">Address</h6>
                                                    <div class="row">
                                                        <div class="form-group col-md-3">
                                                            <label class="control-label active"
                                                                for="edit_emp_address">Address
                                                                <small>*</small></label>
                                                            <input type="text" class="form-control" id="edit_emp_address"
                                                                name="edit_emp_address">
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <label class="control-label active" for="edit_emp_city">City
                                                                <small>*</small></label>
                                                            <input type="text" class="form-control" id="edit_emp_city"
                                                                name="edit_emp_city">
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <label class="control-label active" for="edit_emp_state">State
                                                                <small>*</small></label>
                                                            <input type="text" class="form-control" id="edit_emp_state"
                                                                name="edit_emp_state">
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <label class="control-label active" for="edit_postal">Postal
                                                                Code
                                                                <small>*</small></label>
                                                            <input type="text" class="form-control" id="edit_postal"
                                                                name="edit_postal">
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <label class="control-label active" for="edit_emp_phone">Phone
                                                                No
                                                            </label>
                                                            <input id="edit_emp_phone" name="edit_emp_phone" type="tel"
                                                                class="form-control">
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <label class="control-label active"
                                                                for="edit_alterphoneno">Alternate
                                                                Phone No</label>
                                                            <input type="tel" class="form-control" id="edit_alterphoneno"
                                                                name="edit_alterphoneno">
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <label class="control-label active" for="edit_emp_aadhar">Aadhar
                                                                card
                                                                <small>*</small></label>
                                                            <input type="text" class="form-control" id="edit_emp_aadhar"
                                                                name="edit_emp_aadhar">
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <label class="control-label active" for="edit_emp_pan">Pan Card
                                                                No
                                                                <small>*</small></label>
                                                            <input type="text" class="form-control" id="edit_emp_pan"
                                                                name="edit_emp_pan">
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <label for="edit_jdate">Joining Date <small>*</small></label>
                                                            <input id="edit_jdate" name="edit_jdate" type="date"
                                                                class="flatpickr flatpickr-input form-control">
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <label for="edit_rdate">Resignation Date
                                                                <small>*</small></label>
                                                            <input id="edit_rdate" name="edit_rdate" type="date"
                                                                class="flatpickr flatpickr-input form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer flex-nowrap">
                                            <button type="button" class="btn btn-outline-success btn-block btn-lg mt-0"
                                                data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-success btn-block btn-lg mt-0">Done</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- End Edit profile -->
                        <!-- Modal Delete profile -->
                        <div class="modal fade custom-modal" id="deleteuser" tabindex="-1" role="dialog"
                            aria-labelledby="deleteuser" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="icon">
                                        <span class="fa-stack fa-2x text-primary">
                                            <i class="fas fa-circle fa-stack-2x"></i>
                                            <i class="fas fa-trash-alt fa-stack-1x fa-inverse"></i>
                                        </span>
                                    </div>
                                    <div class="modal-header justify-content-center">
                                        <h3 class="modal-title text-capitalize font-weight-bolder mt-3" id="deleteuser">
                                            Delete Branch?
                                        </h3>
                                    </div>
                                    <div class="modal-body pb-0">
                                        <p class="font-weight-bold text-center">
                                            Are you sure you want to delete the branch?
                                            <br>
                                            You cannot undo this operation!
                                        </p>
                                        <div class="row justify-content-center">
                                            <div class="col-md-11">
                                                <form>
                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <label for="delete_emp_id" class="col-form-label">Employee
                                                                Id</label>
                                                            <input type="text" class="form-control" id="delete_emp_id"
                                                                readonly>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="delete_emp_name" class="col-form-label">Employee
                                                                Name</label>
                                                            <input type="text" class="form-control" id="delete_emp_name"
                                                                readonly>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="delete_emp_phoneno" class="col-form-label">Employee
                                                                Phone
                                                                Number</label>
                                                            <input type="text" class="form-control" id="delete_emp_phoneno"
                                                                readonly>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="delete_emp_pan_no" class="col-form-label">Employee
                                                                PAN
                                                                No</label>
                                                            <input type="text" class="form-control" id="delete_emp_pan_no"
                                                                readonly>
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <label for="delete_emp_branch" class="col-form-label">Employee
                                                                Branch</label>
                                                            <input type="text" class="form-control" id="delete_emp_branch"
                                                                readonly>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer flex-nowrap" id="delete-btn"></div>
                                </div>
                            </div>
                        </div>
                        <!-- End Delete profile -->
                    </div>
                </div>
            </div>
        </section>
    </div>
    @include('components.alertmodal')
@endsection
@section('script')
    @if (session()->has('message'))
        <script>
            $(document).ready(function() {
                $('#successAlert').modal("show");
            });

        </script>
    @endif
    @if (session()->has('update'))
        <script>
            $(document).ready(function() {
                $('#updateAlert').modal("show");
            });

        </script>
    @endif
    @if (session()->has('delete'))
        <script>
            $(document).ready(function() {
                $('#deleteAlert').modal("show");
            });

        </script>
    @endif
    @if (session()->has('error'))
        <script>
            $(document).ready(function() {
                $('#errorAlert').modal("show");
            });

        </script>
    @endif
    <script src="{{ URL::asset('assets/js/datatables.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#datatable').DataTable({
                language: {
                    searchPlaceholder: "Search records",
                    search: "",
                    "lengthMenu": "_MENU_ Entries"
                },
                "lengthMenu": [
                    [8, 16, 32, -1],
                    [8, 16, 32, "All"]
                ],
                "pageLength": 8,
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': ['nosort']
                }]
            });
            getRoles();
            getBranch();

            //View Modal
            $("#viewuser").on('show.bs.modal', function(event) {
                var emp = $(event.relatedTarget);
                var id = emp.data('id');
                var fname = emp.data('fname');
                var lname = emp.data('lname');
                var role = emp.data('role');
                var branch = emp.data('branch');

                var address = emp.data('address');
                var city = emp.data('city');
                var pincode = emp.data('pincode');
                var state = emp.data('state');
                var empAddress =
                    `<span class="mr-1">${address},</span><span>${city}-</span><span class="mr-1">${pincode},</span><span>${state}</span>`;

                var aadhar = emp.data('aadhar');
                var panNo = emp.data('pan_no');
                var joinDate = emp.data('join_date');
                var phoneNo = emp.data('phone_no');
                var alterphoneNo = emp.data('alterphone_no');
                var resignattionDate = emp.data('resignattion_date');

                //push retrieved data into mentioned id 
                $('#id').html(id);
                $('#fname').html(fname);
                $('#lname').html(lname);
                $('#role').html(role);
                $('#branch').html(branch);
                $('#emp-address').html(empAddress);
                $('#aadhar').html(aadhar);
                $('#pan-no').html(panNo);
                $('#join-date').html(joinDate);
                $('#phone-no').html(phoneNo);
                $('#alterphone-no').html(alterphoneNo);
                $('#resignation-date').html(resignattionDate);
            });

            $("#edituser").on('show.bs.modal', function(event) {
                var emp = $(event.relatedTarget);
                //console.log('emp', emp);
                var id = emp.data('id');
                var fname = emp.data('fname');
                var lname = emp.data('lname');
                var branch = emp.data('branch');
                var role = emp.data('role');
                var address = emp.data('address');
                var city = emp.data('city');
                var state = emp.data('state');
                var pincode = emp.data('pincode');
                var phone = emp.data('phone');
                var alterphone = emp.data('alterphone');
                var aadhar = emp.data('aadhar');
                var pan = emp.data('pan');
                var jdate = emp.data('jdate');
                var rdate = emp.data('rdate');

                $("#edit_emp_id").val(id);
                $("#edit_emp_fname").val(fname);
                $("#edit_emp_lname").val(lname);
                $("#emp_edit_user_belongs").val(branch);
                $("#emp_edit_user_role").val(role);
                $("#edit_emp_address").val(address);
                $("#edit_emp_city").val(city);
                $("#edit_emp_state").val(state);
                $("#edit_postal").val(pincode);
                $("#edit_emp_phone").val(phone);
                $("#edit_alterphoneno").val(alterphone);
                $("#edit_emp_aadhar").val(aadhar);
                $("#edit_emp_pan").val(pan);
                $("#edit_jdate").val(jdate);
                $("#edit_rdate").val(rdate);
            });

            function getRoles() {
                return $.ajax({
                    url: "/emproles",
                    type: "GET",
                    dataType: 'json',
                    success: function(data) {
                        //console.log("emproles", data);
                        $('#emp_edit_user_role').empty();
                        $('#emp_edit_user_role').append('<option selected="true" disabled></option>');
                        $('#emp_edit_user_role').prop('selectedIndex', 0);

                        var branch = '';

                        $.each(data, function(key, value) {
                            $('#emp_edit_user_role').append($('<option></option>').attr('value',
                                value.employee_role).text(value.employee_role));
                        });

                        $('#emp_edit_user_role').append('<option value="others">Others</option>');


                    },
                    error: function(error) {
                        console.log('empDetails error:', error);
                    }
                });
            }

            function getBranch() {
                return $.ajax({
                    url: "/branchname",
                    type: "GET",
                    dataType: 'json',
                    success: function(data) {
                        //console.log("branchname", data);
                        $('#emp_edit_user_belongs').empty();
                        $('#emp_edit_user_belongs').append(
                            '<option selected="true" disabled></option>');
                        $('#emp_edit_user_belongs').prop('selectedIndex', 0);

                        var branch = '';

                        $.each(data, function(key, value) {
                            $('#emp_edit_user_belongs').append($('<option></option>').attr(
                                'value',
                                value.org_branch_name).text(value.org_branch_name));
                        });

                        $('#emp_edit_user_belongs').append('<option value="others">Others</option>');


                    },
                    error: function(error) {
                        console.log('empDetails error:', error);
                    }
                });
            }

            $("#deleteuser").on('show.bs.modal', function(event) {
                var emp = $(event.relatedTarget);
                var id = emp.data('id');
                var fname = emp.data('fname');
                var lname = emp.data('lname');
                var name = fname + " " + lname;
                var phone = emp.data('phone');
                var pan = emp.data('pan');
                var branch = emp.data('branch');

                $("#delete_emp_id").val(id);
                $("#delete_emp_name").val(name);
                $("#delete_emp_phoneno").val(phone);
                $("#delete_emp_pan_no").val(pan);
                $("#delete_emp_branch").val(branch);

                $('#delete-btn').html(
                    `<button type="button" class="btn btn-outline-primary btn-block btn-lg mt-0" data-dismiss="modal">Close</button><a href='deleteuser/${id}' class="btn btn-primary btn-block btn-lg mt-0">Delete</a>`
                );

            });
        });

    </script>
@endsection
