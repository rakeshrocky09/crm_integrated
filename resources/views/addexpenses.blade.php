@extends('layouts.app')
@section('mytitle', 'Add EXPENSES')
@section('content')
    <div class="bg-top bg-gray p-4">
        <div class="d-flex">
            <h5 class="font-weight-bold text-uppercase mb-0">ADD EXPENSES</h5>
        </div>
    </div>
    <section class="my-4 p-4 card-main">
        <form action="addexpense" method="POST">
            @csrf
            <div class="row">
                <div class="col-md-12 py-4">
                    <div class="card shadow h-100">
                        <div class="card-top ">
                            <div class="card-title mb-0">
                                <h5 class="font-weight-bold text-uppercase mb-0"><i class="far fa-building"></i></h5>
                            </div>
                        </div>
                        <div class="card-body pt-0">
                            <div class="row fw-90">
                                <div class="form-group col-md-6">
                                    <label class="control-label active" for="date">Expense Date <small>*</small></label>
                                    <input id="date" name="date" type="date" class="flatpickr form-control"
                                        placeholder="yyyy-mm-dd">
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label" for="amount">Amount <small>*</small></label>
                                    <input id="amount" name="amount" type="number" class="form-control">
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label" for="branch_name">Branch Name</label>
                                    <select id="branch_name" name="branch_name" class="form-control"></select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label active" for="remarks">Remarks</label>
                                    <textarea class="form-control mt-1" id="remarks" name="remarks" rows="1"></textarea>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label" for="customer">Customer</label>
                                    <input id="customer" name="customer" type="text" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <a href="{{ url()->previous() }}" type="submit" class="btn btn-outline-primary btn-lg btn-large mr-2">Cancel</a>
                <button type="submit" class="btn btn-primary btn-lg btn-large">Submit</button>
            </div>
        </form>
    </section>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            getBranch();
            $('#branch_name').change(function() { //jQuery Change Function
                var opval = $(this).val(); //Get value from select element
                if (opval == "others") { //Compare it and if true
                    return window.location.href = "/addorganization";
                }
            });
        });

        function getBranch() {
            return $.ajax({
                url: "/branchname",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    //console.log("branchname", data);
                    $('#branch_name').empty();
                    $('#branch_name').append('<option selected="true" disabled></option>');
                    $('#branch_name').prop('selectedIndex', 0);
                    $.each(data, function(key, value) {
                        $('#branch_name').append($('<option></option>').attr('value', value
                            .org_branch_name).text(value.org_branch_name));
                    });
                    $('#branch_name').append('<option value="others">Others</option>');
                },
                error: function(error) {
                    console.log('branchname error:', error);
                }
            });
        }

    </script>
@endsection

