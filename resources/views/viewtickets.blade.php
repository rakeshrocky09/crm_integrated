@extends('layouts.app')
@section('mytitle', 'VIEW Ticket')
@section('content')
    <div class="p-4">
        <section class="my-5">
            <div class="card shadow">
                <div class="card-top ">
                    <div class="card-title mb-0">
                        <h6 class="font-weight-bold text-capitalize mb-0">View Ticket</h6>
                    </div>
                </div>
                <div class="card-body pt-0">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <h6 class="font-weight-bold mb-0 text-sm">Ticket Id</h6>
                                <span class="text-13">{{ $ticket[0]->Ticket_id }}</span>
                            </div>
                            <div class="form-group">
                                <h6 class="font-weight-bold mb-0 text-sm">Customer Name</h6>
                                <span class="text-13">{{ $ticket[0]->customer_name }}</span>
                            </div>
                            <div class="form-group">
                                <h6 class="font-weight-bold mb-0 text-sm">Phone No</h6>
                                <span class="text-13">{{ $ticket[0]->phone_no }}</span>
                            </div>
                            <div class="form-group">
                                <h6 class="font-weight-bold mb-0 text-sm">Branch</h6>
                                <span class="text-13">{{ $ticket[0]->branch_name }}</span>
                            </div>
                            <div class="form-group">
                                <h6 class="font-weight-bold mb-0 text-sm">Address</h6>
                                <address class="text-13">
                                    <span> {{ $ticket[0]->delivery_address_1 }},</span><br>
                                    <span>{{ $ticket[0]->delivery_city }}</span> -
                                    <span>{{ $ticket[0]->delivery_pincode }}</span><br>
                                    <span> {{ $ticket[0]->delivery_state }}</span>
                                </address>
                            </div>
                            <div class="form-group">
                                <h6 class="font-weight-bold mb-0 text-sm">Caller/Sender Name</h6>
                                <span class="text-13">{{ $ticket[0]->Caller_Sender_Name }}</span>
                            </div>
                            <div class="form-group">
                                <h6 class="font-weight-bold mb-0 text-sm">Caller/Sender Email</h6>
                                <span class="text-13">{{ $ticket[0]->Caller_Sender_Email }}</span>
                            </div>
                            <div class="form-group">
                                <h6 class="font-weight-bold mb-0 text-sm">Caller/Sender Phone</h6>
                                <span class="text-13">{{ $ticket[0]->Caller_Sender_Phone }}</span>
                            </div>
                            <div class="my-3">
                                <a href="{{ url('viewticket') }}" class="btn btn-primary">Cancel</a>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="table-responsive">
                                <table class="table table-bordered table-view">
                                    <thead>
                                        <tr>
                                            <th scope="col">Machine Serial No</th>
                                            <th scope="col">Ticket Type</th>
                                            <th scope="col">Ticket Source</th>
                                            <th scope="col">Problem Description</th>
                                            <th scope="col">Created Date</th>
                                            <th scope="col">Closed Date</th>
                                            <th scope="col">Status</th>
                                            <th scope="col">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($ticket as $item)
                                            <tr>
                                                <td>{{ $item->Machine_Serial_No }}</td>
                                                <td>{{ $item->Ticket_Type }}</< /td>
                                                <td>{{ $item->Ticket_Source }}</< /td>
                                                <td>{{ $item->Problem_Description }}</< /td>
                                                <td>{{ $item->Ticket_Created_Date }}</< /td>
                                                <td>{{ $item->Ticket_Closed_Date }}</< /td>
                                                <td>
                                                    @if ($item->Ticket_Status == 'open')
                                                        <span
                                                            class="badge badge-pill badge-success text-12">{{ $item->Ticket_Status }}</span>
                                                    @else
                                                        <span
                                                            class="badge badge-pill badge-secondary text-12">{{ $item->Ticket_Status }}</span>
                                                    @endif
                                                </td>
                                                <td class="d-flex flex-nowrap">
                                                    <button type="button" class="btn btn-outline-info mr-1"
                                                        title="View Ticket" data-toggle="modal" data-target="#viewticket"
                                                        data-serial="{{ $item->Machine_Serial_No }}"
                                                        data-source="{{ $item->Ticket_Source }}"
                                                        data-type="{{ $item->Ticket_Type }}"
                                                        data-code="{{ $item->Ticket_Defect_Code }}"
                                                        data-status="{{ $item->Ticket_Status }}"
                                                        data-tat="{{ $item->Total_Hrs }}"
                                                        data-priority="{{ $item->Ticket_Priority }}"
                                                        data-technician_name="{{ $item->Assign_Technician_Name }}"
                                                        data-service_charges="{{ $item->Service_Charges }}"
                                                        data-tclosed_date="{{ $item->Ticket_Closed_Date }}"
                                                        data-created_date="{{ $item->Created_Date }}"
                                                        data-description="{{ $item->Problem_Description }}"
                                                        data-ba3="{{ $item->Count_Black_A3 }}"
                                                        data-ca3="{{ $item->Count_Color_A3 }}"
                                                        data-ba4="{{ $item->Count_Color_A4 }}"
                                                        data-ca4="{{ $item->Count_Black_A4 }}">
                                                        <i class="fas fa-external-link-alt"></i>
                                                    </button>
                                                    <button type="button" class="btn btn-outline-success mr-1"
                                                        title="Edit Ticket" data-toggle="modal" data-target="#editticket"
                                                        data-id="{{ $item->Ticket_id }}"
                                                        data-source="{{ $item->Ticket_Source }}"
                                                        data-type="{{ $item->Ticket_Type }}"
                                                        data-code="{{ $item->Ticket_Defect_Code }}"
                                                        data-status="{{ $item->Ticket_Status }}"
                                                        data-tat="{{ $item->Total_Hrs }}"
                                                        data-priority="{{ $item->Ticket_Priority }}"
                                                        data-technician_name="{{ $item->Assign_Technician_Name }}"
                                                        data-service_charges="{{ $item->Service_Charges }}"
                                                        data-tclosed_date="{{ $item->Ticket_Closed_Date }}"
                                                        data-description="{{ $item->Problem_Description }}"
                                                        data-ba3="{{ $item->Count_Black_A3 }}"
                                                        data-ca3="{{ $item->Count_Color_A3 }}"
                                                        data-ba4="{{ $item->Count_Color_A4 }}"
                                                        data-ca4="{{ $item->Count_Black_A4 }}">
                                                        <i class="fas fa-edit"></i>
                                                    </button>
                                                    <a href="{{ url('setdisposition/' . $item->Ticket_id . '/' . $item->cus_id) }}"
                                                        class="btn btn-outline-danger asd" title="Add Closure">
                                                        <i class="fas fa-store-alt-slash"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <!-- Modal viewticket -->
                                <div class="modal fade custom-modal" id="viewticket" tabindex="-1" role="dialog"
                                    aria-labelledby="addproducts" aria-hidden="true">
                                    <div class="modal-dialog modal-xl modal-dialog-centered edit" role="document">
                                        <div class="modal-content">
                                            <div class="icon">
                                                <span class="fa-stack fa-2x text-success">
                                                    <i class="fas fa-circle fa-stack-2x"></i>
                                                    <i class="fas fa-table fa-stack-1x fa-inverse"></i>
                                                </span>
                                            </div>
                                            <div class="modal-header justify-content-center">
                                                <h3 class="modal-title text-capitalize font-weight-bolder mt-3"
                                                    id="viewticket">
                                                    View Tickets
                                                </h3>
                                            </div>
                                            <div class="modal-body pb-0">
                                                <h6 class="fw-700 text-uppercase mb-4">TICKET DETAILS</h6>
                                                <div class="row text-sm mb-4">
                                                    <div class="form-group col-md-3">
                                                        <label class="font-weight-bold">Machine Serial No.</label>
                                                        <p class="mb-0" id="machine_serial"></p>
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <label class="font-weight-bold">Source</label>
                                                        <p class="mb-0" id="source"></p>
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <label class="font-weight-bold">Ticket Type</label>
                                                        <p class="mb-0" id="ticket_type"></p>
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <label class="font-weight-bold">Defect Code</label>
                                                        <p class="mb-0" id="defect_code"></p>
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <label class="font-weight-bold">Ticket Status</label>
                                                        <p class="mb-0" id="status"></p>
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <label class="font-weight-bold">Turn Around Time(TAT Hrs)</label>
                                                        <p class="mb-0" id="tat"></p>
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <label class="font-weight-bold">Priority</label>
                                                        <p class="mb-0" id="priority"></p>
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <label class="font-weight-bold">Assign/Re-Assign Techinician</label>
                                                        <p class="mb-0" id="techinician"></p>
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <label class="font-weight-bold">Service Charge</label>
                                                        <p class="mb-0" id="service_charges"></p>
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <label class="font-weight-bold">Ticket Created Date</label>
                                                        <p class="mb-0" id="created_date"></p>
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <label class="font-weight-bold">Ticket Closed Date</label>
                                                        <p class="mb-0" id="tclosed_date"></p>
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <label class="font-weight-bold">Problem Description</label>
                                                        <p class="mb-0" id="description"></p>
                                                    </div>
                                                </div>
                                                <h6 class="fw-700 text-uppercase mb-4">RECORD METER READINGS</h6>
                                                <div class="row text-sm">
                                                    <div class="form-group col-md-3">
                                                        <label class="font-weight-bold">Black A3</label>
                                                        <p class="mb-0" id="blacka3"></p>
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <label class="font-weight-bold">Black A4</label>
                                                        <p class="mb-0" id="colora3"></p>
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <label class="font-weight-bold">Color A3</label>
                                                        <p class="mb-0" id="blacka4"></p>
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <label class="font-weight-bold">Color A4</label>
                                                        <p class="mb-0" id="colora4"></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer justify-content-center">
                                                <button type="button" class="btn btn-success btn-lg btn-large mt-0"
                                                    data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End viewticket -->
                                <!-- Modal Edit Products -->
                                <div class="modal fade custom-modal" id="editticket" tabindex="-1" role="dialog"
                                    aria-labelledby="addproducts" aria-hidden="true">
                                    <div class="modal-dialog modal-xl modal-dialog-centered edit" role="document">
                                        <form id="edit_form" method="POST">
                                            @csrf
                                            <div class="modal-content">
                                                <div class="icon">
                                                    <span class="fa-stack fa-2x text-success">
                                                        <i class="fas fa-circle fa-stack-2x"></i>
                                                        <i class="far fa-edit fa-stack-1x fa-inverse"></i>
                                                    </span>
                                                </div>
                                                <div class="modal-header justify-content-center">
                                                    <h3 class="modal-title text-capitalize font-weight-bolder mt-3"
                                                        id="editticket">
                                                        Edit Ticket
                                                    </h3>
                                                </div>
                                                <div class="modal-body pb-0">
                                                    <div class="row">
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label active"
                                                                for="edit_source">Source</label>
                                                            <select class="form-control" id="edit_source"
                                                                name="edit_source">
                                                                <option disabled selected></option>
                                                                <option value="CALL"> CALL </option>
                                                                <option value="EMAIL"> EMAIL </option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label for="edit_defectcode"
                                                                class="mb-0 control-label active">Defect Code
                                                                <small>*</small></label>
                                                            <select class="form-control" id="edit_defectcode"
                                                                name="edit_defectcode"></select>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label active" for="edit_tat">Turn Around
                                                                Time(TAT Hrs)</label>
                                                            <input type="text" class="form-control" id="edit_tat"
                                                                name="edit_tat">
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label active"
                                                                for="edit_ticket_type">Ticket
                                                                Type <small>*</small>
                                                            </label>
                                                            <select class="form-control" id="edit_ticket_type"
                                                                name="edit_ticket_type"> </select>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label active"
                                                                for="edit_ticket_priority">Priority <small>*</small>
                                                            </label>
                                                            <select class="form-control" id="edit_ticket_priority"
                                                                name="edit_ticket_priority">
                                                                <option disabled selected></option>
                                                                <option value="P1"> P1 </option>
                                                                <option value="P2"> P2 </option>
                                                                <option value="P3"> P3 </option>
                                                                <option value="P4"> P4 </option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label active"
                                                                for="edit_servicecharge">Service
                                                                charge(RS) <small>*</small></label>
                                                            <input type="text" class="form-control" id="edit_servicecharge"
                                                                name="edit_servicecharge">
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label active"
                                                                for="edit_ticket_blacka3">Black
                                                                A3</label>
                                                            <input type="text" class="form-control" id="edit_ticket_blacka3"
                                                                name="edit_ticket_blacka3">
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label active"
                                                                for="edit_ticket_blacka4">Black
                                                                A4</label>
                                                            <input type="text" class="form-control" id="edit_ticket_blacka4"
                                                                name="edit_ticket_blacka4">
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label active"
                                                                for="edit_ticket_colora3">Color
                                                                A3
                                                            </label>
                                                            <input type="text" class="form-control" id="edit_ticket_colora3"
                                                                name="edit_ticket_colora3">
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label active"
                                                                for="edit_ticket_colora4">Color
                                                                A4
                                                            </label>
                                                            <input type="text" class="form-control" id="edit_ticket_colora4"
                                                                name="edit_ticket_colora4">
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label active" for="edit_status">Status
                                                            </label>
                                                            <input type="text" class="form-control" id="edit_status"
                                                                name="edit_status">
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label active"
                                                                for="edit_description">Problem
                                                                Description
                                                                <small>*</small></label>
                                                            <textarea class="form-control mt-1" id="edit_description"
                                                                name="edit_description" rows="1"></textarea>
                                                        </div>
                                                    </div>

                                                    <h6 class="fw-700 text-uppercase pl-1 mb-4 mt-3">
                                                        Assign/Re-assign</h6>
                                                    <div class="row">
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label active"
                                                                for="edit_technician">Techinician/Engineer
                                                            </label>
                                                            <select class="form-control" id="edit_technician"
                                                                name="edit_technician"></select>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label for="edit_closed_date"
                                                                class="mb-0 control-label active">Ticket
                                                                Closed Date</label>
                                                            <input id="edit_closed_date" name="edit_closed_date" type="date"
                                                                class="flatpickr form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer flex-nowrap">
                                                    <button type="button"
                                                        class="btn btn-outline-success btn-block btn-lg mt-0"
                                                        data-dismiss="modal">Close</button>
                                                    <button type="submit"
                                                        class="btn btn-success btn-block btn-lg mt-0">Submit</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <!-- End Edit Products -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('script')
    @if (session()->has('update'))
        <script>
            $(document).ready(function() {
                $('#updateAlert').modal("show");
            });

        </script>
    @endif
    @if (session()->has('error'))
        <script>
            $(document).ready(function() {
                $('#errorAlert').modal("show");
            });

        </script>
    @endif
    <script>
        $(document).ready(function() {
            loadDefectCode();
            loadTicketType();
            loadTechnicians();
            //View Modal
            $("#viewticket").on('show.bs.modal', function(event) {
                var ticket = $(event.relatedTarget);
                var serial = ticket.data('serial');
                var source = ticket.data('source');
                var type = ticket.data('type');
                var code = ticket.data('code');
                var status = ticket.data('status');
                var tat = ticket.data('tat');
                var priority = ticket.data('priority');
                var technician_name = ticket.data('technician_name');
                var service_charges = ticket.data('service_charges');
                var tclosed_date = ticket.data('tclosed_date');
                var created_date = ticket.data('created_date');
                var description = ticket.data('description');
                var blackA3 = ticket.data('ba3');
                var colorA3 = ticket.data('ca3');
                var blackA4 = ticket.data('ba4');
                var colorA4 = ticket.data('ca4');
                //console.log('colorA4', colorA4);

                //push retrieved data into mentioned id 
                $('#machine_serial').html(serial);
                $('#source').html(source);
                $('#ticket_type').html(type);
                $('#defect_code').html(code);
                $('#status').html(status);
                $('#tat').html(tat);
                $('#priority').html(priority);
                $('#techinician').html(technician_name);
                $('#service_charges').html(service_charges);
                $('#tclosed_date').html(tclosed_date);
                $('#created_date').html(created_date);
                $('#description').html(description);
                $('#blacka3').html(blackA3);
                $('#colora3').html(colorA3);
                $('#blacka4').html(blackA4);
                $('#colora4').html(colorA4);
            });

            $("#editticket").on('show.bs.modal', function(event) {
                var ticket = $(event.relatedTarget);
                var id = ticket.data('id');
                var source = ticket.data('source');
                var type = ticket.data('type');
                var code = ticket.data('code');
                var status = ticket.data('status');
                var tat = ticket.data('tat');
                var priority = ticket.data('priority');
                var technician_name = ticket.data('technician_name');
                var service_charges = ticket.data('service_charges');
                var tclosed_date = ticket.data('tclosed_date');
                var description = ticket.data('description');
                var blackA3 = ticket.data('ba3');
                var colorA3 = ticket.data('ca3');
                var blackA4 = ticket.data('ba4');
                var colorA4 = ticket.data('ca4');
                console.log('technician_name', technician_name);

                //push retrieved data into mentioned id
                var action = `{{ url('updateticket/${id}') }}`;
                $('#edit_form').attr('action', action);
                //$('#edit_form').attr('action', 'updateticket/' + serviceId);
                $('#edit_source').val(source);
                $('#edit_defectcode').val(code);
                $('#edit_tat').val(tat);
                $('#edit_ticket_type').val(type);
                $('#edit_ticket_priority').val(priority);
                $('#edit_servicecharge').val(service_charges);
                $('#edit_ticket_blacka3').val(blackA3);
                $('#edit_ticket_blacka4').val(blackA4);
                $('#edit_ticket_colora3').val(colorA3);
                $('#edit_ticket_colora4').val(colorA4);
                $('#edit_status').val(status);
                $('#edit_description').val(description);
                $('#edit_technician').val(technician_name);
                $('#edit_closed_date').val(tclosed_date);

            });

        });

        function loadDefectCode() {
            return $.ajax({
                url: "/ticketdefectcode",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    //console.log("ticketdefectcode", data);
                    $('#edit_defectcode').empty();
                    $('#edit_defectcode').append('<option selected="true" disabled></option>');
                    $('#edit_defectcode').prop('selectedIndex', 0);
                    $.each(data, function(key, value) {
                        $('#edit_defectcode').append($('<option></option>').attr(
                            'value',
                            value.code_name).text(value.code_name));
                    });
                    $('#edit_defectcode').append('<option value="others">Others</option>');
                },
                error: function(error) {
                    console.log('edit_defectcode error:', error);
                }
            });
        }

        function loadTicketType() {
            return $.ajax({
                url: "/tickettype",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    //console.log("tickettype", data);
                    $('#edit_ticket_type').empty();
                    $('#edit_ticket_type').append('<option selected="true" disabled></option>');
                    $('#edit_ticket_type').prop('selectedIndex', 0);
                    $.each(data, function(key, value) {
                        $('#edit_ticket_type').append($('<option></option>').attr(
                            'value',
                            value.ticket_type).text(value.ticket_type));
                    });
                    $('#edit_ticket_type').append('<option value="others">Others</option>');
                },
                error: function(error) {
                    console.log('edit_ticket_type error:', error);
                }
            });
        }

        function loadTechnicians() {
            return $.ajax({
                url: "/technicians",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    $('#edit_technician').empty();
                    $('#edit_technician').append('<option selected="true" disabled></option>');
                    $('#edit_technician').prop('selectedIndex', 0);
                    $.each(data, function(key, value) {
                        var name = value.emp_firstname + value.emp_lastname;
                        //console.log("technicians", name);
                        $('#edit_technician').append($('<option></option>').attr(
                            'value', name).text(name));
                    });
                    $('#edit_technician').append('<option value="others">Others</option>');
                },
                error: function(error) {
                    console.log('edit_technician error:', error);
                }
            });
        }

    </script>
@endsection
