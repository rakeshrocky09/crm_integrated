@extends('layouts.app')
@section('mytitle', 'Create User')
@section('content')
    <div class="bg-top bg-gray p-4">
        <div class="d-flex">
            <h5 class="font-weight-bold text-uppercase mb-0">Create User</h5>
        </div>
    </div>

    <section class="my-4 p-4 card-main">
        <div class="row">
            <div class="col-md-12 py-4">
                <div class="card shadow">
                    <div class="card-top ">
                        <div class="card-title mb-0">
                            <h5 class="font-weight-bold text-uppercase mb-0"><i class="fas fa-user-alt"></i></h5>
                        </div>
                    </div>
                    <div class="card-body pt-0">
                        <form class="row fw-90">
                            <div class="form-group col-md-6">
                                <label class="control-label" for="cname">Company Name</label>
                                <input id="cname" type="text" class="form-control">
                            </div>
                            <div class="form-group col-md-6">
                                <label class="control-label" for="uname">User Name</label>
                                <input id="uname" type="text" class="form-control">
                            </div>
                            <div class="form-group col-md-6">
                                <label class="control-label" for="password">Password</label>
                                <input id="password" type="password" class=" form-control">
                            </div>
                            <div class="form-group col-md-6">
                                <label class="control-label" for="cpassword">Confirm Password</label>
                                <input id="password" type="cpassword" class=" form-control">
                            </div>
                        </form>

                        <button type="submit" class="btn btn-primary btn-lg mt-4 btn-large">Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
