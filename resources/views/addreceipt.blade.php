@extends('layouts.app')
@section('mytitle', 'Add Receipt')
@section('content')

    <div class="p-4">
        <section class="my-5">
            <div class="card shadow">
                <div class="card-top ">
                    <div class="card-title mb-0">
                        <h6 class="font-weight-bold text-capitalize mb-0">Add Receipt</h6>
                    </div>
                </div>
                <div class="card-body pt-0">
                    <form action="{{ url('addreceipt') }}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col-md-3 text-sm">
                                <div class="d-flex flex-column justify-content-between h-100">
                                    <div>
                                        <h6 class="fw-700 text-uppercase mb-3">Customer Details</h6>
                                        <div class="form-group">
                                            <h6 class="font-weight-bold mb-0 text-sm">Customer Name</h6>
                                            <input class="custom-input" id="receipt_customer_name"
                                                name="receipt_customer_name" type="text"
                                                value="{{ $cus[0]->customer_name }}" readonly>
                                        </div>
                                        <div class="form-group">
                                            <h6 class="font-weight-bold mb-0 text-sm">Phone No</h6>
                                            <input class="custom-input" id="receipt_phone_no" name="receipt_phone_no"
                                                type="text" cus="{{ $cus[0]->cus_phone_no }}" readonly>
                                        </div>
                                        <div class="form-group">
                                            <h6 class="font-weight-bold mb-0 text-sm">Email ID</h6>
                                            <input class="custom-input" id="receipt_email_id" name="receipt_email_id"
                                                type="text" value="{{ $cus[0]->cus_email_id }}" readonly>
                                        </div>
                                        <div class="form-group">
                                            <h6 class="font-weight-bold mb-0 text-sm">Alternate Phone No</h6>
                                            <input class="custom-input" id="receipt_alernatephone_no"
                                                name="receipt_alernatephone_no" type="text"
                                                value="{{ $cus[0]->cus_alter_phone }}" readonly>
                                        </div>
                                        <div class="form-group">
                                            <h6 class="font-weight-bold mb-0 text-sm">Belongs to branch</h6>
                                            <input class="custom-input" id="receipt_belongsto_branch"
                                                name="receipt_belongsto_branch" type="text"
                                                value="{{ $cus[0]->cus_branch_name }}" readonly>
                                        </div>
                                        <div class="form-group">
                                            <h6 class="font-weight-bold mb-0 text-sm">Primary Contact Person Name</h6>
                                            <input class="custom-input" id="receipt_primary_name"
                                                name="receipt_primary_name" type="text"
                                                value="{{ $cus[0]->primary_contact_name }}" readonly>
                                        </div>
                                        <div class="form-group">
                                            <h6 class="font-weight-bold mb-0 text-sm">Email ID</h6>
                                            <input class="custom-input" id="receipt_primary_emailid"
                                                name="receipt_primary_emailid" type="text"
                                                value="{{ $cus[0]->primary_contact_email }}" readonly>
                                        </div>
                                        <div class="form-group">
                                            <h6 class="font-weight-bold mb-0 text-sm">Phone No</h6>
                                            <input class="custom-input" id="receipt_primary_phoneid"
                                                name="receipt_primary_phoneid" type="text"
                                                value="{{ $cus[0]->primary_contact_phoneno }}" readonly>
                                        </div>
                                        <div class="form-group">
                                            <h6 class="font-weight-bold mb-0 text-sm">Address</h6>
                                            <input class="custom-input" id="receipt_address" name="receipt_address"
                                                type="text" value="{{ $cus[0]->cus_address }}" readonly>
                                            <input class="custom-input" id="receipt_city" name="receipt_city" type="text"
                                                value="{{ $cus[0]->cus_city }}" readonly>
                                            <input class="custom-input" id="receipt_pincode" name="receipt_pincode"
                                                type="text" value="{{ $cus[0]->cus_pincode }}" readonly>
                                            <input class="custom-input" id="receipt_state" name="receipt_state" type="text"
                                                value="{{ $cus[0]->cus_state }}" readonly>
                                        </div>
                                    </div>
                                    <div>
                                        <a href="{{ url()->previous() }}" type="submit"
                                            class="btn btn-outline-primary btn-large mr-2">Cancel</a>
                                        <button type="submit" class="btn btn-primary btn-large">Submit</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-9 pl-md-0">
                                <ul class="nav nav-pills" id="pills-tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="pills-receipt-tab" data-toggle="pill"
                                            href="#pills-receipt" role="tab" aria-controls="pills-receipt"
                                            aria-selected="true">Receipt</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="pills-paidreceipt-tab" data-toggle="pill"
                                            href="#pills-paidreceipt" role="tab" aria-controls="pills-paidreceipt"
                                            aria-selected="false">Paid Receipt</a>
                                    </li>
                                </ul>
                                <div class="tab-content" id="pills-tabContent">
                                    <div class="tab-pane fade show active my-3" id="pills-receipt" role="tabpanel"
                                        aria-labelledby="pills-receipt-tab">
                                        <div class="mb-3">
                                            <h6 class="fw-700 text-uppercase mb-4">Receipt Details</h6>
                                            <div class="row text-sm">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label" for="receipt_branchname">Branch Name
                                                            <small>*</small></label>
                                                        <input type="text" class="form-control" name="receipt_branchname"
                                                            id="receipt_branchname"
                                                            value="{{ $cus[0]->cus_branch_name }}" readonly>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label active" for="receipt_payment">Mode of
                                                            Payment</label>
                                                        <select class="form-control" id="receipt_payment"
                                                            name="receipt_payment">
                                                            <option selected="true" disabled></option>
                                                            @foreach ($receipt_payment as $item)
                                                                <option value="{{ $item->mode_of_payment }}">
                                                                    {{ $item->mode_of_payment }}
                                                                </option>
                                                            @endforeach
                                                            <option value="others">Others</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label" for="receipt_total_paid">Total Paid
                                                            Amount</label>
                                                        @if ($data['Total_Paid_Amount'] == null)

                                                            <input type="number" class="form-control"
                                                                name="receipt_total_paid" id="receipt_total_paid" readonly
                                                                value="0">
                                                        @else
                                                            <input type="number" class="form-control"
                                                                name="receipt_total_paid" id="receipt_total_paid" readonly
                                                                value="{{ $data['Total_Paid_Amount'] }}">
                                                        @endif
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label" for="receipt_acutual_amount">Invoice
                                                            Amount
                                                            <small>*</small></label>
                                                        <input type="number" class="form-control"
                                                            name="receipt_acutual_amount" id="receipt_acutual_amount"
                                                            value="{{ $data['Invoice_Amount'] }}" readonly>
                                                        <input class="form-control" id="receipt_cus_id"
                                                            name="receipt_cus_id" type="hidden"
                                                            value="{{ $data['Customer_Id'] }}">
                                                        <input class="form-control" id="receipt_product_id"
                                                            name="receipt_product_id" type="hidden"
                                                            value="{{ $data['Item_Id'] }}">
                                                        <input class="form-control" id="receipt_invoice_id"
                                                            name="receipt_invoice_id" type="hidden"
                                                            value="{{ $data['invoice_id'] }}">
                                                        <input class="form-control" id="receipt_invoice_number"
                                                            name="receipt_invoice_number" type="hidden"
                                                            value="{{ $data['invoice_number'] }}">
                                                        <input class="form-control" id="receipt_invoice_date"
                                                            name="receipt_invoice_date" type="hidden"
                                                            value="{{ $data['Invoice_Date'] }}">
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label active" for="receipt_amount_paid">Amount
                                                            Paid
                                                            <small>*</small></label>
                                                        <input type="number" class="form-control" id="receipt_amount_paid"
                                                            name="receipt_amount_paid" onChange="calculateTotals()">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="receipt_amount_date"
                                                            class="mb-0 control-label active">Paid
                                                            Date</label>
                                                        <input id="receipt_amount_date" name="receipt_amount_date"
                                                            class="flatpickr form-control" type="date"
                                                            placeholder="yyyy-mm-dd">
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label" for="input">Ref. No.
                                                            <small>*</small></label>
                                                        <input type="text" class="form-control" id="receipt_ref_no"
                                                            name="receipt_ref_no">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="receipt_last_date"
                                                            class="mb-0 control-label active">Last
                                                            Paided
                                                            Date</label>
                                                        <input id="receipt_last_date" name="receipt_last_date"
                                                            class="flatpickr form-control" type="date" readonly
                                                            disabled="true"
                                                            value="{{ $data['Paid_Date'] }}">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="receipt_pending_date"
                                                            class="mb-0 control-label active">Next
                                                            Followup Date</label>
                                                        <input id="receipt_pending_date" name="receipt_pending_date"
                                                            placeholder="yyyy-mm-dd" class="flatpickr form-control"
                                                            type="date">
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label active"
                                                            for="receipt_amount_pending">Pending
                                                            Amount
                                                            <small>*</small></label>
                                                        <input type="text" class="form-control" id="receipt_amount_pending"
                                                            name="receipt_amount_pending" readonly>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane fade show my-3" id="pills-paidreceipt" role="tabpanel"
                                        aria-labelledby="pills-paidreceipt-tab">
                                        <div class="d-flex justify-content-between align-items-center mb-2">
                                            <h6 class="fw-700 text-uppercase pl-1">Paid Receipt</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>

    {{-- add_cash --}}
    <div class="modal fade custom-modal" id="add_cash" tabindex="-1" role="dialog" aria-labelledby="add_cash"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content success">
                <div class="icon">
                    <span class="fa-stack fa-2x text-success">
                        <i class="fas fa-circle fa-stack-2x"></i>
                        <i class="fas fas fa-plus fa-stack-1x fa-inverse"></i>
                    </span>
                </div>
                <div class="modal-header justify-content-center">
                    <h3 class="modal-title text-capitalize font-weight-bolder mt-3">
                        Add Cash
                    </h3>
                </div>
                <form action="{{ url('addpaymode') }}" method="POST">
                    @csrf
                    <div class="modal-body pb-0">
                        <div class="row justify-content-center">
                            <div class="col-md-11">
                                <div class="form-group @error('new_mode') is-invalid @enderror">
                                    <label for="new_mode" class="col-form-label">Add New Payment Mode</label>
                                    <input type="text" class="form-control" id="new_mode" name="new_mode">
                                    @error('new_mode')
                                        <span class="text-danger text-12">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer flex-nowrap">
                        <button type="button" class="btn btn-outline-success btn-block btn-lg mt-0"
                            data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success btn-block btn-lg mt-0">Done</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @include('components.alertmodal')
@endsection
@section('script')
    @if (session()->has('message'))
        <script>
            $(document).ready(function() {
                $('#successAlert').modal("show");
            });

        </script>
    @endif
    @if (session()->has('error'))
        <script>
            $(document).ready(function() {
                $('#errorAlert').modal("show");
            });

        </script>
    @endif
    <script>
        $(document).ready(function() {
            $('#receipt_payment').change(function() { //jQuery Change Function
                var opval = $(this).val(); //Get value from select element
                console.log(opval);
                if (opval == "others") { //Compare it and if true
                    $('#add_cash').modal("show"); //Open Modal
                }
            });
        });

        function calculateTotals() {
            var subTotItem = 0;
            var p = isNaN(Number($("#receipt_acutual_amount").val())) ? 0 : Number($("#receipt_acutual_amount").val());
            var q = isNaN(Number($("#receipt_amount_paid").val())) ? 0 : Number($("#receipt_amount_paid").val());
            var r = isNaN(Number($("#receipt_total_paid").val())) ? 0 : Number($("#receipt_total_paid").val());
            var tot = p - (q + r);
            $("#receipt_amount_pending").val(tot.toFixed(2));
        }


        /* function validatereceipt() {
            var jsonData = form_to_json("add_receipt_form");
            console.log(jsonData);
            var userid = localStorage.getItem("userid");

            $.ajax({
                url: APP_CONFIG.API_HOST + "/v1/" + userid + "/receipt",
                type: "POST",
                headers: {
                    "Authorization": localStorage.getItem("authToken")
                },
                data: JSON.stringify(jsonData),
                success: function(data) {
                    if (data == "error") {
                        alertjs.showSwal('add-failure');
                    }
                    if (data == "Success") {
                        alertjs.showSwal('add-success');
                        window.location = "viewreceipt.php";
                    }
                    if (data == "Receipt Already exist") {
                        alertjs.showSwal('already-exist');
                    }
                },
                error: function(error) {
                    console.log(error);
                }
            });
        } */

    </script>
@endsection
