@extends('layouts.app')
@section('mytitle', 'Add Invoice')
@section('content')
    <div class="bg-top bg-gray p-4">
        <div class="d-flex">
            <h5 class="font-weight-bold text-uppercase mb-0">Add Invoice</h5>
        </div>
    </div>
    <section class="my-4 p-4 card-main">
        <div class="row">
            <div class="col-md-12 py-4">
                <div class="card shadow h-100">
                    <div class="card-top ">
                        <div class="card-title mb-0">
                            <h6 class="font-weight-bold text-uppercase mb-0">Branch & Customer Details</h6>
                        </div>
                    </div>
                    <div class="card-body pt-0">
                        <div class="row">
                            <div class="col-12">
                                <div class="d-flex justify-content-between">
                                    <p class="text-sm font-weight-bold"><i class="fas fa-question-circle mr-2"></i>Please
                                        select Branch and Enter Customer Number.</p>
                                    <a href="{{ url('addcustomer') }}" class="btn btn-primary text-uppecase"
                                        data-toggle="tooltip" data-placement="top" title="Add Customer">
                                        <i class="fas fa-plus"></i> Add
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <select class="form-control" id="select_branch" name="select_branch"></select>
                                </div>
                                <div class="text-sm" id="branch_detail">
                                    <label id="branch_name" class="font-weight-bold"></label>
                                    <div class="mb-0 d-flex flex-column">
                                        <span id="view_branch_address"></span>
                                        <span id="view_branch_city"></span>
                                        <span id="view_branch_state"></span>
                                        <span id="view_branch_emailid"></span>
                                        <span id="view_branch_phoneno"></span>
                                        <span id="view_branch_id" hidden> </span><br>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <select class="form-control" id="customer_name"></select>
                                </div>
                                <div class="text-sm" id="customer_detail">
                                    <label class="font-weight-bold" id="view_customer_name"></label>
                                    <div class="mb-0 d-flex flex-column">
                                        <span id="view_customer_address"></span>
                                        <span id="view_city_pin"></span>
                                        <span id="view_customer_city" hidden></span>
                                        <span id="view_customer_pincode" hidden></span>
                                        <span id="view_customer_state"></span>
                                        <span id="view_customer_emailid"></span>
                                        <span id="view_customer_phoneno"></span>
                                        <span id="customer_id" hidden></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="text-sm display-none" id="bank_detail">
                                    <label class="font-weight-bold">Seller Bank Details:</label>
                                    <div class="mb-0 d-flex flex-column">
                                        <span id="view_account_name"></span>
                                        <span id="view_account_no"></span>
                                        <span id="view_bank_branch"></span>
                                        <span id="view_account_type"></span>
                                        <span id="view_bank_name"></span>
                                        <span id="view_bank_ifsccode"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12 py-4">
                <div class="card shadow">
                    <div class="card-top ">
                        <div class="card-title mb-0">
                            <h5 class="font-weight-bold text-uppercase mb-0"><i class="fas fa-university"></i></h5>
                        </div>
                    </div>
                    <div class="card-body pt-0">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row align-items-end mb-2">
                                    <label for="invoice_no" class="col-md-4 pb-0 mb-0 font-weight-bold text-sm">Invoice
                                        No.</label>
                                    <div class="form-group col-md-6 mb-0">
                                        <input id="invoice_no" name="invoice_no" type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="row align-items-end mb-2">
                                    <label for="invoice_delivery_note"
                                        class="col-md-4 pb-0 mb-0 font-weight-bold text-sm">Delivery
                                        Note</label>
                                    <div class="form-group col-md-6 mb-0">
                                        <input id="invoice_delivery_note" name="invoice_delivery_note" type="text"
                                            class="form-control">
                                    </div>
                                </div>
                                <div class="row align-items-end mb-2">
                                    <label for="buyer_no" class="col-md-4 pb-0 mb-0 font-weight-bold text-sm">Buyer
                                        Order No</label>
                                    <div class="form-group col-md-6 mb-0">
                                        <input id="buyer_no" name="buyer_no" type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="row align-items-end mb-2">
                                    <label for="invoice_sup_ref_num"
                                        class="col-md-4 pb-0 mb-0 font-weight-bold text-sm">Supplier
                                        Ref Num</label>
                                    <div class="form-group col-md-6 mb-0">
                                        <input id="invoice_sup_ref_num" name="invoice_sup_ref_num" type="text"
                                            class="form-control">
                                    </div>
                                </div>
                                <div class="row align-items-end mb-2">
                                    <label for="invoice_pymt_term_mode"
                                        class="col-md-4 pb-0 mb-0 font-weight-bold text-sm">Mode /
                                        Term of Payment</label>
                                    <div class="form-group col-md-6 mb-0">
                                        <input id="invoice_pymt_term_mode" name="invoice_pymt_term_mode" type="text"
                                            class="form-control">
                                    </div>
                                </div>
                                <div class="row align-items-end mb-2">
                                    <label for="invoice_dispatch_doc_no"
                                        class="col-md-4 pb-0 mb-0 font-weight-bold text-sm">Dispatch
                                        Document No</label>
                                    <div class="form-group col-md-6 mb-0">
                                        <input id="invoice_dispatch_doc_no" name="invoice_dispatch_doc_no" type="text"
                                            class="form-control">
                                    </div>
                                </div>
                                <div class="row align-items-end mb-2">
                                    <label for="invoice_destination"
                                        class="col-md-4 pb-0 mb-0 font-weight-bold text-sm">Destination</label>
                                    <div class="form-group col-md-6 mb-0">
                                        <input id="invoice_destination" name="invoice_destination" type="text"
                                            class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row align-items-end mb-2">
                                    <label for="invoice_date" class="col-md-4 pb-0 mb-0 font-weight-bold text-sm">Invoice
                                        Date</label>
                                    <div class="form-group col-md-6 mb-0">
                                        <input id="invoice_date" name="invoice_date"
                                            class="flatpickr form-control flatpickr-input active" type="date">
                                    </div>
                                </div>
                                <div class="row align-items-end mb-2">
                                    <label for="invoice_del_note_dt"
                                        class="col-md-4 pb-0 mb-0 font-weight-bold text-sm">Delivery
                                        Note
                                        Date</label>
                                    <div class="form-group col-md-6 mb-0">
                                        <input id="invoice_del_note_dt" name="invoice_del_note_dt"
                                            class="flatpickr form-control flatpickr-input active" type="date">
                                    </div>
                                </div>
                                <div class="row align-items-end mb-2">
                                    <label for="buyer_date" class="col-md-4 pb-0 mb-0 font-weight-bold text-sm">Buyer Order
                                        Date</label>
                                    <div class="form-group col-md-6 mb-0">
                                        <input id="buyer_date" name="buyer_date"
                                            class="flatpickr form-control flatpickr-input active" type="date">
                                    </div>
                                </div>
                                <div class="row align-items-end mb-2">
                                    <label for="invoice_other_ref" class="col-md-4 pb-0 mb-0 font-weight-bold text-sm">Othr
                                        Reference</label>
                                    <div class="form-group col-md-6 mb-0">
                                        <input id="invoice_other_ref" name="invoice_other_ref" type="text"
                                            class="form-control">
                                    </div>
                                </div>
                                <div class="row align-items-end mb-2">
                                    <label for="invoice_del_terms" class="col-md-4 pb-0 mb-0 font-weight-bold text-sm">Terms
                                        of
                                        Delivery</label>
                                    <div class="form-group col-md-6 mb-0">
                                        <input id="invoice_del_terms" name="invoice_del_terms" type="text"
                                            class="form-control">
                                    </div>
                                </div>
                                <div class="row align-items-end mb-2">
                                    <label for="invoice_dispatch_thru"
                                        class="col-md-4 pb-0 mb-0 font-weight-bold text-sm">Dispatched
                                        Through</label>
                                    <div class="form-group col-md-6 mb-0">
                                        <input id="invoice_dispatch_thru" name="invoice_dispatch_thru" type="text"
                                            class="form-control">
                                    </div>
                                </div>
                                <div class="row align-items-end mb-2">
                                    <label for="invoice_serial" class="col-md-4 pb-0 mb-0 font-weight-bold text-sm">Machine
                                        Serial No.</label>

                                    <div class="col-md-6" id="serial">
                                        <div class="form-group mb-0">
                                            <label class="control-label"></label>
                                            <select class="form-control" id="invoice_serial" name="invoice_serial"></select>
                                        </div>
                                    </div>
                                    <div class="col-md-6" id="others">
                                        <div class="form-group mb-0">
                                            <label class="control-label"></label>
                                            <input type="text" class="form-control" id="invoice_serial_no"
                                                name="invoice_serial_no">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 mt-5">
                                <h6 class="fw-700 text-uppercase">
                                    Itemised statement:
                                </h6>
                                <div class="overflow-xv table-responsive mb-4">
                                    <table id="invoice_table" class="table modal-table invoice-table">
                                        <thead>
                                            <tr>
                                                <th scope="col">
                                                    <div class="dropdown">
                                                        <button class="btn btn-primary dropdown-toggle" type="button"
                                                            id="dropdownMenuButton" data-toggle="dropdown"
                                                            aria-haspopup="true" aria-expanded="false" data-toggle="tooltip"
                                                            data-placement="top" title="Add invoice item">
                                                            <i class="fas fa-plus"></i> Add
                                                        </button>
                                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                            <a class="dropdown-item" href="#" data-toggle="modal"
                                                                data-target="#addproducts">Products</a>
                                                            <a class="dropdown-item" href="#" data-toggle="modal"
                                                                data-target="#addspares">Spares</a>
                                                            <a class="dropdown-item" href="#" data-toggle="modal"
                                                                data-target="#addconsumables">Consumables</a>
                                                            <a class="dropdown-item" href="#" data-toggle="modal"
                                                                data-target="#addservices">Services</a>
                                                        </div>
                                                    </div>
                                                </th>
                                                <th scope="col">Item Id</th>
                                                <th scope="col">Item Name</th>
                                                <th scope="col">HSN/SAC</th>
                                                <th scope="col">GST</th>
                                                <th scope="col">Unit Price</th>
                                                <th scope="col">Quantity</th>
                                                <th scope="col">Discount</th>
                                                <th scope="col">Description</th>
                                                <th scope="col">Item Amount <br> <small>(excluding GST)</small></th>
                                            </tr>
                                        </thead>
                                    </table>

                                    {{-- add Products modal --}}
                                    <div class="modal fade custom-modal" id="addproducts" tabindex="-1" role="dialog"
                                        aria-labelledby="addproducts" aria-hidden="true">
                                        <div class="modal-dialog modal-xxl modal-dialog-centered edit" role="document">
                                            <div class="modal-content">
                                                <div class="icon">
                                                    <span class="fa-stack fa-2x text-success">
                                                        <i class="fas fa-circle fa-stack-2x"></i>
                                                        <i class="fas fa-plus fa-stack-1x fa-inverse"></i>
                                                    </span>
                                                </div>
                                                <div class="modal-header justify-content-center">
                                                    <h3 class="modal-title text-capitalize font-weight-bolder mt-3"
                                                        id="addproducts">
                                                        Select Product
                                                    </h3>
                                                </div>
                                                <div class="modal-body pb-0">
                                                    <div class="table-responsive">
                                                        <table id="product_table" class="table modal-table">
                                                            <thead>
                                                                <tr>
                                                                    <th scope="col">Check/ Uncheck</th>
                                                                    <th scope="col">Product ID</th>
                                                                    <th scope="col">Product Name</th>
                                                                    <th scope="col">Product Code Name</th>
                                                                    <th scope="col">Product Brand Name</th>
                                                                    <th scope="col">Price</th>
                                                                    <th scope="col">Gst Percentage</th>
                                                                </tr>
                                                            </thead>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="modal-footer flex-nowrap">
                                                    <button type="button"
                                                        class="btn btn-outline-success btn-block btn-lg mt-0"
                                                        data-dismiss="modal">Close</button>
                                                    <button type="button" class="btn btn-success btn-block btn-lg mt-0"
                                                        onclick="product_select()">Done</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- end Products modal --}}
                                    {{-- add Spares modal --}}
                                    <div class="modal fade custom-modal" id="addspares" tabindex="-1" role="dialog"
                                        aria-labelledby="addspares" aria-hidden="true">
                                        <div class="modal-dialog modal-xxl modal-dialog-centered edit" role="document">
                                            <div class="modal-content">
                                                <div class="icon">
                                                    <span class="fa-stack fa-2x text-success">
                                                        <i class="fas fa-circle fa-stack-2x"></i>
                                                        <i class="fas fa-plus fa-stack-1x fa-inverse"></i>
                                                    </span>
                                                </div>
                                                <div class="modal-header justify-content-center">
                                                    <h3 class="modal-title text-capitalize font-weight-bolder mt-3"
                                                        id="addspares">
                                                        Select Spares
                                                    </h3>
                                                </div>
                                                <div class="modal-body pb-0">
                                                    <div class="table-responsive">
                                                        <table id="spare_table" class="table modal-table">
                                                            <thead>
                                                                <tr>
                                                                    <th scope="col">Check/ Uncheck</th>
                                                                    <th scope="col">Spare ID</th>
                                                                    <th scope="col">Spare Code Name</th>
                                                                    <th scope="col">Spare Name</th>
                                                                    <th scope="col">Price</th>
                                                                    <th scope="col">Gst Percentage</th>
                                                                </tr>
                                                            </thead>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="modal-footer flex-nowrap">
                                                    <button type="button"
                                                        class="btn btn-outline-success btn-block btn-lg mt-0"
                                                        data-dismiss="modal">Close</button>
                                                    <button type="button" class="btn btn-success btn-block btn-lg mt-0"
                                                        onclick="spare_select()">Done</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- end Spares modal --}}
                                    {{-- add Consumable modal --}}
                                    <div class="modal fade custom-modal" id="addconsumables" tabindex="-1" role="dialog"
                                        aria-labelledby="addconsumables" aria-hidden="true">
                                        <div class="modal-dialog modal-xxl modal-dialog-centered edit" role="document">
                                            <div class="modal-content">
                                                <div class="icon">
                                                    <span class="fa-stack fa-2x text-success">
                                                        <i class="fas fa-circle fa-stack-2x"></i>
                                                        <i class="fas fa-plus fa-stack-1x fa-inverse"></i>
                                                    </span>
                                                </div>
                                                <div class="modal-header justify-content-center">
                                                    <h3 class="modal-title text-capitalize font-weight-bolder mt-3"
                                                        id="addconsumables">
                                                        Select Consumable Product
                                                    </h3>
                                                </div>
                                                <div class="modal-body pb-0">
                                                    <div class="table-responsive">
                                                        <table id="consumable_table" class="table modal-table">
                                                            <thead>
                                                                <tr>
                                                                    <th scope="col">Check/ Uncheck</th>
                                                                    <th scope="col">Consumable ID</th>
                                                                    <th scope="col">Consumable Code Name</th>
                                                                    <th scope="col">Consumable Name</th>
                                                                    <th scope="col">Price</th>
                                                                    <th scope="col">Gst Percentage</th>
                                                                </tr>
                                                            </thead>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="modal-footer flex-nowrap">
                                                    <button type="button"
                                                        class="btn btn-outline-success btn-block btn-lg mt-0"
                                                        data-dismiss="modal">Close</button>
                                                    <button type="button" class="btn btn-success btn-block btn-lg mt-0"
                                                        onclick="consumable_select()">Done</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- end Consumable modal --}}
                                    {{-- add Services modal --}}
                                    <div class="modal fade custom-modal" id="addservices" tabindex="-1" role="dialog"
                                        aria-labelledby="addservices" aria-hidden="true">
                                        <div class="modal-dialog modal-xxl modal-dialog-centered edit" role="document">
                                            <div class="modal-content">
                                                <div class="icon">
                                                    <span class="fa-stack fa-2x text-success">
                                                        <i class="fas fa-circle fa-stack-2x"></i>
                                                        <i class="fas fa-plus fa-stack-1x fa-inverse"></i>
                                                    </span>
                                                </div>
                                                <div class="modal-header justify-content-center">
                                                    <h3 class="modal-title text-capitalize font-weight-bolder mt-3"
                                                        id="addservices">
                                                        Select Services
                                                    </h3>
                                                </div>
                                                <div class="modal-body pb-0">
                                                    <div class="table-responsive">
                                                        <table id="service_table" class="table modal-table">
                                                            <thead>
                                                                <tr>
                                                                    <th scope="col">Check/ Uncheck</th>
                                                                    <th scope="col">Service ID</th>
                                                                    <th scope="col">Service HSN/SAC Number</th>
                                                                    <th scope="col">Service Name</th>
                                                                    <th scope="col">Type of Copies</th>
                                                                    <th scope="col">Price</th>
                                                                    <th scope="col">Gst Percentage</th>
                                                                </tr>
                                                            </thead>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="modal-footer flex-nowrap">
                                                    <button type="button"
                                                        class="btn btn-outline-success btn-block btn-lg mt-0"
                                                        data-dismiss="modal">Close</button>
                                                    <button type="button" class="btn btn-success btn-block btn-lg mt-0"
                                                        onclick="service_select()">Done</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- end Services modal --}}
                                </div>

                                <h6 class="fw-700 text-uppercase">
                                    Tax split up:
                                </h6>
                                <div class="table-responsive">
                                    <table id="tax_table" class="table modal-table invoice-table">
                                        <thead>
                                            <tr>
                                                <th scope="col">Taxable Amount</th>
                                                <th scope="col">CGST %</th>
                                                <th scope="col">CGST Amount</th>
                                                <th scope="col">SGST %</th>
                                                <th class="text-right" scope="col">SGST Amount</th>
                                                <th class="text-right" scope="col">Tax Amount</th>
                                                <th class="text-right" scope="col">Total Amount</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <p class="fw-700 h5">Total Bill Amount <br><small>(including GST)</small></p>
                                    </div>
                                    <div class="col-md-9">
                                        <p class="fw-700 h5"><i class="fas fa-rupee-sign mr-1"></i><span
                                                id="totalBillAmt">0.00</span>
                                            <br><small class="text-capitalize" id="totalBillAmtWords">Rupees Zero
                                                only</small>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12 py-4">
                <div class="card shadow">
                    <div class="card-top ">
                        <div class="card-title mb-0">
                            <h5 class="font-weight-bold text-uppercase mb-0"><i class="fas fa-map-marker-alt"></i></h5>
                        </div>
                    </div>
                    <div class="card-body pt-0">
                        <form class="row">
                            <div class="col-12">
                                <div class="custom-control custom-checkbox mb-2">
                                    <input type="checkbox" class="custom-control-input" id="address_click">
                                    <label class="custom-control-label font-weight-bold h6 mb-1 text-capitalize"
                                        for="address_click">Delivery Address Same as customers address </label>
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="control-label address_label" for="delivery_name">Name <small>*</small></label>
                                <input type="text" class="form-control" id="delivery_name" name="delivery_name">
                            </div>
                            <div class="form-group col-md-6">
                                <label class="control-label address_label" for="delivery_no">Contact No
                                    <small>*</small></label>
                                <input type="tel" class="form-control" id="delivery_no" name="delivery_no">
                            </div>
                            <div class="form-group col-md-6">
                                <label class="control-label address_label" for="delivery_address">Address
                                    <small>*</small></label>
                                <input type="text" class="form-control" id="delivery_address" name="delivery_address">
                            </div>
                            <div class="form-group col-md-6">
                                <label class="control-label address_label" for="delivery_city">City <small>*</small></label>
                                <input type="text" class="form-control" id="delivery_city" name="delivery_city">
                            </div>
                            <div class="form-group col-md-6">
                                <label class="control-label address_label" for="delivery_state">State
                                    <small>*</small></label>
                                <input type="text" class="form-control" id="delivery_state" name="delivery_state">
                            </div>
                            <div class="form-group col-md-6">
                                <label class="control-label address_label" for="delivery_postalcode">Postal Code
                                    <small>*</small></label>
                                <input type="text" class="form-control" id="delivery_postalcode" name="delivery_postalcode">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div>
            <a href="{{ url()->previous() }}" class="btn btn-outline-primary btn-lg btn-large mr-2">Cancel</a>
            <button type="submit" class="btn btn-primary btn-lg btn-large" onclick="validateChallan()">Submit</button>
        </div>
    </section>
@endsection
@section('script')

    <script src="{{ URL::asset('assets/js/amounttowords.js') }}"></script>
    <script>
        $(document).ready(function() {
            $("#others").hide();
            $("#serial").show();
            $('#invoice_serial').change(function() {
                var opval = $(this).val();
                if (opval == "others") {
                    $("#others").show();
                    $("#serial").hide();
                }
            });
            getBranch();
            $("#select_branch").change(function() {
                var branchid = this.value;
                //console.log("branchid", branchid);
                return $.ajax({
                    url: "/branchinfo/" + branchid,
                    type: "GET",
                    dataType: 'json',
                    success: function(data) {
                        //console.log("branchinfo", data);

                        $("#bank_detail").show();
                        $.each(data, function(key, value) {
                            $("#view_branch_id").text(value.org_branch_id);
                            $("#branch_name").text(value.org_branch_name);
                            $("#view_branch_address").text(value.org_address + ",");
                            $("#view_branch_city").text(value.org_city + " - " + value
                                .org_pincode + ",");
                            $("#view_branch_state").text(value.org_state + ".");
                            $("#view_branch_emailid").text(value.org_emailid);
                            $("#view_branch_phoneno").text(value.org_phone_no);
                            $("#view_account_name").text(value.org_holder_name);
                            $("#view_account_no").text(value.org_account_no);
                            $("#view_bank_name").text(value.org_bank_name);
                            $("#view_bank_branch").text(value.org_bank_branch);
                            $("#view_account_type").text(value.org_acc_type);
                            $("#view_bank_ifsccode").text(value.org_bank_ifsc);
                        });
                        loadCustomerName()
                    },
                    error: function(error) {
                        console.log('branchname error:', error);
                    }
                });
            });
            $("#customer_name").change(function() {
                var opval = $(this).val();
                //console.log("opval:", opval);
                var cusname = opval;

                //this.validateBranchAndCustomer();
                $.ajax({
                    url: "/cusinfo/" + cusname,
                    type: "GET",
                    dataType: 'json',
                    success: function(data) {
                        //console.log("cusinfo:", data);
                        $.each(data, function(key, value) {
                            $("#view_customer_name").text(value.customer_name);
                            $("#view_customer_address").text(value.cus_address);
                            $("#view_city_pin").text(value.cus_city + " - " + value
                                .cus_pincode);
                            $("#view_customer_city").text(value.cus_city);
                            $("#view_customer_pincode").text(value.cus_pincode);
                            $("#view_customer_state").text(value.cus_state);
                            $("#view_customer_emailid").text(value.cus_email_id);
                            $("#view_customer_phoneno").text(value.cus_phone_no);

                            $("#customer_id").text(value.cus_id);
                        });
                        loadcusproduct();
                    },

                    error: function(error) {
                        console.log(error);
                    }
                });
            });
            loadProducts();
            loadspares();
            loadconsumable();
            loadservices();
            loadTable();
            valueChanged();
        });

        function getBranch() {
            return $.ajax({
                url: "/branchname",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    //console.log("branchname", data);
                    $('#select_branch').empty();
                    $('#select_branch').append(
                        '<option selected="true" value="" disabled>Select a branch</option>');
                    $('#select_branch').prop('selectedIndex', 0);
                    $.each(data, function(key, value) {
                        $('#select_branch').append($('<option></option>').attr('value', value
                            .org_branch_id).text(value.org_branch_name));
                    });
                },
                error: function(error) {
                    console.log('branchname error:', error);
                }
            });
        }

        function loadCustomerName() {
            var branchname = $("#branch_name").text().trim();
            //console.log("branchname", branchname);
            return $.ajax({
                url: "/cuslist/" + branchname,
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    // console.log("cuslist", data);
                    $('#customer_name').empty();
                    $('#customer_name').append('<option selected="true" disabled></option>');
                    $('#customer_name').prop('selectedIndex', 0);

                    $.each(data, function(key, value) {
                        $('#customer_name').append($('<option></option>').attr('value', value
                            .customer_name).text(value.customer_name));
                    });
                },
                error: function(error) {
                    console.log('cuslist error:', error);
                }
            });
        }

        function loadcusproduct() {
            var cus_id = $("#customer_id").text();
            //console.log("cus_id:", cus_id);
            $.ajax({
                url: "/getcusprod/" + cus_id,
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    // console.log("getcusprod: ", data)
                    if (data == "") {
                        $('#invoice_serial').empty();
                        $('#invoice_serial').append('<option selected="true" disabled></option>');
                        $('#invoice_serial').prop('selectedIndex', 0);
                        $("#invoice_serial").append('<option value="others">Others</option>');
                        return;
                    }
                    $("#serial").show();
                    $("#others").hide();

                    $('#invoice_serial').empty();
                    $('#invoice_serial').append('<option selected="true" disabled></option>');
                    $('#invoice_serial').prop('selectedIndex', 0);

                    $.each(data, function(key, value) {
                        $('#invoice_serial').append($('<option></option>').attr('value', value
                            .Machine_Serial_No).text(value.Machine_Serial_No));
                    });

                    $('#invoice_serial').append('<option value="others">Others</option>');
                },
                error: function(err) {
                    console.log("Error in getting Serial No. :: ", err)
                }
            });
        }

        function loadProducts() {
            return $.ajax({
                url: "/loadproducts",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    //console.log("loadproducts", data);
                    $('#productTableBody').remove();
                    var product = '';
                    product += '<tbody id="productTableBody">';
                    if (data == "") {
                        product += '<tr>';
                        product += '<td>No Data</td>';
                        product += '</tr>';
                    } else {
                        $.each(data, function(key, value) {
                            product += '<tr>';
                            var check =
                                `<td class='td-actions'><div class="custom-control custom-checkbox"> <input type="checkbox" class="custom-control-input" id="${value.product_id}" name="products" value='${JSON.stringify(value)}'> <label class="custom-control-label" for="${value.product_id}"></label> </div></td>`

                            product += check;
                            product += '<td>' + value.product_const + value.product_id + '</td>';
                            product += '<td>' + value.product_name + '</td>';
                            product += '<td>' + value.product_code + '</td>';
                            product += '<td>' + value.product_brand_name + '</td>';
                            product += '<td>' + value.product_price + '</td>';
                            product += '<td>' + value.product_gst_percentage + '</td>';
                            //product += '<td hidden>' + value.product_id + '</td>';
                            product += '</tr>';
                        });
                    }
                    product += '</tbody>';
                    $('#product_table').append(product);
                },
                error: function(error) {
                    console.log('loadproducts load error:', error);
                }
            });
        }

        function loadspares() {
            return $.ajax({
                url: "/loadspares",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    //console.log("loadspares", data);
                    $('#spareTableBody').remove();
                    var spare = '';
                    spare += '<tbody id="spareTableBody">';
                    if (data == "") {
                        spare += '<tr>';
                        spare += '<td>No Data</td>';
                        spare += '</tr>';
                    } else {
                        $.each(data, function(key, value) {
                            spare += '<tr>';
                            var check =
                                `<td class='td-actions'><div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input" id="${value.spare_const + value.spare_id}" name="spares" value='${JSON.stringify(value)}'> <label class="custom-control-label" for="${value.spare_const + value.spare_id}"></label></div></td>`

                            spare += check;
                            spare += '<td>' + value.spare_const + value.spare_id + '</td>';
                            spare += '<td>' + value.spare_code_name + '</td>';
                            spare += '<td>' + value.spare_model_name + '</td>';
                            spare += '<td>' + value.spare_unit_price + '</td>';
                            spare += '<td>' + value.Gst_Percentage + '</td>';
                            spare += '</tr>';
                        });
                    }
                    spare += '</tbody>';
                    $('#spare_table').append(spare);
                },
                error: function(error) {
                    console.log('loadspares error:', error);
                }
            });
        }

        function loadconsumable() {
            return $.ajax({
                url: "/loadconsumable",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    // console.log("loadconsumable", data);
                    $('#consumableTableBody').remove();
                    var consumable = '';
                    consumable += '<tbody id="consumableTableBody">';
                    if (data == "") {
                        consumable += '<tr>';
                        consumable += '<td>No Data</td>';
                        consumable += '</tr>';
                    } else {
                        $.each(data, function(key, value) {
                            consumable += '<tr>';
                            var check =
                                `<td class='td-actions'><div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input" id="${value.consumable_const + value.consumable_id}" name="consumable" value='${JSON.stringify(value)}'> <label class="custom-control-label" for="${value.consumable_const + value.consumable_id}"></label></div></td>`

                            consumable += check;
                            consumable += '<td>' + value.consumable_const + value.consumable_id +
                                '</td>';
                            consumable += '<td>' + value.consumable_code_name + '</td>';
                            consumable += '<td>' + value.consumable_name + '</td>';
                            consumable += '<td>' + value.consumable_unit_price + '</td>';
                            consumable += '<td>' + value.consumable_gst_percentage + '</td>';
                            consumable += '</tr>';
                        });
                    }
                    consumable += '</tbody>';
                    $('#consumable_table').append(consumable);
                },
                error: function(error) {
                    console.log('loadconsumable error:', error);
                }
            });
        }

        function loadservices() {
            return $.ajax({
                url: "/loadservices",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    //console.log("loadservices", data);
                    $('#serviceTableBody').remove();
                    var service = '';
                    service += '<tbody id="serviceTableBody">';
                    if (data == "") {
                        service += '<tr>';
                        service += '<td>No Data</td>';
                        service += '</tr>';
                    } else {
                        $.each(data, function(key, value) {
                            service += '<tr>';
                            var check =
                                `<td class='td-actions'><div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input" id="${value.service_const + value.service_id}" name="services" value='${JSON.stringify(value)}'> <label class="custom-control-label" for="${value.service_const + value.service_id}"></label></div></td>`

                            service += check;
                            service += '<td>' + value.service_const + value.service_id + '</td>';

                            service += '<td>' + value.service_hsn + '</td>';

                            service += '<td>' + value.service_item_name + '</td>';

                            service += '<td>' + value.service_type + '</td>';

                            service += '<td>' + value.service_unit_price + '</td>';

                            service += '<td>' + value.Gst_Percentage + '</td>';

                            service += '</tr>';
                        });
                    }
                    service += '</tbody>';
                    $('#service_table').append(service);
                },
                error: function(error) {
                    console.log('loadservices error:', error);
                }
            });
        }

        function product_select() {
            var lineItemArray = [];
            $.each($("input[name='products']:checked"), function(index) {
                var lineItem = JSON.parse($(this).val());
                lineItemArray.push({
                    id: lineItem.product_const + lineItem.product_id,
                    name: lineItem.product_name,
                    code: lineItem.product_code,
                    price: lineItem.product_price,
                    gst: lineItem.product_gst_percentage,
                    quantity: '1',
                    discount: '0',
                    descriptions: ' ',
                    type: lineItem.product_const,
                    IDs: lineItem.product_id,
                    unit: lineItem.product_price
                });
            });
            $('input[name=products]').prop('checked', false);
            $('#addproducts').modal('hide');
            //console.log('lineItemArray:', lineItemArray);
            add_to_table(lineItemArray);
        }

        function spare_select() {
            var lineItemArray = [];
            $.each($("input[name='spares']:checked"), function(index) {
                var lineItem = JSON.parse($(this).val());
                lineItemArray.push({
                    id: lineItem.spare_const + lineItem.spare_id,
                    name: lineItem.spare_model_name,
                    code: lineItem.spare_code_name,
                    price: lineItem.spare_unit_price,
                    gst: lineItem.Gst_Percentage,
                    quantity: '1',
                    discount: '0',
                    descriptions: ' ',
                    type: lineItem.spare_const,
                    IDs: lineItem.spare_id,
                    unit: lineItem.spare_unit_price
                });
            });
            $('input[name=spares]').prop('checked', false);
            $('#addspares').modal('hide');
            //console.log('lineItemArray:', lineItemArray);
            add_to_table(lineItemArray);
        }

        function consumable_select() {
            var lineItemArray = [];
            $.each($("input[name='consumable']:checked"), function(index) {
                var lineItem = JSON.parse($(this).val());
                lineItemArray.push({
                    id: lineItem.consumable_const + lineItem.consumable_id,
                    name: lineItem.consumable_name,
                    code: lineItem.consumable_hsn,
                    price: lineItem.consumable_unit_price,
                    gst: lineItem.consumable_gst_percentage,
                    quantity: '1',
                    discount: '0',
                    descriptions: ' ',
                    type: lineItem.consumable_const,
                    IDs: lineItem.consumable_id,
                    unit: lineItem.consumable_unit_price
                });
            });
            $('input[name=consumable]').prop('checked', false);
            $('#addconsumables').modal('hide');
            //console.log('lineItemArray:', lineItemArray);
            add_to_table(lineItemArray);
        }

        function service_select() {
            var lineItemArray = [];
            $.each($("input[name='services']:checked"), function(index) {
                var lineItem = JSON.parse($(this).val());
                lineItemArray.push({
                    id: lineItem.service_const + lineItem.service_id,
                    name: lineItem.service_item_name,
                    code: lineItem.service_hsn,
                    price: lineItem.service_unit_price,
                    gst: lineItem.Gst_Percentage,
                    quantity: '1',
                    discount: '0',
                    descriptions: ' ',
                    type: lineItem.service_type,
                    IDs: lineItem.service_id,
                    unit: lineItem.service_unit_price
                });
            });
            $('input[name=services]').prop('checked', false);
            $('#addservices').modal('hide');
            //console.log('lineItemArray:', lineItemArray);
            add_to_table(lineItemArray);
        }


        function loadTable() {
            var invoice = '';
            invoice += '<tbody id="invoiceTableBody">';
            invoice += '<tfoot><tr><td colspan="9" class="text-right">Total Item Amount</td>';
            invoice +=
                ' <td class="text-right"><i class="fas fa-rupee-sign mr-1"></i><span id="itemTotal">0.00</span></td>';
            invoice += '</tr></tfoot>';
            invoice += '</tbody>';
            $('#invoice_table').append(invoice);
            var tax = '';
            tax += '<tbody id="taxTableBody">';
            tax += '<tfoot><tr><td colspan="5" class="text-right">Total Tax Amount</td>';
            tax += '<td class="text-right"><i class="fas fa-rupee-sign mr-1"></i><span id="taxTotal">0.00</span></td>';
            tax += '<td class="text-right"><i class="fas fa-rupee-sign mr-1"></i><span id="total_amount">0.00</span></td>';
            tax += '</tr></tfoot>';
            tax += '</tbody>';
            $('#tax_table').append(tax);
        }

        function clear_table() {
            $('#invoiceTableBody').html("");
        }

        function add_to_table(lineItemArray) {
            var rowCount = $('#invoiceTableBody >tr').length;
            var i = rowCount + 1;
            var invoice = "";
            console.log("lineItemArray", lineItemArray);
            $.each(lineItemArray, function(key, value) {
                invoice += '<tr>';
                invoice +=
                    `<td class='td-actions text-center'><button type="button" class="btn btn-outline-danger" data-toggle="tooltip" data-placement="top" title="Delete invoice item" onclick='delete_item("${i}")'><i class="fas fa-trash-alt"></i></button></td>`;

                invoice += "<td id='id" + i + "'>" + value.id + "</td>";
                invoice += "<td id='name" + i + "'>" + value.name + "</td>";
                invoice += "<td id='hsn" + i + "'>" + value.code + "</td>";
                invoice += "<td> <span id='gst" + i + "'>" + value.gst + "</span>% </td>";
                invoice += "<td class='td-actions form-group'><input type='text' id='price" + i +
                    "' onChange='calculateTotals()' class='form-control' value= ' " + value.price + " ' ></td>";

                var quantity = "<td class='td-actions form-group'><input type='text' id='quantity" + i +
                    "' onChange='calculateTotals()' class='form-control' placeholder='No.of Quantity' value='" +
                    value.quantity + "' > </td>";

                var discount = "<td class='td-actions form-group'><input type='text' id='discount" + i +
                    "' onChange='calculateTotals()' class='form-control' placeholder='discount amount' value='" +
                    value.discount + "' > </td>";

                var descriptions = "<td class='td-actions form-group'><input type='text' id='descriptions" + i +
                    "' class='form-control' placeholder='Description' value='" + value.descriptions + "' > </td>";

                invoice += "<td id='type" + i + "' hidden>" + value.type + "</td>";

                invoice += "<td id='IDs" + i + "' hidden>" + value.IDs + "</td>";

                invoice += "<td id='unit" + i + "' hidden>" + value.unit + "</td>";

                invoice += quantity;
                invoice += discount;
                invoice += descriptions;
                invoice += '<td><span id="tot' + i + '">0.00</span></td>';
                invoice += '</tr>';
                i++;
            });

            $('#invoice_table').append(invoice);
            calculateTotals();
        }

        function delete_item(itemId) {
            var rowCount = $('#invoiceTableBody >tr').length;
            var lineItemArray = [];
            var i;

            for (i = 0; i < rowCount; i++) {
                var index = i + 1;

                if (index != itemId) {
                    lineItemArray.push({
                        id: $("#id" + index).text(),
                        name: $("#name" + index).text(),
                        code: $("#hsn" + index).text(),
                        price: $("#price" + index).val(),
                        gst: $("#gst" + index).text(),
                        descriptions: $("#descriptions" + index).text(),
                        quantity: $("#quantity" + index).val(),
                        discount: $("#discount" + index).val(),
                        type: $("#type" + index).val(),
                        IDs: $("#IDs" + index).val(),
                        unit: $("#unit" + index).val(),
                    });
                }
            }

            this.clear_table();
            this.add_to_table(lineItemArray);
        }

        function calculateTotals() {
            var rowCount = $('#invoiceTableBody >tr').length;
            var subTotItem = 0;
            var subTotTax = 0;
            var unitTot = 0;
            var total = 0;
            var billTot = 0;
            var taxRow = "";
            $('#taxTableBody').html("");
            var i;

            for (i = 0; i < rowCount; i++) {
                var index = i + 1;
                var p = isNaN(Number($("#price" + index).val())) ? 0 : Number($("#price" + index).val());
                var q = isNaN(Number($("#quantity" + index).val())) ? 0 : Number($("#quantity" + index).val());
                var d = isNaN(Number($("#discount" + index).val())) ? 0 : Number($("#discount" + index).val());
                var g = isNaN(Number($("#gst" + index).text())) ? 0 : Number($("#gst" + index).text());
                var units = isNaN(Number($("#unit" + index).text())) ? 0 : Number($("#unit" + index).text());
                var tot = (p - d) * q;
                var totInclTax = ((p - d) + ((p - d) * g / 100)) * q;
                var hsn = $("#hsn" + index).text();
                var cgst = g / 2;
                var sgst = g / 2;
                var cgstAmt = (tot * cgst) / 100;
                var sgstAmt = (tot * sgst) / 100;
                var taxTot = (cgstAmt + sgstAmt);
                var total_amount = (cgstAmt + sgstAmt + p);
                total += total_amount;
                //var totaltax += total_amount;
                //console.log('total_amount:', total_amount);
               // console.log('total:', total);


                if (!isNaN(tot)) {
                    subTotItem += tot;
                    $("#tot" + index).text("" + tot.toFixed(2));
                } else {
                    $("#tot" + index).text("0.00");
                }


                if (!isNaN(tot)) {
                    subTotItem += tot;
                    $("#tot" + index).text("" + tot.toFixed(2));
                } else {
                    $("#tot" + index).text("0.00");
                }

                if (!isNaN(totInclTax)) {
                    billTot += totInclTax;
                }

                if (!isNaN(taxTot)) {
                    subTotTax += taxTot;
                }

                if (!isNaN(units)) {
                    unitTot += units;
                }


                // Prepare tax table item.
                taxRow += '<tr>';
                taxRow += '<td id="tax_item_tot' + index + '">' + tot.toFixed(2) + '</td>';
                taxRow += '<td id="tax_cgst' + index + '">' + cgst + '</td>';
                taxRow += '<td id="tax_cgst_amt' + index + '">' + cgstAmt.toFixed(2) + '</td>';
                taxRow += '<td id="tax_sgst' + index + '">' + sgst + '</td>';
                taxRow += '<td id="tax_sgst_amt' + index + '" class="text-right">' + sgstAmt.toFixed(2) + '</td>';
                taxRow += '<td id="tax_tot' + index + '" class="text-right">' + taxTot.toFixed(2) + '</td>';
                taxRow += '<td id="tot_amount' + index + '" class="text-right">' + total_amount.toFixed(2) + '</td>';
                taxRow += '</tr>';
            }

            $('#taxTableBody').html(taxRow);
            $("#itemTotal").text("" + subTotItem.toFixed(2));
            $("#taxTotal").text("" + subTotTax.toFixed(2));
            $("#total_amount").text("" + total.toFixed(2));
            $("#totalBillAmt").text("" + billTot.toFixed(2));
            $("#totalBillAmtWords").text(RsPaise(billTot.toFixed(2)));
            //$("#unitTotal").text("" + unitTot.toFixed(2));
        }

        function validateChallan() {
            var rowCount = $('#invoiceTableBody >tr').length;
            var eleIDs = "";
            for (var i = 0; i < rowCount; i++) {
                var index = i + 1;
                eleIDs += "#id" + index + ", " + "#name" + index + ", " + "#hsn" + index + ", " + "#price" + index +
                    ", " +
                    "#gst" + index + ", " + "#IDs" + index + ", " + "#type" + index + ", " + "#descriptions" + index +
                    ", " + "#quantity" + index + ", " + "#discount" + index + ", " + "#tot" + index + ", " +
                    "#tax_hsn" + index + ", " + "#tax_item_tot" + index + ", " + "#tax_cgst" + index + ", " +
                    "#tax_cgst_amt" + index + ", " + "#tax_sgst" + index + ", " + "#tax_sgst_amt" + index + ", " +
                    "#tax_tot" + index + ", " + "#tot_amount" + index + ", " + "#unit" + index + ", ";
            }


            let $items = $(eleIDs + `#branch_name,#view_branch_id, #customer_name, #customer_number, #customer_id, #invoice_date, #invoice_no, #invoice_delivery_note, #invoice_del_note_dt, #invoice_sup_ref_num, #invoice_other_ref, #invoice_pymt_term_mode, #invoice_del_terms, #invoice_dispatch_doc_no, #invoice_dispatch_thru, #invoice_destination, #delivery_address,#delivery_no,#delivery_name, #delivery_city, #delivery_state, #delivery_postalcode, #totalBillAmt, #totalBillAmtWords, #invoice_serial,#invoice_serial_no, #buyer_no,#buyer_date`)

            let keyValue = this.createReqObject($items);
            let request = {
                "branch_id": keyValue["view_branch_id"],
                "branch_name": keyValue["branch_name"],
                "cust_name": keyValue["customer_name"],
                "cus_id": keyValue["customer_id"],
                "invoice_amount": keyValue["totalBillAmt"],
                "invoice_amount_words": keyValue["totalBillAmtWords"]
            };

            nest(request, ["invoice", "buyer_no"], keyValue["buyer_no"]);
            nest(request, ["invoice", "buyer_date"], keyValue["buyer_date"]);
            nest(request, ["invoice", "invoice_serial_no"], keyValue["invoice_serial_no"]);
            nest(request, ["invoice", "invoice_serial"], keyValue["invoice_serial"]);
            nest(request, ["invoice", "invoice_date"], keyValue["invoice_date"]);
            nest(request, ["invoice", "invoice_no"], keyValue["invoice_no"]);
            nest(request, ["invoice", "delivery_note"], keyValue["invoice_delivery_note"]);
            nest(request, ["invoice", "del_note_dt"], keyValue["invoice_del_note_dt"]);
            nest(request, ["invoice", "sup_ref_num"], keyValue["invoice_sup_ref_num"]);
            nest(request, ["invoice", "other_ref"], keyValue["invoice_other_ref"]);
            nest(request, ["invoice", "pymt_term_mode"], keyValue["invoice_pymt_term_mode"]);
            nest(request, ["invoice", "del_terms"], keyValue["invoice_del_terms"]);
            nest(request, ["invoice", "dispatch_doc_no"], keyValue["invoice_dispatch_doc_no"]);
            nest(request, ["invoice", "dispatch_thru"], keyValue["invoice_dispatch_thru"]);
            nest(request, ["invoice", "destination"], keyValue["invoice_destination"]);

            nest(request, ["invoice", "delivery_name"], keyValue["delivery_name"]);
            nest(request, ["invoice", "delivery_no"], keyValue["delivery_no"]);
            nest(request, ["invoice", "delivery_address"], keyValue["delivery_address"]);
            nest(request, ["invoice", "delivery_city"], keyValue["delivery_city"]);
            nest(request, ["invoice", "delivery_state"], keyValue["delivery_state"]);
            nest(request, ["invoice", "delivery_postalcode"], keyValue["delivery_postalcode"]);

            let lineItems = [];

            for (var i = 0; i < rowCount; i++) {
                let index = i + 1;
                let lineItem = {};

                nest(lineItem, ["id"], keyValue["id" + index]);
                nest(lineItem, ["IDs"], keyValue["IDs" + index]);
                nest(lineItem, ["unit"], keyValue["unit" + index]);
                nest(lineItem, ["name"], keyValue["name" + index]);
                nest(lineItem, ["hsn"], keyValue["hsn" + index]);
                nest(lineItem, ["price"], keyValue["price" + index]);
                nest(lineItem, ["gst"], keyValue["gst" + index]);
                nest(lineItem, ["quantity"], keyValue["quantity" + index]);
                nest(lineItem, ["descriptions"], keyValue["descriptions" + index]);
                nest(lineItem, ["type"], keyValue["type" + index]);
                nest(lineItem, ["discount"], keyValue["discount" + index]);
                nest(lineItem, ["item_total"], keyValue["tot" + index]);
                nest(lineItem, ["tax", "hsn"], keyValue["hsn" + index]);
                nest(lineItem, ["tax", "cgst"], keyValue["tax_cgst" + index]);
                nest(lineItem, ["tax", "cgst_amt"], keyValue["tax_cgst_amt" + index]);
                nest(lineItem, ["tax", "sgst"], keyValue["tax_sgst" + index]);
                nest(lineItem, ["tax", "sgst_amt"], keyValue["tax_sgst_amt" + index]);
                nest(lineItem, ["tax", "item_total"], keyValue["tax_item_tot" + index]);
                nest(lineItem, ["tax", "tax_total"], keyValue["tax_tot" + index]);
                nest(lineItem, ["tax", "tot_amount"], keyValue["tot_amount" + index]);

                lineItems.push(lineItem);
            }

            nest(request, ["lineItems"], lineItems);

            //let jsonStr = JSON.stringify(request);
            let jsonStr = request;

            console.log("jsonStr", jsonStr);

            $.ajax({
                url: "/addinvoice",
                type: "POST",
                data: jsonStr,
                dataType: 'json',
                success: function(data) {
                    console.log("data:", data);
                    if (data == "success") {
                        console.log("data: ", data);
                        return window.location.href = "/viewinvoice";
                    }
                    if (data == "error") {
                        console.log("prod added error");
                    }
                },
                error: function(error) {
                    console.log('addcusprod error:', error);
                }
            });
        }

        var nest = function(obj, keys, v) {
            if (keys.length === 1) {
                obj[keys[0]] = v;
            } else {
                var key = keys.shift();
                obj[key] = nest(typeof obj[key] === 'undefined' ? {} : obj[key], keys, v);
            }

            return obj;
        };

        function createReqObject($inItems) {
            var obj = {}
            $inItems.each(function() {
                if ($(this)[0].tagName == "INPUT" || $(this)[0].tagName == "SELECT") obj[this.id] = $(this).val();

                else obj[this.id] = $(this).text();
            });

            return obj;
        }

        function valueChanged() {
            $("#address_click").click(function() {

                if ($(this).is(":checked")) {
                    $("#delivery_name").val($("#view_customer_name").text());
                    $("#delivery_no").val($("#view_customer_phoneno").text());
                    $("#delivery_address").val($("#view_customer_address").text());
                    $("#delivery_city").val($("#view_customer_city").text());
                    $("#delivery_state").val($("#view_customer_state").text());
                    $("#delivery_postalcode").val($("#view_customer_pincode").text());
                    $('.address_label').addClass('active');
                } else {
                    $("#delivery_name").val('');
                    $("#delivery_no").val('');
                    $("#delivery_address").val('');
                    $("#delivery_city").val('');
                    $("#delivery_state").val('');
                    $("#delivery_postalcode").val('');
                    $('.address_label').removeClass('active');
                }

            });
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

    </script>
@endsection
