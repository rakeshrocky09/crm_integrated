@extends('layouts.app')
@section('mytitle', 'Add Product')
@section('content')
    <div class="bg-top bg-gray p-4">
        <div class="d-flex">
            <h5 class="font-weight-bold text-uppercase mb-0">ADD Product</h5>
        </div>
    </div>
    <section class="my-4 p-4 card-main">
        <form action="addproducts" method="POST">
            @csrf
            <div class="row">
                <div class="col-md-6 py-4">
                    <div class="card shadow h-100">
                        <div class="card-top ">
                            <div class="card-title mb-0">
                                <h5 class="font-weight-bold text-uppercase mb-0"><i class="far fa-building"></i></h5>
                            </div>
                        </div>
                        <div class="card-body pt-0">
                            <div class="row">
                                <div class="col-12">
                                    <h6 class="fw-700 text-uppercase pl-1 mb-4 mt-3">
                                        Product Details</h6>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label" for="product_belongs_to">Belongs To (branch name)</label>
                                    <select id="product_belongs_to" name="product_belongs_to"
                                        class="form-control @error('product_belongs_to') is-invalid @enderror"></select>
                                    @error('product_belongs_to')
                                        <span class="text-danger text-12">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label" for="product_code_name">HSN/SAC Number
                                        <small>*</small></label>
                                    <select id="product_code_name" name="product_code_name"
                                        class="form-control @error('product_code_name') is-invalid @enderror"></select>
                                    @error('product_code_name')
                                        <span class="text-danger text-12">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label" for="product_brand_name">Brand Name
                                        <small>*</small></label>
                                    <select id="product_brand_name" name="product_brand_name"
                                        class="form-control @error('product_brand_name') is-invalid @enderror"></select>
                                    @error('product_brand_name')
                                        <span class="text-danger text-12">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label" for="product_name">Product Name<small>*</small></label>
                                    <input id="product_name" name="product_name" type="text"
                                        class="form-control @error('product_name') is-invalid @enderror">
                                    @error('product_name')
                                        <span class="text-danger text-12">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label" for="product_model_name">Model Name<small>*</small></label>
                                    <input id="product_model_name" name="product_model_name" type="text"
                                        class="form-control @error('product_model_name') is-invalid @enderror">
                                    @error('product_model_name')
                                        <span class="text-danger text-12">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label" for="product_category">Product Category</label>
                                    <select id="product_category" name="product_category"
                                        class="form-control @error('product_category') is-invalid @enderror"
                                        name="product_color"></select>
                                    @error('product_category')
                                        <span class="text-danger text-12">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label active" for="product_description">Description
                                        <small>*</small></label>
                                    <textarea class="form-control mt-1 @error('product_description') is-invalid @enderror"
                                        name="product_description" id="product_description" name="product_description"
                                        rows="2"></textarea>
                                    @error('product_description')
                                        <span class="text-danger text-12">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6 mt-3 pt-1">
                                    <label class="control-label" for="product_gst_percentage">GST(In %)
                                        <small>*</small></label>
                                    <input id="product_gst_percentage" name="product_gst_percentage" type="text"
                                        class="form-control @error('product_gst_percentage') is-invalid @enderror"
                                        name="product_color">
                                    @error('product_gst_percentage')
                                        <span class="text-danger text-12">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 py-4">
                    <div class="card shadow h-100">
                        <div class="card-top ">
                            <div class="card-title mb-0">
                                <h5 class="font-weight-bold text-uppercase mb-0"><i class="far fa-building"></i></h5>
                            </div>
                        </div>
                        <div class="card-body pt-0">
                            <h6 class="fw-700 text-uppercase pl-1 mb-4 mt-3">Product Parameters</h6>
                            <div class="form-group">
                                <label class="control-label" for="product_color">Color
                                    <small>*</small></label>
                                <select id="product_color" class="form-control @error('product_color') is-invalid @enderror"
                                    name="product_color">
                                    <option disabled selected></option>
                                    <option value="color"> Color </option>
                                    <option value="black"> BlackWhite</option>
                                </select>
                                @error('product_color')
                                    <span class="text-danger text-12">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="product_stock_unit">Units </label>
                                <input id="product_stock_unit" type="number" name="product_stock_unit"
                                    class="form-control @error('product_stock_unit') is-invalid @enderror">
                                @error('product_stock_unit')
                                    <span class="text-danger text-12">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="prod_part_number">Part Number</label>
                                <input id="prod_part_number" type="text" name="prod_part_number"
                                    class="form-control @error('prod_part_number') is-invalid @enderror">
                                @error('prod_part_number')
                                    <span class="text-danger text-12">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group mt-4 pt-2">
                                <label class="control-label" for="price">Product Price <small>*</small></label>
                                <input id="price" type="text" name="product_price"
                                    class="form-control @error('product_price') is-invalid @enderror">
                                @error('product_price')
                                    <span class="text-danger text-12">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <a href="/viewproduct" type="submit" class="btn btn-outline-primary btn-lg btn-large mr-2">Cancel</a>
                <button type="submit" class="btn btn-primary btn-lg btn-large">Submit</button>
            </div>
        </form>
    </section>
    {{-- add_hsn/sac number --}}
    <div class="modal fade custom-modal" id="add_hsn" tabindex="-1" role="dialog" aria-labelledby="add_hsn"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content success">
                <div class="icon">
                    <span class="fa-stack fa-2x text-success">
                        <i class="fas fa-circle fa-stack-2x"></i>
                        <i class="fas fas fa-plus fa-stack-1x fa-inverse"></i>
                    </span>
                </div>
                <div class="modal-header justify-content-center">
                    <h3 class="modal-title text-capitalize font-weight-bolder mt-3">
                        Add HSN Code
                    </h3>
                </div>
                <form action="addprdhsn" method="POST">
                    @csrf
                    <div class="modal-body pb-0">
                        <div class="row justify-content-center">
                            <div class="col-md-11">
                                <div class="form-group">
                                    <label for="add_hsn" class="col-form-label">Enter New HSN Code</label>
                                    <input type="text" class="form-control @error('add_hsn') is-invalid @enderror"
                                        id="add_hsn" name="add_hsn">
                                    @error('add_hsn')
                                        <span class="text-danger text-12">{{ $message }}</span>
                                    @enderror
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer flex-nowrap">
                        <button type="button" class="btn btn-outline-success btn-block btn-lg mt-0"
                            data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success btn-block btn-lg mt-0">Done</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- add_product_brand --}}
    <div class="modal fade custom-modal" id="add_product_brand" tabindex="-1" role="dialog"
        aria-labelledby="add_product_brand" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content success">
                <div class="icon">
                    <span class="fa-stack fa-2x text-success">
                        <i class="fas fa-circle fa-stack-2x"></i>
                        <i class="fas fas fa-plus fa-stack-1x fa-inverse"></i>
                    </span>
                </div>
                <div class="modal-header justify-content-center">
                    <h3 class="modal-title text-capitalize font-weight-bolder mt-3">
                        Add Product Brand
                    </h3>
                </div>
                <form action="addbrand" method="POST">
                    @csrf
                    <div class="modal-body pb-0">
                        <div class="row justify-content-center">
                            <div class="col-md-11">
                                <div class="form-group">
                                    <label for="add_brand_name" class="col-form-label">Enter Brand Name</label>
                                    <input type="text" class="form-control @error('add_brand_name') is-invalid @enderror"
                                        id="add_brand_name" name="add_brand_name">
                                    @error('add_brand_name')
                                        <span class="text-danger text-12">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer flex-nowrap">
                        <button type="button" class="btn btn-outline-success btn-block btn-lg mt-0"
                            data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success btn-block btn-lg mt-0">Done</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- add_product_category --}}
    <div class="modal fade custom-modal" id="add_product_category" tabindex="-1" role="dialog"
        aria-labelledby="add_product_category" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content success">
                <div class="icon">
                    <span class="fa-stack fa-2x text-success">
                        <i class="fas fa-circle fa-stack-2x"></i>
                        <i class="fas fas fa-plus fa-stack-1x fa-inverse"></i>
                    </span>
                </div>
                <div class="modal-header justify-content-center">
                    <h3 class="modal-title text-capitalize font-weight-bolder mt-3">
                        Add Product Category
                    </h3>
                </div>
                <form action="addprdcategory" method="POST">
                    @csrf
                    <div class="modal-body pb-0">
                        <div class="row justify-content-center">
                            <div class="col-md-11">
                                <div class="form-group">
                                    <label for="add_category" class="col-form-label">Enter New Product Category</label>
                                    <input type="text" class="form-control @error('add_category') is-invalid @enderror"
                                        id="add_category" name="add_category">
                                    @error('add_category')
                                        <span class="text-danger text-12">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer flex-nowrap">
                        <button type="button" class="btn btn-outline-success btn-block btn-lg mt-0"
                            data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success btn-block btn-lg mt-0">Done</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- add_units --}}
  {{--   <div class="modal fade custom-modal" id="add_units" tabindex="-1" role="dialog" aria-labelledby="add_units"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content success">
                <div class="icon">
                    <span class="fa-stack fa-2x text-success">
                        <i class="fas fa-circle fa-stack-2x"></i>
                        <i class="fas fas fa-plus fa-stack-1x fa-inverse"></i>
                    </span>
                </div>
                <div class="modal-header justify-content-center">
                    <h3 class="modal-title text-capitalize font-weight-bolder mt-3">
                        Add Unit
                    </h3>
                </div>
                <form action="addunit" method="POST">
                    @csrf
                    <div class="modal-body pb-0">
                        <div class="row justify-content-center">
                            <div class="col-md-11">
                                <div class="form-group">
                                    <label for="add_units" class="col-form-label">Enter New Unit</label>
                                    <input type="text" class="form-control @error('add_units') is-invalid @enderror"
                                        id="add_units" name="add_units">
                                    @error('add_units')
                                        <span class="text-danger text-12">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer flex-nowrap">
                        <button type="button" class="btn btn-outline-success btn-block btn-lg mt-0"
                            data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success btn-block btn-lg mt-0">Done</button>
                    </div>
                </form>
            </div>
        </div>
    </div> --}}
    @include('components.alertmodal')
@endsection
@section('script')
    @if (session()->has('message'))
        <script>
            $(document).ready(function() {
                $('#successAlert').modal("show");
            });

        </script>
    @endif
    @if (session()->has('error'))
        <script>
            $(document).ready(function() {
                $('#errorAlert').modal("show");
            });

        </script>
    @endif
    <script>
        $(document).ready(function() {
            getBranch();
            loadBrandname();
            getHsn();
            //loadUnit();
            loadProductCategory();
            $('#product_code_name').change(function() { //jQuery Change Function
                var opval = $(this).val(); //Get value from select element
                if (opval == "others") { //Compare it and if true
                    $('#add_hsn').modal("show"); //Open Modal
                }
            });
            $('#product_brand_name').change(function() { //jQuery Change Function
                var opval = $(this).val(); //Get value from select element
                if (opval == "others") { //Compare it and if true
                    $('#add_product_brand').modal("show"); //Open Modal
                }
            });
            $('#product_category').change(function() { //jQuery Change Function
                var opval = $(this).val(); //Get value from select element
                if (opval == "others") { //Compare it and if true
                    $('#add_product_category').modal("show"); //Open Modal
                }
            });
           /*  $('#product_stock_unit').change(function() { //jQuery Change Function
                var opval = $(this).val(); //Get value from select element
                if (opval == "others") { //Compare it and if true
                    $('#add_units').modal("show"); //Open Modal
                }
            }); */
            $('#product_belongs_to').change(function() { //jQuery Change Function
                var opval = $(this).val(); //Get value from select element
                if (opval == "others") { //Compare it and if true
                    return window.location.href = "/addorganization";
                }
            });
        });

        function getBranch() {
            return $.ajax({
                url: "/branchname",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    //console.log("branchname", data);
                    $('#product_belongs_to').empty();
                    $('#product_belongs_to').append('<option selected="true" disabled></option>');
                    $('#product_belongs_to').prop('selectedIndex', 0);
                    $.each(data, function(key, value) {
                        $('#product_belongs_to').append($('<option></option>').attr('value', value
                            .org_branch_name).text(value.org_branch_name));
                    });
                    $('#product_belongs_to').append('<option value="others">Others</option>');
                },
                error: function(error) {
                    console.log('branchname error:', error);
                }
            });
        }

        function loadBrandname() {
            return $.ajax({
                url: "/brand",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    //console.log("branchname", data);
                    $('#product_brand_name').empty();
                    $('#product_brand_name').append('<option selected="true" disabled></option>');
                    $('#product_brand_name').prop('selectedIndex', 0);
                    $.each(data, function(key, value) {
                        $('#product_brand_name').append($('<option></option>').attr('value', value
                            .name).text(value.name));
                    });
                    $('#product_brand_name').append('<option value="others">Others</option>');
                },
                error: function(error) {
                    console.log('branchname error:', error);
                }
            });
        }

        function getHsn() {
            return $.ajax({
                url: "/prdhsn",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    //console.log("prdhsn", data);
                    $('#product_code_name').empty();
                    $('#product_code_name').append('<option selected="true" disabled></option>');
                    $('#product_code_name').prop('selectedIndex', 0);
                    $.each(data, function(key, value) {
                        $('#product_code_name').append($('<option></option>').attr(
                            'value',
                            value.Product_HSN_No).text(value.Product_HSN_No));
                    });
                    $('#product_code_name').append('<option value="others">Others</option>');
                },
                error: function(error) {
                    console.log('branchname error:', error);
                }
            });
        }

        /* function loadUnit() {
            return $.ajax({
                url: "/unit",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    //console.log("prdhsn", data);
                    $('#product_stock_unit').empty();
                    $('#product_stock_unit').append('<option selected="true" disabled></option>');
                    $('#product_stock_unit').prop('selectedIndex', 0);
                    $.each(data, function(key, value) {
                        $('#product_stock_unit').append($('<option></option>').attr(
                            'value',
                            value.unit).text(value.unit));
                    });
                    $('#product_stock_unit').append('<option value="others">Others</option>');
                },
                error: function(error) {
                    console.log('branchname error:', error);
                }
            });
        } */

        function loadProductCategory() {
            return $.ajax({
                url: "/prdcategory",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    //console.log("prdhsn", data);
                    $('#product_category').empty();
                    $('#product_category').append('<option selected="true" disabled></option>');
                    $('#product_category').prop('selectedIndex', 0);
                    $.each(data, function(key, value) {
                        $('#product_category').append($('<option></option>').attr(
                            'value',
                            value.name).text(value.name));
                    });
                    $('#product_category').append('<option value="others">Others</option>');
                },
                error: function(error) {
                    console.log('branchname error:', error);
                }
            });
        }

    </script>
@endsection
