@if (session()->has('message'))
    <div id="successAlert" class="modal fade">
        <div class="modal-dialog modal-confirm modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="icon-box">
                        <i class="material-icons">&#xE876;</i>
                    </div>
                    <h4 class="modal-title w-100">Added!</h4>
                </div>
                <div class="modal-body">
                    <p class="text-center" id="alert_msg">
                        <span class="pr-1">{{ session()->get('message') }}</span>
                        Successfully Added
                    </p>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-success btn-block" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>
@endif
@if (session()->has('update'))
    <div id="updateAlert" class="modal fade">
        <div class="modal-dialog modal-confirm modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="icon-box">
                        <i class="material-icons">&#xE876;</i>
                    </div>
                    <h4 class="modal-title w-100">Updated!</h4>
                </div>
                <div class="modal-body">
                    <p class="text-center" id="alert_msg">
                        <span class="pr-1">{{ session()->get('update') }}</span>
                        Successfully Updated
                    </p>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-success btn-block" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>
@endif
@if (session()->has('deleted'))
    <div id="deleteAlert" class="modal fade">
        <div class="modal-dialog modal-confirm danger modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="icon-box">
                        <i class="material-icons">&#xE876;</i>
                    </div>
                    <h4 class="modal-title w-100">Deleted!</h4>
                </div>
                <div class="modal-body">
                    <p class="text-center" id="alert_msg">
                        <span class="pr-1">{{ session()->get('deleted') }}</span>
                        Successfully Deleted
                    </p>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary btn-block" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>
@endif
@if (session()->has('error'))
    <div id="errorAlert" class="modal fade">
        <div class="modal-dialog modal-confirm danger modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="icon-box">
                        <i class="fas fa-times"></i>
                    </div>
                    <h4 class="modal-title w-100">Failed!</h4>
                </div>
                <div class="modal-body">
                    <p class="text-center" id="alert_msg">
                        <span class="pr-1">{{ session()->get('error') }}</span>
                    </p>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary btn-block" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>
@endif
