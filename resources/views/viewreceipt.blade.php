@extends('layouts.app')
@section('mytitle', 'View Receipt')
@section('head-links')
    <link href="{{ URL::asset('assets/css/datatables.min.css') }}" rel="stylesheet">
@endsection
@section('content')
    <div class="bg-top bg-gray p-4">
        <div class="d-flex">
            <h5 class="font-weight-bold text-uppercase mb-0">View Receipt</h5>
        </div>
    </div>

    <div class="p-4 card-main">
        <section class="my-5">
            <div class="card shadow">
                <div class="card-top ">
                    <div class="card-title mb-0">
                        <h5 class="font-weight-bold text-uppercase mb-0"><i class="fas fa-file-invoice"></i></h5>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table" id="datatable">
                            <thead class="text-uppercase">
                                <tr>
                                    <th scope="col">Receipt ID</th>
                                    <th scope="col">CUSTOMER NAME</th>
                                    <th scope="col">INVOICE NO</th>
                                    <th scope="col">INVOICE DATE</th>
                                    <th scope="col">ACTUAL AMOUNT</th>
                                    <th scope="col">AMOUNT PAID</th>
                                    <th scope="col">PENDING AMOUNT</th>
                                    <th scope="col">STATUS</th>
                                    <th scope="col" class="nosort">ACTION</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (count($select) >= 1)
                                    @foreach ($select as $receipt)
                                        <tr>
                                            <td>{{ $receipt->Receipt_Id }}</td>
                                            <td>{{ $receipt->customer_name }}</td>
                                            <td>{{ $receipt->invoice_number }}</td>
                                            <td>{{ $receipt->Invoice_Date }}</td>
                                            <td>{{ $receipt->total_amount }}</td>
                                            <td>{{ $receipt->Total_Paid_Amount }}</td>
                                            <td>{{ $receipt->Pending_Amount }}</td>
                                            <td>
                                                @if ($receipt->Status == 'open')
                                                    <span
                                                        class="badge badge-pill badge-success text-12">{{ $receipt->Status }}</span>
                                                @else
                                                    <span
                                                        class="badge badge-pill badge-secondary text-12">{{ $receipt->Status }}</span>
                                                @endif
                                            </td>
                                            <td class="d-flex flex-nowrap">
                                                <a href="/viewreceipts/{{ $receipt->Invoice_Id }}"
                                                    class="btn btn-outline-info mr-1" data-toggle="tooltip"
                                                    data-placement="top" title="View Receipt">
                                                    <i class="fas fa-external-link-alt"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="9">Sorry, No records found</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('script')
    <script src="{{ URL::asset('assets/js/datatables.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#datatable').DataTable({
                language: {
                    searchPlaceholder: "Search records",
                    search: "",
                    "lengthMenu": "_MENU_ Entries"
                },
                "lengthMenu": [
                    [8, 16, 32, -1],
                    [8, 16, 32, "All"]
                ],
                "pageLength": 8,
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': ['nosort']
                }]
            });
        });

    </script>
@endsection
