@extends('layouts.app')
@section('mytitle', 'Add Enquiry')
@section('content')
    <div class="bg-top bg-gray p-4">
        <div class="d-flex">
            <h5 class="font-weight-bold text-uppercase mb-0">ADD ENQUIRY</h5>
        </div>
    </div>
    <section class="my-4 p-4 card-main">
        <form action="addenq" method="post">
            @csrf
            <div class="row">
                <div class="col-md-6 py-4">
                    <div class="card shadow h-100">
                        <div class="card-top ">
                            <div class="card-title mb-0">
                                <h5 class="font-weight-bold text-uppercase mb-0"><i class="fas fa-user-alt"></i></h5>
                            </div>
                        </div>
                        <div class="card-body pt-0">
                            <div class="form-group @error('customer_name') is-invalid @enderror">
                                <label class="control-label" for="customer_name">Name <small>*</small></label>
                                <input id="customer_name" name="customer_name" type="text" class="form-control">
                                @error('customer_name')
                                    <span class="text-danger text-12">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group @error('customer_phoneno') is-invalid @enderror">
                                <label class="control-label" for="customer_phoneno">Phone No <small>*</small></label>
                                <input id="customer_phoneno" name="customer_phoneno" type="tel" class="form-control">
                                @error('customer_phoneno')
                                    <span class="text-danger text-12">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="customer_email">Email ID </label>
                                <input id="customer_email" name="customer_email" type="text" class="form-control">
                            </div>
                            <div class="form-group @error('customer_address') is-invalid @enderror">
                                <label class="control-label" for="customer_address">Address <small>*</small></label>
                                <input type="text" class="form-control" id="customer_address" name="customer_address">
                                @error('customer_address')
                                    <span class="text-danger text-12">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group @error('branch_name') is-invalid @enderror">
                                <label class="control-label" for="branch_name">Branch Name
                                    <small>*</small></label>
                                <select id="branch_name" name="branch_name" class="form-control"></select>
                                @error('branch_name')
                                    <span class="text-danger text-12">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group mb-2 @error('date') is-invalid @enderror">
                                <label for="date">Date</label>
                                <input id="date" name="date" type="date" placeholder="yyyy-mm-dd"
                                    class="flatpickr flatpickr-input form-control">
                                @error('date')
                                    <span class="text-danger text-12">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 py-4">
                    <div class="card shadow">
                        <div class="card-top ">
                            <div class="card-title mb-0">
                                <h5 class="font-weight-bold text-uppercase mb-0"><i class="far fa-building"></i></h5>
                            </div>
                        </div>
                        <div class="card-body pt-0">
                            <div class="form-group @error('brand_name') is-invalid @enderror">
                                <label class="control-label" for="brand_name">Brand Name
                                    <small>*</small></label>
                                <select id="brand_name" name="brand_name" class="form-control"> </select>
                                @error('brand_name')
                                    <span class="text-danger text-12">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group @error('enquiry_category') is-invalid @enderror">
                                <label class="control-label" for="enquiry_category">Category </label>
                                <select class="form-control" id="enquiry_category" name="enquiry_category"></select>
                                @error('enquiry_category')
                                    <span class="text-danger text-12">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group @error('details') is-invalid @enderror">
                                <label class="control-label" for="details">Details<small>*</small></label>
                                <textarea class="form-control mt-1" id="details" name="details" rows="1"></textarea>
                                @error('details')
                                    <span class="text-danger text-12">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label class="control-label active" for="reschedule_date">Reschedule Date</label>
                                <input id="reschedule_date" name="reschedule_date" type="date" placeholder="yyyy-mm-dd"
                                    class="flatpickr flatpickr-input form-control">
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="enquiry_through">Enquiry through
                                    <small>*</small></label>
                                <select id="enquiry_through" name="enquiry_through" class="form-control"></select>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="remarks">Remarks <small>*</small></label>
                                <input id="remarks" name="remarks" type="text" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <a href="/viewenquiry" type="submit" class="btn btn-outline-primary btn-lg btn-large mr-2">Cancel</a>
                <button type="submit" class="btn btn-primary btn-lg btn-large">Submit</button>
            </div>
        </form>
    </section>
    {{-- add_brand --}}
    <div class="modal fade custom-modal" id="add_brand" tabindex="-1" role="dialog" aria-labelledby="add_brand"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content success">
                <div class="icon">
                    <span class="fa-stack fa-2x text-success">
                        <i class="fas fa-circle fa-stack-2x"></i>
                        <i class="fas fas fa-plus fa-stack-1x fa-inverse"></i>
                    </span>
                </div>
                <div class="modal-header justify-content-center">
                    <h3 class="modal-title text-capitalize font-weight-bolder mt-3">
                        Add Brand
                    </h3>
                </div>
                <form action="addbrand" method="POST">
                    @csrf
                    <div class="modal-body pb-0">
                        <div class="row justify-content-center">
                            <div class="col-md-11">
                                <div class="form-group">
                                    <label for="add_brand_name" class="col-form-label">Enter Brand Name</label>
                                    <input type="text" class="form-control @error('add_brand_name') is-invalid @enderror"
                                        id="add_brand_name" name="add_brand_name">
                                    @error('add_brand_name')
                                        <span class="text-danger text-12">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer flex-nowrap">
                        <button type="button" class="btn btn-outline-success btn-block btn-lg mt-0"
                            data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success btn-block btn-lg mt-0">Done</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- add_enquiry_category --}}
    <div class="modal fade custom-modal" id="add_enquiry_category" tabindex="-1" role="dialog"
        aria-labelledby="add_enquiry_category" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content success">
                <div class="icon">
                    <span class="fa-stack fa-2x text-success">
                        <i class="fas fa-circle fa-stack-2x"></i>
                        <i class="fas fas fa-plus fa-stack-1x fa-inverse"></i>
                    </span>
                </div>
                <div class="modal-header justify-content-center">
                    <h3 class="modal-title text-capitalize font-weight-bolder mt-3">
                        Add Category
                    </h3>
                </div>
                <form action="addenqcategory" method="POST">
                    @csrf
                    <div class="modal-body pb-0">
                        <div class="row justify-content-center">
                            <div class="col-md-11">
                                <div class="form-group">
                                    <label for="add_category" class="col-form-label">Enter New Category</label>
                                    <input type="text" class="form-control @error('add_category') is-invalid @enderror"
                                        id="add_category" name="add_category">
                                    @error('add_category')
                                        <span class="text-danger text-12">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer flex-nowrap">
                        <button type="button" class="btn btn-outline-success btn-block btn-lg mt-0"
                            data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success btn-block btn-lg mt-0">Done</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- add_enquiry_through --}}
    <div class="modal fade custom-modal" id="add_enquiry_through" tabindex="-1" role="dialog"
        aria-labelledby="add_enquiry_through" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content success">
                <div class="icon">
                    <span class="fa-stack fa-2x text-success">
                        <i class="fas fa-circle fa-stack-2x"></i>
                        <i class="fas fas fa-plus fa-stack-1x fa-inverse"></i>
                    </span>
                </div>
                <div class="modal-header justify-content-center">
                    <h3 class="modal-title text-capitalize font-weight-bolder mt-3">
                        Add Enquiry through
                    </h3>
                </div>
                <form action="addenqthrough" method="POST">
                    @csrf
                    <div class="modal-body pb-0">
                        <div class="row justify-content-center">
                            <div class="col-md-11">
                                <div class="form-group">
                                    <label for="add_enqthrough" class="col-form-label">Enter New Enquiry through</label>
                                    <input type="text" class="form-control @error('add_enqthrough') is-invalid @enderror"
                                        id="add_enqthrough" name="add_enqthrough">
                                    @error('add_enqthrough')
                                        <span class="text-danger text-12">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer flex-nowrap">
                        <button type="button" class="btn btn-outline-success btn-block btn-lg mt-0"
                            data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success btn-block btn-lg mt-0">Done</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @include('components.alertmodal')
@endsection
@section('script')
    @if (session()->has('message'))
        <script>
            $(document).ready(function() {
                $('#successAlert').modal("show");
            });

        </script>
    @endif
    @if (session()->has('error'))
        <script>
            $(document).ready(function() {
                $('#errorAlert').modal("show");
            });

        </script>
    @endif
    <script>
        $(document).ready(function() {
            getBranch();
            loadBrandname();
            loadEnquiryCategory();
            loadEnquiryThrough();
            $('#branch_name').change(function() { //jQuery Change Function
                var opval = $(this).val(); //Get value from select element
                if (opval == "others") { //Compare it and if true
                    return window.location.href = "/addorganization";
                }
            });
            $('#brand_name').change(function() { //jQuery Change Function
                var opval = $(this).val(); //Get value from select element
                if (opval == "others") { //Compare it and if true
                    $('#add_brand').modal("show"); //Open Modal
                }
            });
            $('#enquiry_category').change(function() { //jQuery Change Function
                var opval = $(this).val(); //Get value from select element
                if (opval == "others") { //Compare it and if true
                    $('#add_enquiry_category').modal("show"); //Open Modal
                }
            });
            $('#enquiry_through').change(function() { //jQuery Change Function
                var opval = $(this).val(); //Get value from select element
                if (opval == "others") { //Compare it and if true
                    $('#add_enquiry_through').modal("show"); //Open Modal
                }
            });
        });

        function getBranch() {
            return $.ajax({
                url: "/branchname",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    //console.log("branchname", data);
                    $('#branch_name').empty();
                    $('#branch_name').append('<option selected="true" disabled></option>');
                    $('#branch_name').prop('selectedIndex', 0);
                    $.each(data, function(key, value) {
                        $('#branch_name').append($('<option></option>').attr('value', value
                            .org_branch_name).text(value.org_branch_name));
                    });
                    $('#branch_name').append('<option value="others">Others</option>');
                },
                error: function(error) {
                    console.log('branchname error:', error);
                }
            });
        }

        function loadBrandname() {
            return $.ajax({
                url: "/brand",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    //console.log("branchname", data);
                    $('#brand_name').empty();
                    $('#brand_name').append('<option selected="true" disabled></option>');
                    $('#brand_name').prop('selectedIndex', 0);
                    $.each(data, function(key, value) {
                        $('#brand_name').append($('<option></option>').attr('value', value
                            .name).text(value.name));
                    });
                    $('#brand_name').append('<option value="others">Others</option>');
                },
                error: function(error) {
                    console.log('branchname error:', error);
                }
            });
        }

        function loadEnquiryCategory() {
            return $.ajax({
                url: "/enqcategory",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    console.log("enqcategory", data);
                    $('#enquiry_category').empty();
                    $('#enquiry_category').append('<option selected="true" disabled></option>');
                    $('#enquiry_category').prop('selectedIndex', 0);
                    $.each(data, function(key, value) {
                        $('#enquiry_category').append($('<option></option>').attr(
                            'value',
                            value.name).text(value.name));
                    });
                    $('#enquiry_category').append('<option value="others">Others</option>');
                },
                error: function(error) {
                    console.log('branchname error:', error);
                }
            });
        }

        function loadEnquiryThrough() {
            return $.ajax({
                url: "/enqthrough",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    console.log("enqthrough", data);
                    $('#enquiry_through').empty();
                    $('#enquiry_through').append('<option selected="true" disabled></option>');
                    $('#enquiry_through').prop('selectedIndex', 0);
                    $.each(data, function(key, value) {
                        $('#enquiry_through').append($('<option></option>').attr(
                            'value',
                            value.name).text(value.name));
                    });
                    $('#enquiry_through').append('<option value="others">Others</option>');
                },
                error: function(error) {
                    console.log('branchname error:', error);
                }
            });
        }

    </script>
@endsection
