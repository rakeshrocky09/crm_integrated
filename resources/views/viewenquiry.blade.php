@extends('layouts.app')
@section('mytitle', 'View Enquiry')
@section('head-links')
    <link href="{{ URL::asset('assets/css/datatables.min.css') }}" rel="stylesheet">
@endsection
@section('content')
    <div class="bg-top bg-gray p-4">
        <div class="d-flex">
            <h5 class="font-weight-bold text-uppercase mb-0">View Enquiry</h5>
        </div>
    </div>

    <div class="p-4 card-main">
        <section class="my-5">
            <div class="card shadow">
                <div class="card-top ">
                    <div class="card-title mb-0">
                        <h5 class="font-weight-bold text-uppercase mb-0"><i class="far fa-question-circle"></i></h5>
                    </div>
                    <div class="btn-right position-relative float-right">
                        <a href="#" class="btn btn-primary text-uppecase">Print</a>
                        <a href="/addenquiry" class="btn btn-primary text-uppecase" data-toggle="tooltip"
                            data-placement="top" title="Add Enquiry">
                            <i class="fas fa-plus"></i> Add
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table" id="datatable">
                            <thead class="text-uppercase">
                                <tr>
                                    <th scope="col">ENQUIRY ID</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Phone no</th>
                                    <th scope="col">RESCHEDULE DATE</th>
                                    <th scope="col">CATEGORY</th>
                                    <th scope="col">STATUS</th>
                                    <th scope="col">BRANCH NAME</th>
                                    <th scope="col" class="nosort">ACTION</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (count($select) >= 1)
                                    @foreach ($select as $enquiry)
                                        <tr>
                                            <td>{{ $enquiry->enquiry_const }}{{ $enquiry->enquiry_id }}</td>
                                            <td>{{ $enquiry->enquiry_cus_name }}</td>
                                            <td>{{ $enquiry->enquiry_cus_phone_no }}</td>
                                            <td>{{ $enquiry->enquiry_reschedule_date }}</td>
                                            <td>{{ $enquiry->enquiry_category }}</td>
                                            <td>
                                                @if ($enquiry->enquiry_status == 'open')
                                                    <span class="badge badge-pill badge-success text-12">
                                                        {{ $enquiry->enquiry_status }}
                                                    </span>
                                                @else
                                                    <span class="badge badge-pill badge-secondary text-12">
                                                        {{ $enquiry->enquiry_status }}
                                                    </span>
                                                @endif
                                            </td>
                                            <td>{{ $enquiry->enquiry_branch_name }}</td>
                                            <td>
                                                <a href="{{ url('/viewenquirys', [$enquiry->enquiry_id]) }}"
                                                    class="btn btn-outline-info mr-1">
                                                    <i class="fas fa-user"></i>
                                                </a>
                                                {{-- <a href="viewenquirys/{{ $enquiry->enquiry_id }}" class="btn btn-outline-info mr-1">
                                                    <i class="fas fa-user"></i>
                                                </a> --}}
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="8">Sorry, No records found</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </div>
    @include('components.alertmodal')
@endsection
@section('script')
    @if (session()->has('message'))
        <script>
            $(document).ready(function() {
                $('#successAlert').modal("show");
            });

        </script>
    @endif
    @if (session()->has('update'))
        <script>
            $(document).ready(function() {
                $('#updateAlert').modal("show");
            });

        </script>
    @endif
    @if (session()->has('error'))
        <script>
            $(document).ready(function() {
                $('#errorAlert').modal("show");
            });

        </script>
    @endif
    <script src="{{ URL::asset('assets/js/datatables.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#datatable').DataTable({
                language: {
                    searchPlaceholder: "Search records",
                    search: "",
                    "lengthMenu": "_MENU_ Entries"
                },
                "lengthMenu": [
                    [8, 16, 32, -1],
                    [8, 16, 32, "All"]
                ],
                "pageLength": 8,
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': ['nosort']
                }]
            });
        });

    </script>
@endsection
