<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Invoice</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <style>
        /* html,
        body {
            width: 100%;
            height: 100%;
            margin: 15px;
            padding: 0;
        } */

        .text-sm {
            font-size: 10.5px;
        }

        .text-12 {
            font-size: 12px;
        }

        .text-14 {
            font-size: 14px;
        }

        .va-t {
            vertical-align: top;
        }

        .va-m {
            vertical-align: middle;
        }

        table.table th,
        td {
            padding: 4px !important;
        }

        .w-59 {
            width: 59%;
        }

        .w-50 {
            width: 50%;
        }

        .w-40 {
            width: 40%;
        }

        .w-38 {
            width: 38%;
        }

        .w-30 {
            width: 30%;
        }

        .w-29 {
            width: 29%;
        }

        .w-21 {
            width: 21%;
        }

        .w-20 {
            width: 20%;
        }

        label {
            margin-bottom: 0;
        }

        @page {
            margin: 25px;
        }
        footer {
            position: fixed;
            bottom: 0;
            left: 0;
            right: 0;
            height: 4.3cm;
        }

    </style>
</head>

<body>
    {{-- <section>
        <div class="mb-2">
            <img class="d-inline-block va-m" src="{{ public_path('assets/img/km_logo.png') }}">
            <h6 class="d-inline-block va-m">{{ $org[0]->org_branch_name }}</h6>
        </div>
        <div class="text-12">
            <div class="d-inline-block va-t w-40">
                <address class="mb-0">
                    {{ $org[0]->org_address }}<br>
                    {{ $org[0]->org_city }}<br>
                    {{ $org[0]->org_state }}<span> - </span>{{ $org[0]->org_pincode }}
                </address>
                <p class="mb-0"><span>phone:</span> {{ $org[0]->org_phone_no }}</p>
                <p class="mb-0"><span class="font-weight-bold">GST No: </span>{{ $org[0]->org_gstno }}</p>
            </div>
            <div class="d-inline-block va-t w-29">
                <label class="font-weight-bold d-inline-block">Invoice No:</label>
                <label class="d-inline-block">{{ $org[0]->invoice_number }}</label>
                <br>
                <label class="font-weight-bold d-inline-block">Invoice Date:</label>
                <label class="d-inline-block">{{ $org[0]->Invoice_Date }}</label>
                <br>
                <label class="font-weight-bold d-inline-block">Delivery Note:</label>
                <label class="d-inline-block">{{ $org[0]->Delivery_Note }}</label>
                <br>
                <label class="font-weight-bold d-inline-block">Delivery Note Date:</label>
                <label class="d-inline-block">{{ $org[0]->Delivery_Note_Date }}</label>
            </div>
            <div class="d-inline-block va-t w-29">
                <label class="font-weight-bold d-inline-block">Mode of Payment:</label>
                <label class="d-inline-block">{{ $org[0]->Mode_Terms_Payment }}</label>
                <br>
                <label class="font-weight-bold d-inline-block">Supplier Ref Num:</label>
                <label class="d-inline-block">{{ $org[0]->Supplier_Reference }}</label>
                <br>
                <label class="font-weight-bold d-inline-block">Other Reference:</label>
                <label class="d-inline-block">{{ $org[0]->Other_Reference }}</label>
                <br>
                <label class="font-weight-bold d-inline-block">Buyer Order No:</label>
                <label class="d-inline-block">{{ $org[0]->Buyer_Order_No }}</label>
                <br>
                <label class="font-weight-bold d-inline-block">Buyer Order Date:</label>
                <label class="d-inline-block">{{ $org[0]->Buyer_Order_Date }}</label>
            </div>
        </div>
        <div class="text-12 mt-3">
            <div class="d-inline-block va-t w-40">
                <h6 class="font-weight-bold text-14">Billed To</h6>
                <address class="mb-0">
                    <span>{{ $cus[0]->customer_name }}</span><br>
                    {{ $cus[0]->cus_address }}<br>{{ $cus[0]->cus_city }}<br>{{ $cus[0]->cus_state }}<span> -
                    </span>{{ $cus[0]->cus_pincode }}
                </address>
                <p class="mb-0"><span>phone: </span>{{ $cus[0]->cus_phone_no }}</p>
                <p class="mb-0"><span class="font-weight-bold">GST No: </span>{{ $cus[0]->cus_gst_percentage }}</p>
            </div>
            <div class="d-inline-block va-t w-40">
                <h6 class="font-weight-bold text-14">Consignee</h6>
                <address class="mb-0">
                    <span>{{ $org[0]->Customer_Phone_No }}</span><br>
                    {{ $org[0]->delivery_address }}<br>{{ $org[0]->delivery_city }}<br>{{ $org[0]->delivery_state }}<span>
                        -
                    </span>{{ $org[0]->delivery_pincode }}
                </address>
                <p class="mb-0"><span>phone: </span>9500168299</p>
            </div>
        </div>
    </section>
    <section>
        <h6 class="fw-700 mt-4">Order Summary</h6>
        <table class="table table-bordered text-center small">
            <thead>
                <tr>
                    <th scope="col">Item Id</th>
                    <th scope="col">Item Name</th>
                    <th scope="col">HSN/SAC</th>
                    <th scope="col">GST</th>
                    <th scope="col">Unit Price</th>
                    <th scope="col">Quantity</th>
                    <th scope="col">Discount</th>
                    <th scope="col">Description</th>
                    <th scope="col">Item Amount</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($invoice_details as $item)
                    <tr>
                        <td>{{ $item->Item_Id }}</td>
                        <td>{{ $item->name }}</td>
                        <td>{{ $item->hsn }}</td>
                        <td>{{ $item->gst }}</td>
                        <td>{{ $item->price }}</td>
                        <td>{{ $item->quantity }}</td>
                        <td>{{ $item->discount }}</td>
                        <td>{{ $item->descriptions }}</td>
                        <td>{{ $item->item_total }}</td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan="8">Total</td>
                    <td>{{ $total[0]->items_total }}</td>
                </tr>
            </tbody>
        </table>
        <h6 class="fw-700 mt-4">Tax details</h6>
        <table class="table table-bordered text-center small">
            <thead>
                <tr>
                    <th scope="col">Taxable Amount</th>
                    <th scope="col">CGST %</th>
                    <th scope="col">CGST Amount</th>
                    <th scope="col">SGST %</th>
                    <th scope="col">SGST Amount</th>
                    <th scope="col">Tax Amount</th>
                    <th scope="col">Total Amount</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($invoice_details as $item)
                    <tr>
                        <td>{{ $item->item_total }}</td>
                        <td>{{ $item->cgst }}</td>
                        <td>{{ $item->cgst_amount }}</td>
                        <td>{{ $item->sgst }}</td>
                        <td>{{ $item->sgst_amount }}</td>
                        <td>{{ $item->tax_amount }}</td>
                        <td>{{ $item->item_total + $item->tax_amount }}</td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan="5">Total</td>
                    <td>{{ $total[0]->total_tax }}</td>
                    <td>6920.34</td>
                </tr>
            </tbody>
        </table>
    </section> --}}
    {{-- <section>
        <div class="text-12">
            <div class="d-inline-block va-t w-20">
                <h6 class="font-weight-bold text-14">Bank Details</h6>
                <p class="mb-0 text-sm">
                    <span>{{ $org[0]->org_holder_name }}</span>
                    <br>{{ $org[0]->org_bank_name }}<br>{{ $org[0]->org_account_no }}<br>{{ $org[0]->org_bank_branch }}<br>{{ $org[0]->org_bank_ifsc }}<br>
                </p>
            </div>
            <div class="d-inline-block va-t w-20">
                <label class="font-weight-bold d-block">Total Bill Amount</label>
                <label class="d-inline-block d-block">(including GST)</label>
            </div>
            <div class="d-inline-block va-t w-59">
                <label class="font-weight-bold d-block">{{ $org[0]->total_amount }}</label>
                <label class="d-block">{{ $org[0]->Invoice_amount_words }}</label>
            </div>
<div class="d-inline-block va-t w-40">
                <h6 class="font-weight-bold text-sm">Declaration</h6>
                <p class="mb-0" style="font-size: 8px;">We declare that this invoice shows the actual price of the
                    goods described
                    <br>and that all the particulars are true and correct.
                </p>
            </div><div class="d-inline-block va-t float-right 29">
                <h6 class="text-uppercase mb-4 pb-2 text-14 font-weight-normal">{{ $org[0]->company_name }}</h6>
                <p class="font-weight-bold">Authorised signatory</p>
            </div>
        </div>
    </section> --}}

    <header>
        <div>
            <img class="d-inline-block va-m" src="{{ public_path('assets/img/km_logo.png') }}" width="30">
            <h6 class="d-inline-block va-m">{{ $org[0]->org_branch_name }}</h6>
        </div>
        <div class="text-12 mt-2">
            <div class="d-inline-block va-t w-40">
                <address class="mb-0">
                    {{ $org[0]->org_address }}<br>
                    {{ $org[0]->org_city }}<span>, </span>{{ $org[0]->org_state }}<span> -
                    </span>{{ $org[0]->org_pincode }}
                </address>
                <p class="mb-0"><span>phone:</span> {{ $org[0]->org_phone_no }}</p>
                <p class="mb-0"><span class="font-weight-bold">GST No: </span>{{ $org[0]->org_gstno }}</p>
            </div>
            <div class="d-inline-block va-t w-29">
                <label class="font-weight-bold d-inline-block">Invoice No:</label>
                <label class="d-inline-block">{{ $org[0]->invoice_number }}</label>
                <br>
                <label class="font-weight-bold d-inline-block">Invoice Date:</label>
                <label class="d-inline-block">{{ $org[0]->Invoice_Date }}</label>
                <br>
                <label class="font-weight-bold d-inline-block">Delivery Note:</label>
                <label class="d-inline-block">{{ $org[0]->Delivery_Note }}</label>
                <br>
                <label class="font-weight-bold d-inline-block">Delivery Note Date:</label>
                <label class="d-inline-block">{{ $org[0]->Delivery_Note_Date }}</label>
            </div>
            <div class="d-inline-block va-t w-29">
                <label class="font-weight-bold d-inline-block">Mode of Payment:</label>
                <label class="d-inline-block">{{ $org[0]->Mode_Terms_Payment }}</label>
                <br>
                <label class="font-weight-bold d-inline-block">Supplier Ref Num:</label>
                <label class="d-inline-block">{{ $org[0]->Supplier_Reference }}</label>
                <br>
                <label class="font-weight-bold d-inline-block">Other Reference:</label>
                <label class="d-inline-block">{{ $org[0]->Other_Reference }}</label>
                <br>
                <label class="font-weight-bold d-inline-block">Buyer Order No:</label>
                <label class="d-inline-block">{{ $org[0]->Buyer_Order_No }}</label>
                <br>
                <label class="font-weight-bold d-inline-block">Buyer Order Date:</label>
                <label class="d-inline-block">{{ $org[0]->Buyer_Order_Date }}</label>
            </div>
        </div>
        <div class="text-12 mt-2">
            <div class="d-inline-block va-t w-40">
                <h6 class="font-weight-bold text-14 mb-1">Billed To</h6>
                <address class="mb-0">
                    <span>{{ $cus[0]->customer_name }}</span><br>
                    {{ $cus[0]->cus_address }}<br>{{ $cus[0]->cus_city }}<span>, </span>{{ $cus[0]->cus_state }}<span> -
                    </span>{{ $cus[0]->cus_pincode }}
                </address>
                <p class="mb-0"><span>phone: </span>{{ $cus[0]->cus_phone_no }}</p>
                <p class="mb-0"><span class="font-weight-bold">GST No: </span>{{ $cus[0]->cus_gst_percentage }}</p>
            </div>
            <div class="d-inline-block va-t w-40">
                <h6 class="font-weight-bold text-14 mb-1">Consignee</h6>
                <address class="mb-0">
                    <span>{{ $org[0]->Customer_Phone_No }}</span><br>
                    {{ $org[0]->delivery_address }}<br>{{ $org[0]->delivery_city }}<span>, </span>{{ $org[0]->delivery_state }}<span>
                        -
                    </span>{{ $org[0]->delivery_pincode }}
                </address>
                <p class="mb-0"><span>phone: </span>9500168299</p>
            </div>
        </div>

    </header>
    <!-- Wrap the content of your PDF inside a main tag -->
    <main>
        <section>
            <h6 class="fw-700 mt-2 text-14">Order Summary</h6>
            <table class="table table-bordered text-center small">
                <thead>
                    <tr>
                        <th scope="col">Item Id</th>
                        <th scope="col">Item Name</th>
                        <th scope="col">HSN/SAC</th>
                        <th scope="col">GST</th>
                        <th scope="col">Unit Price</th>
                        <th scope="col">Quantity</th>
                        <th scope="col">Discount</th>
                        <th scope="col">Description</th>
                        <th scope="col">Item Amount</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($invoice_details as $item)
                        <tr>
                            <td>{{ $item->Item_Id }}</td>
                            <td>{{ $item->name }}</td>
                            <td>{{ $item->hsn }}</td>
                            <td>{{ $item->gst }}</td>
                            <td>{{ $item->price }}</td>
                            <td>{{ $item->quantity }}</td>
                            <td>{{ $item->discount }}</td>
                            <td>{{ $item->descriptions }}</td>
                            <td>{{ $item->item_total }}</td>
                        </tr>
                    @endforeach
                    <tr>
                        <td colspan="8">Total</td>
                        <td>{{ $total[0]->items_total }}</td>
                    </tr>
                </tbody>
            </table>
            <h6 class="fw-700 mt-2 text-14">Tax details</h6>
            <table class="table table-bordered text-center small">
                <thead>
                    <tr>
                        <th scope="col">Taxable Amount</th>
                        <th scope="col">CGST %</th>
                        <th scope="col">CGST Amount</th>
                        <th scope="col">SGST %</th>
                        <th scope="col">SGST Amount</th>
                        <th scope="col">Tax Amount</th>
                        <th scope="col">Total Amount</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($invoice_details as $item)
                        <tr>
                            <td>{{ $item->item_total }}</td>
                            <td>{{ $item->cgst }}</td>
                            <td>{{ $item->cgst_amount }}</td>
                            <td>{{ $item->sgst }}</td>
                            <td>{{ $item->sgst_amount }}</td>
                            <td>{{ $item->tax_amount }}</td>
                            <td>{{ $item->item_total + $item->tax_amount }}</td>
                        </tr>
                    @endforeach
                    <tr>
                        <td colspan="5">Total</td>
                        <td>{{ $total[0]->total_tax }}</td>
                        <td>6920.34</td>
                    </tr>
                </tbody>
            </table>
        </section>
    </main>


    <footer>
        <div class="d-block text-12">
            <div class="d-inline-block va-t w-20">
                <h6 class="font-weight-bold text-14 mb-1">Bank Details</h6>
                <p class="mb-0 text-sm">
                    <span>{{ $org[0]->org_holder_name }}</span>
                    <br>{{ $org[0]->org_bank_name }}<br>{{ $org[0]->org_account_no }}<br>{{ $org[0]->org_bank_branch }}<br>{{ $org[0]->org_bank_ifsc }}<br>
                </p>
            </div>
            <div class="d-inline-block va-t w-20">
                <label class="font-weight-bold d-block">Total Bill Amount</label>
                <label class="d-block">(including GST)</label>
            </div>
            <div class="d-inline-block va-t w-59">
                <label class="font-weight-bold d-block">{{ $org[0]->total_amount }}</label>
                <label class="d-block">{{ $org[0]->Invoice_amount_words }}</label>
            </div>
        </div>
        <div class="d-block text-12 mt-2">
            <div class="d-inline-block va-t w-40">
                <h6 class="font-weight-bold text-sm mb-1">Declaration</h6>
                <p class="mb-0" style="font-size: 8px;">We declare that this invoice shows the actual price of the
                    goods described
                    <br>and that all the particulars are true and correct.
                </p>
            </div>
            <div class="d-inline-block va-t float-right 29">
                <h6 class="text-uppercase mb-4 pb-2 text-14 font-weight-normal">{{ $org[0]->company_name }}</h6>
                <p class="font-weight-bold">Authorised signatory</p>
            </div>
        </div>
    </footer>
</body>

</html>
