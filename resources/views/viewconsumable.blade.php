@extends('layouts.app')
@section('mytitle', 'View Consumable')
@section('head-links')
    <link href="{{ URL::asset('assets/css/datatables.min.css') }}" rel="stylesheet">
@endsection
@section('content')
    <div class="bg-top bg-gray p-4">
        <div class="d-flex">
            <h5 class="font-weight-bold text-uppercase mb-0">View Consumable</h5>
        </div>
    </div>

    <div class="p-4 card-main">
        <section class="my-5">
            <div class="card shadow">
                <div class="card-top ">
                    <div class="card-title mb-0">
                        <h5 class="font-weight-bold text-uppercase mb-0"><i class="fas fa-user-alt"></i></h5>
                    </div>
                    <div class="btn-right position-relative float-right">
                        <a href="#" class="btn btn-primary text-uppecase">Print</a>
                        <a href="/addconsumable" class="btn btn-primary text-uppecase" data-toggle="tooltip"
                            data-placement="top" title="Add Consumable">
                            <i class="fas fa-plus"></i> Add
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table" id="datatable">
                            <thead class="text-uppercase">
                                <tr>
                                    <th scope="col">CONSUMABLE ID</th>
                                    <th scope="col">CONSUMABLE Code NAME.</th>
                                    <th scope="col">CONSUMABLE NAME</th>
                                    <th scope="col">UNIT</th>
                                    <th scope="col">UNIT PRICE</th>
                                    <th scope="col" class="nosort">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (count($product) >= 1)
                                    @foreach ($product as $con)
                                        <tr>
                                            <td>{{ $con->consumable_const }}{{ $con->consumable_id }}</td>
                                            <td>{{ $con->consumable_code_name }}</td>
                                            <td>{{ $con->consumable_name }}</td>
                                            <td>{{ $con->consumable_unit }}</td>
                                            <td>{{ $con->consumable_unit_price }}</td>
                                            <td class="d-flex flex-nowrap">
                                                <span data-toggle="modal" data-target="#viewconsumable" class="mr-1"
                                                    data-con_const="{{ $con->consumable_const }}"
                                                    data-con_id="{{ $con->consumable_id }}"
                                                    data-branch="{{ $con->consumable_branch_name }}"
                                                    data-hsn="{{ $con->consumable_hsn }}"
                                                    data-gst="{{ $con->consumable_gst_percentage }}"
                                                    data-code_name="{{ $con->consumable_code_name }}"
                                                    data-prod_name="{{ $con->consumable_name }}"
                                                    data-unit="{{ $con->consumable_unit }}"
                                                    data-price="{{ $con->consumable_unit_price }}"
                                                    data-brand="{{ $con->consumable_brand_name }}"
                                                    data-no_copies="{{ $con->consumable_copies }}"
                                                    data-msl="{{ $con->consumable_msl }}"
                                                    data-chargeable="{{ $con->consumable_chargeable }}">
                                                    <button type="button" class="btn btn-outline-info" data-toggle="tooltip"
                                                        data-placement="top" title="View Consumable">
                                                        <i class="fas fa-user-alt"></i>
                                                    </button>
                                                </span>
                                                <span data-toggle="modal" data-target="#editconsumable" class="mr-1"
                                                    data-con_const="{{ $con->consumable_const }}"
                                                    data-con_id="{{ $con->consumable_id }}"
                                                    data-branch="{{ $con->consumable_branch_name }}"
                                                    data-hsn="{{ $con->consumable_hsn }}"
                                                    data-gst="{{ $con->consumable_gst_percentage }}"
                                                    data-code_name="{{ $con->consumable_code_name }}"
                                                    data-hsn="{{ $con->consumable_hsn }}"
                                                    data-prod_name="{{ $con->consumable_name }}"
                                                    data-unit="{{ $con->consumable_unit }}"
                                                    data-price="{{ $con->consumable_unit_price }}"
                                                    data-brand="{{ $con->consumable_brand_name }}"
                                                    data-no_copies="{{ $con->consumable_copies }}"
                                                    data-msl="{{ $con->consumable_msl }}"
                                                    data-chargeable="{{ $con->consumable_chargeable }}">
                                                    <button type="button" class="btn btn-outline-success"
                                                        data-toggle="tooltip" data-placement="top" title="Edit Consumable">
                                                        <i class="fas fa-edit"></i>
                                                    </button>
                                                </span>
                                                <span data-toggle="modal" data-target="#deleteconsumable"
                                                    data-con_id="{{ $con->consumable_id }}"
                                                    data-branch="{{ $con->consumable_branch_name }}"
                                                    data-code_name="{{ $con->consumable_code_name }}"
                                                    data-prod_name="{{ $con->consumable_name }}"
                                                    data-price="{{ $con->consumable_unit_price }}">
                                                    <button type="button" class="btn btn-outline-danger"
                                                        data-toggle="tooltip" data-placement="top"
                                                        title="Delete Consumable">
                                                        <i class="fas fa-trash-alt"></i>
                                                    </button>
                                                </span>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="6">Sorry, No records found</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                        <!-- Modal view Consumable -->
                        <div class="modal fade custom-modal" id="viewconsumable" tabindex="-1" role="dialog"
                            aria-labelledby="viewconsumable" aria-hidden="true">
                            <div class="modal-dialog modal-xl modal-dialog-centered edit" role="document">
                                <div class="modal-content">
                                    <div class="icon">
                                        <span class="fa-stack fa-2x text-success">
                                            <i class="fas fa-circle fa-stack-2x"></i>
                                            <i class="fas fa-table fa-stack-1x fa-inverse"></i>
                                        </span>
                                    </div>
                                    <div class="modal-header justify-content-center">
                                        <h3 class="modal-title text-capitalize font-weight-bolder mt-3" id="viewconsumable">
                                            View Consumable Details
                                        </h3>
                                    </div>
                                    <div class="modal-body pb-0 text-sm">
                                        <div class="row text-sm mb-4">
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">Consumable ID</label>
                                                <p class="mb-0"><span id="id"></span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">Consumable Belongs To Branch</label>
                                                <p class="mb-0"><span id="branch"></span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">HSN/SAC Number</label>
                                                <p class="mb-0"><span id="hsn"></span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">GST(In %)</label>
                                                <p class="mb-0"><span id="gst"></span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">Consumable Code Name</label>
                                                <p class="mb-0"><span id="code-name"></span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">Consumable Product Name</label>
                                                <p class="mb-0"><span id="prod-name"></span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">Unit</label>
                                                <p class="mb-0"><span id="unit"></span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">Unit Price</label>
                                                <p class="mb-0"><span id="price"></span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">Brand Name</label>
                                                <p class="mb-0"><span id="brand"></span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">No.Copy(ies)</label>
                                                <p class="mb-0"><span id="no-copies"></span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">MSL</label>
                                                <p class="mb-0"><span id="msl"></span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">Chargeable</label>
                                                <p class="mb-0"><span id="chargeable"></span></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer justify-content-center">
                                        <button type="button" class="btn btn-success btn-lg btn-large mt-0"
                                            data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End view Consumable -->
                        <!-- Modal Edit Consumable -->
                        <div class="modal fade custom-modal" id="editconsumable" tabindex="-1" role="dialog"
                            aria-labelledby="editconsumable" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered edit modal-xl" role="document">
                                <form id="edit_form" method="post">
                                    @csrf
                                    <div class="modal-content">
                                        <div class="icon">
                                            <span class="fa-stack fa-2x text-success">
                                                <i class="fas fa-circle fa-stack-2x"></i>
                                                <i class="far fa-edit fa-stack-1x fa-inverse"></i>
                                            </span>
                                        </div>
                                        <div class="modal-header justify-content-center">
                                            <h3 class="modal-title text-capitalize font-weight-bolder mt-3"
                                                id="editconsumable">
                                                Edit Consumable Details
                                            </h3>
                                        </div>
                                        <div class="modal-body pb-0">
                                            <div class="row">
                                                <div class="form-group col-md-4">
                                                    <label for="edit_id" class="mb-0 control-label active">Consumable ID
                                                        <small>*</small></label>
                                                    <input id="edit_id" name="edit_id" type="text" class="form-control"
                                                        readonly>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label class="control-label active" for="edit_brand">Brand Name
                                                        <small>*</small>
                                                    </label>
                                                    <select class="form-control" id="edit_brand" name="edit_brand"></select>
                                                </div>

                                                <div class="form-group col-md-4">
                                                    <label class="control-label active" for="edit_hsn">HSN/SAC
                                                        Number
                                                        <small>*</small>
                                                    </label>
                                                    <select class="form-control" id="edit_hsn" name="edit_hsn"></select>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="edit_productname"
                                                        class="mb-0 control-label active">Consumable
                                                        Product
                                                        Name</label>
                                                    <input id="edit_productname" name="edit_productname" type="text"
                                                        class="form-control">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="edit_codename" class="mb-0 control-label active">Consumable
                                                        Code
                                                        Name</label>
                                                    <input id="edit_codename" name="edit_codename" type="text"
                                                        class="form-control">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="edit_branch" class="mb-0 control-label active">Belongs To
                                                        Branch</label>
                                                    <input id="edit_branch" name="edit_branch" type="text"
                                                        class="form-control" readonly>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="edit_no_ofcopies" class="mb-0 control-label active">No.of
                                                        Copies</label>
                                                    <input id="edit_no_ofcopies" name="edit_no_ofcopies" type="text"
                                                        class="form-control">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="edit_price" class="mb-0 control-label active">Unit Price
                                                        <small>*</small></label>
                                                    <input id="edit_price" name="edit_price" type="text"
                                                        class="form-control">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label class="control-label active" for="edit_unit">Unit</label>
                                                    <input id="edit_unit" name="edit_unit" type="number"
                                                        class="form-control">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="edit_gst" class="mb-0 control-label active">GST(In
                                                        %)</label>
                                                    <input id="edit_gst" name="edit_gst" type="text" class="form-control">
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="font-weight-bold">MSL</label>
                                                    <div class="mb-2">
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input type="radio" id="msl_true" name="msl" value="yes"
                                                                class="custom-control-input">
                                                            <label class="custom-control-label" for="msl_true">Yes</label>
                                                        </div>
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input type="radio" id="msl_false" name="msl" value="no"
                                                                class="custom-control-input">
                                                            <label class="custom-control-label" for="msl_false">No</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="font-weight-bold">Chargeable</label>
                                                    <div class="mb-2">
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input type="radio" id="chargeable_true" name="chargeable"
                                                                value="yes" class="custom-control-input">
                                                            <label class="custom-control-label"
                                                                for="chargeable_true">Yes</label>
                                                        </div>
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input type="radio" id="chargeable_false" name="chargeable"
                                                                value="no" class="custom-control-input">
                                                            <label class="custom-control-label"
                                                                for="chargeable_false">No</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer flex-nowrap">
                                            <button type="button" class="btn btn-outline-success btn-block btn-lg mt-0"
                                                data-dismiss="modal">Close</button>
                                            <button type="submit"
                                                class="btn btn-success btn-block btn-lg mt-0">Done</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- End Edit Consumable -->
                        <!-- Modal Delete Consumable -->
                        <div class="modal fade custom-modal" id="deleteconsumable" tabindex="-1" role="dialog"
                            aria-labelledby="deleteconsumable" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="icon">
                                        <span class="fa-stack fa-2x text-primary">
                                            <i class="fas fa-circle fa-stack-2x"></i>
                                            <i class="fas fa-trash-alt fa-stack-1x fa-inverse"></i>
                                        </span>
                                    </div>
                                    <div class="modal-header justify-content-center">
                                        <h3 class="modal-title text-capitalize font-weight-bolder mt-3"
                                            id="deleteconsumable">
                                            Delete Consumable?
                                        </h3>
                                    </div>
                                    <div class="modal-body pb-0">
                                        <p class="font-weight-bold text-center">
                                            Are you sure you want to delete the Consumable?
                                            <br>
                                            You cannot undo this operation!
                                        </p>
                                        <div class="row justify-content-center">
                                            <div class="col-md-11">
                                                <form>
                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <label for="delete_id" class="col-form-label">Consumable
                                                                ID</label>
                                                            <input type="text" class="form-control" id="delete_id" readonly>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="delete_code" class="col-form-label">Consumable Code
                                                                Name</label>
                                                            <input type="text" class="form-control" id="delete_code"
                                                                readonly>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="delete_name" class="col-form-label">Consumable
                                                                Name</label>
                                                            <input type="text" class="form-control" id="delete_name"
                                                                readonly>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="delete_price" class="col-form-label">Consumable
                                                                Unit Price</label>
                                                            <input type="text" class="form-control" id="delete_price"
                                                                readonly>
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <label for="delete_branch" class="col-form-label">Belongs To
                                                                Branch</label>
                                                            <input type="text" class="form-control" id="delete_branch"
                                                                readonly>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer flex-nowrap" id="delete_btn"></div>
                                </div>
                            </div>
                        </div>
                        <!-- End Delete spares -->
                    </div>
                </div>
            </div>
        </section>
    </div>
    @include('components.alertmodal')
@endsection
@section('script')
    @if (session()->has('message'))
        <script>
            $(document).ready(function() {
                $('#successAlert').modal("show");
            });

        </script>
    @endif
    @if (session()->has('update'))
        <script>
            $(document).ready(function() {
                $('#updateAlert').modal("show");
            });

        </script>
    @endif
    @if (session()->has('delete'))
        <script>
            $(document).ready(function() {
                $('#deleteAlert').modal("show");
            });

        </script>
    @endif
    @if (session()->has('error'))
        <script>
            $(document).ready(function() {
                $('#errorAlert').modal("show");
            });

        </script>
    @endif
    <script src="{{ URL::asset('assets/js/datatables.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            loadBrandname();
            getHsn();
            //loadUnit();
            $('#datatable').DataTable({
                language: {
                    searchPlaceholder: "Search records",
                    search: "",
                    "lengthMenu": "_MENU_ Entries"
                },
                "lengthMenu": [
                    [8, 16, 32, -1],
                    [8, 16, 32, "All"]
                ],
                "pageLength": 8,
                "aaSorting": [
                    [0, 'desc']
                ],
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': ['nosort']
                }]
            });
            //View Modal
            $("#viewconsumable").on('show.bs.modal', function(event) {
                var consumable = $(event.relatedTarget);
                var conConst = consumable.data('con_const');
                var conId = consumable.data('con_id');
                var id = `${conConst}${conId}`;
                var branch = consumable.data('branch');
                var hsn = consumable.data('hsn');
                var gst = consumable.data('gst');
                var codeName = consumable.data('code_name');
                var prodName = consumable.data('prod_name');
                var unit = consumable.data('unit');
                var price = consumable.data('price');
                var brand = consumable.data('brand');
                var noCopies = consumable.data('no_copies');
                var msl = consumable.data('msl');
                var chargeable = consumable.data('chargeable');

                //push retrieved data into mentioned id 
                $('#id').html(id);
                $('#branch').html(branch);
                $('#hsn').html(hsn);
                $('#gst').html(gst);
                $('#code-name').html(codeName);
                $('#prod-name').html(prodName);
                $('#unit').html(unit);
                $('#price').html(price);
                $('#brand').html(brand);
                $('#no-copies').html(noCopies);
                $('#msl').html(msl);
                $('#chargeable').html(chargeable);
            });

            $("#editconsumable").on('show.bs.modal', function(event) {
                var consumable = $(event.relatedTarget);
                var conConst = consumable.data('con_const');
                var conId = consumable.data('con_id');
                var id = `${conConst}${conId}`;
                var branch = consumable.data('branch');
                var hsn = consumable.data('hsn');
                var gst = consumable.data('gst');
                var codeName = consumable.data('code_name');
                var prodName = consumable.data('prod_name');
                var unit = consumable.data('unit');
                var price = consumable.data('price');
                var brand = consumable.data('brand');
                var noCopies = consumable.data('no_copies');
                var msl = consumable.data('msl');
                var chargeable = consumable.data('chargeable');
                console.log('hsn', hsn);
                console.log('codeName', codeName);

                $('#edit_form').attr('action', 'updateconsumable/' + conId);
                $("#edit_id").val(id);
                $("#edit_brand").val(brand);
                $("#edit_hsn").val(hsn);
                $("#edit_productname").val(prodName);
                $("#edit_codename").val(codeName);
                $("#edit_price").val(price);
                $("#edit_unit").val(unit);
                $("#edit_no_ofcopies").val(noCopies);
                $("#edit_branch").val(branch);
                $("#edit_gst").val(gst);
                if (msl == 'yes') {
                    $("#msl_true").prop("checked", true);
                } else if (msl == 'no') {
                    $("#msl_false").prop("checked", true);
                }
                if (chargeable == 'yes') {
                    $("#chargeable_true").prop("checked", true);
                } else if (chargeable == 'no') {
                    $("#chargeable_false").prop("checked", true);
                }
            });


            $("#deleteconsumable").on('show.bs.modal', function(event) {
                var consumable = $(event.relatedTarget);
                var id = consumable.data('con_id');
                var codeName = consumable.data('code_name');
                var name = consumable.data('prod_name');
                var price = consumable.data('price');
                var branch = consumable.data('branch');

                $("#delete_id").val(id);
                $("#delete_code").val(codeName);
                $("#delete_name").val(name);
                $("#delete_price").val(price);
                $("#delete_branch").val(branch);

                $('#delete_btn').html(
                    `<button type="button" class="btn btn-outline-primary btn-block btn-lg mt-0" data-dismiss="modal">Close</button><a href='deleteconsumable/${id}' class="btn btn-primary btn-block btn-lg mt-0">Delete</a>`
                );
            });
        });


        function loadBrandname() {
            return $.ajax({
                url: "/brand",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    //console.log("brand_name", data);
                    $('#edit_brand').empty();
                    $('#edit_brand').append('<option selected="true" disabled></option>');
                    $('#edit_brand').prop('selectedIndex', 0);
                    $.each(data, function(key, value) {
                        $('#edit_brand').append($('<option></option>').attr('value', value
                            .name).text(value.name));
                    });
                    $('#edit_brand').append('<option value="others">Others</option>');
                },
                error: function(error) {
                    console.log('brand_name error:', error);
                }
            });
        }

        function getHsn() {
            return $.ajax({
                url: "/consumablehsn",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    //console.log("consumablehsn", data);
                    $('#edit_hsn').empty();
                    $('#edit_hsn').append('<option selected="true" disabled></option>');
                    $('#edit_hsn').prop('selectedIndex', 0);
                    $.each(data, function(key, value) {
                        $('#edit_hsn').append($('<option></option>').attr(
                            'value',
                            value.Consumable_HSN_No).text(value.Consumable_HSN_No));
                    });
                    $('#edit_hsn').append('<option value="others">Others</option>');
                },
                error: function(error) {
                    console.log('consumablehsn error:', error);
                }
            });
        }

        /*  function loadUnit() {
             return $.ajax({
                 url: "/unit",
                 type: "GET",
                 dataType: 'json',
                 success: function(data) {
                     //console.log("stock_unit", data);
                     $('#edit_unit').empty();
                     $('#edit_unit').append('<option selected="true" disabled></option>');
                     $('#edit_unit').prop('selectedIndex', 0);
                     $.each(data, function(key, value) {
                         $('#edit_unit').append($('<option></option>').attr(
                             'value',
                             value.unit).text(value.unit));
                     });
                     $('#edit_unit').append('<option value="others">Others</option>');
                 },
                 error: function(error) {
                     console.log('stock_unit error:', error);
                 }
             });
         } */

    </script>
@endsection
