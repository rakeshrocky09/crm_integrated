@extends('layouts.app')
@section('mytitle', 'VIEW Contract')
@section('content')
    <div class="p-4">
        <section class="my-5">
            <div class="card shadow">
                <div class="card-top ">
                    <div class="card-title mb-0">
                        <h6 class="font-weight-bold text-capitalize mb-0">View Contract</h6>
                    </div>
                </div>
                <div class="card-body pt-0">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <h6 class="font-weight-bold mb-0 text-sm">Contract Id</h6>
                                <span class="text-13">{{ $contract[0]->Contract_ID }}</span>
                            </div>
                            <div class="form-group">
                                <h6 class="font-weight-bold mb-0 text-sm">Customer Name</h6>
                                <span class="text-13">{{ $contract[0]->customer_name }}</span>
                            </div>
                            <div class="form-group">
                                <h6 class="font-weight-bold mb-0 text-sm">Phone No</h6>
                                <span class="text-13">{{ $contract[0]->cus_phone_no }}</span>
                            </div>
                            <div class="form-group">
                                <h6 class="font-weight-bold mb-0 text-sm">Branch Name</h6>
                                <span class="text-13">{{ $contract[0]->Branch_Name }}</span>
                            </div>
                            <div class="form-group">
                                <h6 class="font-weight-bold mb-0 text-sm">Address</h6>
                                <address class="text-13">
                                    <span>{{ $contract[0]->cus_address }}</span><br>
                                    <span>{{ $contract[0]->cus_city }}</span> -
                                    <span>{{ $contract[0]->cus_pincode }}</span><br>
                                    <span>{{ $contract[0]->cus_state }}</span>
                                </address>
                            </div>
                            <div class="my-3">
                                <a href="{{ url('/viewcontract') }}" class="btn btn-primary">Cancel</a>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="table-responsive">
                                <table class="table table-bordered table-view">
                                    <thead>
                                        <tr>
                                            <th scope="col">Contract Type</th>
                                            <th scope="col">Product Ownership</th>
                                            <th scope="col">Start Date</th>
                                            <th scope="col">End Date</th>
                                            <th scope="col">Contract Amount</th>
                                            <th scope="col">Status</th>
                                            <th scope="col">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($contract as $item)
                                            <tr>
                                                <td>{{ $item->Contract_type }}</td>
                                                <td>{{ $item->Product_Ownership }}</td>
                                                <td>{{ $item->Start_Date }}</td>
                                                <td>{{ $item->End_Date }}</td>
                                                <td>{{ $item->Contract_Amount }}</td>
                                                <td>
                                                    @if ($item->Contract_Status == 'open')
                                                        <span class="badge badge-pill badge-success text-12">
                                                            {{ $item->Contract_Status }}
                                                        </span>
                                                    @else
                                                        <span class="badge badge-pill badge-secondary text-12">
                                                            {{ $item->Contract_Status }}
                                                        </span>
                                                    @endif
                                                </td>
                                                <td class="d-flex flex-nowrap">
                                                    <a href="{{ url('/view_contract', $contract[0]->Contract_ID) }}"
                                                        class="btn btn-outline-info mr-2" title="View Tickets">
                                                        <i class="fas fa-external-link-alt"></i>
                                                    </a>
                                                    <a href="{{ url('/addexpenses', $contract[0]->Contract_ID) }}" class="btn btn-outline-primary"
                                                        title="Add Expenses">
                                                        <i class="fas fa-plus"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
