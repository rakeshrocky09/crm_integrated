@extends('layouts.app')
@section('mytitle', 'View Product')
@section('head-links')
    <link href="{{ URL::asset('assets/css/datatables.min.css') }}" rel="stylesheet">
@endsection
@section('content')
    <div class="bg-top bg-gray p-4">
        <div class="d-flex">
            <h5 class="font-weight-bold text-uppercase mb-0">View Product</h5>
        </div>
    </div>

    <div class="p-4 card-main">
        <section class="my-5">
            <div class="card shadow">
                <div class="card-top ">
                    <div class="card-title mb-0">
                        <h5 class="font-weight-bold text-uppercase mb-0"><i class="fas fa-user-alt"></i></h5>
                    </div>
                    <div class="btn-right position-relative float-right">
                        <a href="#" class="btn btn-primary text-uppecase">Print</a>
                        <a href="/addproduct" class="btn btn-primary text-uppecase" data-toggle="tooltip"
                            data-placement="top" title="Add Product">
                            <i class="fas fa-plus"></i> Add
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table" id="datatable">
                            <thead class="text-uppercase">
                                <tr>
                                    <th scope="col">PRODUCT ID</th>
                                    <th scope="col">PART NO.</th>
                                    <th scope="col">NAME</th>
                                    <th scope="col">BRAND NAME</th>
                                    <th scope="col">UNITS</th>
                                    <th scope="col">PRICE</th>
                                    <th scope="col" class="nosort">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (count($product) >= 1)
                                    @foreach ($product as $prod)
                                        <tr>
                                            <td>{{ $prod->product_const }}{{ $prod->product_id }}</td>
                                            <td>{{ $prod->product_part_num }}</td>
                                            <td>{{ $prod->product_name }}</td>
                                            <td>{{ $prod->product_brand_name }}</td>
                                            <td>{{ $prod->product_stock_unit }}</td>
                                            <td>{{ $prod->product_price }}</td>
                                            <td class="d-flex flex-nowrap">
                                                <span data-toggle="modal" data-target="#viewproduct" class="mr-1"
                                                    data-branch="{{ $prod->product_branch_name }}"
                                                    data-prod_const="{{ $prod->product_const }}"
                                                    data-prod_id="{{ $prod->product_id }}"
                                                    data-prod_code="{{ $prod->product_code }}"
                                                    data-prod_brand_name="{{ $prod->product_brand_name }}"
                                                    data-prod_model_name="{{ $prod->product_model_name }}"
                                                    data-prod_name="{{ $prod->product_name }}"
                                                    data-prod_category="{{ $prod->Product_Category }}"
                                                    data-description="{{ $prod->product_description }}"
                                                    data-part_no="{{ $prod->product_part_num }}"
                                                    data-gst="{{ $prod->product_gst_percentage }}"
                                                    data-color="{{ $prod->product_color }}"
                                                    data-price="{{ $prod->product_price }}"
                                                    data-unit="{{ $prod->product_stock_unit }}">
                                                    <button type="button" class="btn btn-outline-info" data-toggle="tooltip"
                                                        data-placement="top" title="View Product">
                                                        <i class="fas fa-user-alt"></i>
                                                    </button>
                                                </span>
                                                <span data-toggle="modal" data-target="#editproduct" class="mr-1"
                                                    data-branch="{{ $prod->product_branch_name }}"
                                                    data-const="{{ $prod->product_const }}"
                                                    data-prod_id="{{ $prod->product_id }}"
                                                    data-brand="{{ $prod->product_brand_name }}"
                                                    data-model="{{ $prod->product_model_name }}"
                                                    data-hsn="{{ $prod->product_code }}"
                                                    data-description="{{ $prod->product_description }}"
                                                    data-prod_name="{{ $prod->product_name }}"
                                                    data-category="{{ $prod->Product_Category }}"
                                                    data-color="{{ $prod->product_color }}"
                                                    data-unit="{{ $prod->product_stock_unit }}"
                                                    data-gst="{{ $prod->product_gst_percentage }}"
                                                    data-part_no="{{ $prod->product_part_num }}"
                                                    data-price="{{ $prod->product_price }}">
                                                    <button type="button" class="btn btn-outline-success"
                                                        data-toggle="tooltip" data-placement="top" title="Edit Product">
                                                        <i class="fas fa-edit"></i>
                                                    </button>
                                                </span>
                                                <span data-toggle="modal" data-target="#deleteproduct"
                                                    data-prod_id="{{ $prod->product_id }}"
                                                    data-prod_code="{{ $prod->product_code }}"
                                                    data-prod_name="{{ $prod->product_name }}"
                                                    data-prod_brand_name="{{ $prod->product_brand_name }}"
                                                    data-branch="{{ $prod->product_branch_name }}">
                                                    <button type="button" class="btn btn-outline-danger"
                                                        data-toggle="tooltip" data-placement="top" title="Delete Product">
                                                        <i class="fas fa-trash-alt"></i>
                                                    </button>
                                                </span>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="7">Sorry, No records found</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                        <!-- Modal viewproduct -->
                        <div class="modal fade custom-modal" id="viewproduct" tabindex="-1" role="dialog"
                            aria-labelledby="viewproduct" aria-hidden="true">
                            <div class="modal-dialog modal-xl modal-dialog-centered edit" role="document">
                                <div class="modal-content">
                                    <div class="icon">
                                        <span class="fa-stack fa-2x text-success">
                                            <i class="fas fa-circle fa-stack-2x"></i>
                                            <i class="fas fa-table fa-stack-1x fa-inverse"></i>
                                        </span>
                                    </div>
                                    <div class="modal-header justify-content-center">
                                        <h3 class="modal-title text-capitalize font-weight-bolder mt-3" id="viewproduct">
                                            View Branch Details
                                        </h3>
                                    </div>
                                    <div class="modal-body pb-0 text-sm">
                                        <h6 class="fw-700 text-uppercase mb-4">PRODUCT DETAILS</h6>
                                        <div class="row text-sm mb-4">
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">Belongs To Branch Name</label>
                                                <p class="mb-0"><span id="branch"></span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">Product ID</label>
                                                <p class="mb-0"><span id="prod-id"></span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">HSN/SAC Number</label>
                                                <p class="mb-0"><span id="prod-code"></span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">Brand Name</label>
                                                <p class="mb-0"><span id="prod-brand"></span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">Model Name</label>
                                                <p class="mb-0"><span id="prod-model"></span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">Product Name</label>
                                                <p class="mb-0"><span id="prod-name"></span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">Product Category</label>
                                                <p class="mb-0"><span id="prod-category"></span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">Description</label>
                                                <p class="mb-0"><span id="description"></span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">Part Number</label>
                                                <p class="mb-0"><span id="part-no"></span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">GST(In %)</label>
                                                <p class="mb-0"><span id="gst"></span></p>
                                            </div>
                                        </div>
                                        <h6 class="fw-700 text-uppercase mb-4">PRODUCT PARAMETERS</h6>
                                        <div class="row text-sm">
                                            <div class="form-group col-md-4">
                                                <label class="font-weight-bold">Color</label>
                                                <p class="mb-0"><span id="color"></span></p>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label class="font-weight-bold">Product Price</label>
                                                <p class="mb-0"><span id="price"></span></p>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label class="font-weight-bold">Units</label>
                                                <p class="mb-0"><span id="unit"></span></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer justify-content-center">
                                        <button type="button" class="btn btn-success btn-lg btn-large mt-0"
                                            data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End viewprofile -->
                        <!-- Modal Edit product -->
                        <div class="modal fade custom-modal" id="editproduct" tabindex="-1" role="dialog"
                            aria-labelledby="editproduct" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered edit modal-xl" role="document">
                                <form id="edit_form" method="post">
                                    @csrf
                                    <div class="modal-content">
                                        <div class="icon">
                                            <span class="fa-stack fa-2x text-success">
                                                <i class="fas fa-circle fa-stack-2x"></i>
                                                <i class="far fa-edit fa-stack-1x fa-inverse"></i>
                                            </span>
                                        </div>
                                        <div class="modal-header justify-content-center">
                                            <h3 class="modal-title text-capitalize font-weight-bolder mt-3"
                                                id="editproduct">
                                                Edit Branch Details
                                            </h3>
                                        </div>
                                        <div class="modal-body pb-0">
                                            <h6 class="fw-700 text-uppercase pl-1 mb-4 mt-3">
                                                Product Details</h6>
                                            <div class="row">
                                                <div class="form-group col-md-4">
                                                    <label for="edit_branch" class="mb-0 control-label active">Belongs To
                                                        Branch <small>*</small></label>
                                                    <input id="edit_branch" name="edit_branch" type="text"
                                                        class="form-control" readonly>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label class="control-label active" for="edit_brand_name">Brand Name
                                                        <small>*</small>
                                                    </label>
                                                    <select class="form-control" id="edit_brand_name"
                                                        name="edit_brand_name"></select>
                                                </div>

                                                <div class="form-group col-md-4">
                                                    <label for="edit_prod_id" class="mb-0 control-label active">Product Id
                                                        <small>*</small></label>
                                                    <input id="edit_prod_id" name="edit_prod_id" type="text"
                                                        class="form-control" readonly>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="edit_model" class="mb-0 control-label active">Model Name
                                                        <small>*</small></label>
                                                    <input id="edit_model" name="edit_model" type="text"
                                                        class="form-control">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label class="control-label active" for="edit_hsn">HSN/SAC
                                                        Number<small>*</small>
                                                    </label>
                                                    <select class="form-control" id="edit_hsn" name="edit_hsn"></select>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label class="control-label active" for="edit_description">Description
                                                        <small>*</small></label>
                                                    <textarea class="form-control mt-1" id="edit_description"
                                                        name="edit_description" rows="1"></textarea>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="edit_prod_name" class="mb-0 control-label active">Product
                                                        Name
                                                        <small>*</small></label>
                                                    <input id="edit_prod_name" name="edit_prod_name" type="text"
                                                        class="form-control">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label class="control-label active" for="edit_prod_category">Product
                                                        Category</label>
                                                    <select class="form-control" id="edit_prod_category"
                                                        name="edit_prod_category"></select>
                                                </div>
                                            </div>
                                            <h6 class="fw-700 text-uppercase pl-1 mb-4 mt-3">
                                                Product Parameters</h6>
                                            <div class="row">
                                                <div class="form-group col-md-4">
                                                    <label for="edit_color" class="mb-0 control-label active">Color</label>
                                                    <input id="edit_color" name="edit_color" type="text"
                                                        class="form-control">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label class="control-label active" for="edit_unit">Units
                                                    </label>
                                                    <input id="edit_unit" name="edit_unit" type="number" class="form-control"
                                                        value="18">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="edit_gst" class="mb-0 control-label active">GST(In
                                                        %)</label>
                                                    <input id="edit_gst" name="edit_gst" type="text" class="form-control"
                                                        value="18">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="edit_part_no" class="mb-0 control-label active">Part
                                                        Number</label>
                                                    <input id="edit_part_no" name="edit_part_no" type="text"
                                                        class="form-control" value="A798047">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="edit_price" class="mb-0 control-label active">Product
                                                        Price</label>
                                                    <input id="edit_price" name="edit_price" type="text"
                                                        class="form-control" value="135000">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer flex-nowrap">
                                            <button type="button" class="btn btn-outline-success btn-block btn-lg mt-0"
                                                data-dismiss="modal">Close</button>
                                            <button type="submit"
                                                class="btn btn-success btn-block btn-lg mt-0">Done</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- End Edit profile -->
                        <!-- Modal Delete product -->
                        <div class="modal fade custom-modal" id="deleteproduct" tabindex="-1" role="dialog"
                            aria-labelledby="deleteproduct" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="icon">
                                        <span class="fa-stack fa-2x text-primary">
                                            <i class="fas fa-circle fa-stack-2x"></i>
                                            <i class="fas fa-trash-alt fa-stack-1x fa-inverse"></i>
                                        </span>
                                    </div>
                                    <div class="modal-header justify-content-center">
                                        <h3 class="modal-title text-capitalize font-weight-bolder mt-3" id="deleteproduct">
                                            Delete Branch?
                                        </h3>
                                    </div>
                                    <div class="modal-body pb-0">
                                        <p class="font-weight-bold text-center">
                                            Are you sure you want to delete the Product?
                                            <br>
                                            You cannot undo this operation!
                                        </p>
                                        <div class="row justify-content-center">
                                            <div class="col-md-11">
                                                <form>
                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <label for="delete_product_id" class="col-form-label">Product
                                                                ID</label>
                                                            <input type="text" class="form-control" id="delete_product_id"
                                                                readonly>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="delete_product_code" class="col-form-label">Product
                                                                Code</label>
                                                            <input type="text" class="form-control" id="delete_product_code"
                                                                readonly>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="delete_product_name" class="col-form-label">Product
                                                                Name</label>
                                                            <input type="text" class="form-control" id="delete_product_name"
                                                                readonly>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="delete_brandname" class="col-form-label">Brand
                                                                Name</label>
                                                            <input type="text" class="form-control" id="delete_brandname"
                                                                readonly>
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <label for="delete_branch" class="col-form-label">Belongs To
                                                                Branch</label>
                                                            <input type="text" class="form-control" id="delete_branch"
                                                                readonly>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer flex-nowrap" id="delete_btn"></div>
                                </div>
                            </div>
                        </div>
                        <!-- End Delete organizaton -->
                    </div>
                </div>
            </div>
        </section>
    </div>
    @include('components.alertmodal')
@endsection
@section('script')
    @if (session()->has('message'))
        <script>
            $(document).ready(function() {
                $('#successAlert').modal("show");
            });

        </script>
    @endif
    @if (session()->has('update'))
        <script>
            $(document).ready(function() {
                $('#updateAlert').modal("show");
            });

        </script>
    @endif
    @if (session()->has('delete'))
        <script>
            $(document).ready(function() {
                $('#deleteAlert').modal("show");
            });

        </script>
    @endif
    @if (session()->has('error'))
        <script>
            $(document).ready(function() {
                $('#errorAlert').modal("show");
            });

        </script>
    @endif
    <script src="{{ URL::asset('assets/js/datatables.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            loadBrandname();
            getHsn();
            //loadUnit();
            loadProductCategory();
            $('#datatable').DataTable({
                language: {
                    searchPlaceholder: "Search records",
                    search: "",
                    "lengthMenu": "_MENU_ Entries"
                },
                "lengthMenu": [
                    [8, 16, 32, -1],
                    [8, 16, 32, "All"]
                ],
                "pageLength": 8,
                "aaSorting": [
                    [0, 'desc']
                ],
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': ['nosort']
                }]
            });
            //View Modal
            $("#viewproduct").on('show.bs.modal', function(event) {
                var prod = $(event.relatedTarget);
                var branch = prod.data('branch');
                var prodConst = prod.data('prod_const');
                var prodId = prod.data('prod_id');
                var id = `${prodConst}${prodId}`;
                var prodCode = prod.data('prod_code');
                var prodBrand = prod.data('prod_brand_name');
                var prodModel = prod.data('prod_model_name');
                var prodName = prod.data('prod_name');
                var prodCategory = prod.data('prod_category');
                var description = prod.data('description');
                var partNo = prod.data('part_no');
                var gst = prod.data('gst');
                var color = prod.data('color');
                var price = prod.data('price');
                var unit = prod.data('unit');

                //push retrieved data into mentioned id 
                $('#branch').html(branch);
                $('#prod-const').html(prodConst);
                $('#prod-id').html(id);
                $('#prod-code').html(prodCode);
                $('#prod-brand').html(prodBrand);
                $('#prod-model').html(prodModel);
                $('#prod-name').html(prodName);
                $('#prod-category').html(prodCategory);
                $('#description').html(description);
                $('#part-no').html(partNo);
                $('#gst').html(gst);
                $('#color').html(color);
                $('#price').html(price);
                $('#unit').html(unit);
            });

            $("#editproduct").on('show.bs.modal', function(event) {
                var prod = $(event.relatedTarget);
                var branch = prod.data('branch');
                var prodConst = prod.data('const');
                var prodId = prod.data('prod_id');
                var id = `${prodConst}${prodId}`;
                var prodCode = prod.data('hsn');
                var prodBrand = prod.data('brand');
                var prodModel = prod.data('model');
                var prodName = prod.data('prod_name');
                var prodCategory = prod.data('category');
                var description = prod.data('description');
                var partNo = prod.data('part_no');
                var gst = prod.data('gst');
                var color = prod.data('color');
                var price = prod.data('price');
                var unit = prod.data('unit');
                console.log('unit', id);

                //push retrieved data into mentioned id 
                $('#edit_form').attr('action', 'updateproduct/' + prodId);


                $('#edit_branch').val(branch);
                $('#edit_prod_id').val(id);
                $('#edit_hsn').val(prodCode);
                $('#edit_brand_name').val(prodBrand);
                $('#edit_model').val(prodModel);
                $('#edit_prod_name').val(prodName);
                $('#edit_prod_category').val(prodCategory);
                $('#edit_description').val(description);
                $('#edit_part_no').val(partNo);
                $('#edit_gst').val(gst);
                $('#edit_color').val(color);
                $('#edit_price').val(price);
                $('#edit_unit').val(unit);
            });

            $("#deleteproduct").on('show.bs.modal', function(event) {
                var prod = $(event.relatedTarget);
                var branch = prod.data('branch');
                var id = prod.data('prod_id');
                var prodCode = prod.data('prod_code');
                var prodName = prod.data('prod_name');
                var prodBrand = prod.data('prod_brand_name');

                $("#delete_product_id").val(id);
                $("#delete_product_code").val(prodCode);
                $("#delete_product_name").val(prodName);
                $("#delete_brandname").val(prodBrand);
                $("#delete_branch").val(branch);

                $('#delete_btn').html(
                    `<button type="button" class="btn btn-outline-primary btn-block btn-lg mt-0" data-dismiss="modal">Close</button><a href='deleteproduct/${id}' class="btn btn-primary btn-block btn-lg mt-0">Delete</a>`
                );
            });
        });

        function loadBrandname() {
            return $.ajax({
                url: "/brand",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    console.log("branchname", data);
                    $('#edit_brand_name').empty();
                    $('#edit_brand_name').append('<option selected="true" disabled></option>');
                    $('#edit_brand_name').prop('selectedIndex', 0);
                    $.each(data, function(key, value) {
                        $('#edit_brand_name').append($('<option></option>').attr('value',
                            value
                            .name).text(value.name));
                    });
                    $('#edit_brand_name').append('<option value="others">Others</option>');
                },
                error: function(error) {
                    console.log('brandname error:', error);
                }
            });
        }

        function getHsn() {
            return $.ajax({
                url: "/prdhsn",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    //console.log("prdhsn", data);
                    $('#edit_hsn').empty();
                    $('#edit_hsn').append('<option selected="true" disabled></option>');
                    $('#edit_hsn').prop('selectedIndex', 0);
                    $.each(data, function(key, value) {
                        $('#edit_hsn').append($('<option></option>').attr(
                            'value',
                            value.Product_HSN_No).text(value.Product_HSN_No));
                    });
                    $('#edit_hsn').append('<option value="others">Others</option>');
                },
                error: function(error) {
                    console.log('prdhsn error:', error);
                }
            });
        }

      /*   function loadUnit() {
            return $.ajax({
                url: "/unit",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    //console.log("prdhsn", data);
                    $('#edit_unit').empty();
                    $('#edit_unit').append('<option selected="true" disabled></option>');
                    $('#edit_unit').prop('selectedIndex', 0);
                    $.each(data, function(key, value) {
                        $('#edit_unit').append($('<option></option>').attr(
                            'value',
                            value.unit).text(value.unit));
                    });
                    $('#edit_unit').append('<option value="others">Others</option>');
                },
                error: function(error) {
                    console.log('unit error:', error);
                }
            });
        } */

        function loadProductCategory() {
            return $.ajax({
                url: "/prdcategory",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    //console.log("prdhsn", data);
                    $('#edit_prod_category').empty();
                    $('#edit_prod_category').append('<option selected="true" disabled></option>');
                    $('#edit_prod_category').prop('selectedIndex', 0);
                    $.each(data, function(key, value) {
                        $('#edit_prod_category').append($('<option></option>').attr(
                            'value',
                            value.name).text(value.name));
                    });
                    $('#edit_prod_category').append('<option value="others">Others</option>');
                },
                error: function(error) {
                    console.log('edit_prod_category error:', error);
                }
            });
        }

    </script>
@endsection
