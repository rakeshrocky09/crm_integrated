@extends('layouts.app')
@section('mytitle', 'Add Employee')
@section('content')
    <div class="bg-top bg-gray p-4">
        <div class="d-flex">
            <h5 class="font-weight-bold text-uppercase mb-0">Add Employee</h5>
        </div>
    </div>
    <section class="my-4 p-4 card-main">
        <form action="addemp" method="POST">
            @csrf
            <div class="row">
                <div class="col-md-6 py-4">
                    <div class="card shadow">
                        <div class="card-top ">
                            <div class="card-title mb-0">
                                <h5 class="font-weight-bold text-uppercase mb-0"><i class="fas fa-user-alt"></i></h5>
                            </div>
                        </div>
                        <div class="card-body pt-0">
                            <div class="form-group">
                                <label class="control-label" for="fname">First Name <small>*</small></label>
                                <input id="fname" name="fname" type="text"
                                    class="form-control @error('fname') is-invalid @enderror">
                                @error('fname')
                                    <span class="text-danger text-12">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="lname">Last Name <small>*</small></label>
                                <input id="lname" name="lname" type="text"
                                    class="form-control @error('lname') is-invalid @enderror">
                                @error('lname')
                                    <span class="text-danger text-12">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="emp_role">Employee Role <small>*</small></label>
                                <select class="form-control @error('emp_role') is-invalid @enderror" id="emp_role"
                                    name="emp_role"></select>
                                @error('emp_role')
                                    <span class="text-danger text-12">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="user_belongs_to">User Belongs To (branch)
                                    <small>*</small></label>
                                <select class="form-control @error('user_belongs_to') is-invalid @enderror"
                                    id="user_belongs_to" name="user_belongs_to"></select>
                                @error('user_belongs_to')
                                    <span class="text-danger text-12">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label class="control-label active" for="joining_date">Joining Date
                                        <small>*</small></label>
                                    <input id="joining_date" name="joining_date" type="date"
                                        class="flatpickr flatpickr-input form-control @error('joining_date') is-invalid @enderror"
                                        placeholder="yyyy-mm-dd">
                                    @error('joining_date')
                                        <span class="text-danger text-12">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label active" for="resignation_date">Resignation Date
                                        <small>*</small></label>
                                    <input id="resignation_date" name="resignation_date" type="date"
                                        class="flatpickr flatpickr-input form-control" placeholder="yyyy-mm-dd">
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label" for="phoneno">Phone No <small>*</small></label>
                                    <input id="phoneno" name="phoneno" type="tel"
                                        class="form-control @error('phoneno') is-invalid @enderror">
                                    @error('phoneno')
                                        <span class="text-danger text-12">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label" for="alterphoneno">Alternate Phone No</label>
                                    <input type="tel" class="form-control" id="alterphoneno" name="alterphoneno">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 py-4">
                    <div class="card shadow">
                        <div class="card-top ">
                            <div class="card-title mb-0">
                                <h5 class="font-weight-bold text-uppercase mb-0"><i class="fas fa-map-marker-alt"></i></h5>
                            </div>
                        </div>
                        <div class="card-body pt-0">
                            <div class="form-group">
                                <label class="control-label" for="emp_address">Address <small>*</small></label>
                                <input type="text" class="form-control @error('emp_address') is-invalid @enderror"
                                    id="emp_address" name="emp_address">
                                @error('emp_address')
                                    <span class="text-danger text-12">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="emp_city">City <small>*</small></label>
                                <input type="text" class="form-control @error('emp_city') is-invalid @enderror"
                                    id="emp_city" name="emp_city">
                                @error('emp_city')
                                    <span class="text-danger text-12">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="emp_state">State <small>*</small></label>
                                <input type="text" class="form-control @error('emp_state') is-invalid @enderror"
                                    id="emp_state" name="emp_state">
                                @error('emp_state')
                                    <span class="text-danger text-12">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="emp_postal">Postal Code <small>*</small></label>
                                <input type="text" class="form-control @error('emp_postal') is-invalid @enderror"
                                    id="emp_postal" name="emp_postal">
                                @error('emp_postal')
                                    <span class="text-danger text-12">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="aadhar">Aadhar card No </label>
                                <input id="aadhar" name="aadhar" type="number" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="pancard">Pancard No <small>*</small></label>
                                <input id="pancard" name="pancard" type="text" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <a href="{{url()->previous()}}" type="submit" class="btn btn-outline-primary btn-lg btn-large mr-2">Cancel</a>
                <button type="submit" class="btn btn-primary btn-lg btn-large">Submit</button>
            </div>
        </form>
    </section>

    {{-- add_emp_role --}}
    <div class="modal fade custom-modal" id="add_emp_role" tabindex="-1" role="dialog" aria-labelledby="add_emp_role"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content success">
                <div class="icon">
                    <span class="fa-stack fa-2x text-success">
                        <i class="fas fa-circle fa-stack-2x"></i>
                        <i class="fas fas fa-plus fa-stack-1x fa-inverse"></i>
                    </span>
                </div>
                <div class="modal-header justify-content-center">
                    <h3 class="modal-title text-capitalize font-weight-bolder mt-3">
                        Add Employee Role
                    </h3>
                </div>
                <form action="addrole" method="POST">
                    @csrf
                    <div class="modal-body pb-0">
                        <div class="row justify-content-center">
                            <div class="col-md-11">
                                <div class="form-group">
                                    <label for="add_role" class="col-form-label">Enter New Role</label>
                                    <input type="text" class="form-control @error('add_role') is-invalid @enderror"
                                        id="add_role" name="add_role">
                                    @error('add_role')
                                        <span class="text-danger text-12">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer flex-nowrap">
                        <button type="button" class="btn btn-outline-success btn-block btn-lg mt-0"
                            data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success btn-block btn-lg mt-0">Done</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @include('components.alertmodal')
@endsection
@section('script')
    @if (session()->has('message'))
        <script>
            $(document).ready(function() {
                $('#successAlert').modal("show");
            });

        </script>
    @endif
    @if (session()->has('error'))
        <script>
            $(document).ready(function() {
                $('#errorAlert').modal("show");
            });

        </script>
    @endif
    <script>
        $(document).ready(function() {
            getBranch();
            loadProductCategory();
            $('#emp_role').change(function() { //jQuery Change Function
                var opval = $(this).val(); //Get value from select element
                if (opval == "others") { //Compare it and if true
                    $('#add_emp_role').modal("show"); //Open Modal
                }
            });
            $('#user_belongs_to').change(function() { //jQuery Change Function
                var opval = $(this).val(); //Get value from select element
                if (opval == "others") { //Compare it and if true
                    return window.location.href = "/addorganization";
                }
            });
        });

        function getBranch() {
            return $.ajax({
                url: "/branchname",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    //console.log("branchname", data);
                    $('#user_belongs_to').empty();
                    $('#user_belongs_to').append('<option selected="true" disabled></option>');
                    $('#user_belongs_to').prop('selectedIndex', 0);
                    $.each(data, function(key, value) {
                        $('#user_belongs_to').append($('<option></option>').attr('value', value
                            .org_branch_name).text(value.org_branch_name));
                    });
                    $('#user_belongs_to').append('<option value="others">Others</option>');
                },
                error: function(error) {
                    console.log('branchname error:', error);
                }
            });
        }

        function loadProductCategory() {
            return $.ajax({
                url: "/emproles",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    console.log("emproles", data);
                    $('#emp_role').empty();
                    $('#emp_role').append('<option selected="true" disabled></option>');
                    $('#emp_role').prop('selectedIndex', 0);
                    $.each(data, function(key, value) {
                        $('#emp_role').append($('<option></option>').attr(
                            'value',
                            value.employee_role).text(value.employee_role));
                    });
                    $('#emp_role').append('<option value="others">Others</option>');
                },
                error: function(error) {
                    console.log('branchname error:', error);
                }
            });
        }

    </script>
@endsection
