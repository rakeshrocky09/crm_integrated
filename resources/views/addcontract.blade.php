@extends('layouts.app')
@section('mytitle', 'Add Contract')
@section('content')
    <div class="p-4">
        <section class="my-5">
            <div class="card shadow">
                <div class="card-top ">
                    <div class="card-title mb-0">
                        <h6 class="font-weight-bold text-capitalize mb-0">Add Contract</h6>
                    </div>
                </div>
                <div class="card-body pt-0">
                    <div class="row">
                        <div class="col-md-3">
                            <h6 class="font-weight-bold text-uppercase text-primary">Customer Info</h6>
                            <div class="form-group">
                                <h6 class="font-weight-bold mb-0 text-sm">Customer Name</h6>
                                <input class="custom-input" id="view_cus_name" name="view_cus_name" type="text"
                                    value="{{ $sql[0]->customer_name }}" readonly>
                            </div>
                            <div class="form-group">
                                <h6 class="font-weight-bold mb-0 text-sm">Phone No</h6>
                                <input class="custom-input" id="view_cus_phoneno" name="view_cus_phoneno" type="text"
                                    value="{{ $sql[0]->cus_phone_no }}" readonly>
                            </div>
                            <div class="form-group">
                                <h6 class="font-weight-bold mb-0 text-sm">Branch Name</h6>
                                <input class="custom-input" id="view_cus_branch" name="view_cus_branch" type="text"
                                    value="{{ $sql[0]->cus_branch_name }}" readonly>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="mb-2">
                                <h6 class="font-weight-bold text-uppercase text-primary">
                                    Product(s)
                                </h6>
                                <div class="table-responsive">
                                    <table class="table modal-table">
                                        <thead>
                                            <tr>
                                                <th scope="col">Brand Name</th>
                                                <th scope="col">Product Name</th>
                                                <th scope="col">Location</th>
                                                <th scope="col">Machine Serial No</th>
                                                <th scope="col">Installed Date</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <input class="custom-input" id="contract_cus_name"
                                                        name="contract_cus_name" type="text"
                                                        value="{{ $sql[0]->product_brand_name }}" readonly>
                                                </td>
                                                <td>
                                                    <input class="custom-input" id="product_name" name="product_name"
                                                        type="text" value="{{ $sql[0]->product_name }}" readonly>
                                                </td>
                                                <td>
                                                    <input class="custom-input" id="contract_branch_name"
                                                        name="contract_branch_name" type="text"
                                                        value="{{ $sql[0]->cus_section }}" readonly>
                                                </td>
                                                <td>
                                                    <input class="custom-input" id="contract_Machine_serial"
                                                        name="contract_Machine_serial" type="text"
                                                        value="{{ $sql[0]->Machine_Serial_No }}" readonly>
                                                </td>
                                                <td>
                                                    <input class="custom-input" id="installation_date"
                                                        name="installation_date" type="text"
                                                        value="{{ $sql[0]->installation_date }}" readonly>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="mb-2">
                                <h6 class="font-weight-bold text-uppercase text-primary mb-0">
                                    Contract Details
                                </h6>
                                <input class="form-control" value="{{ $sql[0]->cus_id }}" id="contract_cus_id"
                                    name="contract_cus_id" type="hidden">
                                <input class="form-control" value="{{ $sql[0]->product_id }}" id="contract_product_id"
                                    name="contract_product_id" type="hidden">
                                <input class="form-control" value="{{ $sql[0]->cus_phone_no }}" id="contract_cus_phoneno"
                                    name="contract_cus_phoneno" type="hidden">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="form-group col-md-6 fw-90">
                                                <label class="control-label" for="contract_type">
                                                    Contract Type
                                                </label>
                                                <select class="form-control" id="contract_type" name="contract_type">
                                                    <option selected disabled></option>
                                                    <option value="warranty">Warranty</option>
                                                    <option value="amc">AMC</option>
                                                    <option value="rental">Rental</option>
                                                    <option value="4c">4C</option>
                                                </select>
                                                <input class="form-control " id="contracttype" name="contracttype"
                                                    type="hidden">
                                            </div>
                                            <div id="warranty" class="panel col-md-6">
                                                <div class="form-group fw-90">
                                                    <label class="control-label" for="warranty_noof_month">Number of Months
                                                        <small>*</small></label>
                                                    <input type="number" class="form-control" id="warranty_noof_month"
                                                        name="warranty_noof_month">
                                                </div>
                                            </div>
                                            <div id="amc" class="panel col-md-6">
                                                <div class="form-group fw-90">
                                                    <label class="control-label" for="amc_month">Number of Months
                                                        <small>*</small></label>
                                                    <input type="number" class="form-control" id="amc_month"
                                                        name="amc_month">
                                                </div>
                                            </div>
                                            <div id="rental" class="panel col-md-12">
                                                <div class="row">
                                                    <div class="form-group fw-90 col-md-6">
                                                        <label class="control-label" id="label_blue">Credit Period(#Days)
                                                        </label>
                                                        <input type="text" class="form-control" id="rental_credit_period"
                                                            name="rental_credit_period">
                                                    </div>
                                                    <div class="form-group fw-90 col-md-6">
                                                        <label class="control-label" id="label_blue">Rental Chargers
                                                            <small>*</small></label>
                                                        <input type="text" class="form-control" id="rent_rental_charges"
                                                            name="rent_rental_charges">
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="mb-2 d-flex justify-content-between">
                                                            <label class="control-label" id="label_blue">chargers per copies
                                                                <small>*</small>
                                                            </label>
                                                            <span data-toggle="modal" data-target="#addcopies">
                                                                <button type="button"
                                                                    class="btn btn-primary btn-sm text-uppecase"
                                                                    data-toggle="tooltip" data-placement="top" title=""
                                                                    data-original-title="Add No. of Copies">
                                                                    <i class="fas fa-plus"></i> Add
                                                                </button>
                                                            </span>
                                                        </div>
                                                        <div class="table-responsive mt-4">
                                                            <table class="table modal-table"
                                                                id="rental_copies_select_table">
                                                                <thead>
                                                                    <th>Black A3</th>
                                                                    <th>Black A4</th>
                                                                    <th>Black Copies</th>
                                                                    <th>Color A3</th>
                                                                    <th>Color A4</th>
                                                                    <th>Color Copies</th>
                                                                    <th>Scanner A3</th>
                                                                    <th>Scanner A4</th>
                                                                    <th>Scanner Copies</th>
                                                                </thead>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="4c" class="panel col-md-12">
                                                <div class="row">
                                                    <div class="form-group fw-90 col-md-6">
                                                        <label class="control-label" for="c4_credit_period">Credit
                                                            Period(#Days)
                                                        </label>
                                                        <input type="text" class="form-control" id="c4_credit_period"
                                                            name="c4_credit_period">
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="mb-2 d-flex justify-content-between">
                                                            <label class="control-label">chargers per copies
                                                                <small>*</small>
                                                            </label>
                                                            <span data-toggle="modal" data-target="#addcopies">
                                                                <button type="button"
                                                                    class="btn btn-primary btn-sm text-uppecase"
                                                                    data-toggle="tooltip" data-placement="top" title=""
                                                                    data-original-title="Add No. of Copies">
                                                                    <i class="fas fa-plus"></i> Add
                                                                </button>
                                                            </span>
                                                        </div>
                                                        <div class="table-responsive mt-4">
                                                            <table class="table modal-table" id="4c_copies_select_table">
                                                                <thead>
                                                                    <th>Black A3</th>
                                                                    <th>Black A4</th>
                                                                    <th>Black Copies</th>
                                                                    <th>Color A3</th>
                                                                    <th>Color A4</th>
                                                                    <th>Color Copies</th>
                                                                    <th>Scanner A3</th>
                                                                    <th>Scanner A4</th>
                                                                    <th>Scanner Copies</th>
                                                                </thead>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="form-group fw-90 col-md-6">
                                                <label class="control-label" for="contract_product_ownership">Product
                                                    Ownership
                                                </label>
                                                <select class="form-control" id="contract_product_ownership"
                                                    name="contract_product_ownership">
                                                    <option selected disabled></option>
                                                    @foreach ($product_list as $item)
                                                        <option value="{{ $item->ownership }}">{{ $item->ownership }}
                                                        </option>
                                                    @endforeach
                                                    <option value="Others">Others</option>
                                                </select>
                                            </div>
                                            <div class="form-group fw-90 col-md-6">
                                                <label class="control-label active" for="contract_billdate">
                                                    Monthly Billing Date<small>*</small>
                                                </label>
                                                <input type="date" class="flatpickr form-control" id="contract_billdate"
                                                    name="contract_billdate" placeholder="yyyy-mm-dd">
                                            </div>
                                            <div class="form-group fw-90 col-md-6">
                                                <label for="contract_startdate" class="mb-0 control-label active">
                                                    Start Date <small>*</small>
                                                </label>
                                                <input id="contract_startdate" name="contract_startdate" type="date"
                                                    class="flatpickr form-control" placeholder="yyyy-mm-dd">
                                            </div>
                                            <div class="form-group fw-90 col-md-6">
                                                <label for="contract_end_date" class="mb-0 control-label active">
                                                    End Date <small>*</small>
                                                </label>
                                                <input id="contract_end_date" name="contract_end_date" type="date"
                                                    class="flatpickr form-control" placeholder="yyyy-mm-dd">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="mb-2">
                                <h6 class="font-weight-bold text-uppercase text-primary mb-0">
                                    PM Schedule
                                </h6>
                                <div class="row">
                                    <div class="form-group col-md-4">
                                        <label class="control-label" for="maintenance_month">
                                            Maintenance Month
                                        </label>
                                        <select class="form-control" id="maintenance_month" name="maintenance_month">
                                            <option selected disabled></option>
                                            <option value="0">0 Month</option>
                                            <option value="1">1 Month</option>
                                            <option value="2">2 Month</option>
                                            <option value="3">3 Month</option>
                                            <option value="4">4 Month</option>
                                            <option value="5">5 Month</option>
                                            <option value="6">6 Month</option>
                                            <option value="7">7 Month</option>
                                            <option value="8">8 Month</option>
                                            <option value="9">9 Month</option>
                                            <option value="10">10 Month</option>
                                            <option value="11">11 Month</option>
                                            <option value="12">12 Month</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-2">
                                <h6 class="font-weight-bold text-uppercase text-primary mb-0">
                                    Service Related Charges
                                </h6>
                                <div class="row">
                                    <div class="form-group col-md-4">
                                        <label for="contract_service_charge" class="mb-0 control-label">
                                            Service Charge(Rs) <small>*</small>
                                        </label>
                                        <input id="contract_service_charge" name="contract_service_charge" type="text"
                                            class="form-control">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="contract_install_charge" class="mb-0 control-label">
                                            Installation Charge(Rs) <small>*</small>
                                        </label>
                                        <input id="contract_install_charge" name="contract_install_charge" type="text"
                                            class="form-control">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="contract_visit_charge" class="mb-0 control-label">
                                            Visit Charge(Rs) <small>*</small>
                                        </label>
                                        <input id="contract_visit_charge" name="contract_visit_charge" type="text"
                                            class="form-control">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="contract_amount" class="mb-0 control-label">
                                            Contract Amount <small>*</small>
                                        </label>
                                        <input id="contract_amount" name="contract_amount" type="text" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="mb-2">
                                <h6 class="font-weight-bold text-uppercase text-primary mb-4">
                                    Free Spares and Consumables
                                </h6>
                                <div class="row">
                                    <div class="col-12">
                                        <label class="font-weight-bold">All Spares Free</label>
                                        <div class="mb-2 d-flex justify-content-between">
                                            <div class="d-flex align-item-center">
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input type="radio" id="spares_true" name="sparesfree"
                                                        class="custom-control-input">
                                                    <label class="custom-control-label" for="spares_true">Yes</label>
                                                </div>
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input type="radio" id="spares_false" name="sparesfree"
                                                        class="custom-control-input">
                                                    <label class="custom-control-label" for="spares_false">No</label>
                                                </div>
                                            </div>
                                            <span data-toggle="modal" data-target="#addspares">
                                                <button type="button" class="btn btn-primary btn-sm text-uppecase"
                                                    data-toggle="tooltip" data-placement="top" title=""
                                                    data-original-title="Add Free Spares">
                                                    <i class="fas fa-plus"></i> Add
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="table-responsive mt-4">
                                            <table class="table modal-table">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">Spare Id</th>
                                                        <th scope="col">Spare Code Name</th>
                                                        <th scope="col">Spare Name</th>
                                                        <th scope="col">No.Of Units</th>
                                                        <th scope="col">Unit Price</th>
                                                        <th scope="col">Reading</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="spare_select_table"></tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-12">
                                        <label class="font-weight-bold">All Consumable Free</label>
                                        <div class="mb-2 d-flex justify-content-between">
                                            <div class="d-flex align-item-center">
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input type="radio" id="consumable_true" name="consumablefree"
                                                        class="custom-control-input">
                                                    <label class="custom-control-label" for="consumable_true">Yes</label>
                                                </div>
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input type="radio" id="consumable_false" name="consumablefree"
                                                        class="custom-control-input">
                                                    <label class="custom-control-label" for="consumable_false">No</label>
                                                </div>
                                            </div>
                                            <span data-toggle="modal" data-target="#addconsumables">
                                                <button type="button" class="btn btn-primary btn-sm text-uppecase"
                                                    data-toggle="tooltip" data-placement="top" title=""
                                                    data-original-title="Add Free Consumable">
                                                    <i class="fas fa-plus"></i> Add
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="table-responsive mt-4">
                                            <table class="table modal-table">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">Consumable Id</th>
                                                        <th scope="col">Consumable Code Name</th>
                                                        <th scope="col">Consumable Name</th>
                                                        <th scope="col">No.Of Units</th>
                                                        <th scope="col">Unit Price</th>
                                                        <th scope="col">Reading</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="consumable_select_table"></tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex">
                                <button type="submit" class="btn btn-primary" onclick="contractvalidate()">Submit</button>
                                <a href="{{ url()->previous() }}" class="btn btn-primary mx-2">Cancel</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    {{-- add Spares modal --}}
    <div class="modal fade custom-modal" id="addspares" tabindex="-1" role="dialog" aria-labelledby="addspares"
        aria-hidden="true">
        <div class="modal-dialog modal-xxl modal-dialog-centered edit" role="document">
            <div class="modal-content">
                <div class="icon">
                    <span class="fa-stack fa-2x text-success">
                        <i class="fas fa-circle fa-stack-2x"></i>
                        <i class="fas fa-plus fa-stack-1x fa-inverse"></i>
                    </span>
                </div>
                <div class="modal-header justify-content-center">
                    <h3 class="modal-title text-capitalize font-weight-bolder mt-3" id="addspares">
                        Select Spares
                    </h3>
                </div>
                <div class="modal-body pb-0">
                    <div class="table-responsive">
                        <table id="spare_table" class="table modal-table">
                            <thead>
                                <tr>
                                    <th scope="col">Check/ Uncheck</th>
                                    <th scope="col">Spare ID</th>
                                    <th scope="col">Spare Code Name</th>
                                    <th scope="col">Spare Name</th>
                                    <th scope="col">Price</th>
                                    <th scope="col">Gst Percentage</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <div class="modal-footer flex-nowrap">
                    <button type="button" class="btn btn-outline-success btn-block btn-lg mt-0"
                        data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-success btn-block btn-lg mt-0"
                        onclick="spare_select()">Done</button>
                </div>
            </div>
        </div>
    </div>
    {{-- end Spares modal --}}
    {{-- add Consumable modal --}}
    <div class="modal fade custom-modal" id="addconsumables" tabindex="-1" role="dialog" aria-labelledby="addconsumables"
        aria-hidden="true">
        <div class="modal-dialog modal-xxl modal-dialog-centered edit" role="document">
            <div class="modal-content">
                <div class="icon">
                    <span class="fa-stack fa-2x text-success">
                        <i class="fas fa-circle fa-stack-2x"></i>
                        <i class="fas fa-plus fa-stack-1x fa-inverse"></i>
                    </span>
                </div>
                <div class="modal-header justify-content-center">
                    <h3 class="modal-title text-capitalize font-weight-bolder mt-3" id="addconsumables">
                        Select Consumable Product
                    </h3>
                </div>
                <div class="modal-body pb-0">
                    <div class="table-responsive">
                        <table id="consumable_table" class="table modal-table">
                            <thead>
                                <tr>
                                    <th scope="col">Check/ Uncheck</th>
                                    <th scope="col">Consumable ID</th>
                                    <th scope="col">Consumable Code Name</th>
                                    <th scope="col">Consumable Name</th>
                                    <th scope="col">Price</th>
                                    <th scope="col">Gst Percentage</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <div class="modal-footer flex-nowrap">
                    <button type="button" class="btn btn-outline-success btn-block btn-lg mt-0"
                        data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-success btn-block btn-lg mt-0"
                        onclick="consumable_select()">Done</button>
                </div>
            </div>
        </div>
    </div>
    {{-- end Consumable modal --}}
    {{-- addcopies modal --}}
    <div class="modal fade custom-modal" id="addcopies" tabindex="-1" role="dialog" aria-labelledby="addcopies"
        aria-hidden="true">
        <div class="modal-dialog modal-xl  modal-dialog-centered edit" role="document">
            <div class="modal-content">
                <div class="icon">
                    <span class="fa-stack fa-2x text-success">
                        <i class="fas fa-circle fa-stack-2x"></i>
                        <i class="fas fa-plus fa-stack-1x fa-inverse"></i>
                    </span>
                </div>
                <div class="modal-header justify-content-center">
                    <h3 class="modal-title text-capitalize font-weight-bolder mt-3" id="addconsumables">
                        Select Consumable Product
                    </h3>
                </div>
                <form id="add_copies_form">
                    <div class="modal-body pb-0">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label" for="install_blacka3">Black A3</label>
                                    <input type="text" class="form-control" id="install_blacka3" name="install_blacka3">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label" for="install_blacka4">Black A4</label>
                                    <input type="text" class="form-control" id="install_blacka4" name="install_blacka4">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label" for="black_copies">No.of Free Copies</label>
                                    <input type="text" class="form-control" id="black_copies" name="black_copies">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="install_colora3" class="control-label">Color A3</label>
                                    <input type="text" class="form-control" id="install_colora3" name="install_colora3">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="install_colora4" class="control-label">Color A4</label>
                                    <input type="text" class="form-control" id="install_colora4" name="install_colora4">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="color_copies" class="control-label">No.of Free Copies</label>
                                    <input type="text" class="form-control" id="color_copies" name="color_copies">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="install_scanner3" class="control-label">Scanner A3</label>
                                    <input type="text" class="form-control" id="install_scanner3" name="install_scanner3">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="install_scanner4" class="control-label">Scanner A4</label>
                                    <input type="text" class="form-control" id="install_scanner4" name="install_scanner4">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="scanner_copies" class="control-label">No.of Free Copies</label>
                                    <input type="text" class="form-control" id="scanner_copies"
                                        name="scanner_copies">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer flex-nowrap">
                        <button type="button" class="btn btn-outline-success btn-block btn-lg mt-0"
                            data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-success btn-block btn-lg mt-0"
                            onclick="reading_details()">Done</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- end addcopies modal --}}
    {{-- add_product_brand --}}
    <div class="modal fade custom-modal" id="add_ownership" tabindex="-1" role="dialog" aria-labelledby="add_ownership"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content success">
                <div class="icon">
                    <span class="fa-stack fa-2x text-success">
                        <i class="fas fa-circle fa-stack-2x"></i>
                        <i class="fas fas fa-plus fa-stack-1x fa-inverse"></i>
                    </span>
                </div>
                <div class="modal-header justify-content-center">
                    <h3 class="modal-title text-capitalize font-weight-bolder mt-3">
                        Add Product Ownership
                    </h3>
                </div>
                <form action="{{ url('addownership') }}" method="POST">
                    @csrf
                    <div class="modal-body pb-0">
                        <div class="row justify-content-center">
                            <div class="col-md-11">
                                <div class="form-group">
                                    <label for="new_ownership" class="col-form-label">Enter Product Ownership Name</label>
                                    <input type="text" class="form-control" id="new_ownership" name="new_ownership">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer flex-nowrap">
                        <button type="button" class="btn btn-outline-success btn-block btn-lg mt-0"
                            data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success btn-block btn-lg mt-0">Done</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @include('components.alertmodal')
@endsection
@section('script')
    @if (session()->has('message'))
        <script>
            $(document).ready(function() {
                $('#successAlert').modal("show");
            });

        </script>
    @endif
    @if (session()->has('error'))
        <script>
            $(document).ready(function() {
                $('#errorAlert').modal("show");
            });

        </script>
    @endif
    <script src="{{ URL::asset('assets/js/script.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#contract_product_ownership').change(function() { //jQuery Change Function
                var opval = $(this).val(); //Get value from select element
                if (opval == "Others") { //Compare it and if true
                    $('#add_ownership').modal("show"); //Open Modal
                }
            });
            $("#contract_type").change(function() {
                $(this).find("option:selected").each(function() {
                    var optionValue = $(this).attr("value");
                    if (optionValue) {
                        $(".panel").not("#" + optionValue).hide();
                        $("#" + optionValue).show();
                    } else {
                        $(".panel").hide();
                    }
                });
            }).change();

            loadspares();
            loadconsumable();
        });

        function loadspares() {
            return $.ajax({
                url: "/loadspares",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    //console.log("loadspares", data);
                    $('#spareTableBody').remove();
                    var spare = '';
                    spare += '<tbody id="spareTableBody">';
                    if (data == "") {
                        spare += '<tr>';
                        spare += '<td>No Data</td>';
                        spare += '</tr>';
                    } else {
                        $.each(data, function(key, value) {
                            spare += '<tr>';
                            var check =
                                `<td class='td-actions'><div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input" id="${value.spare_const + value.spare_id}" name="spares" value='${JSON.stringify(value)}'> <label class="custom-control-label" for="${value.spare_const + value.spare_id}"></label></div></td>`

                            spare += check;
                            spare += '<td>' + value.spare_const + value.spare_id + '</td>';
                            spare += '<td>' + value.spare_code_name + '</td>';
                            spare += '<td>' + value.spare_model_name + '</td>';
                            spare += '<td>' + value.spare_unit_price + '</td>';
                            spare += '<td>' + value.Gst_Percentage + '</td>';
                            spare += '</tr>';
                        });
                    }
                    spare += '</tbody>';
                    $('#spare_table').append(spare);
                },
                error: function(error) {
                    console.log('loadspares error:', error);
                }
            });
        }

        function loadconsumable() {
            return $.ajax({
                url: "/loadconsumable",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    // console.log("loadconsumable", data);
                    $('#consumableTableBody').remove();
                    var consumable = '';
                    consumable += '<tbody id="consumableTableBody">';
                    if (data == "") {
                        consumable += '<tr>';
                        consumable += '<td>No Data</td>';
                        consumable += '</tr>';
                    } else {
                        $.each(data, function(key, value) {
                            consumable += '<tr>';
                            var check =
                                `<td class='td-actions'><div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input" id="${value.consumable_const + value.consumable_id}" name="consumable" value='${JSON.stringify(value)}'> <label class="custom-control-label" for="${value.consumable_const + value.consumable_id}"></label></div></td>`

                            consumable += check;
                            consumable += '<td>' + value.consumable_const + value.consumable_id +
                                '</td>';
                            consumable += '<td>' + value.consumable_code_name + '</td>';
                            consumable += '<td>' + value.consumable_name + '</td>';
                            consumable += '<td>' + value.consumable_unit_price + '</td>';
                            consumable += '<td>' + value.consumable_gst_percentage + '</td>';
                            consumable += '</tr>';
                        });
                    }
                    consumable += '</tbody>';
                    $('#consumable_table').append(consumable);
                },
                error: function(error) {
                    console.log('loadconsumable error:', error);
                }
            });
        }

        function spare_select() {
            var lineItemArray = [];
            $.each($("input[name='spares']:checked"), function(index) {
                var lineItem = JSON.parse($(this).val());
                lineItemArray.push({
                    id: lineItem.spare_const + lineItem.spare_id,
                    code: lineItem.spare_code_name,
                    name: lineItem.spare_model_name,
                    units: lineItem.spare_parts_unit,
                    price: lineItem.spare_unit_price
                });
            });
            $('input[name=spares]').prop('checked', false);
            $('#addspares').modal('hide');
            console.log('lineItemArray:', lineItemArray);
            add_to_spare(lineItemArray);
        }

        function consumable_select() {
            var lineItemArray = [];
            $.each($("input[name='consumable']:checked"), function(index) {
                var lineItem = JSON.parse($(this).val());
                lineItemArray.push({
                    id: lineItem.consumable_id,
                    code: lineItem.consumable_code_name,
                    name: lineItem.consumable_name,
                    units: lineItem.consumable_unit,
                    price: lineItem.consumable_unit_price
                });
            });
            $('input[name=consumable]').prop('checked', false);
            $('#addconsumables').modal('hide');
            //console.log('lineItemArray:', lineItemArray);
            add_to_consumable(lineItemArray);
        }

        function add_to_spare(lineItemArray) {
            var rowCount = $('#spare_select_table >tr').length;
            var i = rowCount + 1
            var spare = "";
            $.each(lineItemArray, function(key, value) {
                spare += '<tr>';
                spare += "<td id='spare_id" + i + "'>" + value.id + "</td>";
                spare += "<td id='spare_codename" + i + "'>" + value.code + "</td>";
                spare += "<td id='spare_name" + i + "'>" + value.name + "</td>";
                spare += "<td id='spare_noofunits" + i + "'>" + value.units + "</td>";
                spare += "<td id='spare_unitprice" + i + "'>" + value.price + "</td>";
                var reading = "<td class='td-actions form-group'><input type='text' id='spare_reading" + i +
                    "' class='form-control'> </td>";
                spare += reading;
                spare += '</tr>';
                i++;
            });
            $('#spare_select_table').append(spare);
        }

        function add_to_consumable(lineItemArray) {
            var rowCount = $('#consumable_select_table >tr').length;
            var i = rowCount + 1
            var consumable = "";
            $.each(lineItemArray, function(key, value) {
                consumable += '<tr>';
                consumable += "<td id='consumable_id" + i + "'>" + value.id + "</td>";
                consumable += "<td id='consumable_codename" + i + "'>" + value.code + "</td>";
                consumable += "<td id='consumable_name" + i + "'>" + value.name + "</td>";
                consumable += "<td id='consumable_noofunit" + i + "'>" + value.units + "</td>";
                consumable += "<td id='consumable_unitprice" + i + "'>" + value.price + "</td>";
                var reading = "<td class='td-actions form-group'><input type='text' id='consumable_reading" +
                    i +
                    "' class='form-control'> </td>";
                consumable += reading;
                consumable += '</tr>';
                i++;
            });
            $('#consumable_select_table').append(consumable);
        }

        function reading_details() {
            var lineItemArray = [];
            var lineItem = form_to_json("add_copies_form");
            lineItemArray.push({
                install_blacka3: lineItem.install_blacka3,
                install_blacka4: lineItem.install_blacka4,
                edit_black_copies: lineItem.edit_black_copies,
                install_colora3: lineItem.install_colora3,
                install_colora4: lineItem.install_colora4,
                edit_color_copies: lineItem.edit_color_copies,
                install_scanner3: lineItem.install_scanner3,
                install_scanner4: lineItem.install_scanner4,
                edit_scanner_copies: lineItem.edit_scanner_copies
            });
            console.log('lineItemArray:', lineItemArray);
            $('#addcopies').modal('hide');
            add_to_copies(lineItemArray);
        }

        function add_to_copies(lineItemArray) {
            var inputVal = document.getElementById("contract_type").value;
            $('#copyBody').remove();
            var copydata = "";
            copydata += '<tbody id="copyBody">';

            $.each(lineItemArray, function(key, value) {
                copydata += '<tr>';
                copydata += "<td id='install_blacka3'>" + value.install_blacka3 + "</td>";
                copydata += "<td id='install_blacka4'>" + value.install_blacka4 + "</td>";
                copydata += "<td id='edit_black_copies'>" + value.edit_black_copies + "</td>";
                copydata += "<td id='install_colora3'>" + value.install_colora3 + "</td>";
                copydata += "<td id='install_colora4'>" + value.install_colora4 + "</td>";
                copydata += "<td id='edit_color_copies'>" + value.edit_color_copies + "</td>";
                copydata += "<td id='install_scanner3'>" + value.install_scanner3 + "</td>";
                copydata += "<td id='install_scanner4'>" + value.install_scanner4 + "</td>";
                copydata += "<td id='edit_scanner_copies'>" + value.edit_scanner_copies + "</td>";
                copydata += "<td id='inputval' hidden>" + inputVal + "</td>";
                copydata += '</tr>';
            });
            copydata += '</tbody>';
            if (inputVal == '4c') {
                $('#4c_copies_select_table').append(copydata);
            } else {
                $('#rental_copies_select_table').append(copydata);
            }
        }



        function contractvalidate() {
            var rowCount1 = $('#spare_select_table >tr').length;
            var eleIDs1 = "";
            for (var i = 0; i < rowCount1; i++) {
                var index = i + 1;
                eleIDs1 += "#spare_id" + index + ", " + "#spare_codename" + index + ", " + "#spare_name" + index +
                    ", " + "#spare_noofunits" + index + ", " + "#spare_unitprice" + index + ", " + "#spare_reading" +
                    index + ", ";
            }
            var rowCount2 = $('#consumable_select_table  >tr').length;
            var eleIDs2 = "";
            for (var i = 0; i < rowCount2; i++) {
                var index = i + 1;
                eleIDs2 += "#consumable_id" + index + ", " + "#consumable_codename" + index + ", " + "#consumable_name" +
                    index + ", " + "#consumable_noofunit" + index + ", " + "#consumable_unitprice" + index + ", " +
                    "#consumable_reading" + index + ", ";
            }
            var eleIDs3 = "";
            eleIDs3 += "#install_blacka3" + ", " + "#install_blacka4" + ", " + "#edit_black_copies" + ", " +
                "#install_colora3" + ", " + "#install_colora4" + ", " + "#edit_color_copies" + ", " + "#install_scanner3" +
                ", " + "#install_scanner4" + ", " + "#edit_scanner_copies" + ", " + "#contracttype" + ",";

            let $items = $(eleIDs1 + eleIDs2 + eleIDs3 +
                `#contract_branch_name, #contract_cus_id, #contract_product_id, #contract_Machine_serial, #installation_date, #product_name, #contract_type, #warranty_noof_month, #amc_month, #rent_rental_charges,#contract_product_ownership, #contract_startdate,#contract_end_date, #contract_billdate, #c4_credit_period,#rental_credit_period,#contract_service_charge,#contract_install_charge, #contract_visit_charge, #contract_amount, #consumables_free,#maintenance_month`
            )
            let keyValue = this.createReqObject($items);

            let request = {
                "branch_name": keyValue["contract_branch_name"],
                "customer_id": keyValue["contract_cus_id"],
                "product_id": keyValue["contract_product_id"],
                "machine_serialno": keyValue["contract_Machine_serial"],
                "installation_date": keyValue["installation_date"],
                "product_name": keyValue["product_name"]
            };

            nest(request, ["contract", "maintenance_month"], keyValue["maintenance_month"]);
            nest(request, ["contract", "contract_type"], keyValue["contract_type"]);
            nest(request, ["contract", "product_ownership"], keyValue["contract_product_ownership"]);
            nest(request, ["contract", "startdate"], keyValue["contract_startdate"]);
            nest(request, ["contract", "end_date"], keyValue["contract_end_date"]);
            nest(request, ["contract", "billdate"], keyValue["contract_billdate"]);
            nest(request, ["contract", "service_charge"], keyValue["contract_service_charge"]);
            nest(request, ["contract", "install_charge"], keyValue["contract_install_charge"]);
            nest(request, ["contract", "visit_charge"], keyValue["contract_visit_charge"]);
            nest(request, ["contract", "warranty_month"], keyValue["warranty_noof_month"]);
            nest(request, ["contract", "amc_month"], keyValue["amc_month"]);
            nest(request, ["contract", "rent_rental_charges"], keyValue["rent_rental_charges"]);
            nest(request, ["contract", "contract_amount"], keyValue["contract_amount"]);
            nest(request, ["contract", "c4_credit_period"], keyValue["rental_credit_period"]);
            nest(request, ["contract", "rent_credit_period"], keyValue["rental_credit_period"]);
            //undefined fields
            //nest(request, ["contract", "consumable_free"], keyValue["consumables_free"]);
            //nest(request, ["contract", "c4_copies"], keyValue["c4_charges_copies"]);
            //nest(request, ["contract", "credit_period"], keyValue["contract_credit_period"]);
            //nest(request, ["contract", "spare_free"], keyValue["spares_free"]);
            //nest(request, ["contract", "c4_pages"], keyValue["c4_noof_pages"]);
            //nest(request, ["contract", "rental_pages"], keyValue["rental_pages"]);
            //nest(request, ["contract", "rental_percopies"], keyValue["rental_per_copies"]);
            //nest(request, ["contract", "c4_rental_charges"], keyValue["c4_rental_charges"]);

            let lineItems = {};
            let spares = [];
            let consumables = [];

            let input = nest(request, ["contract", "inputval"], keyValue["contracttype"]);
            console.log('contract_type:', input.contract.contract_type);
            var contract_type = input.contract.contract_type;
            $('#contracttype').val(contract_type);


            if (input.contract.contract_type == "rental") {

                nest(request, ["contract", "rental_install_blacka3"], keyValue["install_blacka3"]);
                nest(request, ["contract", "rental_install_blacka4"], keyValue["install_blacka4"]);
                nest(request, ["contract", "rental_black_copies"], keyValue["black_copies"]);
                nest(request, ["contract", "rental_install_scanner3"], keyValue["install_scanner3"]);
                nest(request, ["contract", "rental_install_colora3"], keyValue["install_colora3"]);
                nest(request, ["contract", "rental_install_colora4"], keyValue["install_colora4"]);
                nest(request, ["contract", "rental_color_copies"], keyValue["color_copies"]);
                nest(request, ["contract", "rental_install_scanner4"], keyValue["install_scanner4"]);
                nest(request, ["contract", "rental_scanner_copes"], keyValue["scanner_copies"]);
                nest(request, ["contract", "rental_inputval"], keyValue["contracttype"]);
            } else {

                nest(request, ["contract", "fourc_install_blacka3"], keyValue["install_blacka3"]);
                nest(request, ["contract", "fourc_install_blacka4"], keyValue["install_blacka4"]);
                nest(request, ["contract", "fourc_black_copies"], keyValue["black_copies"]);
                nest(request, ["contract", "fourc_install_scanner3"], keyValue["install_scanner3"]);
                nest(request, ["contract", "fourc_install_colora3"], keyValue["install_colora3"]);
                nest(request, ["contract", "fourc_install_colora4"], keyValue["install_colora4"]);
                nest(request, ["contract", "fourc_color_copies"], keyValue["color_copies"]);
                nest(request, ["contract", "fourc_install_scanner4"], keyValue["install_scanner4"]);
                nest(request, ["contract", "fourc_scanner_copes"], keyValue["scanner_copies"]);
                nest(request, ["contract", "fourc_inputval"], keyValue["contracttype"]);
            }


            for (var i = 0; i < rowCount1; i++) {
                let index = i + 1;
                let lineItem = {};
                nest(lineItem, ["id"], keyValue["spare_id" + index]);
                nest(lineItem, ["codename"], keyValue["spare_codename" + index]);
                nest(lineItem, ["name"], keyValue["spare_name" + index]);
                nest(lineItem, ["units"], keyValue["spare_noofunits" + index]);
                nest(lineItem, ["price"], keyValue["spare_unitprice" + index]);
                nest(lineItem, ["reading"], keyValue["spare_reading" + index]);
                spares.push(lineItem);
            }

            for (var i = 0; i < rowCount2; i++) {
                let index = i + 1;
                let lineItem = {};
                nest(lineItem, ["cons_id"], keyValue["consumable_id" + index]);
                nest(lineItem, ["cons_codename"], keyValue["consumable_codename" + index]);
                nest(lineItem, ["cons_name"], keyValue["consumable_name" + index]);
                nest(lineItem, ["cons_units"], keyValue["consumable_noofunit" + index]);
                nest(lineItem, ["cons_price"], keyValue["consumable_unitprice" + index]);
                nest(lineItem, ["cons_reading"], keyValue["consumable_reading" + index]);
                consumables.push(lineItem);
            }
            nest(lineItems, ["spares"], spares);
            nest(lineItems, ["consumables"], consumables);
            //nest(lineItems, ["rental"], rental);
            nest(request, ["lineItems"], lineItems);

            let jsonStr = request;
            console.log('jsonStr:', jsonStr);

            $.ajax({
                url: "/addcontract",
                type: "POST",
                data: jsonStr,
                dataType: 'json',
                success: function(data) {
                    console.log("data:", data);
                    if (data == "success") {
                        console.log("data: ", data);
                        return window.location.href = "/viewcontract";
                    }
                    if (data == "error") {
                        console.log("contract added error");
                    }
                },
                error: function(error) {
                    console.log('addcontract error:', error);
                }
            });
        }

        var nest = function(obj, keys, v) {
            if (keys.length === 1) {
                obj[keys[0]] = v;
            } else {
                var key = keys.shift();
                obj[key] = nest(typeof obj[key] === 'undefined' ? {} : obj[key], keys, v);
            }
            return obj;
        };

        function createReqObject($inItems) {
            var obj = {}
            $inItems.each(function() {
                if ($(this)[0].tagName == "INPUT" || $(this)[0].tagName == "SELECT") obj[this.id] = $(this).val();
                else obj[this.id] = $(this).text();
            });
            return obj;
        }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

    </script>
@endsection
