<div class="sidebar">
    <div class="sidebar-header">
        <a class="logo" href="{{ url('/') }}">
            <img class="lg" src="{{ asset('/assets/img/logo.png') }}" width="70" alt="Boffo CRM">
            <img class="sm" src="{{ asset('/assets/img/logo-sm.png') }}" width="25" alt="Boffo CRM">
        </a>
    </div>
    <div class="sidebar-menu">
        <ul class="main-menu py-3 profile-dropdown">
            <li class="menu-list dropdown">
                <a class="menu-item profile-logo" href="#">
                    <span class="menu-label text-center">
                        <img alt="user-img" class="rounded-circle lg" src="{{ asset('/assets/img/user1.jpg') }}"
                            width="70">
                        <img alt="user-img" class="rounded-circle sm" src="{{ asset('/assets/img/user1.jpg') }}"
                            width="30">
                    </span>
                    <div class="user-info text-center">
                        <h4 class="font-weight-semibold mt-3 mb-0">Petey Cruiser</h4>
                        <span class="mb-0 text-muted">Premium Member</span>
                    </div>
                </a>
                <ul class="sidebar-submenu">
                    <li>
                        <a href="{{url('/viewprofile')}}" class="submenu-item">
                            <span class="menu-icon mini-label material-icons">person</span>
                            <span class="menu-label">View Profile</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('/changepassword')}}" class="submenu-item">
                            <span class="menu-icon mini-label material-icons">lock</span>
                            <span class="menu-label">Change password</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('/logout')}}" class="submenu-item">
                            <span class="menu-icon mini-label material-icons">logout</span>
                            <span class="menu-label">Logout</span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
        <ul class="main-menu py-3">

            <li class="menu-list">
                <a class="menu-item active" href="{{url('/dashboard')}}">
                    <span class="menu-icon material-icons">dashboard </span>
                    <span class="menu-label">Dashboard</span>
                </a>
            </li>
            <li class="menu-list dropdown">
                <a class="menu-item" href="#">
                    <span class="menu-icon material-icons">location_city</span>
                    <span class="menu-label">Organization</span>
                </a>
                <ul class="sidebar-submenu">
                    <li class="menu-list dropdown">
                        <a class="submenu-item" href="#">
                            <span class="menu-icon mini-label text-14">MO</span>
                            <span class="menu-label">Manage Organization </span>
                        </a>
                        <ul class="sidebar-submenu nested">
                            <li>
                                <a class="submenu-item" href="{{url('/vieworganization')}}">
                                    <span class="menu-icon mini-label">VM</span>
                                    <span class="menu-label">View Organization</span>
                                </a>
                            </li>
                            <li>
                                <a class="submenu-item" href="{{url('/createuser')}}">
                                    <span class="menu-icon mini-label">CU</span>
                                    <span class="menu-label">Create user</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="menu-list">
                        <a class="submenu-item" href="{{url('/viewuser')}}">
                            <span class="menu-icon mini-label">ED</span>
                            <span class="menu-label"> Employee Details </span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="menu-list dropdown">
                <a class="menu-item" href="#">
                    <span class="menu-icon material-icons">add_shopping_cart</span>
                    <span class="menu-label"> Purchases Details </span>
                </a>
                <ul class="sidebar-submenu">
                    <li class="menu-list">
                        <a class="submenu-item" href="{{url('/viewpurchase_product')}}">
                            <span class="menu-icon mini-label">P</span>
                            <span class="menu-label"> Products </span>
                        </a>
                    </li>
                    <li class="menu-list">
                        <a href="{{url('/viewpurchase_spares')}}" class="submenu-item">
                            <span class="menu-icon mini-label material-icons">location_city</span>
                            <span class="menu-label"> Spares </span>
                        </a>
                    </li>
                    <li class="menu-list">
                        <a href="{{url('/viewpurchase_consumable')}}" class="submenu-item">
                            <span class="menu-icon mini-label">C</span>
                            <span class="menu-label"> Consumables </span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="menu-list dropdown">
                <a class="menu-item" href="#">
                    <span class="menu-icon material-icons">help_outline</span>
                    <span class="menu-label"> Inventory Management </span>
                </a>
                <ul class="sidebar-submenu">
                    <li class="menu-list">
                        <a href="{{url('/viewproduct')}}" class="submenu-item">
                            <span class="menu-icon mini-label">P</span>
                            <span class="menu-label">Products</span>
                        </a>
                    </li>
                    <li class="menu-list">
                        <a href="{{url('/viewspares')}}" class="submenu-item">
                            <span class="menu-icon mini-label material-icons">location_city</span>
                            <span class="menu-label">Spares</span>
                        </a>
                    </li>
                    <li class="menu-list">
                        <a href="{{url('/viewconsumable')}}" class="submenu-item">
                            <span class="menu-icon mini-label">C</span>
                            <span class="menu-label">Consumables</span>
                        </a>
                    </li>
                    <li class="menu-list">
                        <a href="{{url('/viewservices')}}" class="submenu-item">
                            <span class="menu-icon mini-label">S</span>
                            <span class="menu-label">Services</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="menu-list dropdown">
                <a class="menu-item" href="#">
                    <span class="menu-icon material-icons">perm_contact_calendar</span>
                    <span class="menu-label">Customer</span>
                </a>
                <ul class="sidebar-submenu">
                    <li class="menu-list">
                        <a href="{{url('/addcustomer')}}" class="submenu-item">
                            <span class="menu-icon mini-label">AC</span>
                            <span class="menu-label">Add Customer</span>
                        </a>
                    </li>
                    <li class="menu-list">
                        <a href="{{url('/viewcustomer')}}" class="submenu-item">
                            <span class="menu-icon mini-label">VC</span>
                            <span class="menu-label">View Customer</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="menu-list dropdown">
                <a class="menu-item" href="#">
                    <span class="menu-icon material-icons">queue</span>
                    <span class="menu-label">Contract</span>
                </a>
                <ul class="sidebar-submenu">
                    <li class="menu-list">
                        <a href="{{url('/viewcontract')}}" class="submenu-item">
                            <span class="menu-icon mini-label">VC</span>
                            <span class="menu-label"> View Contract </span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="menu-list dropdown">
                <a class="menu-item" href="#">
                    <span class="menu-icon material-icons">help_outline</span>
                    <span class="menu-label"> Enquiry </span>
                </a>
                <ul class="sidebar-submenu">
                    <li class="menu-list">
                        <a href="{{url('/addenquiry')}}" class="submenu-item">
                            <span class="menu-icon mini-label">AE</span>
                            <span class="menu-label"> Add Enquiry </span>
                        </a>
                    </li>
                    <li class="menu-list">
                        <a href="{{url('/viewenquiry')}}" class="submenu-item">
                            <span class="menu-icon mini-label">VE</span>
                            <span class="menu-label"> View Enquiry </span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="menu-list dropdown">
                <a class="menu-item" href="#">
                    <span class="menu-icon material-icons">local_play</span>
                    <span class="menu-label"> Tickets </span>
                </a>
                <ul class="sidebar-submenu">
                    <li class="menu-list">
                        <a href="{{url('/viewticket')}}" class="submenu-item">
                            <span class="menu-icon mini-label">VT</span>
                            <span class="menu-label"> View Tickets </span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="menu-list">
                <a class="menu-item" href="{{url('/datamigration')}}">
                    <span class="menu-icon material-icons">schedule</span>
                    <span class="menu-label"> Data Migration </span>
                </a>
            </li>
            <li class="menu-list dropdown">
                <a class="menu-item" href="#">
                    <span class="menu-icon material-icons">receipt</span>
                    <span class="menu-label"> Invoice </span>
                </a>
                <ul class="sidebar-submenu">
                    <li class="menu-list">
                        <a href="{{url('/addinvoice')}}" class="submenu-item">
                            <span class="menu-icon mini-label">AI</span>
                            <span class="menu-label"> Add Invoice </span>
                        </a>
                    </li>
                    <li class="menu-list">
                        <a href="{{url('/viewinvoice')}}" class="submenu-item">
                            <span class="menu-icon mini-label">VI</span>
                            <span class="menu-label"> View Invoice </span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="menu-list dropdown">
                <a class="menu-item" href="#">
                    <span class="menu-icon material-icons">receipt</span>
                    <span class="menu-label"> Delivery Challan </span>
                </a>
                <ul class="sidebar-submenu">
                    <li class="menu-list">
                        <a href="{{url('/adddc')}}" class="submenu-item">
                            <span class="menu-icon mini-label">ADC</span>
                            <span class="menu-label"> Add Delivery Challan </span>
                        </a>
                    </li>
                    <li class="menu-list">
                        <a href="{{url('/viewdc')}}" class="submenu-item">
                            <span class="menu-icon mini-label">VDC</span>
                            <span class="menu-label"> View Delivery Challan </span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="menu-list dropdown">
                <a class="menu-item" href="#">
                    <span class="menu-icon material-icons">receipt</span>
                    <span class="menu-label"> Receipt </span>
                </a>
                <ul class="sidebar-submenu">
                    <li class="menu-list">
                        <a href="{{url('/viewreceipt')}}" class="submenu-item">
                            <span class="menu-icon mini-label">VR</span>
                            <span class="menu-label"> View Receipt </span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="menu-list dropdown">
                <a class="menu-item" href="#">
                    <span class="menu-icon material-icons">monetization_on</span>
                    <span class="menu-label"> Expenses </span>
                </a>
                <ul class="sidebar-submenu">
                    <li class="menu-list">
                        <a href="{{url('/addexpenses')}}" class="submenu-item">
                            <span class="menu-icon mini-label">AE</span>
                            <span class="menu-label"> Add Expenses </span>
                        </a>
                    </li>
                    <li class="menu-list">
                        <a href="{{url('/viewexpenses')}}" class="submenu-item">
                            <span class="menu-icon mini-label">VE</span>
                            <span class="menu-label"> View Expenses </span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="menu-list">
                <a class="menu-item" href="{{url('/pmsschedule')}}">
                    <span class="menu-icon material-icons">schedule</span>
                    <span class="menu-label"> PM Schedule </span>
                </a>
            </li>
            <li class="menu-list">
                <a class="menu-item" href="{{url('/paymentfollowup')}}">
                    <span class="menu-icon material-icons">date_range</span>
                    <span class="menu-label"> Payment Followup </span>
                </a>
            </li>
            <li class="menu-list">
                <a class="menu-item" href="{{url('/')}}">
                    <span class="menu-icon material-icons">place</span>
                    <span class="menu-label"> Technician Location </span>
                </a>
            </li>
            <li class="menu-list pb-5 mb-5">
                <a class="menu-item" href="{{url('/reports')}}">
                    <span class="menu-icon material-icons">assignment</span>
                    <span class="menu-label"> Reports </span>
                </a>
            </li>
        </ul>
    </div>
</div>
