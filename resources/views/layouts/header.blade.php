<nav class="navbar navbar-expand-md navbar-light bg-white">
    <div class="collapse navbar-collapse">
        <!-- Left Side Of Navbar -->
        <ul class="navbar-nav navbar-menu mr-auto text-uppercase font-weight-bold">
            <li class="nav-item mr-3">
                <button id="nav-toggle" class="btn btn-default switch" type="submit">
                    <i class="fas fa-align-left"></i>
                </button>
            </li>
            <li class="nav-item">
                <form class="navbar-form" role="search">
                    <div class="input-group search-bar">
                        <input class="form-control" placeholder="Search" name="srch-term" id="srch-term" type="search">
                        <div class="input-group-btn">
                            <button class="btn btn-default" type="submit"><i class="fas fa-search"></i></button>
                        </div>
                    </div>
                </form>
            </li>
        </ul>
        <!-- Right Side Of Navbar -->
        <ul class="navbar-nav nav-right">
            <li class="nav-item">
                <a class="nav-link" href="/dashboard"><i class="fas fa-qrcode"></i></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/viewcustomer"><i class="far fa-user"></i></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/viewticket"><i class="fas fa-ticket-alt"></i></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/viewenquiry"><i class="far fa-question-circle"></i></a>
            </li>
            <li class="nav-item">
                <div class="dropdown user-dropdown">
                    <button class="dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                        <img src="{{ asset('/assets/img/user1.jpg') }}" width="37" alt="user1" class="rounded-circle">
                    </button>
                    <div class="dropdown-menu dropdown-menu-right text-capitalize" aria-labelledby="dropdownMenuButton">
                        <div class="header-profile bg-primary p-3">
                            <div class="d-flex wd-100p">
                                <div class="main-img-user">
                                    <img src="{{ asset('/assets/img/user1.jpg') }}" width="45" alt="user1"
                                        class="rounded-circle">
                                </div>
                                <div class="ml-3 my-auto">
                                    <h6 class="text-white mb-0 font-weight-bold">Petey Cruiser</h6>
                                    <span class="small">Premium Member</span>
                                </div>
                            </div>
                        </div>
                        <a class="dropdown-item" href="/viewprofile"><i class="fas fa-user icon"></i>View Profile</a>
                        <a class="dropdown-item" href="/changepassword"><i class="fas fa-lock icon"></i>Change password</a>
                        <a class="dropdown-item" href="/login"><i class="fas fa-sign-out-alt icon"></i>Logout</a>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</nav>
