@extends('layouts.app')
@section('mytitle', 'Reports')
@section('content')
    <div class="bg-top bg-gray p-4">
        <div class="d-flex">
            <h5 class="font-weight-bold text-uppercase mb-0">Reports Query</h5>
        </div>
    </div>

    <section class="my-4 p-4 card-main">
        <div class="row">
            <div class="col-md-12 py-4">
                <div class="card shadow">
                    <div class="card-top ">
                        <div class="card-title mb-0">
                            <h5 class="font-weight-bold text-uppercase mb-0"><i class="far fa-clipboard"></i></h5>
                        </div>
                    </div>
                    <div class="card-body pt-0">
                        <form class="row">
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label active" for="report_type">Report on</label>
                                            <select class="form-control" id="report_type" name="report_type">
                                                <option value="" selected disabled>Select</option>
                                                <option value="ticket"> Tickets </option>
                                                <option value="contract"> Contract </option>
                                                <option value="enquiry"> Enquiry </option>
                                                <option value="employee"> Employees </option>
                                                <option value="pm"> PM Schedule </option>
                                                <option value="invoice"> Invoice </option>
                                                <option value="dc"> Delivery Challan </option>
                                                <option value="products"> Products </option>
                                                <option value="spares"> Spares</option>
                                                <option value="consumables"> Consumables </option>
                                                <option value="services"> Services </option>
                                                <option value="serials"> Machine Serial</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4" id="employe">
                                        <div class="form-group">
                                            <label for="employee_type" class="control-label active">Based on
                                                Employee</label>
                                            <select class="form-control" id="employee_type" name="employee_type">
                                                <option value="" selected disabled>Select</option>
                                                <option value="install_emp"> Installation </option>
                                                <option value="enquiry_emp"> Enquiry </option>
                                                <option value="pm_emp"> PM Schedule </option>
                                                <option value="ticket_emp"> Ticket </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4" id="serial">
                                        <div class="form-group">
                                            <label for="machine_type" class="control-label active">Based on Serial
                                                No.</label>
                                            <select class="form-control" id="machine_type" name="machine_type">
                                                <option value="" selected disabled>Select</option>
                                                <option value="product"> Product </option>
                                                <option value="contract"> Contract </option>
                                                <option value="ticket"> Ticket </option>
                                                <option value="invoice"> Invoice </option>
                                                <option value="delivery"> Delivery </option>
                                                <option value="consumables"> Consumables </option>
                                                <option value="spares"> Spares </option>
                                                <option value="services"> Services </option>
                                                <option value="pms"> PMS </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4" id="reportss">
                                        <div class="form-group">
                                            <label class="control-label active">Based on</label>
                                            <select class="form-control" id="report_category" name="report_category">
                                                <option value="" selected disabled>Select</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4" id="machine">
                                        <div class="form-group">
                                            <label for="machine_no" class="control-label">Enter Serial No.</label>
                                            <input type="text" class="form-control" id="machine_no" name="machine_no">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="from_date" class="mb-0 control-label active">From Date
                                                <small>*</small></label>
                                            <input id="from_date" name="from_date" class="flatpickr form-control"
                                                placeholder="dd-mm-yy">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="to_date" class="mb-0 control-label active">To Date
                                                <small>*</small></label>
                                            <input id="to_date" name="to_date" class="flatpickr form-control"
                                                placeholder="dd-mm-yy">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <button type="button" class="btn btn-primary btn-lg btn-large" onclick="loadReportData()">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>


            <div class="col-md-12 py-4" id="reportData">
                <div class="card shadow">
                    <div class="card-top ">
                        <div class="card-title mb-0">
                            <h5 class="font-weight-bold text-uppercase mb-0"><i class="far fa-clipboard"></i></h5>
                        </div>
                        <div class="btn-right position-relative float-right">
                            <a href="#" class="btn btn-primary text-uppecase">Print</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <form>
                            <!-- Below table will be dynamically loaded based on the type-category selection. -->
                            <span id="value_counter"></span>
                            <div class="table-responsive">
                                <table class="table" id="report_data_table"></table>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        var report_sub_category_map = [{
                "key": "customer",
                "value": [{
                        "withoutContract": "Without Contract"
                    },
                    {
                        "ticket": "Ticket"
                    },
                    {
                        "products": "Products"
                    },
                    {
                        "spares": "Spares"
                    },
                    {
                        "consumables": "Consumables"
                    },
                    {
                        "basicInfo": "Basic Information"
                    }
                ]
            },
            {
                "key": "ticket",
                "value": [{
                        "closed": "Closed Tickets"
                    },
                    {
                        "open": "Open Tickets"
                    },
                    {
                        "broken": "Broken Calls"
                    }
                ]
            },
            {
                "key": "contract",
                "value": [{
                        "warranty": "Warranty"
                    },
                    {
                        "amc": "AMC"
                    },
                    {
                        "4c": "4C"
                    },
                    {
                        "rental": "Rental"
                    }
                ]
            },
            {
                "key": "invoice",
                "value": [{
                    "basicinfo": "Basic Info"
                }]
            },
            {
                "key": "dc",
                "value": [{
                    "basicinfo": "Basic Info"
                }]
            },
            {
                "key": "pm",
                "value": [{
                    "basicinfo": "Basic Info"
                }]
            },
            {
                "key": "enquiry",
                "value": [{
                    "basicinfo": "Basic Info"
                }]
            }
        ];

        $(document).ready(function() {
            $("#employe").hide();
            $("#machine").hide();
            $("#serial").hide();
            $("#reportss").hide();
            $("#report_type").change(function() {
                $(this).find("option:selected").each(function() {
                    var optionValue = $(this).attr("value");
                    $('#report_category')
                        .find('option')
                        .remove()
                        .end()
                        .append('<option value="">Select</option>');

                    if (optionValue) {
                        $("#employe").hide();
                        $("#reportss").show();
                        $("#machine").hide();
                        $("#serial").hide();

                        $.each(report_sub_category_map, function(index, item) {
                            if (item["key"] === optionValue) {
                                $.each(item["value"], function(index, itemValue) {
                                    for (var key in itemValue) {
                                        $("#report_category").append(
                                            '<option value="' + key + '">' +
                                            itemValue[key] + '</option>');
                                    }
                                });
                                return false;
                            }
                        });
                    }

                    if (optionValue == "products") {
                        $("#reportss").show();
                        $("#machine").hide();
                        $("#serial").hide();
                        var userid = localStorage.getItem("userid");
                        $.ajax({
                            url: "/loadproducts",
                            type: "GET",
                            dataType: 'json',
                            success: function(data) {

                                $('#report_category').empty();
                                $('#report_category').append(
                                    '<option selected="true" disabled></option>');
                                $('#report_category').prop('selectedIndex', 0);

                                var branch = '';

                                $("#report_category").append('<option value="' +
                                    "productsinfo" + '">' + "Productinfo" +
                                    '</option>');

                                $.each(data, function(key, value) {

                                    $('#report_category').append($(
                                        '<option></option>').attr(
                                        'value', value
                                        .product_model_name).text(value
                                        .product_model_name));
                                });
                            },
                            error: function(err) {
                                console.log("Error in getting Role :: ", err)
                            }
                        });

                        return false;
                    } else if (optionValue == "spares") {
                        $("#employe").hide();
                        $("#reportss").show();
                        $("#machine").hide();
                        $("#serial").hide();
                        var userid = localStorage.getItem("userid");
                        $.ajax({
                            url: "/loadspares",
                            type: "GET",
                            dataType: 'json',
                            success: function(data) {

                                $('#report_category').empty();
                                $('#report_category').append(
                                    '<option selected="true" disabled></option>');
                                $('#report_category').prop('selectedIndex', 0);

                                var branch = '';

                                $("#report_category").append('<option value="' +
                                    "sparesinfo" + '">' + "Spareinfo" + '</option>');

                                $.each(data, function(key, value) {
                                    $('#report_category').append($(
                                            '<option></option>').attr(
                                            'value', value.spare_model_name)
                                        .text(value.spare_model_name));
                                });
                            },
                            error: function(err) {
                                console.log("Error in getting Role :: ", err)
                            }
                        });

                        return false;
                    } else if (optionValue == "consumables") {
                        $("#employe").hide();
                        $("#reportss").show();
                        $("#machine").hide();
                        $("#serial").hide();
                        var userid = localStorage.getItem("userid");
                        $.ajax({
                            url: "/loadconsumable",
                            type: "GET",
                            dataType: 'json',
                            success: function(data) {

                                $('#report_category').empty();
                                $('#report_category').append(
                                    '<option selected="true" disabled></option>');
                                $('#report_category').prop('selectedIndex', 0);

                                var branch = '';

                                $("#report_category").append('<option value="' +
                                    "consumablesinfo" + '">' + "Consumableinfo" +
                                    '</option>');

                                $.each(data, function(key, value) {

                                    $('#report_category').append($(
                                            '<option></option>').attr(
                                            'value', value.consumable_name)
                                        .text(value.consumable_name));
                                });
                            },
                            error: function(err) {
                                console.log("Error in getting Role :: ", err)
                            }
                        });

                        return false;
                    } else if (optionValue == "services") {
                        $("#employe").hide();
                        $("#reportss").show();
                        $("#machine").hide();
                        $("#serial").hide();
                        var userid = localStorage.getItem("userid");
                        $.ajax({
                            url: "/loadservices",
                            type: "GET",
                            dataType: 'json',
                            success: function(data) {
                                $('#report_category').empty();
                                $('#report_category').append(
                                    '<option selected="true" disabled></option>');
                                $('#report_category').prop('selectedIndex', 0);

                                var branch = '';

                                $("#report_category").append('<option value="' +
                                    "servicesinfo" + '">' + "Serviceinfo" +
                                    '</option>');

                                $.each(data, function(key, value) {

                                    $('#report_category').append($(
                                        '<option></option>').attr(
                                        'value', value.service_item_name
                                    ).text(value.service_item_name));
                                });
                            },
                            error: function(err) {
                                console.log("Error in getting Role :: ", err)
                            }
                        });

                        return false;
                    } else if (optionValue == "employee") {
                        $("#employe").show();
                        $("#reportss").show();
                        $("#machine").hide();
                        $("#serial").hide();
                        var userid = localStorage.getItem("userid");
                        $.ajax({
                            url: "/loademployees",
                            type: "GET",
                            type: "GET",
                            dataType: 'json',
                            success: function(data) {

                                $('#report_category').empty();
                                $('#report_category').append(
                                    '<option selected="true" disabled></option>');
                                $('#report_category').prop('selectedIndex', 0);

                                var branch = '';

                                $("#report_category").append('<option value="' +
                                    "employeeinfo" + '">' + "BasicInfo" +
                                    '</option>');
                                $("#report_category").append('<option value="' +
                                    "call" + '">' + "Call" + '</option>');
                                $("#report_category").append('<option value="' +
                                    "Email" + '">' + "Email" + '</option>');
                                $("#report_category").append('<option value="' +
                                    "In Person" + '">' + "In Person" + '</option>');

                                $.each(data, function(key, value) {

                                    $('#report_category').append($(
                                        '<option></option>').attr(
                                        'value', value.emp_firstname +
                                        value.emp_lastname).text(value
                                        .emp_firstname + value
                                        .emp_lastname));
                                });


                            },
                            error: function(err) {
                                console.log("Error in getting Role :: ", err)
                            }


                        });

                        return false;
                    } else if (optionValue == "serials") {
                        $("#employe").hide();
                        $("#reportss").hide();
                        $("#machine").show();
                        $("#serial").show();
                    }



                });
            });
        });

        function loadReportData() {
            var report_type = $("#report_type").val();
            var employee_type = $("#employee_type").val();
            var report_category = $("#report_category").val();
            var machine_type = $("#machine_type").val();
            var machine_no = $("#machine_no").val();
            var from_date = $("#from_date").val();
            var to_date = $("#to_date").val();
            let requestObj = {
                "type": report_type,
                "emp": employee_type,
                "category": report_category,
                "fromDate": from_date,
                "toDate": to_date,
                "machine_category": machine_type,
                "machine_id": machine_no
            };
            console.log('requestObj: ', requestObj);
            /*  $.ajax({
                 url: APP_CONFIG.API_HOST + "/v1/" + userid + "/report",
                 type: "POST",
                 data: JSON.stringify(requestObj),
                 headers: {
                     "Authorization": localStorage.getItem("authToken")
                 },
                 success: function(data) {
                     if (data == "") {
                         if (reportTable instanceof $.fn.dataTable.Api) {
                             $('#report_data_table').DataTable().clear();
                             $('#report_data_table').DataTable().destroy();
                         }
                     }
                     populateDatatable(data, report_type, report_category, employee_type, machine_type);

                 },
                 error: function(error) {
                     if (reportTable instanceof $.fn.dataTable.Api) {
                         $('#report_data_table').DataTable().clear();
                         $('#report_data_table').DataTable().destroy();
                     }
                 }
             }); */
        }

    </script>
@endsection
