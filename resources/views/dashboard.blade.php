@extends('layouts.app')
@section('mytitle', 'Dashboard')
@section('head-links')
    <link href="{{ URL::asset('assets/css/datatables.min.css') }}" rel="stylesheet">
@endsection
@section('content')
    <div class="p-3">
        <section>
            <div class="row mb-3">
                <div class="col-md-3 my-3">
                    <a href="/viewcustomer" class="text-decoration-none">
                        <div class="card shadow card-stats">
                            <div class="card-body">
                                <div class="row align-items-center">
                                    <div class="col px-md-2">
                                        <h6 class="mb-3 font-weight-bold">Customer</h6>
                                        <h3 class="fw-700 text-primary">{{ $cus_count }}</h3>
                                        <p class="mb-0 text-12 font-weight-bold text-muted"> Number of customers</p>
                                    </div>
                                    <div class="col-auto px-md-2">
                                        <i class="fas fa-users bg-primary-gradient text-white dash-icon"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 my-3">
                    <a href="/viewcontract" class="text-decoration-none">
                        <div class="card shadow card-stats">
                            <div class="card-body">
                                <div class="row align-items-center">
                                    <div class="col px-md-2">
                                        <h6 class="mb-3 font-weight-bold">Active Contract</h6>
                                        <h3 class="fw-700 text-info">{{ $active_contract_count }}</h3>
                                        <p class="mb-0 text-12 font-weight-bold text-muted">No.of active contract</p>
                                    </div>
                                    <div class="col-auto px-md-2">
                                        <i class="fas fa-file-signature bg-secondary-gradient text-white dash-icon"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 my-3">
                    <a href="/viewticket" class="text-decoration-none">
                        <div class="card shadow card-stats">
                            <div class="card-body">
                                <div class="row align-items-center">
                                    <div class="col px-md-2">
                                        <h6 class="mb-3 font-weight-bold">UnResolved</h6>
                                        <h3 class="fw-700 text-success">{{ $unresolved_count }}</h3>
                                        <p class="mb-0 text-12 font-weight-bold text-muted">UnResolved Tickets</p>
                                    </div>
                                    <div class="col-auto px-md-2">
                                        <i class="fas fa-question bg-success-gradient text-white dash-icon"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 my-3">
                    <a href="/viewticket" class="text-decoration-none">
                        <div class="card shadow card-stats">
                            <div class="card-body">
                                <div class="row align-items-center">
                                    <div class="col px-md-2">
                                        <h6 class="mb-3 font-weight-bold">Resolved</h6>
                                        <h3 class="fw-700 text-warning">{{ $resolved_count }}</h3>
                                        <p class="mb-0 text-12 font-weight-bold text-muted">Tickets Resolved But Not Closed
                                        </p>
                                    </div>
                                    <div class="col-auto px-md-2">
                                        <i class="fas fa-clipboard-check bg-warning-gradient text-white dash-icon"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>


                <div class="col-md-3 my-3">
                    <a href="/pmsschedule" class="text-decoration-none">
                        <div class="card shadow card-stats">
                            <div class="card-body">
                                <div class="row align-items-center">
                                    <div class="col px-md-2">
                                        <h6 class="mb-3 font-weight-bold">PMS calls</h6>
                                        <h3 class="fw-700 text-primary">{{ $pms_overdue }}</h3>
                                        <p class="mb-0 text-12 font-weight-bold text-muted">Overdue PMS Calls</p>
                                    </div>
                                    <div class="col-auto px-md-2"><i
                                            class="fas fa-phone-alt bg-primary-gradient text-white dash-icon"></i></div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 my-3">
                    <a href="/viewcontract" class="text-decoration-none">
                        <div class="card shadow card-stats">
                            <div class="card-body">
                                <div class="row align-items-center">
                                    <div class="col px-md-2">
                                        <h6 class="mb-3 font-weight-bold">W/O Contract</h6>
                                        <h3 class="fw-700 text-info">{{ $contract_count }}</h3>
                                        <p class="mb-0 text-12 font-weight-bold text-muted">Products W/O Contract</p>
                                    </div>
                                    <div class="col-auto px-md-2">
                                        <i class="fas fa-file-contract bg-secondary-gradient text-white dash-icon"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 my-3">
                    <a href="/viewenquiry" class="text-decoration-none">
                        <div class="card shadow card-stats">
                            <div class="card-body">
                                <div class="row align-items-center">
                                    <div class="col px-md-2">
                                        <h6 class="mb-3 font-weight-bold">Enquiry</h6>
                                        <h3 class="fw-700 text-success">{{ $enquiry_count }}</h3>
                                        <p class="mb-0 text-12 font-weight-bold text-muted">In the Last 15 Days</p>
                                    </div>
                                    <div class="col-auto px-md-2">
                                        <i class="far fa-question-circle bg-success-gradient text-white dash-icon"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 my-3">
                    <a href="/paymentfollowup" class="text-decoration-none">
                        <div class="card shadow card-stats">
                            <div class="card-body">
                                <div class="row align-items-center">
                                    <div class="col px-md-2">
                                        <h6 class="mb-3 font-weight-bold">Followups</h6>
                                        <h3 class="fw-700 text-warning">{{ $pending_payment }}</h3>
                                        <p class="mb-0 text-12 font-weight-bold text-muted">Pending Payment Followup</p>
                                    </div>
                                    <div class="col-auto px-md-2">
                                        <i class="fas fa-user-plus bg-warning-gradient text-white dash-icon"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-12 pb-3">
                    <div class="card shadow">
                        <div class="card-body">
                            <div class="table-responsive">
                                <h6 class="font-weight-bolder text-uppercase mb-4 text-primary">Recent Tickets</h6>
                                <table class="table" id="recent-tickets">
                                    <thead class="text-uppercase">
                                        <tr>
                                            <th scope="col">Ticket No</th>
                                            <th scope="col">Model Name</th>
                                            <th scope="col">Machine Serial</th>
                                            <th scope="col">Defect Code</th>
                                            <th scope="col">Assignee</th>
                                            <th scope="col">Ticket Created Date</th>
                                            <th scope="col">Priority</th>
                                            <th scope="col">Customer</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (count($recent_ticket) > 0)
                                            @foreach ($recent_ticket as $ticket)
                                                <tr>
                                                    <td>{{ $ticket->ticket_id }}</td>
                                                    <td>{{ $ticket->product_model }}</td>
                                                    <td>{{ $ticket->Machine_Serial_No }}</td>
                                                    <td>{{ $ticket->Ticket_Defect_Code }}</td>
                                                    <td>{{ $ticket->Assign_Technician_Name }}</td>
                                                    <td>{{ $ticket->Ticket_Created_Date }}</td>
                                                    <td>{{ $ticket->Ticket_Priority }}</td>
                                                    <td>{{ $ticket->customer_name }}</td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td>Sorry, No records found</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 py-3">
                    <div class="card shadow">
                        <div class="card-body">
                            <div class="table-responsive">
                                <h6 class="font-weight-bolder text-uppercase mb-4 text-primary">Delayed Payment Alerts</h6>
                                <table class="table" id="delayed-payment">
                                    <thead class="text-uppercase">
                                        <tr>
                                            <th scope="col">Customer Name</th>
                                            <th scope="col">Invoice No.</th>
                                            <th scope="col">Invoice Date</th>
                                            <th scope="col">Received Amount</th>
                                            <th scope="col">Pending Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (!($delayed_payment_alerts == null))
                                            @foreach ($delayed_payment_alerts as $payment)
                                                <tr>
                                                    <td>{{ $payment->Customer_Name }}</td>
                                                    <td>{{ $payment->Invoice_ID }}</td>
                                                    <td>{{ $payment->Invoice_Date }}</td>
                                                    <td>{{ $payment->Recieved_Amount }}</td>
                                                    <td>{{ $payment->Followup_Date }}</td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td>Sorry, No records found</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 py-3">
                    <div class="card shadow">
                        <div class="card-body">
                            <div class="table-responsive">
                                <h6 class="font-weight-bolder text-uppercase mb-4 text-primary">Delayed PMS Alert!</h6>
                                <table class="table" id="delayed-pms">
                                    <thead class="text-uppercase">
                                        <tr>
                                            <th scope="col">Machine Serial</th>
                                            <th scope="col">Model Name</th>
                                            <th scope="col">Scheduled Date</th>
                                            <th scope="col">Customer</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (count($delayed_pms) > 0)
                                            @foreach ($delayed_pms as $pms)
                                                <tr>
                                                    <td>{{ $pms->Machine_Serial_No }}</td>
                                                    <td>{{ $pms->Item_Model_Name }}</td>
                                                    <td>{{ $pms->Follow_Up_Date }}</td>
                                                    <td>{{ $pms->customer_name }}</td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="4">Sorry, No records found</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 py-3">
                    <div class="card shadow">
                        <div class="card-body">
                            <div class="table-responsive">
                                <h6 class="font-weight-bolder text-uppercase text-primary">Upcomming PMS Alert!</h6>
                                <p class="font-weight-bold text-primary text-12 mb-1">PMS Calls scheduled for next 7 days.
                                </p>
                                <table class="table" id="upcoming-pms">
                                    <thead class="text-uppercase">
                                        <tr>
                                            <th scope="col">Machine Serial</th>
                                            <th scope="col">Model Name</th>
                                            <th scope="col">Scheduled Date</th>
                                            <th scope="col">Customer</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (count($next_pms) > 0)
                                            @foreach ($next_pms as $pms)
                                                <tr>
                                                    <td>{{ $pms->Machine_Serial_No }}</td>
                                                    <td>{{ $pms->Item_Model_Name }}</td>
                                                    <td>{{ $pms->Follow_Up_Date }}</td>
                                                    <td>{{ $pms->customer_name }}</td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="4">Sorry, No records found</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('script')
    <script src="{{ URL::asset('assets/js/datatables.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#recent-tickets, #delayed-payment, #delayed-pms, #upcoming-pms').DataTable({
                language: {
                    searchPlaceholder: "Search records",
                    search: "",
                    "lengthMenu": "_MENU_ Entries"
                },
                "lengthMenu": [
                    [8, 16, 32, -1],
                    [8, 16, 32, "All"]
                ],
                "pageLength": 8,
                "sPaginationType": "simple"
            });
        });

    </script>
@endsection
