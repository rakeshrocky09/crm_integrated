@extends('layouts.app')
@section('mytitle', 'View Product')
@section('content')
    <div class="bg-top bg-gray p-4">
        <div class="d-flex">
            <h5 class="font-weight-bold text-uppercase mb-0">View Product</h5>
        </div>
    </div>

    <div class="p-4 card-main">
        <section class="my-5">
            <div class="card shadow">
                <div class="card-top ">
                    <div class="card-title mb-0">
                        <h5 class="font-weight-bold text-uppercase mb-0"><i class="fas fa-user-alt"></i></h5>
                    </div>
                    <div class="btn-right position-relative float-right">
                        <a href="#" class="btn btn-primary text-uppecase">Print</a>
                        <a href="/addpurchase_product" class="btn btn-primary text-uppecase" data-toggle="tooltip"
                            data-placement="top" title="Add Product">
                            <i class="fas fa-plus"></i> Add
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <div class="row mx-0">
                            <div class="col-sm-12 col-md-6 pl-0 py-3">
                                <div class="dataTables_length" id="example1_length"><select aria-controls="example1"
                                        class="custom-select custom-select-sm form-control form-control-sm">
                                        <option value="10">10</option>
                                        <option value="25">25</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select></div>
                            </div>
                            <div class="col-sm-12 col-md-6 pr-0 py-3">
                                <div id="example1_filter" class="float-right">
                                    <input type="search" class="form-control form-control-sm" placeholder="Search..."
                                        aria-controls="example1">
                                </div>
                            </div>
                        </div>
                        <table class="table">
                            <thead class="text-uppercase">
                                <tr>
                                    <th scope="col">PRODUCT ID</th>
                                    <th scope="col">PART NO.</th>
                                    <th scope="col">NAME</th>
                                    <th scope="col">BRAND NAME</th>
                                    <th scope="col">UNITS</th>
                                    <th scope="col">PRICE</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>PROD49</td>
                                    <td>A798047</td>
                                    <td>Bizhub C227 MFP</td>
                                    <td>Konica</td>
                                    <td>Nos</td>
                                    <td>135000</td>
                                    <td class="d-flex flex-nowrap">
                                        <span data-toggle="modal" data-target="#viewproduct" class="mr-1">
                                            <button type="button" class="btn btn-outline-info" data-toggle="tooltip"
                                                data-placement="top" title="View Product">
                                                <i class="fas fa-user-alt"></i>
                                            </button>
                                        </span>
                                        <span data-toggle="modal" data-target="#editproduct" class="mr-1">
                                            <button type="button" class="btn btn-outline-success" data-toggle="tooltip"
                                                data-placement="top" title="Edit Product">
                                                <i class="fas fa-edit"></i>
                                            </button>
                                        </span>
                                        <span data-toggle="modal" data-target="#deleteproduct">
                                            <button type="button" class="btn btn-outline-danger" data-toggle="tooltip"
                                                data-placement="top" title="Delete Product">
                                                <i class="fas fa-trash-alt"></i>
                                            </button>
                                        </span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <!-- Modal viewproduct -->
                        <div class="modal fade custom-modal" id="viewproduct" tabindex="-1" role="dialog"
                            aria-labelledby="viewproduct" aria-hidden="true">
                            <div class="modal-dialog modal-xl modal-dialog-centered edit" role="document">
                                <div class="modal-content">
                                    <div class="icon">
                                        <span class="fa-stack fa-2x text-success">
                                            <i class="fas fa-circle fa-stack-2x"></i>
                                            <i class="fas fa-table fa-stack-1x fa-inverse"></i>
                                        </span>
                                    </div>
                                    <div class="modal-header justify-content-center">
                                        <h3 class="modal-title text-capitalize font-weight-bolder mt-3" id="viewproduct">
                                            View Branch Details
                                        </h3>
                                    </div>
                                    <div class="modal-body pb-0 text-sm">
                                        <h6 class="fw-700 text-uppercase mb-4">PRODUCT DETAILS</h6>
                                        <div class="row text-sm mb-4">
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">Belongs To Branch Name</label>
                                                <p class="mb-0"><span>KM Enterprises</span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">Product ID</label>
                                                <p class="mb-0"><span>PROD49</span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">HSN/SAC Number</label>
                                                <p class="mb-0"><span>8443</span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">Brand Name</label>
                                                <p class="mb-0"><span>Konica</span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">Model Name</label>
                                                <p class="mb-0"><span>Bizhub C227 MFP</span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">Product Name</label>
                                                <p class="mb-0"><span>Bizhub C227 MFP</span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">Product Category</label>
                                                <p class="mb-0"><span>MFP</span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">Description</label>
                                                <p class="mb-0"><span>with df-628 and tn 221 kymc toner</span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">Part Number</label>
                                                <p class="mb-0"><span>A798047</span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">GST(In %)</label>
                                                <p class="mb-0"><span>18</span></p>
                                            </div>
                                        </div>
                                        <h6 class="fw-700 text-uppercase mb-4">PRODUCT PARAMETERS</h6>
                                        <div class="row text-sm">
                                            <div class="form-group col-md-4">
                                                <label class="font-weight-bold">Color</label>
                                                <p class="mb-0"><span>color</span></p>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label class="font-weight-bold">Product Price</label>
                                                <p class="mb-0"><span>135000</span></p>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label class="font-weight-bold">Units</label>
                                                <p class="mb-0"><span>Nos</span></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer justify-content-center">
                                        <button type="button" class="btn btn-success btn-lg btn-large mt-0"
                                            data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End viewprofile -->
                        <!-- Modal Edit product -->
                        <div class="modal fade custom-modal" id="editproduct" tabindex="-1" role="dialog"
                            aria-labelledby="editproduct" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered edit modal-xl" role="document">
                                <div class="modal-content">
                                    <div class="icon">
                                        <span class="fa-stack fa-2x text-success">
                                            <i class="fas fa-circle fa-stack-2x"></i>
                                            <i class="far fa-edit fa-stack-1x fa-inverse"></i>
                                        </span>
                                    </div>
                                    <div class="modal-header justify-content-center">
                                        <h3 class="modal-title text-capitalize font-weight-bolder mt-3" id="editproduct">
                                            Edit Branch Details
                                        </h3>
                                    </div>
                                    <div class="modal-body pb-0">
                                        <form>
                                            <h6 class="fw-700 text-uppercase pl-1 mb-4 mt-3">
                                                Product Details</h6>
                                            <div class="row">
                                                <div class="form-group col-md-4">
                                                    <label for="date" class="mb-0 control-label active">Belongs To
                                                        Branch <small>*</small></label>
                                                    <input id="date" type="text" class="form-control"
                                                        value="KM Enterprises">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label class="control-label active" for="techician_branch">Brand Name
                                                        <small>*</small>
                                                    </label>
                                                    <select class="form-control" id="techician_branch"
                                                        name="techician_branch">
                                                        <option selected="true" value="1">Konica</option>
                                                        <option value="3">Option value</option>
                                                        <option value="3">Option value</option>
                                                    </select>
                                                </div>

                                                <div class="form-group col-md-4">
                                                    <label for="date" class="mb-0 control-label active">Product Id
                                                        <small>*</small></label>
                                                    <input id="date" type="text" class="form-control" value="PROD49">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="date" class="mb-0 control-label active">Model Name
                                                        <small>*</small></label>
                                                    <input id="date" type="text" class="form-control"
                                                        value="Bizhub  C227 MFP">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label class="control-label active" for="techician_branch">HSN/SAC
                                                        Number
                                                        <small>*</small>
                                                    </label>
                                                    <select class="form-control" id="techician_branch"
                                                        name="techician_branch">
                                                        <option selected="true" value="1">8443</option>
                                                        <option value="2">Option 1</option>
                                                        <option value="3">Option 2</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label class="control-label active" for="techician_name">Description
                                                        <small>*</small></label>
                                                    <textarea class="form-control mt-1" id="exampleFormControlTextarea1"
                                                        rows="1">with df-628 and tn 221 kymc toner</textarea>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="date" class="mb-0 control-label active">Product Name
                                                        <small>*</small></label>
                                                    <input id="date" type="text" class="form-control"
                                                        value="Bizhub  C227 MFP">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label class="control-label active" for="techician_branch">Product
                                                        Category</label>
                                                    <select class="form-control" id="techician_branch"
                                                        name="techician_branch">
                                                        <option selected="true" value="1">MPF</option>
                                                        <option value="2">Option 1</option>
                                                        <option value="3">Option 2</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <h6 class="fw-700 text-uppercase pl-1 mb-4 mt-3">
                                                Product Parameters</h6>
                                            <div class="row">
                                                <div class="form-group col-md-4">
                                                    <label for="date" class="mb-0 control-label active">Color</label>
                                                    <input id="date" type="text" class="form-control" value="color">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label class="control-label active" for="techician_branch">Units
                                                    </label>
                                                    <select class="form-control" id="techician_branch"
                                                        name="techician_branch">
                                                        <option selected="true" value="1">Nos</option>
                                                        <option value="2">Option 1</option>
                                                        <option value="3">Option 2</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="date" class="mb-0 control-label active">GST(In %)</label>
                                                    <input id="date" type="text" class="form-control" value="18">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="date" class="mb-0 control-label active">Part Number</label>
                                                    <input id="date" type="text" class="form-control" value="A798047">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="date" class="mb-0 control-label active">Product
                                                        Price</label>
                                                    <input id="date" type="text" class="form-control" value="135000">
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="modal-footer flex-nowrap">
                                        <button type="button" class="btn btn-outline-success btn-block btn-lg mt-0"
                                            data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-success btn-block btn-lg mt-0">Done</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Edit profile -->
                        <!-- Modal Delete product -->
                        <div class="modal fade custom-modal" id="deleteproduct" tabindex="-1" role="dialog"
                            aria-labelledby="deleteproduct" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="icon">
                                        <span class="fa-stack fa-2x text-primary">
                                            <i class="fas fa-circle fa-stack-2x"></i>
                                            <i class="fas fa-trash-alt fa-stack-1x fa-inverse"></i>
                                        </span>
                                    </div>
                                    <div class="modal-header justify-content-center">
                                        <h3 class="modal-title text-capitalize font-weight-bolder mt-3" id="deleteproduct">
                                            Delete Branch?
                                        </h3>
                                    </div>
                                    <div class="modal-body pb-0">
                                        <p class="font-weight-bold text-center">
                                            Are you sure you want to delete the Product?
                                            <br>
                                            You cannot undo this operation!
                                        </p>
                                        <div class="row justify-content-center">
                                            <div class="col-md-11">
                                                <form>
                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <label for="delete_product_id" class="col-form-label">Product
                                                                ID</label>
                                                            <input type="text" class="form-control" id="delete_product_id"
                                                                value="PROD49" readonly>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="delete_product_code" class="col-form-label">Product
                                                                Code</label>
                                                            <input type="text" class="form-control" id="delete_product_code"
                                                                value="8443" readonly>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="delete_product_name" class="col-form-label">Product
                                                                Name</label>
                                                            <input type="text" class="form-control" id="delete_product_name"
                                                                value="Bizhub C227 MFP" readonly>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="delete_brandname" class="col-form-label">Brand
                                                                Name</label>
                                                            <input type="text" class="form-control" id="delete_brandname"
                                                                value="Konica" readonly>
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <label for="delete_branch" class="col-form-label">Belongs To
                                                                Branch</label>
                                                            <input type="text" class="form-control" id="delete_branch"
                                                                value="KM Enterprises" readonly>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer flex-nowrap">
                                        <button type="button" class="btn btn-outline-primary btn-block btn-lg mt-0"
                                            data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary btn-block btn-lg mt-0">Delete</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Delete organizaton -->
                    </div>
                    <div class="row my-3">
                        <div class="col-md-5">
                            <div>Showing 1 to 10 of 50 entries</div>
                        </div>
                        <div class="col-md-7">
                            <nav aria-label="Page navigation example">
                                <ul class="pagination justify-content-end mb-0">
                                    <li class="page-item disabled">
                                        <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a>
                                    </li>
                                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                                    <li class="page-item"><a class="page-link" href="#">4</a></li>
                                    <li class="page-item"><a class="page-link" href="#">5</a></li>
                                    <li class="page-item mr-0">
                                        <a class="page-link" href="#">Next</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
