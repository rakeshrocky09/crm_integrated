@extends('layouts.app')
@section('mytitle', 'Add Customer Ticket')
@section('content')
    <div class="p-4">
        <section class="my-5">
            <div class="card shadow">
                <div class="card-top ">
                    <div class="card-title mb-0">
                        <h6 class="font-weight-bold text-capitalize mb-0">Tickets</h6>
                    </div>
                </div>
                <div class="card-body pt-0">
                    <form action="{{ url('addcustickets') }}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col-md-3">
                                <h6 class="font-weight-bold text-uppercase text-primary">Customer Info</h6>
                                <div class="form-group">
                                    <h6 class="font-weight-bold mb-0 text-sm">Customer Name</h6>
                                    <input class="custom-input" id="customer_name" name="customer_name" type="text"
                                        value="{{ $cus[0]->customer_name }}" readonly>
                                    <input class="d-none custom-input" id="cus_id" name="cus_id" type="text"
                                        value="{{ $cus[0]->cus_id }}" readonly>
                                </div>
                                <div class="form-group">
                                    <h6 class="font-weight-bold mb-0 text-sm">Address</h6>
                                    <input class="custom-input" id="cus_address" name="cus_address" type="text"
                                        value="{{ $cus[0]->cus_address }}" readonly>
                                    <input class="custom-input" id="cus_city" name="cus_city" type="text"
                                        value="{{ $cus[0]->cus_city }}" readonly>
                                    <input class="custom-input" id="cus_pincode" name="cus_pincode" type="text"
                                        value="{{ $cus[0]->cus_pincode }}" readonly>
                                    <input class="custom-input" id="cus_state" name="cus_state" type="text"
                                        value="{{ $cus[0]->cus_state }}" readonly>
                                </div>
                                <div class="form-group">
                                    <h6 class="font-weight-bold mb-0 text-sm">Customer Phone no</h6>
                                    <input class="custom-input" id="cus_phone_no" name="cus_phone_no" type="text"
                                        value="{{ $cus[0]->cus_phone_no }}" readonly>
                                </div>
                                <div class="form-group">
                                    <h6 class="font-weight-bold mb-0 text-sm">Branch Name</h6>
                                    <input class="custom-input" id="cus_branch_name" name="cus_branch_name" type="text"
                                        value="{{ $cus[0]->cus_branch_name }}" readonly>
                                </div>
                                <div class="custom-control custom-checkbox mb-2">
                                    <input type="checkbox" class="custom-control-input" id="sameaddress"
                                        onchange="valueChanged()">
                                    <label class="custom-control-label font-weight-bold h6 mb-1 text-uppercase"
                                        for="sameaddress">Same as Customer address</label>
                                </div>
                                <div class="form-group mb-1">
                                    <label class="control-label address_label" for="address">
                                        Address <small>*</small>
                                    </label>
                                    <input type="text" class="form-control" id="address" name="address">
                                </div>
                                <div class="form-group mb-1">
                                    <label class="control-label address_label" for="city">
                                        City <small>*</small>
                                    </label>
                                    <input type="text" class="form-control" id="city" name="city">
                                </div>
                                <div class="form-group mb-1">
                                    <label class="control-label address_label" for="state">
                                        State <small>*</small>
                                    </label>
                                    <input type="text" class="form-control" id="state" name="state">
                                </div>
                                <div class="form-group mb-1">
                                    <label class="control-label address_label" for="pincode">
                                        Postal Code <small>*</small>
                                    </label>
                                    <input type="type" class="form-control" id="pincode" name="pincode">
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="mb-2">
                                    <h6 class="font-weight-bold text-uppercase text-primary">
                                        Product(s)
                                    </h6>
                                    <div class="table-responsive">
                                        <table class="table modal-table">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Product Name</th>
                                                    <th scope="col">Brand Name</th>
                                                    <th scope="col">Machine Serial No</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td id="prod_name">
                                                        <input class="custom-input" id="prod_name" name="prod_name"
                                                            type="text" value="{{ $product[0]->product_model_name }}"
                                                            readonly>
                                                    </td>
                                                    <td id="brand_name">
                                                        <input class="custom-input" id="brand_name" name="brand_name"
                                                            type="text" value="{{ $product[0]->product_brand_name }}"
                                                            readonly>
                                                    </td>
                                                    <td id="mac_serial">
                                                        <input class="custom-input" id="mac_serial" name="mac_serial"
                                                            type="text" value="{{ $product[0]->Machine_Serial_No }}"
                                                            readonly>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="mb-2">
                                    <h6 class="font-weight-bold text-uppercase text-primary mb-0">
                                        TICKET DETAILS
                                    </h6>
                                    <div class="row">
                                        <div class="form-group col-md-4">
                                            <label class="control-label" for="source">
                                                Source
                                            </label>
                                            <select class="form-control" id="source" name="source">
                                                <option disabled selected></option>
                                                <option value="CALL"> CALL </option>
                                                <option value="EMAIL"> EMAIL </option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label class="control-label" for="defect_code">
                                                Defect Code <small>*</small>
                                            </label>
                                            <select class="form-control" id="defect_code" name="defect_code"></select>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label class="control-label" for="tat">
                                                Turn Around Time(TAT Hrs)
                                            </label>
                                            <input type="number" class="form-control" id="tat" name="tat">
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label class="control-label" for="ticket_type">
                                                Ticket Type
                                            </label>
                                            <select class="form-control" id="ticket_type" name="ticket_type"></select>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label class="control-label" for="priority">
                                                Priority
                                            </label>
                                            <select class="form-control" id="priority" name="priority">
                                                <option disabled selected></option>
                                                <option value="P1"> P1 </option>
                                                <option value="P2"> P2 </option>
                                                <option value="P3"> P3 </option>
                                                <option value="P4"> P4 </option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="description" class="mb-0 control-label">
                                                Problem Description
                                            </label>
                                            <input id="description" name="description" type="text" class="form-control">
                                        </div>
                                    </div>
                                </div>

                                <div class="mb-2">
                                    <h6 class="font-weight-bold text-uppercase text-primary mb-0">
                                        CALLER DETAILS
                                    </h6>
                                    <div class="row">
                                        <div class="form-group col-md-4">
                                            <label class="control-label" for="sender_name">
                                                Caller/Sender Name <small>*</small>
                                            </label>
                                            <input type="text" class="form-control" id="sender_name" name="sender_name">
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label class="control-label" for="sender_email">
                                                Caller/Sender Email <small>*</small>
                                            </label>
                                            <input type="email" class="form-control" id="sender_email" name="sender_email">
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label class="control-label" for="sender_phone">
                                                Caller/Sender Phone <small>*</small>
                                            </label>
                                            <input type="phone" class="form-control" id="sender_phone" name="sender_phone">
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label class="control-label" for="assign_technician">
                                                Assign Technician
                                            </label>
                                            <select class="form-control" id="assign_technician"
                                                name="assign_technician"></select>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label class="control-label" for="assign_branch">
                                                Assign Branch
                                            </label>
                                            <select class="form-control" id="assign_branch" name="assign_branch"></select>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="service_charges" class="mb-0 control-label">
                                                Service charge(RS)
                                            </label>
                                            <input id="service_charges" name="service_charges" type="text"
                                                class="form-control">
                                        </div>
                                    </div>
                                </div>

                                <div class="mb-2">
                                    <h6 class="font-weight-bold text-uppercase text-primary mb-0">
                                        RECORD METER READINGS
                                    </h6>
                                    <div class="row">
                                        <div class="form-group col-md-3">
                                            <label class="control-label" for="black_a3">
                                                Black A3
                                            </label>
                                            <input type="text" class="form-control" id="black_a3" name="black_a3">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label class="control-label" for="black_a4">
                                                Black A4
                                            </label>
                                            <input type="text" class="form-control" id="black_a4" name="black_a4">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label class="control-label" for="color_a3">
                                                Color A3
                                            </label>
                                            <input type="text" class="form-control" id="color_a3" name="color_a3">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label class="control-label" for="color_a4">
                                                Color A4
                                            </label>
                                            <input type="text" class="form-control" id="color_a4" name="color_a4">
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex ">
                                    <button type="submit" class="btn btn-primary mr-2">Submit</button>
                                    <button class="btn btn-primary">Clear</button>
                                </div>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>

    <div class="modal fade custom-modal" id="add_defect_code" tabindex="-1" role="dialog" aria-labelledby="add_defect_code"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content success">
                <div class="icon">
                    <span class="fa-stack fa-2x text-success">
                        <i class="fas fa-circle fa-stack-2x"></i>
                        <i class="fas fas fa-plus fa-stack-1x fa-inverse"></i>
                    </span>
                </div>
                <div class="modal-header justify-content-center">
                    <h3 class="modal-title text-capitalize font-weight-bolder mt-3">
                        Add Defect Code
                    </h3>
                </div>
                <form action="{{ url('addticketdefectcode') }}" method="POST">
                    @csrf
                    <div class="modal-body pb-0">
                        <div class="row justify-content-center">
                            <div class="col-md-11">
                                <div class="form-group">
                                    <label for="add_defect_code" class="col-form-label">Enter Defect Code</label>
                                    <input type="text" class="form-control" id="add_defect_code" name="add_defect_code">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer flex-nowrap">
                            <button type="button" class="btn btn-outline-success btn-block btn-lg mt-0"
                                data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-success btn-block btn-lg mt-0">Done</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade custom-modal" id="add_ticket_type" tabindex="-1" role="dialog" aria-labelledby="add_ticket_type"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content success">
                <div class="icon">
                    <span class="fa-stack fa-2x text-success">
                        <i class="fas fa-circle fa-stack-2x"></i>
                        <i class="fas fas fa-plus fa-stack-1x fa-inverse"></i>
                    </span>
                </div>
                <div class="modal-header justify-content-center">
                    <h3 class="modal-title text-capitalize font-weight-bolder mt-3">
                        Add Ticket Type
                    </h3>
                </div>
                <form action="{{ url('addtickettype') }}" method="POST">
                    @csrf
                    <div class="modal-body pb-0">
                        <div class="row justify-content-center">
                            <div class="col-md-11">
                                <div class="form-group">
                                    <label for="add_ticket_type" class="col-form-label">Enter Ticket Type Name</label>
                                    <input type="text" class="form-control" id="add_ticket_type" name="add_ticket_type">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer flex-nowrap">
                        <button type="button" class="btn btn-outline-success btn-block btn-lg mt-0"
                            data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success btn-block btn-lg mt-0">Done</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @include('components.alertmodal')
@endsection
@section('script')
    @if (session()->has('message'))
        <script>
            $(document).ready(function() {
                $('#successAlert').modal("show");
            });

        </script>
    @endif
    @if (session()->has('error'))
        <script>
            $(document).ready(function() {
                $('#errorAlert').modal("show");
            });

        </script>
    @endif
    <script>
        function valueChanged() {
            if ($('#sameaddress').is(":checked")) {
                console.log('checked');
                var address = $("#cus_address").val();
                var city = $("#cus_city").val();
                var pincode = $("#cus_pincode").val();
                var state = $("#cus_state").val();
                console.log('address:', address);
                console.log('city:', city);
                console.log('pincode:', pincode);
                console.log('state:', state);

                $("#address").val(address);
                $("#city").val(city);
                $("#pincode").val(pincode);
                $("#state").val(state);
                $('.address_label').addClass('active');
            } else {
                console.log('unchecked');
                $("#address").val('');
                $("#city").val('');
                $("#pincode").val('');
                $("#state").val('');
                $('.address_label').removeClass('active');
            }
        }
        $(document).ready(function() {
            loadDefectCode();
            loadTicketType();
            loadTechnicians();
            getBranch();

            $('#defect_code').change(function() { //jQuery Change Function
                var opval = $(this).val(); //Get value from select element
                if (opval == "others") { //Compare it and if true
                    $('#add_defect_code').modal("show"); //Open Modal
                }
            });
            $('#ticket_type').change(function() { //jQuery Change Function
                var opval = $(this).val(); //Get value from select element
                if (opval == "others") { //Compare it and if true
                    $('#add_ticket_type').modal("show"); //Open Modal
                }
            });
            $('#assign_branch').change(function() { //jQuery Change Function
                var opval = $(this).val(); //Get value from select element
                if (opval == "others") { //Compare it and if true
                    return window.location.href = "/addorganization";
                }
            });
        });

        function loadDefectCode() {
            return $.ajax({
                url: "/ticketdefectcode",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    console.log("ticketdefectcode", data);
                    $('#defect_code').empty();
                    $('#defect_code').append('<option selected="true" disabled></option>');
                    $('#defect_code').prop('selectedIndex', 0);
                    $.each(data, function(key, value) {
                        $('#defect_code').append($('<option></option>').attr('value',
                            value.code_name).text(value.code_name));
                    });
                    $('#defect_code').append('<option value="others">Others</option>');
                },
                error: function(error) {
                    console.log('ticketdefectcode load error:', error);
                }
            });
        }

        function loadTicketType() {
            return $.ajax({
                url: "/tickettype",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    //console.log("tickettype", data);
                    $('#ticket_type').empty();
                    $('#ticket_type').append('<option selected="true" disabled></option>');
                    $('#ticket_type').prop('selectedIndex', 0);
                    $.each(data, function(key, value) {
                        $('#ticket_type').append($('<option></option>').attr(
                            'value',
                            value.ticket_type).text(value.ticket_type));
                    });
                    $('#ticket_type').append('<option value="others">Others</option>');
                },
                error: function(error) {
                    console.log('ticket_type error:', error);
                }
            });
        }

        function loadTechnicians() {
            return $.ajax({
                url: "/technicians",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    //console.log("Technicians", data);
                    $('#assign_technician').empty();
                    $('#assign_technician').append('<option selected="true" disabled></option>');
                    $('#assign_technician').prop('selectedIndex', 0);
                    $.each(data, function(key, value) {
                        var name = value.emp_firstname + value.emp_lastname;
                        $('#assign_technician').append($('<option></option>').attr('value',
                            name).text(name));
                    });
                },
                error: function(error) {
                    console.log('Technicians load error:', error);
                }
            });
        }

        function getBranch() {
            return $.ajax({
                url: "/branchname",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    //console.log("branchname", data);
                    $('#assign_branch').empty();
                    $('#assign_branch').append('<option selected="true" disabled></option>');
                    $('#assign_branch').prop('selectedIndex', 0);
                    $.each(data, function(key, value) {
                        $('#assign_branch').append($('<option></option>').attr('value', value
                            .org_branch_name).text(value.org_branch_name));
                    });
                    $('#assign_branch').append('<option value="others">Others</option>');
                },
                error: function(error) {
                    console.log('assign_branch error:', error);
                }
            });
        }

    </script>
@endsection
