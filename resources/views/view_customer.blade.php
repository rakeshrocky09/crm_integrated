@extends('layouts.app')
@section('mytitle', 'CUSTOMER DETAILS')
@section('content')

    <div class="p-4">
        <section class="my-5">
            <div class="card shadow">
                <div class="card-top ">
                    <div class="card-title mb-0">
                        <h6 class="font-weight-bold text-capitalize mb-0">View Customer Details</h6>
                    </div>
                </div>
                <div class="card-body pt-0">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="text-sm">
                                <div class="form-group">
                                    <h6 class="font-weight-bold mb-0 text-sm">Type</h6>
                                    <span class="text-13">{{ $cus[0]->cus_type }}</span>
                                </div>
                                <div class="form-group">
                                    <h6 class="font-weight-bold mb-0 text-sm">Customer Name/Organization Name</h6>
                                    <span class="text-13">{{ $cus[0]->company_name }}</span>
                                </div>
                                <div class="form-group">
                                    <h6 class="font-weight-bold mb-0 text-sm">Email ID</h6>
                                    <span class="text-13">{{ $cus[0]->cus_email_id }}</span>
                                </div>
                                <div class="form-group">
                                    <h6 class="font-weight-bold mb-0 text-sm">Phone No</h6>
                                    <span class="text-13">{{ $cus[0]->cus_phone_no }}</span>
                                </div>
                                <div class="form-group">
                                    <h6 class="font-weight-bold mb-0 text-sm">Land Line No</h6>
                                    <span class="text-13">{{ $cus[0]->cus_landline }}</span>
                                </div>
                                <div class="form-group">
                                    <h6 class="font-weight-bold mb-0 text-sm">Alternate Phone No</h6>
                                    <span class="text-13">{{ $cus[0]->cus_alter_phone }}</span>
                                </div>
                                <div class="form-group">
                                    <h6 class="font-weight-bold mb-0 text-sm">Belongs to branch</h6>
                                    <span class="text-13">{{ $cus[0]->cus_branch_name }}</span>
                                </div>
                                <div class="form-group">
                                    <h6 class="font-weight-bold mb-0 text-sm">Primary Contact Person Name</h6>
                                    <span class="text-13">{{ $cus[0]->primary_contact_name }}</span>
                                </div>
                                <div class="form-group">
                                    <h6 class="font-weight-bold mb-0 text-sm">Email ID</h6>
                                    <span class="text-13">{{ $cus[0]->primary_contact_email }}</span>
                                </div>
                                <div class="form-group">
                                    <h6 class="font-weight-bold mb-0 text-sm">Phone No</h6>
                                    <span class="text-13">{{ $cus[0]->primary_contact_phoneno }}</span>
                                </div>
                                <div class="form-group">
                                    <h6 class="font-weight-bold mb-0 text-sm">Address</h6>
                                    <address class="text-13">
                                        <span>{{ $cus[0]->cus_address }}</span><br>
                                        <span>{{ $cus[0]->cus_city }} - {{ $cus[0]->cus_pincode }}</span> <br>
                                        <span>{{ $cus[0]->cus_state }}</span>
                                    </address>
                                </div>
                                <div class="form-group">
                                    <h6 class="font-weight-bold mb-0 text-sm">GST No</h6>
                                    <span class="text-13">{{ $cus[0]->cus_gst_percentage }}</span>
                                </div>
                                <div class="form-group">
                                    <h6 class="font-weight-bold mb-0 text-sm">Section</h6>
                                    <span class="text-13">{{ $cus[0]->cus_section }}</span>
                                </div>
                                <div class="form-group mb-0">
                                    <h6 class="font-weight-bold mb-0 text-sm">Life Time</h6>
                                    <span class="text-13">{{ $cus[0]->cus_lifetime }}</span>
                                    <span id="cus_id" hidden>{{ $cus[0]->cus_id }}</span>
                                    <span id="cus_branch" hidden>{{ $cus[0]->cus_branch_name }}</span>
                                </div>
                                <div class="my-3">
                                    <a href="{{ url('/viewcustomer') }}" class="btn btn-primary">Cancel</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9 pl-md-0">
                            <ul class="nav nav-pills" id="pills-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="pills-product-tab" data-toggle="pill"
                                        href="#pills-product" role="tab" aria-controls="pills-product"
                                        aria-selected="true">Product</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-invoices-tab" data-toggle="pill" href="#pills-invoices"
                                        role="tab" aria-controls="pills-invoices" aria-selected="false">Invoices</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-tickets-tab" data-toggle="pill" href="#pills-tickets"
                                        role="tab" aria-controls="pills-tickets" aria-selected="false">Tickets</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-delivery-tab" data-toggle="pill" href="#pills-delivery"
                                        role="tab" aria-controls="pills-delivery" aria-selected="false">Delivery</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-consumables-tab" data-toggle="pill"
                                        href="#pills-consumables" role="tab" aria-controls="pills-consumables"
                                        aria-selected="false">Consumables</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-spares-tab" data-toggle="pill" href="#pills-spares"
                                        role="tab" aria-controls="pills-spares" aria-selected="false">Spares</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-services-tab" data-toggle="pill" href="#pills-services"
                                        role="tab" aria-controls="pills-services" aria-selected="false">Services</a>
                                </li>
                            </ul>
                            <div class="tab-content" id="pills-tabContent">
                                <div class="tab-pane fade show active my-3" id="pills-product" role="tabpanel"
                                    aria-labelledby="pills-product-tab">
                                    <div class="d-flex justify-content-between align-items-center mb-3">
                                        <h6 class="fw-700 text-uppercase pl-1 mb-4">Products Details</h6>
                                        {{-- add products --}}
                                        <div>
                                            <span data-toggle="modal" data-target="#select_products">
                                                <button type="button" class="btn btn-primary btn-sm text-uppecase"
                                                    data-toggle="tooltip" data-placement="top" title="Add Products">
                                                    <i class="fas fa-plus"></i> Add
                                                </button>
                                            </span>
                                            <!-- Modal Add Products -->
                                            <div class="modal fade custom-modal" id="select_products" tabindex="-1"
                                                role="dialog" aria-labelledby="select_products" aria-hidden="true">
                                                <div class="modal-dialog modal-xl modal-dialog-centered edit"
                                                    role="document">
                                                    <div class="modal-content">
                                                        <div class="icon">
                                                            <span class="fa-stack fa-2x text-success">
                                                                <i class="fas fa-circle fa-stack-2x"></i>
                                                                <i class="fas fa-plus fa-stack-1x fa-inverse"></i>
                                                            </span>
                                                        </div>
                                                        <div class="modal-header justify-content-center">
                                                            <h3 class="modal-title text-capitalize font-weight-bolder mt-3"
                                                                id="select_products">
                                                                Select Products
                                                            </h3>
                                                        </div>
                                                        <div class="modal-body pb-0">
                                                            <div class="table-responsive">
                                                                <table class="table modal-table" id="add_products_table">
                                                                    <thead>
                                                                        <tr>
                                                                            <th scope="col">Check/ Uncheck</th>
                                                                            <th scope="col">Product Category</th>
                                                                            <th scope="col">Brand Name</th>
                                                                            <th scope="col">Model Name</th>
                                                                            <th scope="col">Price</th>
                                                                        </tr>
                                                                    </thead>
                                                                </table>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer flex-nowrap">
                                                            <button type="button"
                                                                class="btn btn-outline-success btn-block btn-lg mt-0"
                                                                data-dismiss="modal">Close</button>
                                                            <button type="button" onclick="product_select()"
                                                                class="btn btn-success btn-block btn-lg mt-0">Done</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="modal fade custom-modal" id="prod_add" tabindex="-1" role="dialog"
                                                aria-labelledby="prod_add" aria-hidden="true">
                                                <div class="modal-dialog modal-xxl modal-dialog-centered edit"
                                                    role="document">
                                                    <div class="modal-content">
                                                        <div class="icon">
                                                            <span class="fa-stack fa-2x text-success">
                                                                <i class="fas fa-circle fa-stack-2x"></i>
                                                                <i class="fas fa-plus fa-stack-1x fa-inverse"></i>
                                                            </span>
                                                        </div>
                                                        <div class="modal-header justify-content-center">
                                                            <h3 class="modal-title text-capitalize font-weight-bolder mt-3"
                                                                id="prod_add">
                                                                Add Products
                                                            </h3>
                                                        </div>
                                                        <div class="modal-body pb-0">
                                                            <div class="table-responsive">
                                                                <table class="table modal-table" id="cusprod_table">
                                                                    <thead>
                                                                        <tr class="border-bottom">
                                                                            <th scope="col">Machine Serial No </th>
                                                                            <th scope="col">Serial Key No </th>
                                                                            <th scope="col">Model Name</th>
                                                                            <th scope="col">Contract Type</th>
                                                                            <th scope="col">Section</th>
                                                                        </tr>
                                                                    </thead>
                                                                </table>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer flex-nowrap">
                                                            <button type="button"
                                                                class="btn btn-outline-success btn-block btn-lg mt-0"
                                                                data-dismiss="modal">Close</button>
                                                            <button type="button" onclick="cuspro_select()"
                                                                class="btn btn-success btn-block btn-lg mt-0">Done</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- End Add Products -->
                                        </div>
                                    </div>
                                    {{-- Load customer products --}}
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-view">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Machine Serial No.</th>
                                                    <th scope="col">Serial Key No.</th>
                                                    <th scope="col">Model Name</th>
                                                    <th scope="col">Section</th>
                                                    <th scope="col">Contract Type</th>
                                                    <th scope="col">Installation Date</th>
                                                    <th scope="col">Contract</th>
                                                    <th scope="col">Ticket</th>
                                                    <th scope="col">Delivery</th>
                                                    <th scope="col">Invoice</th>
                                                    <th scope="col">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($product as $item)
                                                    <tr>
                                                        <td>{{ $item->Machine_Serial_No }}</td>
                                                        <td>{{ $item->Serial_Key_No }}</td>
                                                        <td>{{ $item->product_model_name }}</td>
                                                        <td>{{ $item->section }}</td>
                                                        <td>{{ $item->Contract_Type }}</td>
                                                        <td>{{ $item->installation_date }}</td>
                                                        <td>
                                                            @if (!$item->contract_id)
                                                                <a href="{{ url('/addcontract/' . $item->Machine_Serial_No) }}"
                                                                    class="btn btn-success text-uppercase">Create</a>
                                                            @else
                                                                {{ $item->contract_id }}
                                                            @endif
                                                        </td>
                                                        <td>
                                                            @if (!$item->ticket_id)
                                                                <a href="{{ url('/customerticket/' . $item->customer_id . '/' . $item->product_id . '/' . $item->Machine_Serial_No) }}"
                                                                    class="btn btn-success text-uppercase">Create</a>
                                                            @else
                                                                {{ $item->ticket_id }}
                                                            @endif
                                                        </td>
                                                        <td>DC{{ $item->challan_id }}</td>
                                                        <td>INV{{ $item->invoice_id }}</td>
                                                        <td class="d-flex flex-nowrap">
                                                            <span data-toggle="modal" data-target="#editproduct"
                                                                class="mr-1" data-cus_id="{{ $item->customer_id }}"
                                                                data-cus_branch="{{ $item->cus_branch_name }}"
                                                                data-prod_id="{{ $item->product_id }}"
                                                                data-machine_serial="{{ $item->Machine_Serial_No }}"
                                                                data-serial_key="{{ $item->Serial_Key_No }}"
                                                                data-model="{{ $item->product_model_name }}"
                                                                data-contract_type="{{ $item->Contract_Type }}"
                                                                data-section="{{ $item->section }}"
                                                                data-product_sold_by="{{ $item->product_sold_by }}"
                                                                data-technician_id="{{ $item->technician_id }}"
                                                                data-installation_date="{{ $item->installation_date }}"
                                                                data-techinician_branch="{{ $item->product_branch_name }}"
                                                                data-blacka3="{{ $item->Black_A3 }}"
                                                                data-blacka4="{{ $item->Black_A4 }}"
                                                                data-colora3="{{ $item->Color_A3 }}"
                                                                data-colora4="{{ $item->Color_A4 }}"
                                                                data-date="{{ $item->Date }}">
                                                                <button type="button" class="btn btn-outline-success"
                                                                    data-toggle="tooltip" data-placement="top"
                                                                    title="Edit Product">
                                                                    <i class="fas fa-edit"></i>
                                                                </button>
                                                            </span>
                                                            <span data-toggle="modal" data-target="#deleteproduct"
                                                                data-serial="{{ $item->Machine_Serial_No }}"
                                                                data-model="{{ $item->product_model_name }}">
                                                                <button type="button" class="btn btn-outline-danger"
                                                                    data-toggle="tooltip" data-placement="top"
                                                                    title="Delete Product">
                                                                    <i class="fas fa-trash-alt"></i>
                                                                </button>
                                                            </span>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                        <!-- Modal Edit Products -->
                                        <div class="modal fade custom-modal" id="editproduct" tabindex="-1" role="dialog"
                                            aria-labelledby="editproduct" aria-hidden="true">
                                            <div class="modal-dialog modal-xl modal-dialog-centered edit" role="document">
                                                <div class="modal-content">
                                                    <div class="icon">
                                                        <span class="fa-stack fa-2x text-success">
                                                            <i class="fas fa-circle fa-stack-2x"></i>
                                                            <i class="far fa-edit fa-stack-1x fa-inverse"></i>
                                                        </span>
                                                    </div>
                                                    <div class="modal-header justify-content-center">
                                                        <h3 class="modal-title text-capitalize font-weight-bolder mt-3"
                                                            id="editproduct">
                                                            Edit Product
                                                        </h3>
                                                    </div>
                                                    <form id="edit_form" method="POST">
                                                        @csrf
                                                        <div class="modal-body pb-0">
                                                            <div class="table-responsive">
                                                                <table class="table modal-table">
                                                                    <thead>
                                                                        <tr>
                                                                            <th scope="col">Machine Serial No</th>
                                                                            <th scope="col">Serial Key No</th>
                                                                            <th scope="col">Model Name</th>
                                                                            <th scope="col">Contract Type</th>
                                                                            <th scope="col">Section</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <tr>
                                                                            <td>
                                                                                <div class="form-group mb-0">
                                                                                    <input type="text" class="form-control"
                                                                                        id="edit_machine_serial"
                                                                                        name="edit_machine_serial" readonly>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="form-group mb-0">
                                                                                    <input type="text" class="form-control"
                                                                                        id="edit_serial_key"
                                                                                        name="edit_serial_key">
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="form-group mb-0">
                                                                                    <input type="text" class="form-control"
                                                                                        id="edit_model" name="edit_model"
                                                                                        readonly>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="form-group mb-0">
                                                                                    <input type="text" class="form-control"
                                                                                        id="edit_contract_type"
                                                                                        name="edit_contract_type">
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="form-group mb-0">
                                                                                    <input type="text" class="form-control"
                                                                                        id="edit_section"
                                                                                        name="edit_section">
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                            <h6 class="fw-700 text-uppercase pl-1 mb-4">
                                                                Installation Report</h6>
                                                            <div class="row">
                                                                <div class="form-group col-md-4">
                                                                    <label class="control-label active"
                                                                        for="edit_sold_by">Sold
                                                                        By
                                                                        <small>*</small></label>
                                                                    <select class="form-control" id="edit_sold_by"
                                                                        name="edit_sold_by"></select>
                                                                </div>
                                                                <div class="form-group col-md-4">
                                                                    <label for="edit_installation_date"
                                                                        class="mb-0 control-label active">Installed
                                                                        Date</label>
                                                                    <input id="edit_installation_date"
                                                                        type="edit_installation_date"
                                                                        class="flatpickr form-control">
                                                                </div>
                                                                <div class="form-group col-md-4">
                                                                    <label class="control-label active"
                                                                        for="edit_technician_id">Assigned Techician
                                                                        <small>*</small></label>
                                                                    <select class="form-control" id="edit_technician_id"
                                                                        name="edit_technician_id"></select>
                                                                </div>
                                                                <div class="form-group col-md-4">
                                                                    <label class="control-label active"
                                                                        for="edit_techinician_branch">Techician Branch
                                                                    </label>
                                                                    <select class="form-control"
                                                                        id="edit_techinician_branch"
                                                                        name="edit_techinician_branch"></select>
                                                                </div>
                                                            </div>

                                                            <h6 class="fw-700 text-uppercase pl-1 mb-4 mt-3">
                                                                Readings</h6>
                                                            <div class="row">
                                                                <div class="form-group col-md-4">
                                                                    <label class="control-label active"
                                                                        for="edit_blacka3">Black
                                                                        A3</label>
                                                                    <input type="text" class="form-control"
                                                                        id="edit_blacka3" name="edit_blacka3">
                                                                </div>
                                                                <div class="form-group col-md-4">
                                                                    <label class="control-label active"
                                                                        for="edit_blacka4">Black A4</label>
                                                                    <input type="text" class="form-control"
                                                                        id="edit_blacka4" name="edit_blacka4">
                                                                </div>
                                                                <div class="form-group col-md-4">
                                                                    <label class="control-label active"
                                                                        for="edit_colora3">Color
                                                                        A3
                                                                    </label>
                                                                    <input type="text" class="form-control"
                                                                        id="edit_colora3" name="edit_colora3">
                                                                </div>
                                                                <div class="form-group col-md-4">
                                                                    <label class="control-label active"
                                                                        for="edit_colora4">Color
                                                                        A4
                                                                    </label>
                                                                    <input type="text" class="form-control"
                                                                        id="edit_colora4" name="edit_colora4">
                                                                </div>
                                                                <div class="form-group col-md-4">
                                                                    <label for="edit_date"
                                                                        class="mb-0 control-label active">Date<small>*</small></label>
                                                                    <input id="edit_date" name="edit_date" type="date"
                                                                        class="flatpickr form-control">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer flex-nowrap">
                                                            <button type="button"
                                                                class="btn btn-outline-success btn-block btn-lg mt-0"
                                                                data-dismiss="modal">Close</button>
                                                            <button type="submit"
                                                                class="btn btn-success btn-block btn-lg mt-0">Done</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End Edit Products -->


                                        <!-- Modal Delete Products -->
                                        <div class="modal fade custom-modal" id="deleteproduct" tabindex="-1" role="dialog"
                                            aria-labelledby="deleteproduct" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                    <div class="icon">
                                                        <span class="fa-stack fa-2x text-primary">
                                                            <i class="fas fa-circle fa-stack-2x"></i>
                                                            <i class="fas fa-trash-alt fa-stack-1x fa-inverse"></i>
                                                        </span>
                                                    </div>
                                                    <form id="delete_form" method="POST">
                                                        @csrf
                                                        <div class="modal-header justify-content-center">
                                                            <h3 class="modal-title text-capitalize font-weight-bolder mt-3">
                                                                Delete Product?
                                                            </h3>
                                                        </div>
                                                        <div class="modal-body pb-0">
                                                            <p class="font-weight-bold text-center">
                                                                Are you sure you want to delete the Product?
                                                                <br>
                                                                You cannotundo this operation!
                                                            </p>
                                                            <div class="row justify-content-center">
                                                                <div class="col-md-11">
                                                                    <div class="row">
                                                                        <div class="form-group col-md-6">
                                                                            <label for="delete_serial_no"
                                                                                class="col-form-label">Machine
                                                                                Serial No</label>
                                                                            <input type="text" class="form-control"
                                                                                id="delete_serial_no"
                                                                                name="delete_serial_no" readonly>
                                                                        </div>
                                                                        <div class="form-group col-md-6">
                                                                            <label for="delete_model_name"
                                                                                class=" col-form-label">Model Name</label>
                                                                            <input type="text" class="form-control"
                                                                                id="delete_model_name" readonly>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer flex-nowrap">
                                                            <button type="button"
                                                                class="btn btn-outline-primary btn-block btn-lg mt-0"
                                                                data-dismiss="modal">Close</button>
                                                            <button type="submit"
                                                                class="btn btn-primary btn-block btn-lg mt-0">Delete</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End Delete Products -->
                                    </div>
                                </div>

                                <div class="tab-pane fade show my-3" id="pills-invoices" role="tabpanel"
                                    aria-labelledby="pills-invoices-tab">
                                    <h6 class="fw-700 text-uppercase pl-1 mb-4">Invoice Details</h6>
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-view">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Invoice ID</th>
                                                    <th scope="col">Invoice No.</th>
                                                    <th scope="col">Machine Serial No.</th>
                                                    <th scope="col">Invoice Date</th>
                                                    <th scope="col">Invoice Amount</th>
                                                    <th scope="col">Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($invoice as $item)
                                                    <tr>
                                                        <td>{{ $item->Invoice_Const }}{{ $item->Invoice_ID }}</td>
                                                        <td>{{ $item->invoice_number }}</td>
                                                        <td>{{ $item->Machine_Serial_No }}</td>
                                                        <td>{{ $item->Invoice_Date }}</td>
                                                        <td>{{ $item->Invoice_Amount }}</td>
                                                        <td>
                                                            <span class="badge badge-pill badge-secondary text-12">
                                                                {{ $item->Invoice_Status }}
                                                            </span>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="tab-pane fade show my-3" id="pills-tickets" role="tabpanel"
                                    aria-labelledby="pills-tickets-tab">
                                    <h6 class="fw-700 text-uppercase pl-1 mb-4">Ticket Details</h6>
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-view">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Ticket Id</th>
                                                    <th scope="col">Product Model Name</th>
                                                    <th scope="col">Machine Serial No</th>
                                                    <th scope="col">Defect Code</th>
                                                    <th scope="col">Created Date</th>
                                                    <th scope="col">Status</th>
                                                    <th scope="col">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($ticket as $item)
                                                    <tr>
                                                        <td>{{ $item->ticket_id }}</td>
                                                        <td>{{ $item->product_model }}</td>
                                                        <td>{{ $item->Machine_Serial_No }}</td>
                                                        <td>{{ $item->Ticket_Defect_Code }}</td>
                                                        <td>{{ $item->Created_Date }}</td>
                                                        <td>
                                                            @if ($item->Ticket_Status == 'open')
                                                                <span
                                                                    class="badge badge-pill badge-success text-12">{{ $item->Ticket_Status }}</span>
                                                            @else
                                                                <span
                                                                    class="badge badge-pill badge-secondary text-12">{{ $item->Ticket_Status }}</span>
                                                            @endif
                                                        </td>
                                                        <td>
                                                            <a href="{{ url('/viewtickets', [$item->ticket_id]) }}"
                                                                class="btn btn-outline-info">
                                                                <i class="fas fa-external-link-alt"></i>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="tab-pane fade show my-3" id="pills-delivery" role="tabpanel"
                                    aria-labelledby="pills-delivery-tab">
                                    <h6 class="fw-700 text-uppercase pl-1 mb-4">Delivery Challan</h6>
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-view">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Delivery ID</th>
                                                    <th scope="col">Machine Serial No.</th>
                                                    <th scope="col">Delivery Note</th>
                                                    <th scope="col">Delivery Date</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($delivery as $item)

                                                    <tr>
                                                        <td>{{ $item->Challan_Const }}{{ $item->Challan_ID }}</td>
                                                        <td>{{ $item->Machine_Serial_No }}</td>
                                                        <td>{{ $item->Delivery_Note }}</td>
                                                        <td>{{ $item->Delivery_Date }}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="tab-pane fade show my-3" id="pills-consumables" role="tabpanel"
                                    aria-labelledby="pills-consumables-tab">
                                    <h6 class="fw-700 text-uppercase pl-1 mb-4">Consumable Details</h6>
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-view">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Quantity</th>
                                                    <th scope="col">ID</th>
                                                    <th scope="col">Machine Serial No.</th>
                                                    <th scope="col">Model Name</th>
                                                    <th scope="col">Unit Price</th>
                                                    <th scope="col">Total Price</th>
                                                    <th scope="col">Date</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($consumables as $item)
                                                    <tr>
                                                        <td>{{ $item->Quantity }}</td>
                                                        <td>{{ $item->Delivery_Note }}</td>
                                                        <td>{{ $item->Machine_Serial_No }}</td>
                                                        <td>{{ $item->Item_Name }}</td>
                                                        <td>{{ $item->Unit_Price }}</td>
                                                        <td>{{ $item->Total_Bill_Amount }}</td>
                                                        <td>{{ $item->Delivery_Date }}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="tab-pane fade show my-3" id="pills-spares" role="tabpanel"
                                    aria-labelledby="pills-spares-tab">
                                    <h6 class="fw-700 text-uppercase pl-1 mb-4">Spare Details</h6>
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-view">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Quantity</th>
                                                    <th scope="col">ID</th>
                                                    <th scope="col">Machine Serial No.</th>
                                                    <th scope="col">Model Name</th>
                                                    <th scope="col">Unit Price</th>
                                                    <th scope="col">Total Price</th>
                                                    <th scope="col">Date</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($spares as $item)
                                                    <tr>
                                                        <td>{{ $item->Quantity }}</td>
                                                        <td>{{ $item->Delivery_Note }}</td>
                                                        <td>{{ $item->Machine_Serial_No }}</td>
                                                        <td>{{ $item->spare_model_name }}</td>
                                                        <td>{{ $item->spare_unit_price }}</td>
                                                        <td>{{ $item->Total_Bill_Amount }}</td>
                                                        <td>{{ $item->Delivery_Date }}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="tab-pane fade show my-3" id="pills-services" role="tabpanel"
                                    aria-labelledby="pills-services-tab">
                                    <h6 class="fw-700 text-uppercase pl-1 mb-4">Service Details</h6>
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-view">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Quantity</th>
                                                    <th scope="col">ID</th>
                                                    <th scope="col">Machine Serial No.</th>
                                                    <th scope="col">Model Name</th>
                                                    <th scope="col">Unit Price</th>
                                                    <th scope="col">Total Price</th>
                                                    <th scope="col">Date</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($services as $item)
                                                    <tr>
                                                        <td>{{ $item->Quantity }}</td>
                                                        <td>{{ $item->service_id }}</td>
                                                        <td>{{ $item->Machine_Serial_No }}</td>
                                                        <td>{{ $item->Item_Name }}</td>
                                                        <td>{{ $item->service_unit_price }}</td>
                                                        <td>{{ $item->Total_Bill_Amount }}</td>
                                                        <td>{{ date('Y-m-d', strtotime($item->Invoice_Date)) }}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    @include('components.alertmodal')
@endsection
@section('script')
    @if (session()->has('update'))
        <script>
            $(document).ready(function() {
                $('#updateAlert').modal("show");
            });

        </script>
    @endif
    @if (session()->has('delete'))
        <script>
            $(document).ready(function() {
                $('#deleteAlert').modal("show");
            });

        </script>
    @endif
    @if (session()->has('error'))
        <script>
            $(document).ready(function() {
                $('#errorAlert').modal("show");
            });

        </script>
    @endif
    <script>
        let cusid = $("#cus_id").text();
        let cusbranch = $("#cus_branch").text();
        $(document).ready(function() {
            loadProducts();
            //product_select();
            loadTechnicians();
            loadAssignedTechnicians();
            getBranch();
            $("#editproduct").on('show.bs.modal', function(event) {
                var prod = $(event.relatedTarget);
                var cus_id = prod.data('cus_id');
                var prod_id = prod.data('prod_id');
                var machine_serial = prod.data('machine_serial');
                var serial_key = prod.data('serial_key');
                var model = prod.data('model');
                var contract_type = prod.data('contract_type');
                var section = prod.data('section');
                var sold_by = prod.data('product_sold_by');
                var technician_id = prod.data('technician_id');
                var installation_date = prod.data('installation_date');
                var techinician_branch = prod.data('techinician_branch');
                var blacka3 = prod.data('blacka3');
                var blacka4 = prod.data('blacka4');
                var colora3 = prod.data('colora3');
                var colora4 = prod.data('colora4');
                var date = prod.data('date');
                console.log('colora4', date);

                var action = `{{ url('updatecusprod/${cus_id}/${prod_id}/${machine_serial}') }}`

                $('#edit_form').attr('action', action);
                $("#edit_machine_serial").val(machine_serial);
                $("#edit_serial_key").val(serial_key);
                $("#edit_model").val(model);
                $("#edit_contract_type").val(contract_type);
                $("#edit_section").val(section);
                $("#edit_sold_by").val(sold_by);
                $("#edit_technician_id").val(technician_id);
                $("#edit_installation_date").val(installation_date);
                $("#edit_techinician_branch").val(techinician_branch);
                $("#edit_blacka3").val(blacka3);
                $("#edit_blacka4").val(blacka4);
                $("#edit_colora3").val(colora3);
                $("#edit_colora4").val(colora4);
                $("#edit_date").val(date);
            });

            $("#deleteproduct").on('show.bs.modal', function(event) {
                var prod = $(event.relatedTarget);
                var serial = prod.data('serial');
                var model = prod.data('model');
                //console.log('model', model);
                var action = `{{ url('delcusprod/${serial}') }}`

                $('#delete_form').attr('action', action);
                $("#delete_serial_no").val(serial);
                $("#delete_model_name").val(model);
            });
        });


        function loadTechnicians() {
            return $.ajax({
                url: "/technicians",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    //console.log("Technicians", data);
                    $('#edit_sold_by').empty();
                    $('#edit_sold_by').append('<option selected="true" disabled></option>');
                    $('#edit_sold_by').prop('selectedIndex', 0);
                    $.each(data, function(key, value) {
                        var name = value.emp_firstname + value.emp_lastname;
                        $('#edit_sold_by').append($('<option></option>').attr('value',
                            name).text(name));
                    });
                    $('#edit_sold_by').append('<option value="others">Others</option>');
                },
                error: function(error) {
                    console.log('Technicians load error:', error);
                }
            });
        }

        function loadAssignedTechnicians() {
            return $.ajax({
                url: "/technicians",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    //console.log("Technicians", data);
                    $('#edit_technician_id').empty();
                    $('#edit_technician_id').append('<option selected="true" disabled></option>');
                    $('#edit_technician_id').prop('selectedIndex', 0);
                    $.each(data, function(key, value) {
                        var name = value.emp_firstname + value.emp_lastname;
                        $('#edit_technician_id').append($('<option></option>').attr('value',
                            name).text(name));
                    });
                    $('#edit_technician_id').append('<option value="others">Others</option>');
                },
                error: function(error) {
                    console.log('Technicians load error:', error);
                }
            });
        }

        function getBranch() {
            return $.ajax({
                url: "/branchname",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    //console.log("branchname", data);
                    $('#edit_techinician_branch').empty();
                    $('#edit_techinician_branch').append('<option selected="true" disabled></option>');
                    $('#edit_techinician_branch').prop('selectedIndex', 0);
                    $.each(data, function(key, value) {
                        $('#edit_techinician_branch').append($('<option></option>').attr('value', value
                            .org_branch_name).text(value.org_branch_name));
                    });
                    $('#edit_techinician_branch').append('<option value="others">Others</option>');
                },
                error: function(error) {
                    console.log('branchname error:', error);
                }
            });
        }

        function loadProducts() {
            return $.ajax({
                url: "/loadproducts",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    //console.log("loadproducts", data);
                    $('#productTableBody').remove();
                    var product = '';
                    product += '<tbody id="productTableBody">';
                    if (data == "") {
                        product += '<tr>';
                        product += '<td>No Data</td>';
                        product += '</tr>';
                    } else {
                        $.each(data, function(key, value) {
                            product += '<tr>';
                            var check =
                                `<td class='td-actions'><div class="custom-control custom-checkbox"> <input type="checkbox" class="custom-control-input" id="${value.product_id}" name="products" value='${JSON.stringify(value)}'> <label class="custom-control-label" for="${value.product_id}"></label> </div></td>`

                            product += check;
                            product += '<td>' + value.Product_Category + '</td>';
                            product += '<td>' + value.product_brand_name + '</td>';
                            product += '<td>' + value.product_model_name + '</td>';
                            product += '<td>' + value.product_price + '</td>';
                            product += '<td hidden>' + value.product_id + '</td>';
                            product += '</tr>';
                        });
                    }
                    product += '</tbody>';
                    $('#add_products_table').append(product);
                },
                error: function(error) {
                    console.log('loadproducts load error:', error);
                }
            });
        }

        function product_select() {
            var lineItemArray = [];
            $.each($("input[name='products']:checked"), function(index) {
                var lineItem = JSON.parse($(this).val());
                lineItemArray.push({
                    product_id: lineItem.product_id,
                    product_model: lineItem.product_model_name,
                    product_brand: lineItem.product_brand_name,
                });
            });
            $('input[name=products]').prop('checked', false);
            $('#select_products').modal('hide');
            $('#prod_add').modal('show');
            $('#prodcusTableBody').remove();
            var i = 1;
            var prodcus = '';
            prodcus += '<tbody id="prodcusTableBody">';
            $.each(lineItemArray, function(key, value) {
                prodcus += '<tr class="border-0">';
                var serialno = "<td class='td-actions'><div class='form-group'><input type='text' id='serialno" +
                    i +
                    "' class='form-control' placeholder='Machine No' ></div> </td>";
                prodcus += serialno;
                var serialkey = "<td class='td-actions'><div class='form-group'><input type='text' id='serialkey" +
                    i +
                    "' class='form-control' placeholder='Serial Key No' ></div>  </td>";
                prodcus += serialkey;
                prodcus += "<td id='product_model" + i + "'>" + value.product_model + "</td>";
                var contractype =
                    "<td class='td-actions'><div class='form-group'><input type='text' id='contractype" + i +
                    "' class='form-control' placeholder='contractype' ></div> </td>";
                prodcus += contractype;
                prodcus += "<td id='product_id" + i + "' hidden>" + value.product_id + "</td>";
                var section = "<td class='td-actions'><div class='form-group'><input type='text' id='section" + i +
                    "' class='form-control' placeholder='section' ></div> </td>";
                prodcus += section;
                prodcus += '</tr>';
                i++;
            });
            prodcus += '</tbody>';
            $('#cusprod_table').append(prodcus);
        }

        var nest = function(obj, keys, v) {
            if (keys.length === 1) {
                obj[keys[0]] = v;
            } else {
                var key = keys.shift();
                obj[key] = nest(typeof obj[key] === 'undefined' ? {} : obj[key], keys, v);
            }
            return obj;
        };

        function createReqObject($inItems) {
            var obj = {}
            $inItems.each(function() {
                if ($(this)[0].tagName == "INPUT" || $(this)[0].tagName == "SELECT") obj[this.id] = $(this).val();
                else obj[this.id] = $(this).text();
            });
            return obj;
        }

        function cuspro_select() {
            console.log("cuspro_select clicked");

            var rowCount = $('#prodcusTableBody >tr').length;
            var eleIDs = "";

            for (var i = 0; i < rowCount; i++) {
                var index = i + 1;
                eleIDs += "#serialno" + index + ", " + "#serialkey" + index + ", " + "#product_model" + index + ", " +
                    "#contractype" + index + ", " + "#product_id" + index + ", " + "#section" + index + ", ";
            }

            let $items = $(eleIDs + `#view_cus_id, #view_cus_branch`)

            let keyValue = this.createReqObject($items);
            let request = {
                "cus_id": cusid,
                "cus_branch": cusbranch,
            };

            let lineItems = [];
            for (var i = 0; i < rowCount; i++) {
                let index = i + 1;
                let lineItem = {};

                nest(lineItem, ["serialno"], keyValue["serialno" + index]);
                nest(lineItem, ["serialkey"], keyValue["serialkey" + index]);
                nest(lineItem, ["product_model"], keyValue["product_model" + index]);
                nest(lineItem, ["contractype"], keyValue["contractype" + index]);
                nest(lineItem, ["product_id"], keyValue["product_id" + index]);
                nest(lineItem, ["section"], keyValue["section" + index]);
                lineItems.push(lineItem);
            }
            console.log('lineitem', lineItems);
            nest(request, ["lineItems"], lineItems);
            let jsonStr = request; //JSON.stringify(request);

            console.log("jsonStr", jsonStr);
            $.ajax({
                url: "/addcusprod",
                type: "POST",
                data: jsonStr,
                dataType: 'json',
                success: function(data) {
                    console.log("data:", data);
                    if (data == "success") {
                        //console.log("prod added success");
                        $('#prod_add').modal('hide');
                        location.reload();
                    }
                    if (data == "error") {
                        console.log("prod added error");
                    }
                },
                error: function(error) {
                    console.log('addcusprod error:', error);
                }
            });
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

    </script>
@endsection
