@extends('layouts.app')
@section('mytitle', 'Add Customer')
@section('content')
    <div class="bg-top bg-gray p-4">
        <div class="d-flex">
            <h5 class="font-weight-bold text-uppercase mb-0">CLOSURE</h5>
        </div>
    </div>

    <section class="my-4 p-4 card-main">
        <div class="row">
            <div class="col-md-12 py-4">
                <form>
                    <div class="card shadow mb-4">
                        <div class="card-top ">
                            <div class="card-title mb-0">
                                <h5 class="font-weight-bold text-uppercase mb-0"><i class="fas fa-user-alt"></i></h5>
                            </div>
                        </div>
                        <div class="card-body pt-0">
                            <div class="row">
                                <div class="form-group col-md-6 fw-90">
                                    <label class="control-label active" for="closure_type">Closure Type</label>
                                    <select class="form-control" id="closure_type" name="closure_type">
                                        <option disabled selected></option>
                                        <option value="BrokenCall"> Broken Call </option>
                                        <option value="Resolved"> Resolved </option>
                                        <option value="Cancelled by Customer"> Cancelled by Customer </option>
                                        <option value="Closed with Discreation"> Closed with Discreation </option>
                                    </select>
                                </div>
                                <div class="form-group col-md-6 fw-90">
                                    <label class="control-label active" for="remarks">Remarks</label>
                                    <input type="text" class="form-control" id="closure_remarks" name="closure_remarks">
                                    <input type="hidden" class="form-control" id="closure_ticket_id"
                                        name="closure_ticket_id" value="{{ $id }}">
                                    <input type="hidden" class="form-control" id="closure_customer_id"
                                        name="closure_customer_id" value="{{ $cusid }}">
                                    <input type="hidden" class="form-control" id="assigned_technician"
                                        name="assigned_technician" value="{{ $ticket[0]->Assign_Technician_Name }}">
                                </div>
                            </div>
                            <div class="row hold" id="BrokenCall">
                                <div class="form-group col-md-6 fw-90">
                                    <label class="control-label active" id="label_blue">Resolution Type</label>
                                    <select class="form-control" id="broken_closure_disposition"
                                        name="broken_closure_disposition">
                                        <option disabled selected></option>
                                        <option value="Spare Not Available">Spare Not Available </option>
                                        <option value="Consumable Not Available">Consumable Not Available</option>
                                        <option value="EngineerNotAvailable"> Engineer Not Available</option>
                                    </select>
                                </div>

                                <div id="EngineerNotAvailable" class="col-md-12 pick">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label>Assign/Re-assign</label>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label active"
                                                    for="edit_technician">Techinician/Engineer</label>
                                                <select class="form-control" id="edit_technician" name="edit_technician">
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label active" for="edit_assigned_date">Assigned
                                                    Date</label>
                                                <input type="date" class="form-control flatpickr" id="edit_assigned_date"
                                                    name="edit_assigned_date">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label active" for="edit_comments">Comments</label>
                                                <input type="text" class="form-control" id="edit_comments"
                                                    name="edit_comments">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row hold" id="Resolved">
                                <div class="col-md-6 fw-90">
                                    <div class="form-group label-floating">
                                        <label class="control-label active" id="label_blue">Resolution Type</label>
                                        <select class="form-control" id="closure_disposition" name="closure_disposition">
                                            <option disabled selected></option>
                                            <option value="education">Customer Education </option>
                                            <option value="adjustment">Adjustment</option>
                                            <option value="replacement"> Parts Replacement</option>
                                            <option value="repair">General Repair</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label id="control-label active"
                                            class="font-weight-bold text-uppercase text-primary">Chargers</label>
                                    </div>
                                    <div class="form-group">
                                        <label id="control-label active">Services Chargers</label>
                                        <input type="text" class="form-control" id="closure_services_charges"
                                            name="closure_services_charges">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="font-weight-bold text-uppercase text-primary">Readings</label>
                                    </div>
                                    <div class="form-group">
                                        <label id="control-label active">Black A3</label>
                                        <input type="text" class="form-control" id="closure_black_a3"
                                            name="closure_black_a3">
                                    </div>
                                    <div class="form-group">
                                        <label id="control-label active">Color A3</label>
                                        <input type="text" class="form-control" id="closure_color_a3"
                                            name="closure_color_a3">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group pb-2">
                                        <label></label>
                                    </div>
                                    <div class="form-group ">
                                        <label id="control-label active">Black A4</label>
                                        <input type="text" class="form-control" id="closure_black_a4"
                                            name="closure_black_a4">
                                    </div>
                                    <div class="form-group">
                                        <label id="control-label active">Color A4</label>
                                        <input type="text" class="form-control" id="closure_color_a4"
                                            name="closure_color_a4">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div id="replacement" class="row pick">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label id="label_blue"
                                                    class="font-weight-bold text-uppercase text-primary">Parts
                                                    Replaced</label>
                                                <span data-toggle="modal" data-target="#addspares">
                                                    <button type="button"
                                                        class="btn btn-primary btn-sm text-uppecase d-block"
                                                        data-toggle="tooltip" data-placement="top"
                                                        data-original-title="Add Spares">
                                                        <i class="fas fa-plus"></i> Add
                                                    </button>
                                                </span>
                                            </div>
                                            <div class="form-group">
                                                <div class="table-responsive">
                                                    <table class="table modal-table">
                                                        <thead>
                                                            <tr>
                                                                <th scope="col">Spare Id</th>
                                                                <th scope="col">Spare Code Name</th>
                                                                <th scope="col">Spare Name</th>
                                                                <th scope="col">No.Of Units</th>
                                                                <th scope="col">Unit Price</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="spare_select_table"></tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 mt-2 pt-1">
                                            <div class="form-group">
                                                <span data-toggle="modal" data-target="#addconsumables">
                                                    <button type="button"
                                                        class="btn btn-primary btn-sm text-uppecase d-block"
                                                        data-toggle="tooltip" data-placement="top"
                                                        data-original-title="Add Consumable">
                                                        <i class="fas fa-plus"></i> Add
                                                    </button>
                                                </span>
                                            </div>
                                            <div class="form-group ">
                                                <div class="table-responsive">
                                                    <table class="table modal-table">
                                                        <thead class="text-center">
                                                            <th>Consumable Id</th>
                                                            <th>Consumable Code Name</th>
                                                            <th>Consumable Name</th>
                                                            <th>No.Of Units</th>
                                                            <th>Unit Price</th>
                                                        </thead>
                                                        <tbody id="consumable_select_table"></tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <a href="{{ url()->previous() }}" class="btn btn-outline-primary btn-lg btn-large mr-2">Cancel</a>
                        <button type="button" class="btn btn-primary btn-lg btn-large"
                            onclick="closurevalidate()">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </section>

    {{-- add Spares modal --}}
    <div class="modal fade custom-modal" id="addspares" tabindex="-1" role="dialog" aria-labelledby="addspares"
        aria-hidden="true">
        <div class="modal-dialog modal-xxl modal-dialog-centered edit" role="document">
            <div class="modal-content">
                <div class="icon">
                    <span class="fa-stack fa-2x text-success">
                        <i class="fas fa-circle fa-stack-2x"></i>
                        <i class="fas fa-plus fa-stack-1x fa-inverse"></i>
                    </span>
                </div>
                <div class="modal-header justify-content-center">
                    <h3 class="modal-title text-capitalize font-weight-bolder mt-3" id="addspares">
                        Select Spares
                    </h3>
                </div>
                <div class="modal-body pb-0">
                    <div class="table-responsive">
                        <table id="spare_table" class="table modal-table">
                            <thead>
                                <tr>
                                    <th scope="col">Check/ Uncheck</th>
                                    <th scope="col">Spare ID</th>
                                    <th scope="col">Spare Code Name</th>
                                    <th scope="col">Spare Name</th>
                                    <th scope="col">Price</th>
                                    <th scope="col">Gst Percentage</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <div class="modal-footer flex-nowrap">
                    <button type="button" class="btn btn-outline-success btn-block btn-lg mt-0"
                        data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-success btn-block btn-lg mt-0"
                        onclick="spare_select()">Done</button>
                </div>
            </div>
        </div>
    </div>
    {{-- end Spares modal --}}
    {{-- add Consumable modal --}}
    <div class="modal fade custom-modal" id="addconsumables" tabindex="-1" role="dialog" aria-labelledby="addconsumables"
        aria-hidden="true">
        <div class="modal-dialog modal-xxl modal-dialog-centered edit" role="document">
            <div class="modal-content">
                <div class="icon">
                    <span class="fa-stack fa-2x text-success">
                        <i class="fas fa-circle fa-stack-2x"></i>
                        <i class="fas fa-plus fa-stack-1x fa-inverse"></i>
                    </span>
                </div>
                <div class="modal-header justify-content-center">
                    <h3 class="modal-title text-capitalize font-weight-bolder mt-3" id="addconsumables">
                        Select Consumable Product
                    </h3>
                </div>
                <div class="modal-body pb-0">
                    <div class="table-responsive">
                        <table id="consumable_table" class="table modal-table">
                            <thead>
                                <tr>
                                    <th scope="col">Check/ Uncheck</th>
                                    <th scope="col">Consumable ID</th>
                                    <th scope="col">Consumable Code Name</th>
                                    <th scope="col">Consumable Name</th>
                                    <th scope="col">Price</th>
                                    <th scope="col">Gst Percentage</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <div class="modal-footer flex-nowrap">
                    <button type="button" class="btn btn-outline-success btn-block btn-lg mt-0"
                        data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-success btn-block btn-lg mt-0"
                        onclick="consumable_select()">Done</button>
                </div>
            </div>
        </div>
    </div>
    {{-- end Consumable modal --}}

    <!-- Modal edit_ticket -->
    {{-- <div class="modal fade custom-modal" id="edit_ticket" tabindex="-1" role="dialog" aria-labelledby="edit_ticket"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered edit modal-xl" role="document">
            <div class="modal-content">
                <div class="icon">
                    <span class="fa-stack fa-2x text-success">
                        <i class="fas fa-circle fa-stack-2x"></i>
                        <i class="far fa-edit fa-stack-1x fa-inverse"></i>
                    </span>
                </div>
                <div class="modal-header justify-content-center">
                    <h3 class="modal-title text-capitalize font-weight-bolder mt-3" id="edit_ticket">
                        Edit Ticket
                    </h3>
                </div>
                <form id="edit_ticket_form">
                    <div class="modal-body pb-0">
                        <div class="row">
                            <div class="col-md-12">
                                <label>Assign/Re-assign</label>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label active" for="edit_technician">Techinician/Engineer</label>
                                    <select class="form-control" id="edit_technician" name="edit_technician">
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label active" for="edit_assigned_date">Assigned Date</label>
                                    <input type="date" class="form-control flatpickr" id="edit_assigned_date"
                                        name="edit_assigned_date">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label active" for="edit_comments">Comments</label>
                                    <input type="text" class="form-control" id="edit_comments" name="edit_comments">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer flex-nowrap">
                        <button type="button" class="btn btn-outline-success btn-block btn-lg mt-0"
                            data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-success btn-block btn-lg mt-0">Done</button>
                    </div>
                </form>
            </div>
        </div>
    </div> --}}
    <!-- End edit_ticket -->
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $("#closure_type").change(function() {
                $(this).find("option:selected").each(function() {
                    var optionValue = $(this).attr("value");
                    if (optionValue) {
                        $(".hold").not("#" + optionValue).hide();
                        $("#" + optionValue).show();
                    } else {
                        $(".hold").hide();
                    }
                });
            }).change();
            $("#closure_disposition").change(function() {
                $(this).find("option:selected").each(function() {
                    var optionValue = $(this).attr("value");
                    if (optionValue) {
                        $(".pick").not("#" + optionValue).hide();
                        $("#" + optionValue).show();
                    } else {
                        $(".pick").hide();
                    }
                });
            }).change();
            $("#broken_closure_disposition").change(function() {
                $(this).find("option:selected").each(function() {
                    var optionValue = $(this).attr("value");
                    if (optionValue) {
                        $(".pick").not("#" + optionValue).hide();
                        $("#" + optionValue).show();
                    } else {
                        $(".pick").hide();
                    }
                });
            }).change();

            var cus_id = $("#closure_ticket_id").val();
            var ticket_id = $("#closure_customer_id").val();
            //console.log('cus_id:', cus_id)
            //console.log('ticket_id:', ticket_id)
            loadTechnicians();
            loadspares();
            loadconsumable();
        });


        function loadTechnicians() {
            return $.ajax({
                url: "/technicians",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    //console.log("Technicians", data);
                    $('#edit_technician').empty();
                    $('#edit_technician').append('<option selected="true" disabled></option>');
                    $('#edit_technician').prop('selectedIndex', 0);
                    $.each(data, function(key, value) {
                        var name = value.emp_firstname + value.emp_lastname;
                        $('#edit_technician').append($('<option></option>').attr('value',
                            name).text(name));
                    });
                },
                error: function(error) {
                    console.log('Technicians load error:', error);
                }
            });
        }

        function loadspares() {
            return $.ajax({
                url: "/loadspares",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    //console.log("loadspares", data);
                    $('#spareTableBody').remove();
                    var spare = '';
                    spare += '<tbody id="spareTableBody">';
                    if (data == "") {
                        spare += '<tr>';
                        spare += '<td>No Data</td>';
                        spare += '</tr>';
                    } else {
                        $.each(data, function(key, value) {
                            spare += '<tr>';
                            var check =
                                `<td class='td-actions'><div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input" id="${value.spare_const + value.spare_id}" name="spares" value='${JSON.stringify(value)}'> <label class="custom-control-label" for="${value.spare_const + value.spare_id}"></label></div></td>`

                            spare += check;
                            spare += '<td>' + value.spare_const + value.spare_id + '</td>';
                            spare += '<td>' + value.spare_code_name + '</td>';
                            spare += '<td>' + value.spare_model_name + '</td>';
                            spare += '<td>' + value.spare_unit_price + '</td>';
                            spare += '<td>' + value.Gst_Percentage + '</td>';
                            spare += '</tr>';
                        });
                    }
                    spare += '</tbody>';
                    $('#spare_table').append(spare);
                },
                error: function(error) {
                    console.log('loadspares error:', error);
                }
            });
        }

        function loadconsumable() {
            return $.ajax({
                url: "/loadconsumable",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    // console.log("loadconsumable", data);
                    $('#consumableTableBody').remove();
                    var consumable = '';
                    consumable += '<tbody id="consumableTableBody">';
                    if (data == "") {
                        consumable += '<tr>';
                        consumable += '<td>No Data</td>';
                        consumable += '</tr>';
                    } else {
                        $.each(data, function(key, value) {
                            consumable += '<tr>';
                            var check =
                                `<td class='td-actions'><div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input" id="${value.consumable_const + value.consumable_id}" name="consumable" value='${JSON.stringify(value)}'> <label class="custom-control-label" for="${value.consumable_const + value.consumable_id}"></label></div></td>`

                            consumable += check;
                            consumable += '<td>' + value.consumable_const + value.consumable_id +
                                '</td>';
                            consumable += '<td>' + value.consumable_code_name + '</td>';
                            consumable += '<td>' + value.consumable_name + '</td>';
                            consumable += '<td>' + value.consumable_unit_price + '</td>';
                            consumable += '<td>' + value.consumable_gst_percentage + '</td>';
                            consumable += '</tr>';
                        });
                    }
                    consumable += '</tbody>';
                    $('#consumable_table').append(consumable);
                },
                error: function(error) {
                    console.log('loadconsumable error:', error);
                }
            });
        }

        function spare_select() {
            var lineItemArray = [];
            $.each($("input[name='spares']:checked"), function(index) {
                var lineItem = JSON.parse($(this).val());
                lineItemArray.push({
                    id: lineItem.spare_const + lineItem.spare_id,
                    code: lineItem.spare_code_name,
                    name: lineItem.spare_model_name,
                    units: lineItem.spare_parts_unit,
                    price: lineItem.spare_unit_price
                });
            });
            $('input[name=spares]').prop('checked', false);
            $('#addspares').modal('hide');
            //console.log('lineItemArray:', lineItemArray);
            add_to_spare(lineItemArray);
        }

        function consumable_select() {
            var lineItemArray = [];
            $.each($("input[name='consumable']:checked"), function(index) {
                var lineItem = JSON.parse($(this).val());
                lineItemArray.push({
                    id: lineItem.consumable_id,
                    code: lineItem.consumable_code_name,
                    name: lineItem.consumable_name,
                    units: lineItem.consumable_unit,
                    price: lineItem.consumable_unit_price
                });
            });
            $('input[name=consumable]').prop('checked', false);
            $('#addconsumables').modal('hide');
            //console.log('lineItemArray:', lineItemArray);
            add_to_consumable(lineItemArray);
        }

        function add_to_spare(lineItemArray) {
            var rowCount = $('#spare_select_table >tr').length;
            var i = rowCount + 1
            var spare = "";
            $.each(lineItemArray, function(key, value) {
                spare += '<tr>';
                spare += "<td id='spare_id" + i + "'>" + value.id + "</td>";
                spare += "<td id='spare_codename" + i + "'>" + value.code + "</td>";
                spare += "<td id='spare_name" + i + "'>" + value.name + "</td>";
                spare += "<td id='spare_noofunits" + i + "'>" + value.units + "</td>";
                spare += "<td id='spare_unitprice" + i + "'>" + value.price + "</td>";
                spare += '</tr>';
                i++;
            });
            $('#spare_select_table').append(spare);
        }

        function add_to_consumable(lineItemArray) {
            var rowCount = $('#consumable_select_table >tr').length;
            var i = rowCount + 1
            var consumable = "";
            $.each(lineItemArray, function(key, value) {
                consumable += '<tr>';
                consumable += "<td id='consumable_id" + i + "'>" + value.id + "</td>";
                consumable += "<td id='consumable_codename" + i + "'>" + value.code + "</td>";
                consumable += "<td id='consumable_name" + i + "'>" + value.name + "</td>";
                consumable += "<td id='consumable_noofunit" + i + "'>" + value.units + "</td>";
                consumable += "<td id='consumable_unitprice" + i + "'>" + value.price + "</td>";
                consumable += '</tr>';
                i++;
            });
            $('#consumable_select_table').append(consumable);
        }


        function closurevalidate() {
            var rowCount1 = $('#spare_select_table >tr').length;
            var eleIDs1 = "";
            for (var i = 0; i < rowCount1; i++) {
                var index = i + 1;

                eleIDs1 += "#spare_id" + index + ", " + "#spare_codename" + index + ", " + "#spare_name" + index + ", " +
                    "#spare_noofunits" + index + ", " + "#spare_unitprice" + index + ", ";

            }
            var rowCount2 = $('#consumable_select_table  >tr').length;
            var eleIDs2 = "";
            for (var i = 0; i < rowCount2; i++) {
                var index = i + 1;

                eleIDs2 += "#consumable_id" + index + ", " + "#consumable_codename" + index + ", " + "#consumable_name" +
                    index + ", " + "#consumable_noofunit" + index + ", " + "#consumable_unitprice" + index + ", ";

            }

            let $items = $(eleIDs1 + eleIDs2 +
                `#closure_ticket_id, #closure_customer_id, #closure_type, #closure_remarks, #closure_disposition, #broken_closure_disposition,#assigned_technician, #edit_technician, #edit_assigned_date, #edit_comments, #closure_services_charges, #closure_black_a3, #closure_color_a3, #closure_black_a4, #closure_color_a4`
            )
            let keyValue = this.createReqObject($items);

            let request = {
                "ticket_id": keyValue["closure_ticket_id"],
                "customer_id": keyValue["closure_customer_id"]
            };

            nest(request, ["closure", "closure_type"], keyValue["closure_type"]);
            nest(request, ["closure", "closure_remarks"], keyValue["closure_remarks"]);
            nest(request, ["closure", "closure_disposition"], keyValue["closure_disposition"]);
            nest(request, ["closure", "broken_closure_disposition"], keyValue["broken_closure_disposition"]);
            nest(request, ["closure", "assigned_technician"], keyValue["assigned_technician"]);
            nest(request, ["closure", "reassigned_technician"], keyValue["edit_technician"]);
            nest(request, ["closure", "assigned_date"], keyValue["edit_assigned_date"]);
            nest(request, ["closure", "comments"], keyValue["edit_comments"]);
            nest(request, ["closure", "closure_services_charges"], keyValue["closure_services_charges"]);
            nest(request, ["closure", "closure_black_a3"], keyValue["closure_black_a3"]);
            nest(request, ["closure", "closure_color_a3"], keyValue["closure_color_a3"]);
            nest(request, ["closure", "closure_black_a4"], keyValue["closure_black_a4"]);
            nest(request, ["closure", "closure_color_a4"], keyValue["closure_color_a4"]);
            //undefined fields
            //nest(request, ["closure", "closure_broken"], keyValue["closure_broken"]);

            let lineItems = {};
            let spares = [];
            let consumables = [];
            for (var i = 0; i < rowCount1; i++) {

                let index = i + 1;

                let lineItem = {};
                nest(lineItem, ["id"], keyValue["spare_id" + index]);
                nest(lineItem, ["codename"], keyValue["spare_codename" + index]);
                nest(lineItem, ["name"], keyValue["spare_name" + index]);
                nest(lineItem, ["units"], keyValue["spare_noofunits" + index]);
                nest(lineItem, ["price"], keyValue["spare_unitprice" + index]);

                spares.push(lineItem);
            }

            for (var i = 0; i < rowCount2; i++) {

                let index = i + 1;
                let lineItem = {};

                nest(lineItem, ["cons_id"], keyValue["consumable_id" + index]);
                nest(lineItem, ["cons_codename"], keyValue["consumable_codename" + index]);
                nest(lineItem, ["cons_name"], keyValue["consumable_name" + index]);
                nest(lineItem, ["cons_units"], keyValue["consumable_noofunit" + index]);
                nest(lineItem, ["cons_price"], keyValue["consumable_unitprice" + index]);

                consumables.push(lineItem);
            }
            if (spares.length === 0) {
                console.log('spares is empty');
                spares = 'empty'
            }
            if (consumables.length === 0) {
                console.log('consumables is empty');
                consumables = 'empty'
            }

            nest(lineItems, ["spares"], spares);
            nest(lineItems, ["consumables"], consumables);
            nest(request, ["lineItems"], lineItems);

            let jsonStr = request;
            console.log('closurevalidate clicked')
            console.log('jsonStr:', jsonStr)

            $.ajax({
                url: "/addclosure",
                type: "POST",
                data: jsonStr,
                dataType: 'json',
                success: function(data) {
                    console.log("data:", data);
                    if (data == "success") {
                        console.log("data: ", data);
                        return window.location.href = "/viewticket";
                    }
                    if (data == "error") {
                        console.log("contract added error");
                    }
                },
                error: function(error) {
                    console.log('addcontract error:', error);
                }
            });
        }
        var nest = function(obj, keys, v) {
            if (keys.length === 1) {
                obj[keys[0]] = v;
            } else {
                var key = keys.shift();
                obj[key] = nest(typeof obj[key] === 'undefined' ? {} : obj[key], keys, v);
            }

            return obj;
        };

        function createReqObject($inItems) {
            var obj = {}
            $inItems.each(function() {
                if ($(this)[0].tagName == "INPUT" || $(this)[0].tagName == "SELECT") obj[this.id] = $(this).val();
                else obj[this.id] = $(this).text();
            });
            return obj;
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

    </script>
@endsection
