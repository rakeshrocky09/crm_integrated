@extends('layouts.app')
@section('mytitle', 'View Invoice')
@section('head-links')
    <link href="{{ URL::asset('assets/css/datatables.min.css') }}" rel="stylesheet">
@endsection
@section('content')
    <div class="bg-top bg-gray p-4">
        <div class="d-flex">
            <h5 class="font-weight-bold text-uppercase mb-0">View Invoice</h5>
        </div>
    </div>

    <div class="p-4 card-main">
        <section class="my-5">
            <div class="card shadow">
                <div class="card-top ">
                    <div class="card-title mb-0">
                        <h5 class="font-weight-bold text-uppercase mb-0"><i class="fas fa-ticket-alt"></i></h5>
                    </div>
                    <div class="btn-right position-relative float-right">
                        <a href="/addinvoice" class="btn btn-primary text-uppecase" data-toggle="tooltip"
                            data-placement="top" title="Add Invoice">
                            <i class="fas fa-plus"></i> Add
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table display" id="datatable">
                            <thead class="text-uppercase">
                                <tr>
                                    <th scope="col">INVOICE ID</th>
                                    <th scope="col">CUSTOMER NAME</th>
                                    <th scope="col">INVOICE NO</th>
                                    <th scope="col">INVOICE DATE</th>
                                    <th scope="col">AMOUNT</th>
                                    <th scope="col">STATUS</th>
                                    <th scope="col" class="nosort">ACTION</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($invoicelist as $item)
                                    <tr>
                                        <td>{{ $item->Invoice_ID }}</td>
                                        <td>{{ $item->customer_name }}</td>
                                        <td>{{ $item->invoice_number }}</td>
                                        <td>{{ $item->Invoice_Created_Date }}</td>
                                        <td>{{ $item->total_amount }}</td>
                                        <td>
                                            @if ($item->Status == 'open')
                                                <span class="badge badge-pill badge-success text-12">
                                                    {{ $item->Status }}
                                                </span>
                                            @else
                                                <span class="badge badge-pill badge-secondary text-12">
                                                    {{ $item->Status }}
                                                </span>
                                            @endif
                                        </td>
                                        <td class="d-flex flex-nowrap nosort">
                                            <span id="view_data" data-toggle="modal" data-target="#view_invoice_modal"
                                                data-id="{{ $item->Invoice_ID }}"
                                                data-branch_id="{{ $item->Seller_Branch_Id }}"
                                                data-cus_id="{{ $item->Customer_Id }}">
                                                <button type="button" class="btn btn-outline-info mr-1"
                                                    data-toggle="tooltip" data-placement="top" title="View invoice details">
                                                    <i class="fas fa-external-link-alt"></i>
                                                </button>
                                            </span>
                                            <a href="{{ url('addreceipt/' . $item->Invoice_ID) }}"
                                                class="btn btn-outline-primary mr-1" data-toggle="tooltip"
                                                data-placement="top" title="Add receipt">
                                                <i class="fas fa-plus"></i>
                                            </a>
                                            <a href="{{ url('edit_invoice', $item->Invoice_ID) }}"
                                                class="btn btn-outline-success mr-1" data-toggle="tooltip"
                                                data-placement="top" title="Edit invoice">
                                                <i class="fas fa-edit"></i>
                                            </a>
                                            <span data-toggle="modal" data-target="#deleteinvoice"
                                                data-inv_id="{{ $item->Invoice_ID }}"
                                                data-cus_name="{{ $item->customer_name }}"
                                                data-inv_number="{{ $item->invoice_number }}"
                                                data-inv_date="{{ $item->Invoice_Created_Date }}"
                                                data-inv_amount="{{ $item->total_amount }}">
                                                <button type="button" class="btn btn-outline-danger" data-toggle="tooltip"
                                                    data-placement="top" title="Delete invoice">
                                                    <i class="fas fa-trash-alt"></i>
                                                </button>
                                            </span>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <!-- Modal View Invoice -->
                        <div class="modal fade custom-modal" id="view_invoice_modal" tabindex="-1" role="dialog"
                            aria-labelledby="view_invoice_modal" aria-hidden="true">
                            <div class="modal-dialog modal-xxl modal-dialog-centered edit" role="document">
                                <div class="modal-content">
                                    <div class="icon">
                                        <span class="fa-stack fa-2x text-success">
                                            <i class="fas fa-circle fa-stack-2x"></i>
                                            <i class="fas fa-table fa-stack-1x fa-inverse"></i>
                                        </span>
                                    </div>
                                    <div class="modal-header justify-content-center">
                                        <h3 class="modal-title text-capitalize font-weight-bolder mt-3"
                                            id="view_invoice_modal">
                                            TAX INVOICE
                                        </h3>
                                    </div>

                                    <div class="modal-body pb-0">
                                        <div class="invoice_content">
                                            <div id="invoice_content">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="row text-sm">
                                                            <div class="col-12 mb-3">
                                                                <img class="d-inline-block"
                                                                    src="{{ asset('assets/img/km_logo.png') }}">
                                                                <h6 class="d-inline-block fw-700 text-uppercase"
                                                                    id="c_name"></h6>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <address id="v_comp_addr" class="mb-0"></address>
                                                                <p class="mb-0" id="gst_no"></p>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group mb-0">
                                                                    <label class="font-weight-bold mb-0">Invoice No:</label>
                                                                    <label class="mb-0" id="invoice_no"></label>
                                                                </div>
                                                                <div class="form-group mb-0">
                                                                    <label class="font-weight-bold mb-0">Invoice
                                                                        Date:</label>
                                                                    <label class="mb-0" id="invoice_date"></label>
                                                                </div>
                                                                <div class="form-group mb-0">
                                                                    <label class="font-weight-bold mb-0">Delivery
                                                                        Note:</label>
                                                                    <label class="mb-0" id="delivery_note"></label>
                                                                </div>
                                                                <div class="form-group mb-0">
                                                                    <label class="font-weight-bold mb-0">Delivery Note
                                                                        Date:</label>
                                                                    <label class="mb-0" id="delivery_date"></label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group mb-0">
                                                                    <label class="font-weight-bold mb-0">Mode of
                                                                        Payment:</label>
                                                                    <label class="mb-0" id="mod_payment"></label>
                                                                </div>
                                                                <div class="form-group mb-0">
                                                                    <label class="font-weight-bold mb-0">Supplier
                                                                        Ref Num:</label>
                                                                    <label class="mb-0" id="supplier_ref_no"></label>
                                                                </div>
                                                                <div class="form-group mb-0">
                                                                    <label class="font-weight-bold mb-0">Other
                                                                        Reference:</label>
                                                                    <label class="mb-0" id="other_ref_no"></label>
                                                                </div>
                                                                <div class="form-group mb-0">
                                                                    <label class="font-weight-bold mb-0">Buyer
                                                                        Order No:</label>
                                                                    <label class="mb-0" id="buyer_order_no"></label>
                                                                </div>
                                                                <div class="form-group mb-0">
                                                                    <label class="font-weight-bold mb-0">Buyer
                                                                        Order Date:</label>
                                                                    <label class="mb-0" id="buyer_order_date"></label>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-12">
                                                                <div class="row mt-3">
                                                                    <div class="col-md-4">
                                                                        <address class="mb-0">
                                                                            <label class="font-weight-bold">Billed
                                                                                To</label>
                                                                            <p id="cust_name" class="mb-0"></p>
                                                                            <p id="cust_addr" class="mb-0"></p>
                                                                            <p class="mb-0" id="cust_phone"></p>
                                                                        </address>
                                                                        <p class="mb-0" id="cust_gst_no"></p>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <address>
                                                                            <label
                                                                                class="font-weight-bold">Consignee</label>
                                                                            <p id="delivery_name" class="mb-0"></p>
                                                                            <p id="delivery_addr" class="mb-0"></p>
                                                                            <p class="mb-0" id="delivery_phone"></p>
                                                                        </address>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-12">
                                                    <h5 class="fw-700 mt-4 mb-0">Order Summary</h5>
                                                    <hr class="mt-2">
                                                </div>
                                            </div>
                                            <table class="table table-bordered text-center">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">Item Id</th>
                                                        <th scope="col">Item Name</th>
                                                        <th scope="col">HSN/SAC</th>
                                                        <th scope="col">GST</th>
                                                        <th scope="col">Unit Price</th>
                                                        <th scope="col">Quantity</th>
                                                        <th scope="col">Discount</th>
                                                        <th scope="col">Description</th>
                                                        <th scope="col">Item Amount</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="item_table_body">
                                                </tbody>
                                                <tfoot id="item_table_footer"></tfoot>
                                            </table>
                                            <h6 class="fw-700 mt-4">Tax details</h6>
                                            <table class="table table-bordered text-center">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">Taxable Amount</th>
                                                        <th scope="col">CGST %</th>
                                                        <th scope="col">CGST Amount</th>
                                                        <th scope="col">SGST %</th>
                                                        <th scope="col">SGST Amount</th>
                                                        <th scope="col">Tax Amount</th>
                                                        <th scope="col">Total Amount</th>
                                                    </tr>
                                                </thead>
                                                <tbody class="font-weight-bold" id="tax_table_body">
                                                </tbody>
                                                <tfoot id="tax_table_footer"></tfoot>
                                            </table>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="row text-sm">
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label class="font-weight-bold h6 text-capitalize">Bank
                                                                    Details</label>
                                                                <address class="text-12 mb-0" id="bank_details"></address>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group mb-0">
                                                                <label class="font-weight-bold">Total Bill
                                                                    Amount</label>
                                                                <p class="mb-0"><span>(including GST)</span></p>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group mb-0 font-weight-bold">
                                                                <label id="invoice_amount"></label>
                                                                <p class="mb-0 text-capitalize" id="invoice_amount_words">
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 my-3">
                                                    <div class="row">
                                                        <div class="col-md-6 font-weight-bold">
                                                            <h6 class="fw-700 text-sm">Declaration</h6>
                                                            <p class="mb-0 font-weight-light text-10">
                                                                We declare that this invoice shows the actual price of
                                                                the
                                                                goods
                                                                described <br>and that all
                                                                the particulars are true and correct.
                                                            </p>
                                                        </div>
                                                        <div class="col-md-3"></div>
                                                        <div class="col-md-3 font-weight-bold float-right">
                                                            <h6 class="text-uppercase mb-4 pb-1">KM ENTERPRISES</h6>
                                                            <p class="mb-0">Authorised signatory</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                    <div id="edit_modal_btn" class="modal-footer flex-nowrap">
                                        {{-- <button type="button" class="btn btn-outline-success btn-block btn-lg mt-0"
                                            data-dismiss="modal">Close</button>
                                        <a href="{{ url('printinvoice') }}" id="download_invoice"
                                            class="btn btn-success btn-block btn-lg mt-0">Download</a> --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End View Invoice -->
                        <!-- Modal Delete Invoice -->
                        <div class="modal fade custom-modal" id="deleteinvoice" tabindex="-1" role="dialog"
                            aria-labelledby="deleteinvoice" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <form id="delete_form" method="GET">
                                    @csrf
                                    <div class="modal-content">
                                        <div class="icon">
                                            <span class="fa-stack fa-2x text-primary">
                                                <i class="fas fa-circle fa-stack-2x"></i>
                                                <i class="fas fa-trash-alt fa-stack-1x fa-inverse"></i>
                                            </span>
                                        </div>
                                        <div class="modal-header justify-content-center">
                                            <h3 class="modal-title text-capitalize font-weight-bolder mt-3"
                                                id="deleteinvoice">
                                                Delete
                                                Invoice?
                                            </h3>
                                        </div>
                                        <div class="modal-body pb-0">
                                            <p class="font-weight-bold text-center">
                                                Are you sure you want to delete the Invoice?
                                                <br>
                                                You cannot
                                                undo this operation!
                                            </p>
                                            <div class="row justify-content-center">
                                                <div class="col-md-11">
                                                    <form>
                                                        <div class="row">
                                                            <div class="form-group col-md-6">
                                                                <label for="delete_cus_name" class="col-form-label">Customer
                                                                    Name</label>
                                                                <input type="text" class="form-control" id="delete_cus_name"
                                                                    name="delete_cus_name" readonly>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label for="delete_inv_no" class="col-form-label">Invoice
                                                                    No.</label>
                                                                <input type="text" class="form-control" id="delete_inv_no"
                                                                    name="delete_inv_no" readonly>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label for="delete_inv_date" class="col-form-label">Invoice
                                                                    Date</label>
                                                                <input type="text" class="form-control" id="delete_inv_date"
                                                                    name="delete_inv_date" readonly>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label for="delete_inv_amount"
                                                                    class="col-form-label">Invoice
                                                                    Amount</label>
                                                                <input type="text" class="form-control"
                                                                    id="delete_inv_amount" name="delete_inv_amount"
                                                                    readonly>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer flex-nowrap">
                                            <button type="button" class="btn btn-outline-primary btn-block btn-lg mt-0"
                                                data-dismiss="modal">Close</button>
                                            <button type="submit"
                                                class="btn btn-primary btn-block btn-lg mt-0">Delete</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- End Delete Invoice -->
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('script')
    <script src="{{ URL::asset('assets/js/datatables.min.js') }}"></script>
    <script>
        var user = {!! json_encode((array) auth()->user()->username) !!};
        /* var inv_id = $('#view_data').attr("data-id");
        var branch_id = $('#view_data').attr("data-branch_id");
        var cus_id = $('#view_data').attr("data-cus_id"); */
        //console.log('branch_id:',branch_id);


        $(document).ready(function() {
            $('#datatable').DataTable({
                language: {
                    searchPlaceholder: "Search records",
                    search: "",
                    "lengthMenu": "_MENU_ Entries"
                },
                "lengthMenu": [
                    [8, 16, 32, -1],
                    [8, 16, 32, "All"]
                ],
                "pageLength": 8,
                "aaSorting": [
                    [0, 'desc']
                ],
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': ['nosort']
                }]
            });
            $("#view_invoice_modal").on('show.bs.modal', function(event) {
                /* inv = $(event.relatedTarget);
                var id = inv.data('id');
                var branch_id = inv.data('branch_id'); */
                //invoice_details(id);
                var challan = $(event.relatedTarget);
                var inv_id = challan.data('id');
                var branch_id = challan.data('branch_id');
                var cus_id = challan.data('cus_id');

                console.log('id:', inv_id);
                console.log('branch_id:', branch_id);
                console.log('cus_id:', cus_id);
                /* invoice_details(inv_id);
                invoice_total(inv_id);
                branch_details(branch_id, inv_id);
                customer_details(cus_id); */
                invoice_details(inv_id).done(function(invoice) {
                    invoice_total(inv_id).done(function(inv_total) {
                        branch_details(branch_id, inv_id).done(function(inv_details) {
                            customer_details(cus_id).done(function(cus_details) {
                                //console.log("inv_total:", inv_details);
                                //grab all api function push all the data into single function
                                load_invoice_view(invoice, inv_total,
                                    inv_details, cus_details, inv_id,
                                    branch_id, cus_id);
                            });
                        });
                    });
                });
            });
            $("#deleteinvoice").on('show.bs.modal', function(event) {
                var cus = $(event.relatedTarget);
                var id = cus.data('inv_id');
                var name = cus.data('cus_name');
                var number = cus.data('inv_number');
                var date = cus.data('inv_date');
                var amount = cus.data('inv_amount');

                var action = `{{ url('deleteinvoice/${id}/') }}`
                $('#delete_form').attr('action', action);
                $("#delete_cus_name").val(name);
                $("#delete_inv_no").val(number);
                $("#delete_inv_date").val(date);
                $("#delete_inv_amount").val(amount);
            });
        });

        function load_invoice_view(invoiceObj, totalObj, branchObj, customerObj, inv_id,
            branch_id, cus_id) {
            console.log("invoice details:", invoiceObj);
            console.log("invoice item total:", totalObj);
            console.log("invoice branchObj:", branchObj);
            console.log("invoice customerObj:", customerObj);
            $('#c_name').html(branchObj[0].org_branch_name);
            $("#v_comp_addr").html(branchObj[0].org_address + "<br>" + branchObj[0].org_city + "<br>" + branchObj[0]
                .org_state + " - " + branchObj[0].org_pincode + "<br><span>phone:</span>  " + branchObj[0].org_phone_no);
            $('#gst_no').html(`<span class="font-weight-bold">GST No:</span>${branchObj[0].org_gstno}`);

            $('#invoice_no').html(branchObj[0].Invoice_ID);
            $('#invoice_date').html(branchObj[0].Invoice_Date);
            $('#delivery_note').html(branchObj[0].Delivery_Note);
            $('#delivery_date').html(branchObj[0].Delivery_Note_Date);

            $('#mod_payment').html(branchObj[0].Mode_Terms_Payment);
            $('#supplier_ref_no').html(branchObj[0].Supplier_Reference);
            $('#other_ref_no').html(branchObj[0].Other_Reference);
            $('#buyer_order_no').html(branchObj[0].Buyer_Order_No);
            $('#buyer_order_date').html(branchObj[0].Mode_Terms_Payment);

            $('#cust_name').html(customerObj[0].customer_name);
            $('#cust_addr').html(
                `${customerObj[0].cus_address}<br>${customerObj[0].cus_city}<br>${customerObj[0].cus_state} - ${customerObj[0].cus_pincode}`
            );
            $('#cust_phone').html(`<span>phone:</span> ${customerObj[0].cus_phone_no}`);
            $('#cust_gst_no').html(`<span class="font-weight-bold">GST No:</span>${customerObj[0].cus_gst_percentage}`);

            $('#delivery_name').html(branchObj[0].Customer_Phone_No);
            $('#delivery_addr').html(
                `${branchObj[0].delivery_address}<br>${branchObj[0].delivery_city}<br>${branchObj[0].delivery_state} - ${branchObj[0].delivery_pincode}`
            );
            $('#delivery_phone').html(`<span>phone:</span> ${branchObj[0].delivery_no}`);

            $('#bank_details').html(
                `${branchObj[0].org_holder_name}<br>${branchObj[0].org_bank_name}<br>${branchObj[0].org_account_no}<br>${branchObj[0].org_bank_branch}<br>${branchObj[0].org_bank_ifsc}<br>`
            );
            $('#invoice_amount').html(branchObj[0].total_amount);
            $('#invoice_amount_words').html(branchObj[0].Invoice_amount_words);


            let item_row = "";
            let item_row_total = "";
            let tax_row = "";
            let tax_row_total = "";
            $.each(invoiceObj, function(key, value) {
                let tax_total = parseFloat(value.item_total) + parseFloat(value.tax_amount);

                //let total = item_total + tax_amount;
                //console.log("for round",total.toFixed(2));

                tax_row += "<tr>";
                item_row += "<tr>";

                item_row += "<td>" + value.Item_Id + "</td>";
                item_row += "<td>" + value.name + "</td>";
                item_row += "<td>" + value.hsn + "</td>";
                item_row += "<td>" + value.gst + "</td>";
                item_row += "<td>" + value.price + "</td>";
                item_row += "<td>" + value.quantity + "</td>";
                item_row += "<td>" + value.discount + "</td>";
                item_row += "<td>" + value.descriptions + "</td>";
                item_row += "<td>" + value.item_total + "</td>";

                tax_row += "<td>" + value.item_total + "</td>";
                tax_row += "<td>" + value.cgst + "</td>";
                tax_row += "<td>" + value.cgst_amount + "</td>";
                tax_row += "<td>" + value.sgst + "</td>";
                tax_row += "<td>" + value.sgst_amount + "</td>";
                tax_row += "<td>" + value.tax_amount + "</td>";
                tax_row += "<td>" + tax_total.toFixed(2) + "</td>";

                tax_row += "</tr>";
                item_row += "</tr>";
            });

            item_row_total =
                `<tr class="border-top"><td colspan="8" class="text-right">Total</td><td>${totalObj[0].items_total}</td></tr>`

            //Total Tax Amount
            let tax_amount_total = parseFloat(totalObj[0].total_tax).toFixed(2);
            tax_row_total =
                `<tr class="border-top"><td colspan="5" class="text-right">Total</td><td>${tax_amount_total}</td><td id='total_tax'></td></tr>`
            //push data into invoice modal 
            $("#item_table_body").html(item_row);
            $("#item_table_footer").html(item_row_total);
            $("#tax_table_body").html(tax_row);
            $("#tax_table_footer").html(tax_row_total);
            $("#total_tax").html(branchObj[0].total_amount);

            $('#edit_modal_btn').html(
                `<button type="button" class="btn btn-outline-success btn-block btn-lg mt-0" data-dismiss="modal">Close</button><a href="{{ url('printinvoice/${inv_id}/${branch_id}/${cus_id}') }}" id="download_invoice" class="btn btn-success btn-block btn-lg mt-0">Download</a>`
            );
        }

        function invoice_details(inv_id) {
            return $.ajax({
                url: "/" + user + "/invoicedetails",
                type: "GET",
                dataType: 'json',
                data: {
                    id: inv_id,
                },
                success: function(data) {
                    //console.log("invoicedetails", data);
                },
                error: function(error) {
                    console.log('invoice_details error:', error);
                }
            });
        }

        function invoice_total(inv_id) {
            var user = {!! json_encode((array) auth()->user()->username) !!};
            return $.ajax({
                url: "/" + user + "/invoicetotal",
                type: "GET",
                dataType: 'json',
                data: {
                    id: inv_id,
                },
                success: function(data) {
                    //console.log("invoice_total", data);
                },
                error: function(error) {
                    console.log('invoice_details error:', error);
                }
            });
        }

        function branch_details(branch_id, inv_id) {
            return $.ajax({
                url: "/" + user + "/branchdetails",
                type: "GET",
                dataType: 'json',
                data: {
                    username: user,
                    orgId: branch_id,
                    sellerBranchId: branch_id,
                    invoiceId: inv_id,
                },
                success: function(data) {
                    //console.log("branch_details and invoice_details", data);
                },
                error: function(error) {
                    console.log('branch_details error:', error);
                }
            });
        }

        function customer_details(cus_id) {
            return $.ajax({
                url: "/" + user + "/customerdetails",
                type: "GET",
                dataType: 'json',
                data: {
                    username: user,
                    cusId: cus_id,
                },
                success: function(data) {
                    //console.log("branch_details and invoice_details", data);
                },
                error: function(error) {
                    console.log(error);
                }
            });
        }

    </script>
@endsection
