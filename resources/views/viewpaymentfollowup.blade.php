@extends('layouts.app')
@section('mytitle', 'View Payment Followup')
@section('head-links')
    <link href="{{ URL::asset('assets/css/datatables.min.css') }}" rel="stylesheet">
@endsection
@section('content')
    <div class="p-4">
        <section class="my-5">
            <div class="card shadow">
                <div class="card-top ">
                    <div class="card-title mb-0">
                        <h6 class="font-weight-bold text-capitalize mb-0">View Payment Followup</h6>
                    </div>
                </div>
                <div class="card-body pt-0">
                    <div class="row">
                        <div class="col-md-3">
                            @foreach ($sql as $payment)
                                <div class="form-group">
                                    <h6 class="font-weight-bold mb-0 text-sm">Customer Name</h6>
                                    <span class="text-13">{{ $payment->Customer_Name }}</span>
                                </div>
                                <div class="form-group">
                                    <h6 class="font-weight-bold mb-0 text-sm">Phone No</h6>
                                    <span class="text-13">{{ $payment->Customer_Phone_No }}</span>
                                </div>
                                <div class="form-group">
                                    <h6 class="font-weight-bold mb-0 text-sm">Invoice Id</h6>
                                    <span class="text-13">{{ $payment->Invoice_ID }}</span>
                                </div>
                                <div class="form-group">
                                    <h6 class="font-weight-bold mb-0 text-sm">Invoice Date</h6>
                                    <span class="text-13">{{ $payment->Invoice_Date }}</span>
                                </div>
                            @endforeach
                        </div>
                        <div class="col-md-9">
                            <div class="table-responsive">
                                <table class="table table-bordered table-view">
                                    <thead>
                                        <tr>
                                            <th scope="col">Payment ID</th>
                                            <th scope="col">Amount Received Date</th>
                                            <th scope="col">Amount Received</th>
                                            <th scope="col">Follow Up Date</th>
                                            <th scope="col">Pending Amount</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($sql as $payment)
                                            <tr>
                                                <td>{{ $payment->Payment_Id }}</td>
                                                <td>{{ $payment->Amount_Received_Date }}</td>
                                                <td>{{ $payment->Recieved_Amount }}</td>
                                                <td>{{ $payment->Followup_Date }}</td>
                                                <td>{{ $payment->Total_Due }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="my-3">
                                <a href="/paymentfollowup" class="btn btn-primary">Cancel</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('script')
    <script src="{{ URL::asset('assets/js/datatables.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#datatable').DataTable({
                language: {
                    searchPlaceholder: "Search records",
                    search: "",
                    "lengthMenu": "_MENU_ Entries"
                },
                "lengthMenu": [
                    [8, 16, 32, -1],
                    [8, 16, 32, "All"]
                ],
                "pageLength": 8,
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': ['nosort']
                }]
            });
        });

    </script>
@endsection
