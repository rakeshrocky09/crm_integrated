@extends('layouts.app')
@section('mytitle', 'View Consumable')
@section('content')
    <div class="bg-top bg-gray p-4">
        <div class="d-flex">
            <h5 class="font-weight-bold text-uppercase mb-0">View Consumable</h5>
        </div>
    </div>

    <div class="p-4 card-main">
        <section class="my-5">
            <div class="card shadow">
                <div class="card-top ">
                    <div class="card-title mb-0">
                        <h5 class="font-weight-bold text-uppercase mb-0"><i class="fas fa-user-alt"></i></h5>
                    </div>
                    <div class="btn-right position-relative float-right">
                        <a href="#" class="btn btn-primary text-uppecase">Print</a>
                        <a href="/addpurchase_consumable" class="btn btn-primary text-uppecase" data-toggle="tooltip"
                            data-placement="top" title="Add Consumable">
                            <i class="fas fa-plus"></i> Add
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <div class="row mx-0">
                            <div class="col-sm-12 col-md-6 pl-0 py-3">
                                <div class="dataTables_length" id="example1_length"><select aria-controls="example1"
                                        class="custom-select custom-select-sm form-control form-control-sm">
                                        <option value="10">10</option>
                                        <option value="25">25</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select></div>
                            </div>
                            <div class="col-sm-12 col-md-6 pr-0 py-3">
                                <div id="example1_filter" class="float-right">
                                    <input type="search" class="form-control form-control-sm" placeholder="Search..."
                                        aria-controls="example1">
                                </div>
                            </div>
                        </div>
                        <table class="table">
                            <thead class="text-uppercase">
                                <tr>
                                    <th scope="col">CONSUMABLE ID</th>
                                    <th scope="col">CONSUMABLE Code NAME.</th>
                                    <th scope="col">CONSUMABLE NAME</th>
                                    <th scope="col">UNIT</th>
                                    <th scope="col">UNIT PRICE</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>CONSUM10</td>
                                    <td>C13T846198</td>
                                    <td>Epson Ink Black WF 8591</td>
                                    <td>Nos</td>
                                    <td>10675</td>
                                    <td class="d-flex flex-nowrap">
                                        <span data-toggle="modal" data-target="#viewconsumable" class="mr-1">
                                            <button type="button" class="btn btn-outline-info" data-toggle="tooltip"
                                                data-placement="top" title="View Consumable">
                                                <i class="fas fa-user-alt"></i>
                                            </button>
                                        </span>
                                        <span data-toggle="modal" data-target="#editconsumable" class="mr-1">
                                            <button type="button" class="btn btn-outline-success" data-toggle="tooltip"
                                                data-placement="top" title="Edit Consumable">
                                                <i class="fas fa-edit"></i>
                                            </button>
                                        </span>
                                        <span data-toggle="modal" data-target="#deleteconsumable">
                                            <button type="button" class="btn btn-outline-danger" data-toggle="tooltip"
                                                data-placement="top" title="Delete Consumable">
                                                <i class="fas fa-trash-alt"></i>
                                            </button>
                                        </span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <!-- Modal view Consumable -->
                        <div class="modal fade custom-modal" id="viewconsumable" tabindex="-1" role="dialog"
                            aria-labelledby="viewconsumable" aria-hidden="true">
                            <div class="modal-dialog modal-xl modal-dialog-centered edit" role="document">
                                <div class="modal-content">
                                    <div class="icon">
                                        <span class="fa-stack fa-2x text-success">
                                            <i class="fas fa-circle fa-stack-2x"></i>
                                            <i class="fas fa-table fa-stack-1x fa-inverse"></i>
                                        </span>
                                    </div>
                                    <div class="modal-header justify-content-center">
                                        <h3 class="modal-title text-capitalize font-weight-bolder mt-3" id="viewconsumable">
                                            View Consumable Details
                                        </h3>
                                    </div>
                                    <div class="modal-body pb-0 text-sm">
                                        <div class="row text-sm mb-4">
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">Consumable ID</label>
                                                <p class="mb-0"><span>CONSUM10</span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">Consumable Belongs To Branch</label>
                                                <p class="mb-0"><span>KM Enterprises</span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">HSN/SAC Number</label>
                                                <p class="mb-0"><span>8443</span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">GST(In %)</label>
                                                <p class="mb-0"><span>18</span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">Consumable Code Name</label>
                                                <p class="mb-0"><span>C13T846198</span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">Consumable Product Name</label>
                                                <p class="mb-0"><span>Epson Ink Black WF 8591</span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">Unit</label>
                                                <p class="mb-0"><span>Month</span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">Unit Price</label>
                                                <p class="mb-0"><span>0</span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">Brand Name</label>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">No.Copy(ies)</label>
                                                <p class="mb-0"><span>10000</span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">MSL</label>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">Chargeable</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer justify-content-center">
                                        <button type="button" class="btn btn-success btn-lg btn-large mt-0"
                                            data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End view Consumable -->
                        <!-- Modal Edit Consumable -->
                        <div class="modal fade custom-modal" id="editconsumable" tabindex="-1" role="dialog"
                            aria-labelledby="editconsumable" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered edit modal-xl" role="document">
                                <div class="modal-content">
                                    <div class="icon">
                                        <span class="fa-stack fa-2x text-success">
                                            <i class="fas fa-circle fa-stack-2x"></i>
                                            <i class="far fa-edit fa-stack-1x fa-inverse"></i>
                                        </span>
                                    </div>
                                    <div class="modal-header justify-content-center">
                                        <h3 class="modal-title text-capitalize font-weight-bolder mt-3" id="editconsumable">
                                            Edit Consumable Details
                                        </h3>
                                    </div>
                                    <div class="modal-body pb-0">
                                        <form>
                                            <div class="row">
                                                <div class="form-group col-md-4">
                                                    <label for="date" class="mb-0 control-label active">Consumable ID
                                                        <small>*</small></label>
                                                    <input id="date" type="text" class="form-control" value="CONSUM10">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label class="control-label active" for="techician_branch">Brand Name
                                                        <small>*</small>
                                                    </label>
                                                    <select class="form-control" id="techician_branch"
                                                        name="techician_branch">
                                                        <option selected="true" value="1">Epson</option>
                                                        <option value="3">Option value</option>
                                                        <option value="3">Option value</option>
                                                    </select>
                                                </div>

                                                <div class="form-group col-md-4">
                                                    <label class="control-label active" for="techician_branch">HSN/SAC
                                                        Number
                                                        <small>*</small>
                                                    </label>
                                                    <select class="form-control" id="techician_branch"
                                                        name="techician_branch">
                                                        <option selected="true" value="1">8443</option>
                                                        <option value="2">Option 1</option>
                                                        <option value="3">Option 2</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="date" class="mb-0 control-label active">Consumable Product Name</label>
                                                    <input id="date" type="text" class="form-control" value="C13T846198">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="date" class="mb-0 control-label active">Unit Price
                                                        <small>*</small></label>
                                                    <input id="date" type="text" class="form-control" value="10675">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label class="control-label active" for="techician_branch">Unit</label>
                                                    <select class="form-control" id="techician_branch"
                                                        name="techician_branch">
                                                        <option selected="true" value="1">Nos</option>
                                                        <option value="2">Option 1</option>
                                                        <option value="3">Option 2</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="date" class="mb-0 control-label active">No.of Copy(ies)</label>
                                                    <input id="date" type="text" class="form-control" value="10000">
                                                </div>

                                                <div class="form-group col-md-4">
                                                    <label for="date" class="mb-0 control-label active">Belongs To
                                                        Branch</label>
                                                    <input id="date" type="text" class="form-control"
                                                        value="10">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="date" class="mb-0 control-label active">GST(In %)</label>
                                                    <input id="date" type="text" class="form-control" value="18">
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="font-weight-bold">MSL</label>
                                                    <div class="mb-2">
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input type="radio" id="msl_true" name="msl"
                                                                class="custom-control-input">
                                                            <label class="custom-control-label"
                                                                for="msl_true">Yes</label>
                                                        </div>
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input type="radio" id="msl_false" name="msl"
                                                                class="custom-control-input">
                                                            <label class="custom-control-label"
                                                                for="msl_false">No</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="font-weight-bold">Chargeable</label>
                                                    <div class="mb-2">
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input type="radio" id="chargeable_true" name="chargeable"
                                                                class="custom-control-input">
                                                            <label class="custom-control-label"
                                                                for="chargeable_true">Yes</label>
                                                        </div>
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input type="radio" id="chargeable_false" name="chargeable"
                                                                class="custom-control-input">
                                                            <label class="custom-control-label"
                                                                for="chargeable_false">No</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="modal-footer flex-nowrap">
                                        <button type="button" class="btn btn-outline-success btn-block btn-lg mt-0"
                                            data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-success btn-block btn-lg mt-0">Done</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Edit Consumable -->
                        <!-- Modal Delete Consumable -->
                        <div class="modal fade custom-modal" id="deleteconsumable" tabindex="-1" role="dialog"
                            aria-labelledby="deleteconsumable" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="icon">
                                        <span class="fa-stack fa-2x text-primary">
                                            <i class="fas fa-circle fa-stack-2x"></i>
                                            <i class="fas fa-trash-alt fa-stack-1x fa-inverse"></i>
                                        </span>
                                    </div>
                                    <div class="modal-header justify-content-center">
                                        <h3 class="modal-title text-capitalize font-weight-bolder mt-3" id="deleteconsumable">
                                            Delete Consumable?
                                        </h3>
                                    </div>
                                    <div class="modal-body pb-0">
                                        <p class="font-weight-bold text-center">
                                            Are you sure you want to delete the Consumable?
                                            <br>
                                            You cannot undo this operation!
                                        </p>
                                        <div class="row justify-content-center">
                                            <div class="col-md-11">
                                                <form>
                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <label for="delete_product_id" class="col-form-label">Consumable ID</label>
                                                            <input type="text" class="form-control" id="delete_product_id"
                                                                value="CONSUM10" readonly>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="delete_product_code" class="col-form-label">Consumable Code Name</label>
                                                            <input type="text" class="form-control" id="delete_product_code"
                                                                value="C13T846198" readonly>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="delete_product_name" class="col-form-label">Consumable Name</label>
                                                            <input type="text" class="form-control" id="delete_product_name"
                                                                value="Epson Ink Black WF 8591" readonly>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="delete_brandname" class="col-form-label">Consumable Unit Price</label>
                                                            <input type="text" class="form-control" id="delete_brandname"
                                                                value="10675" readonly>
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <label for="delete_branch" class="col-form-label">Belongs To
                                                                Branch</label>
                                                            <input type="text" class="form-control" id="delete_branch"
                                                                value="10" readonly>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer flex-nowrap">
                                        <button type="button" class="btn btn-outline-primary btn-block btn-lg mt-0"
                                            data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary btn-block btn-lg mt-0">Delete</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Delete spares -->
                    </div>
                    <div class="row my-3">
                        <div class="col-md-5">
                            <div>Showing 1 to 10 of 50 entries</div>
                        </div>
                        <div class="col-md-7">
                            <nav aria-label="Page navigation example">
                                <ul class="pagination justify-content-end mb-0">
                                    <li class="page-item disabled">
                                        <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a>
                                    </li>
                                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                                    <li class="page-item"><a class="page-link" href="#">4</a></li>
                                    <li class="page-item"><a class="page-link" href="#">5</a></li>
                                    <li class="page-item mr-0">
                                        <a class="page-link" href="#">Next</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
