@extends('layouts.app')
@section('mytitle', 'Add Organization')
@section('content')
    <div class="bg-top bg-gray p-4">
        <div class="d-flex">
            <h5 class="font-weight-bold text-uppercase mb-0">Add organization</h5>
        </div>
    </div>
    <section class="my-4 p-4 card-main">
        <form action="addorganizations" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-12 py-4">
                    <div class="card shadow h-100">
                        <div class="card-top ">
                            <div class="card-title mb-0">
                                <h5 class="font-weight-bold text-uppercase mb-0"><i class="far fa-building"></i></h5>
                            </div>
                        </div>
                        <div class="card-body pt-0">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="img-upload form mb-2">
                                        <h6 class="fw-700 text-uppercase mb-3">Company / Branch Logo</h6>
                                        <div>
                                            <img src="{{ asset('/assets/img/avatar.png') }}" id="preview"
                                                class="img-thumbnail border-0" width="200">
                                        </div>
                                        <div id="msg"></div>
                                        <input type="file" name="org_img" class="file" accept="image/*">
                                        <div class="input-group mt-2 @error('org_img') is-invalid @enderror">
                                            <input type="text" class="form-control" disabled placeholder="Upload File"
                                                id="org_img">
                                            <div class="p-1">
                                                <button type="button" class="browse btn btn-primary text-capitalize">Select
                                                    Image</button>
                                            </div>
                                            @error('org_img')
                                                <span class="text-danger text-12">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    {{-- <input type="file" name="image" class="form-control"> --}}
                                </div>
                                <div class="col-md-9">
                                    <div class="row">
                                        <div class="form-group col-md-4">
                                            <label class="control-label" for="org_branch">Company
                                                Name/Branch Name<small>*</small></label>
                                            <input id="org_branch" name="org_branch" type="text"
                                                class="form-control @error('org_branch') is-invalid @enderror">
                                            @error('org_branch')
                                                <span class="text-danger text-12">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label class="control-label" for="parent_branch">Parent
                                                Branch Name<small>*</small></label>
                                            <input id="parent_branch" name="parent_branch" type="text"
                                                class="form-control @error('parent_branch') is-invalid @enderror">
                                            @error('parent_branch')
                                                <span class="text-danger text-12">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label class="control-label" for="reg_name">Registered
                                                Name<small>*</small></label>
                                            <input id="reg_name" name="reg_name" type="text"
                                                class="form-control @error('reg_name') is-invalid @enderror">
                                            @error('reg_name')
                                                <span class="text-danger text-12">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label class="control-label" for="org_email">Email
                                                address<small>*</small></label>
                                            <input id="org_email" name="org_email" type="email"
                                                class="form-control @error('org_email') is-invalid @enderror">
                                            @error('org_email')
                                                <span class="text-danger text-12">{{ $message }}</span>
                                            @enderror
                                        </div>

                                        <div class="form-group col-md-4">
                                            <label class="control-label" for="org_phone">Phone
                                                No<small>*</small></label>
                                            <input id="org_phone" name="org_phone" type="tel"
                                                class="form-control @error('org_phone') is-invalid @enderror">
                                            @error('org_phone')
                                                <span class="text-danger text-12">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label class="control-label" for="alt_phone">Alternate
                                                Phone No<small>*</small></label>
                                            <input id="alt_phone" name="alt_phone" type="text"
                                                class="form-control @error('alt_phone') is-invalid @enderror">
                                            @error('alt_phone')
                                                <span class="text-danger text-12">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label class="control-label" for="pancard">PAN Card
                                                No<small>*</small></label>
                                            <input id="pancard" name="pancard" type="text"
                                                class="form-control @error('pancard') is-invalid @enderror">
                                            @error('pancard')
                                                <span class="text-danger text-12">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label class="control-label" for="gst_no">GST Registered
                                                No<small>*</small></label>
                                            <input id="gst_no" name="gst_no" type="text"
                                                class="form-control @error('gst_no') is-invalid @enderror">
                                            @error('gst_no')
                                                <span class="text-danger text-12">{{ $message }}</span>
                                            @enderror
                                        </div>

                                        <div class="form-group col-md-4">
                                            <label class="control-label active" for="address">Address
                                                <small>*</small></label>
                                            <input id="address" name="address" type="text"
                                                class="form-control @error('address') is-invalid @enderror">
                                            @error('address')
                                                <span class="text-danger text-12">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label class="control-label active" for="city">City
                                                <small>*</small></label>
                                            <input id="city" name="city" type="text"
                                                class="form-control @error('city') is-invalid @enderror">
                                            @error('city')
                                                <span class="text-danger text-12">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label class="control-label active" for="state">State
                                                <small>*</small></label>
                                            <input id="state" name="state" type="text"
                                                class="form-control @error('state') is-invalid @enderror">
                                            @error('state')
                                                <span class="text-danger text-12">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label class="control-label active" for="postal_code">Postal
                                                Code <small>*</small></label>
                                            <input id="postal_code" name="postal_code" type="number"
                                                class="form-control @error('postal_code') is-invalid @enderror">
                                            @error('postal_code')
                                                <span class="text-danger text-12">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 py-4">
                    <div class="card shadow">
                        <div class="card-top ">
                            <div class="card-title mb-0">
                                <h5 class="font-weight-bold text-uppercase mb-0"><i class="fas fa-university"></i></h5>
                            </div>
                        </div>
                        <div class="card-body pt-0">
                            <div class="row">
                                <div class="col-12 mt-4">
                                    <h6 class="fw-700 text-uppercase mb-3">Bank Details</h6>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label" for="ifsc">Bank IFSC
                                        Code</label>
                                    <input id="ifsc" name="ifsc" type="text"
                                        class="form-control @error('ifsc') is-invalid @enderror">
                                    @error('ifsc')
                                        <span class="text-danger text-12">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label" for="bank_name">Bank Name
                                        <small>*</small></label>
                                    <input id="bank_name" name="bank_name" type="text"
                                        class="form-control @error('bank_name') is-invalid @enderror">
                                    @error('bank_name')
                                        <span class="text-danger text-12">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label active" for="ac_type">Account Type
                                        <small>*</small></label>
                                    <select class="form-control @error('ac_type') is-invalid @enderror" id="ac_type"
                                        name="ac_type">
                                        <option disabled selected></option>
                                        <option value="SAVINGS ACCOUNT"> SAVINGS ACCOUNT </option>
                                        <option value="CURRENT ACCOUNT"> CURRENT ACCOUNT </option>
                                    </select>
                                    @error('ac_type')
                                        <span class="text-danger text-12">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label" for="bank_branch">Bank
                                        Branch Name<small>*</small></label>
                                    <input id="bank_branch" name="bank_branch" type="text"
                                        class="form-control @error('bank_branch') is-invalid @enderror">
                                    @error('bank_branch')
                                        <span class="text-danger text-12">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label" for="ac_no">Account
                                        No<small>*</small></label>
                                    <input id="ac_no" name="ac_no" type="number"
                                        class="form-control @error('ac_no') is-invalid @enderror">
                                    @error('ac_no')
                                        <span class="text-danger text-12">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label" for="ac_name">Account
                                        Holder's
                                        Name<small>*</small></label>
                                    <input id="ac_name" name="ac_name" type="text"
                                        class="form-control @error('ac_name') is-invalid @enderror">
                                    @error('ac_name')
                                        <span class="text-danger text-12">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <a href="{{url()->previous()}}" type="submit" class="btn btn-outline-primary btn-lg btn-large mr-2">Cancel</a>
                <button type="submit" class="btn btn-primary btn-lg btn-large">Submit</button>
            </div>
        </form>
    </section>
@endsection
