@extends('layouts.app')
@section('mytitle', 'View Spares')
@section('head-links')
    <link href="{{ URL::asset('assets/css/datatables.min.css') }}" rel="stylesheet">
@endsection
@section('content')
    <div class="bg-top bg-gray p-4">
        <div class="d-flex">
            <h5 class="font-weight-bold text-uppercase mb-0">View Spares</h5>
        </div>
    </div>

    <div class="p-4 card-main">
        <section class="my-5">
            <div class="card shadow">
                <div class="card-top ">
                    <div class="card-title mb-0">
                        <h5 class="font-weight-bold text-uppercase mb-0"><i class="fas fa-user-alt"></i></h5>
                    </div>
                    <div class="btn-right position-relative float-right">
                        <a href="#" class="btn btn-primary text-uppecase">Print</a>
                        <a href="/addspares" class="btn btn-primary text-uppecase" data-toggle="tooltip"
                            data-placement="top" title="Add Spares">
                            <i class="fas fa-plus"></i> Add
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table" id="datatable">
                            <thead class="text-uppercase">
                                <tr>
                                    <th scope="col">Spare ID</th>
                                    <th scope="col">PART NO.</th>
                                    <th scope="col">Item</th>
                                    <th scope="col">UNIT</th>
                                    <th scope="col">UNIT PRICE</th>
                                    <th scope="col" class="nosort">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (count($product) >= 1)
                                    @foreach ($product as $spares)
                                        <tr>
                                            <td>{{ $spares->spare_const }}{{ $spares->spare_id }}</td>
                                            <td>{{ $spares->spare_const }}</td>
                                            <td>{{ $spares->spare_model_name }}</td>
                                            <td>{{ $spares->spare_stock_unit }}</td>
                                            <td>{{ $spares->spare_unit_price }}</td>
                                            <td class="d-flex flex-nowrap">
                                                <span data-toggle="modal" data-target="#viewspares" class="mr-1"
                                                    data-spare_const="{{ $spares->spare_const }}"
                                                    data-id="{{ $spares->spare_id }}"
                                                    data-code_name="{{ $spares->spare_code_name }}"
                                                    data-model="{{ $spares->spare_model_name }}"
                                                    data-brand="{{ $spares->spare_brand_name }}"
                                                    data-part_no="{{ $spares->spare_parts_name }}"
                                                    data-unit="{{ $spares->spare_stock_unit }}"
                                                    data-price="{{ $spares->spare_unit_price }}"
                                                    data-branch="{{ $spares->spare_branch_name }}"
                                                    data-gst="{{ $spares->Gst_Percentage }}"
                                                    data-msl="{{ $spares->spare_msl }}"
                                                    data-chargeable="{{ $spares->spare_chargeable }}">
                                                    <button type="button" class="btn btn-outline-info" data-toggle="tooltip"
                                                        data-placement="top" title="View Spares">
                                                        <i class="fas fa-user-alt"></i>
                                                    </button>
                                                </span>
                                                <span data-toggle="modal" data-target="#editspares" class="mr-1"
                                                    data-spare_const="{{ $spares->spare_const }}"
                                                    data-id="{{ $spares->spare_id }}"
                                                    data-code_name="{{ $spares->spare_code_name }}"
                                                    data-model="{{ $spares->spare_model_name }}"
                                                    data-brand="{{ $spares->spare_brand_name }}"
                                                    data-part_no="{{ $spares->spare_parts_name }}"
                                                    data-unit="{{ $spares->spare_stock_unit }}"
                                                    data-price="{{ $spares->spare_unit_price }}"
                                                    data-branch="{{ $spares->spare_branch_name }}"
                                                    data-gst="{{ $spares->Gst_Percentage }}"
                                                    data-msl="{{ $spares->spare_msl }}"
                                                    data-chargeable="{{ $spares->spare_chargeable }}">
                                                    <button type="button" class="btn btn-outline-success"
                                                        data-toggle="tooltip" data-placement="top" title="Edit Spares">
                                                        <i class="fas fa-edit"></i>
                                                    </button>
                                                </span>
                                                <span data-toggle="modal" data-target="#deletespares"
                                                    data-id="{{ $spares->spare_id }}"
                                                    data-code_name="{{ $spares->spare_code_name }}"
                                                    data-spare_name="{{ $spares->spare_parts_name }}"
                                                    data-price="{{ $spares->spare_unit_price }}"
                                                    data-branch="{{ $spares->spare_branch_name }}">
                                                    <button type="button" class="btn btn-outline-danger"
                                                        data-toggle="tooltip" data-placement="top" title="Delete Spares">
                                                        <i class="fas fa-trash-alt"></i>
                                                    </button>
                                                </span>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="6">Sorry, No records found</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                        <!-- Modal viewspares -->
                        <div class="modal fade custom-modal" id="viewspares" tabindex="-1" role="dialog"
                            aria-labelledby="viewspares" aria-hidden="true">
                            <div class="modal-dialog modal-xl modal-dialog-centered edit" role="document">
                                <div class="modal-content">
                                    <div class="icon">
                                        <span class="fa-stack fa-2x text-success">
                                            <i class="fas fa-circle fa-stack-2x"></i>
                                            <i class="fas fa-table fa-stack-1x fa-inverse"></i>
                                        </span>
                                    </div>
                                    <div class="modal-header justify-content-center">
                                        <h3 class="modal-title text-capitalize font-weight-bolder mt-3" id="viewspares">
                                            View Spares Details
                                        </h3>
                                    </div>
                                    <div class="modal-body pb-0 text-sm">
                                        <div class="row text-sm mb-4">
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">Spare ID</label>
                                                <p class="mb-0"><span id="id"></span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">HSN/SAC Number</label>
                                                <p class="mb-0"><span id="code-name"></span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">Item</label>
                                                <p class="mb-0"><span id="model"></span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">Brand Name</label>
                                                <p class="mb-0"><span id="brand"></span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">Part No</label>
                                                <p class="mb-0"><span id="part-no"></span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">Unit</label>
                                                <p class="mb-0"><span id="unit"></span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">Unit Price</label>
                                                <p class="mb-0"><span id="price"></span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">Spare Belongs To Branch</label>
                                                <p class="mb-0"><span id="branch"></span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">GST(In %)</label>
                                                <p class="mb-0"><span id="gst"></span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">MSL</label>
                                                <p class="mb-0"><span id="msl"></span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">Chargeable</label>
                                                <p class="mb-0"><span id="chargeable"></span></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer justify-content-center">
                                        <button type="button" class="btn btn-success btn-lg btn-large mt-0"
                                            data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End viewspares -->
                        <!-- Modal Edit spares -->
                        <div class="modal fade custom-modal" id="editspares" tabindex="-1" role="dialog"
                            aria-labelledby="editspares" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered edit modal-xl" role="document">
                                <form id="edit_form" method="post">
                                    @csrf
                                    <div class="modal-content">
                                        <div class="icon">
                                            <span class="fa-stack fa-2x text-success">
                                                <i class="fas fa-circle fa-stack-2x"></i>
                                                <i class="far fa-edit fa-stack-1x fa-inverse"></i>
                                            </span>
                                        </div>
                                        <div class="modal-header justify-content-center">
                                            <h3 class="modal-title text-capitalize font-weight-bolder mt-3" id="editspares">
                                                Edit Spares Details
                                            </h3>
                                        </div>
                                        <div class="modal-body pb-0">
                                            <div class="row">
                                                <div class="form-group col-md-4">
                                                    <label for="edit_id" class="mb-0 control-label active">Spare ID
                                                        <small>*</small></label>
                                                    <input id="edit_id" name="edit_id" type="text" class="form-control"
                                                        readonly>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label class="control-label active" for="edit_brand">Brand Name
                                                        <small>*</small>
                                                    </label>
                                                    <select class="form-control" id="edit_brand" name="edit_brand"></select>
                                                </div>

                                                <div class="form-group col-md-4">
                                                    <label class="control-label active" for="edit_hsn">HSN/SAC
                                                        Number
                                                        <small>*</small>
                                                    </label>
                                                    <select class="form-control" id="edit_hsn" name="edit_hsn"></select>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="edit_partno" class="mb-0 control-label active">Part Number
                                                        <small>*</small></label>
                                                    <input id="edit_partno" name="edit_partno" type="text"
                                                        class="form-control">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="edit_item" class="mb-0 control-label active">Item</label>
                                                    <input id="edit_item" name="edit_item" type="text" class="form-control">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="edit_price" class="mb-0 control-label active">Unit Price
                                                        <small>*</small></label>
                                                    <input id="edit_price" name="edit_price" type="text"
                                                        class="form-control">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label class="control-label active" for="edit_unit">Unit</label><input
                                                        id="edit_unit" name="edit_unit" type="number" class="form-control">
                                                </div>

                                                <div class="form-group col-md-4">
                                                    <label for="edit_branch" class="mb-0 control-label active">Belongs To
                                                        Branch</label>
                                                    <input id="edit_branch" name="edit_branch" type="text"
                                                        class="form-control" readonly>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="edit_gst" class="mb-0 control-label active">GST(In
                                                        %)</label>
                                                    <input id="edit_gst" name="edit_gst" type="text" class="form-control">
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="font-weight-bold">MSL</label>
                                                    <div class="mb-2">
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input type="radio" id="msl_yes" name="msl" value="yes"
                                                                class="custom-control-input">
                                                            <label class="custom-control-label" for="msl_yes">Yes</label>
                                                        </div>
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input type="radio" id="msl_no" name="msl" value="no"
                                                                class="custom-control-input">
                                                            <label class="custom-control-label" for="msl_no">No</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="font-weight-bold">Chargeable</label>
                                                    <div class="mb-2">
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input type="radio" id="chargeable_true" name="chargeable"
                                                                value="yes" class="custom-control-input">
                                                            <label class="custom-control-label"
                                                                for="chargeable_true">Yes</label>
                                                        </div>
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input type="radio" id="chargeable_false" name="chargeable"
                                                                value="no" class="custom-control-input">
                                                            <label class="custom-control-label"
                                                                for="chargeable_false">No</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer flex-nowrap">
                                            <button type="button" class="btn btn-outline-success btn-block btn-lg mt-0"
                                                data-dismiss="modal">Close</button>
                                            <button type="submit"
                                                class="btn btn-success btn-block btn-lg mt-0">Done</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- End Edit spares -->
                        <!-- Modal Delete spares -->
                        <div class="modal fade custom-modal" id="deletespares" tabindex="-1" role="dialog"
                            aria-labelledby="deletespares" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="icon">
                                        <span class="fa-stack fa-2x text-primary">
                                            <i class="fas fa-circle fa-stack-2x"></i>
                                            <i class="fas fa-trash-alt fa-stack-1x fa-inverse"></i>
                                        </span>
                                    </div>
                                    <div class="modal-header justify-content-center">
                                        <h3 class="modal-title text-capitalize font-weight-bolder mt-3" id="deletespares">
                                            Delete Spares?
                                        </h3>
                                    </div>
                                    <div class="modal-body pb-0">
                                        <p class="font-weight-bold text-center">
                                            Are you sure you want to delete the Spares?
                                            <br>
                                            You cannot undo this operation!
                                        </p>
                                        <div class="row justify-content-center">
                                            <div class="col-md-11">
                                                <form>
                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <label for="delete_spare_id" class="col-form-label">Spare
                                                                ID</label>
                                                            <input type="text" class="form-control" id="delete_spare_id"
                                                                readonly>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="delete_spare_code" class="col-form-label">Spare
                                                                Code Name</label>
                                                            <input type="text" class="form-control" id="delete_spare_code"
                                                                readonly>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="delete_spare_name" class="col-form-label">Spare
                                                                Name</label>
                                                            <input type="text" class="form-control" id="delete_spare_name"
                                                                readonly>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="delete_price" class="col-form-label">Spare
                                                                Price</label>
                                                            <input type="text" class="form-control" id="delete_price"
                                                                readonly>
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <label for="delete_branch" class="col-form-label">Belongs To
                                                                Branch</label>
                                                            <input type="text" class="form-control" id="delete_branch"
                                                                readonly>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer flex-nowrap" id="delete_btn"></div>
                                </div>
                            </div>
                        </div>
                        <!-- End Delete spares -->
                    </div>
                </div>
            </div>
        </section>
    </div>
    @include('components.alertmodal')
@endsection
@section('script')
    @if (session()->has('message'))
        <script>
            $(document).ready(function() {
                $('#successAlert').modal("show");
            });

        </script>
    @endif
    @if (session()->has('update'))
        <script>
            $(document).ready(function() {
                $('#updateAlert').modal("show");
            });

        </script>
    @endif
    @if (session()->has('delete'))
        <script>
            $(document).ready(function() {
                $('#deleteAlert').modal("show");
            });

        </script>
    @endif
    @if (session()->has('error'))
        <script>
            $(document).ready(function() {
                $('#errorAlert').modal("show");
            });

        </script>
    @endif
    <script src="{{ URL::asset('assets/js/datatables.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            loadBrandname();
            getHsn();
            //loadUnit();
            $('#datatable').DataTable({
                language: {
                    searchPlaceholder: "Search records",
                    search: "",
                    "lengthMenu": "_MENU_ Entries"
                },
                "lengthMenu": [
                    [8, 16, 32, -1],
                    [8, 16, 32, "All"]
                ],
                "pageLength": 8,
                "aaSorting": [
                    [0, 'desc']
                ],
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': ['nosort']
                }]
            });
            //View Modal
            $("#viewspares").on('show.bs.modal', function(event) {
                var spares = $(event.relatedTarget);
                var spareConst = spares.data('spare_const');
                var spareId = spares.data('id');
                var id = `${spareConst}${spareId}`;
                var codeName = spares.data('code_name');
                var model = spares.data('model');
                var brand = spares.data('brand');
                var partNo = spares.data('part_no');
                var unit = spares.data('unit');
                var price = spares.data('price');
                var branch = spares.data('branch');
                var gst = spares.data('gst');
                var msl = spares.data('msl');
                var chargeable = spares.data('chargeable');

                //push retrieved data into mentioned id 
                $('#id').html(id);
                $('#code-name').html(codeName);
                $('#model').html(model);
                $('#brand').html(brand);
                $('#part-no').html(partNo);
                $('#unit').html(unit);
                $('#price').html(price);
                $('#branch').html(branch);
                $('#gst').html(gst);
                $('#msl').html(msl);
                $('#chargeable').html(chargeable);
            });

            $("#editspares").on('show.bs.modal', function(event) {
                var spares = $(event.relatedTarget);
                var spareConst = spares.data('spare_const');
                var spareId = spares.data('id');
                var id = `${spareConst}${spareId}`;
                var hsn = spares.data('code_name');
                var item = spares.data('model');
                var brand = spares.data('brand');
                var partno = spares.data('part_no');
                var unit = spares.data('unit');
                var price = spares.data('price');
                var branch = spares.data('branch');
                var gst = spares.data('gst');
                var msl = spares.data('msl');
                var chargeable = spares.data('chargeable');
                //console.log('chargeable', chargeable);

                $('#edit_form').attr('action', 'updatespare/' + spareId);
                $("#edit_id").val(id);
                $("#edit_brand").val(brand);
                $("#edit_hsn").val(hsn);
                $("#edit_partno").val(partno);
                $("#edit_item").val(item);
                $("#edit_price").val(price);
                $("#edit_unit").val(unit);
                $("#edit_branch").val(branch);
                $("#edit_gst").val(gst);
                if (msl == 'yes') {
                    console.log('msl yes');
                    $("#msl_yes").prop("checked", true);
                } else if (msl == 'no') {
                    $("#msl_no").prop("checked", true);
                }
                if (chargeable == 'yes') {
                    console.log('msl yes');
                    $("#chargeable_true").prop("checked", true);
                } else if (chargeable == 'no') {
                    $("#chargeable_false").prop("checked", true);
                }
            });

            $("#deletespares").on('show.bs.modal', function(event) {
                var spares = $(event.relatedTarget);
                var id = spares.data('id');
                var codeName = spares.data('code_name');
                var spareName = spares.data('spare_name');
                var price = spares.data('price');
                var branch = spares.data('branch');

                $("#delete_spare_id").val(id);
                $("#delete_spare_code").val(codeName);
                $("#delete_spare_name").val(spareName);
                $("#delete_price").val(price);
                $("#delete_branch").val(branch);

                $('#delete_btn').html(
                    `<button type="button" class="btn btn-outline-primary btn-block btn-lg mt-0" data-dismiss="modal">Close</button><a href='deletespare/${id}' class="btn btn-primary btn-block btn-lg mt-0">Delete</a>`
                );

            });
        });


        function loadBrandname() {
            return $.ajax({
                url: "/sparebrand",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    //console.log("sparebrand", data);
                    $('#edit_brand').empty();
                    $('#edit_brand').append('<option selected="true" disabled></option>');
                    $('#edit_brand').prop('selectedIndex', 0);
                    $.each(data, function(key, value) {
                        $('#edit_brand').append($('<option></option>').attr('value', value
                            .name).text(value.name));
                    });
                    $('#edit_brand').append('<option value="others">Others</option>');
                },
                error: function(error) {
                    console.log('sparebrand error:', error);
                }
            });
        }

        function getHsn() {
            return $.ajax({
                url: "/sparehsn",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    //console.log("sparehsn", data);
                    $('#edit_hsn').empty();
                    $('#edit_hsn').append('<option selected="true" disabled></option>');
                    $('#edit_hsn').prop('selectedIndex', 0);
                    $.each(data, function(key, value) {
                        $('#edit_hsn').append($('<option></option>').attr(
                            'value',
                            value.Spare_HSN_No).text(value.Spare_HSN_No));
                    });
                    $('#edit_hsn').append('<option value="others">Others</option>');
                },
                error: function(error) {
                    console.log('sparehsn error:', error);
                }
            });
        }

/*         function loadUnit() {
            return $.ajax({
                url: "/unit",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    //console.log("unit", data);
                    $('#edit_unit').empty();
                    $('#edit_unit').append('<option selected="true" disabled></option>');
                    $('#edit_unit').prop('selectedIndex', 0);
                    $.each(data, function(key, value) {
                        $('#edit_unit').append($('<option></option>').attr(
                            'value',
                            value.unit).text(value.unit));
                    });
                    $('#edit_unit').append('<option value="others">Others</option>');
                },
                error: function(error) {
                    console.log('unit error:', error);
                }
            });
        } */

    </script>
@endsection
