@extends('layouts.app')
@section('mytitle', 'VIEW PM Schedule')
@section('content')
    <div class="p-4">
        <section class="my-5">
            <div class="card shadow">
                <div class="card-top ">
                    <div class="card-title mb-0">
                        <h6 class="font-weight-bold text-capitalize mb-0">View ENQUIRY</h6>
                    </div>
                </div>
                <div class="card-body pt-0">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <h6 class="font-weight-bold mb-0 text-sm">Machine Serial No</h6>
                                <span class="text-13">{{ $sql[0]->Machine_Serial_No }}</span>
                            </div>
                            <div class="form-group">
                                <h6 class="font-weight-bold mb-0 text-sm">Customer Name</h6>
                                <span class="text-13">{{ $sql[0]->customer_name }}</span>
                            </div>
                            <div class="form-group">
                                <h6 class="font-weight-bold mb-0 text-sm">Phone No</h6>
                                <span class="text-13">{{ $sql[0]->cus_phone_no }}</span>
                            </div>
                            <div class="form-group">
                                <h6 class="font-weight-bold mb-0 text-sm">Contract Id</h6>
                                <span class="text-13">{{ $sql[0]->Contract_ID }}</span>
                            </div>
                            <div class="form-group">
                                <h6 class="font-weight-bold mb-0 text-sm">Item Model Name</h6>
                                <span class="text-13">{{ $sql[0]->Item_Model_Name }}</span>
                            </div>                            
                            <div class="my-3">
                                <a href="/pmsschedule" class="btn btn-primary">Cancel</a>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="table-responsive">
                                <table class="table table-bordered table-view">
                                    <thead>
                                        <tr>
                                            <th scope="col">Assigned Technician</th>
                                            <th scope="col">Last Followed Date</th>
                                            <th scope="col">Follow Up Date</th>
                                            <th scope="col">Status</th>
                                            <th scope="col">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($sql as $pms)
                                            <tr>
                                                <td>{{ $pms->assigned_technician }}</td>
                                                <td>{{ $pms->Last_Followed_Date }}</td>
                                                <td>{{ $pms->Follow_Up_Date }}</td>
                                                <td>
                                                    @if ($pms->Status == 'open')
                                                        <span class="badge badge-pill badge-success text-12">
                                                            {{ $pms->Status }}
                                                        </span>
                                                    @else
                                                        <span class="badge badge-pill badge-secondary text-12">
                                                            {{ $pms->Status }}
                                                        </span>
                                                    @endif
                                                </td>
                                                <td class="d-flex flex-nowrap">
                                                    <button type="button" class="btn btn-outline-success"
                                                        data-technician="{{ $pms->assigned_technician }}"
                                                        data-lastdate="{{ $pms->Last_Followed_Date }}"
                                                        data-status="{{ $pms->Status }}"
                                                        data-contractid="{{ $pms->Contract_ID }}" data-toggle="modal"
                                                        data-target="#editenquiry" class="mr-1">
                                                        <i class="fas fa-edit"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <!-- Modal Edit Products -->
                                <div class="modal fade custom-modal" id="editenquiry" tabindex="-1" role="dialog"
                                    aria-labelledby="editenquiry" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered edit modal-xl" role="document">
                                        <form id="edit_form" accept-charset="UTF-8" method="POST" class="w-100">
                                            @csrf
                                            <div class="modal-content">
                                                <div class="icon">
                                                    <span class="fa-stack fa-2x text-success">
                                                        <i class="fas fa-circle fa-stack-2x"></i>
                                                        <i class="far fa-edit fa-stack-1x fa-inverse"></i>
                                                    </span>
                                                </div>
                                                <div class="modal-header justify-content-center">
                                                    <h3 class="modal-title text-capitalize font-weight-bolder mt-3"
                                                        id="editenquiry">
                                                        Edit Enquiry
                                                    </h3>
                                                </div>
                                                <div class="modal-body pb-0">
                                                    <div class="row">
                                                        <div class="form-group col-md-4">
                                                            <label for="edit_technician"
                                                                class="mb-0 control-label active">Assigned
                                                                Technician</label>
                                                            <select class="form-control" id="edit_technician"
                                                                name="edit_technician"></select>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label for="edit_lastfollowup_date"
                                                                class="mb-0 control-label active">Last Followed Date</label>
                                                            <input id="edit_lastfollowup_date" name="edit_lastfollowup_date"
                                                                type="text" class="form-control" readonly>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label for="edit_status"
                                                                class="mb-0 control-label active">Status
                                                                <small>*</small></label>
                                                            <select class="form-control" id="edit_status"
                                                                name="edit_status">
                                                                <option value="open">Open</option>
                                                                <option value="close">Close</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer flex-nowrap">
                                                    <button type="button"
                                                        class="btn btn-outline-success btn-block btn-lg mt-0"
                                                        data-dismiss="modal">Close</button>
                                                    <button type="submit"
                                                        class="btn btn-success btn-block btn-lg mt-0">Submit</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <!-- End Edit Products -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    @include('components.alertmodal')
@endsection
@section('script')
    <script src="{{ URL::asset('assets/js/datatables.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            loadTechnicians();
            //Edit Modal
            $("#editenquiry").on('show.bs.modal', function(event) {
                //console.log('model opened');
                var pms = $(event.relatedTarget);
                var technician = pms.data('technician');
                var lastdate = pms.data('lastdate');
                var status = pms.data('status');
                var id = pms.data('contractid');

                var action = `{{ url('updatepms/${id}') }}`
                //console.log('action', action);

                //push retrieved data into mentioned id 
                $('#edit_form').attr('action', action);
                $('#edit_technician').val(technician);
                $('#edit_lastfollowup_date').val(lastdate);
                $('#edit_status').val(status);

            });
        });

        function loadTechnicians() {
            return $.ajax({
                url: "/technicians",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    //console.log("Technicians", data);
                    $('#edit_technician').empty();
                    $('#edit_technician').append('<option selected="true" disabled></option>');
                    $('#edit_technician').prop('selectedIndex', 0);
                    $.each(data, function(key, value) {
                        $('#edit_technician').append($('<option></option>').attr('value',
                            value
                            .emp_firstname).text(value.emp_firstname));
                    });
                    $('#edit_technician').append('<option value="others">Others</option>');
                },
                error: function(error) {
                    console.log('Technicians load error:', error);
                }
            });
        }

    </script>
@endsection
