@extends('layouts.app')
@section('mytitle', 'Generate Tickets')
@section('content')
    <div class="bg-top bg-gray p-4">
        <div class="d-flex">
            <h5 class="font-weight-bold text-uppercase mb-0">Generate Tickets</h5>
        </div>
    </div>
    <section class="my-4 p-4 card-main">
        <form action="{{ url('addticket') }}" method="POST">
            @csrf
            <div class="row">
                <div class="col-md-12 py-4">
                    <div class="card shadow h-100">
                        <div class="card-top ">
                            <div class="card-title mb-0">
                                <h5 class="font-weight-bold text-uppercase mb-0"><i class="fas fa-user-alt"></i></h5>
                            </div>
                        </div>
                        <div class="card-body pt-0">
                            <h6 class="fw-700 text-uppercase">Customer info</h6>
                            <div class="row">
                                <div class="form-group col-md-4 @error('cus_name') is-invalid @enderror">
                                    <label class="control-label" for="cus_name">Customer Name</label>
                                    <input id="cus_name" name="cus_name" type="text" class="form-control">
                                    @error('cus_name')
                                        <span class="text-danger text-12">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group col-md- @error('cus_phone') is-invalid @enderror">
                                    <label class="control-label" for="cus_phone">Phone No
                                        <small>*</small></label>
                                    <input id="cus_phone" name="cus_phone" type="tel" class="form-control">
                                    @error('cus_phone')
                                        <span class="text-danger text-12">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group col-md-4 @error('mac_serial') is-invalid @enderror">
                                    <label class="control-label" for="mac_serial">Machine Serial No
                                        <small>*</small></label>
                                    <input type="text" class="form-control" id="mac_serial" name="mac_serial">
                                    @error('mac_serial')
                                        <span class="text-danger text-12">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group col-md-4 @error('cus_address') is-invalid @enderror">
                                    <label class="control-label" for="cus_address">Address <small>*</small></label>
                                    <input type="text" class="form-control" id="cus_address" name="cus_address">
                                    @error('cus_address')
                                        <span class="text-danger text-12">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group col-md-4 @error('cus_city') is-invalid @enderror">
                                    <label class="control-label" for="cus_city">City <small>*</small></label>
                                    <input type="text" class="form-control" id="cus_city" name="cus_city">
                                    @error('cus_city')
                                        <span class="text-danger text-12">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group col-md-4 @error('cus_state') is-invalid @enderror">
                                    <label class="control-label" for="cus_state">State <small>*</small></label>
                                    <input type="text" class="form-control" id="cus_state" name="cus_state">
                                    @error('cus_state')
                                        <span class="text-danger text-12">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group col-md-4 @error('cus_pincode') is-invalid @enderror">
                                    <label class="control-label" for="cus_pincode">Postal Code
                                        <small>*</small></label>
                                    <input type="text" class="form-control" id="cus_pincode" name="cus_pincode">
                                    @error('cus_pincode')
                                        <span class="text-danger text-12">{{ $message }}</span>
                                    @enderror
                                </div>

                                <div class="col-md-12">
                                    <div class="custom-control custom-checkbox mb-2 mt-4 pt-3">
                                        <input type="checkbox" class="custom-control-input" id="sameascustomer"
                                            onchange="valueChanged()">
                                        <label class="custom-control-label font-weight-bold h6 mb-1 text-uppercase"
                                            for="sameascustomer">Same as Customer info</label>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label address_label" for="delivery_address">Address
                                        <small>*</small></label>
                                    <input type="text" class="form-control" id="delivery_address" name="delivery_address">
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label address_label" for="delivery_city">City
                                        <small>*</small></label>
                                    <input type="text" class="form-control" id="delivery_city" name="delivery_city">
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label address_label" for="delivery_state">State
                                        <small>*</small></label>
                                    <input type="text" class="form-control" id="delivery_state" name="delivery_state">
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label address_label" for="delivery_pincode">Postal Code
                                        <small>*</small></label>
                                    <input type="text" class="form-control" id="delivery_pincode" name="delivery_pincode">
                                </div>
                                <div class="form-group col-md-4 @error('cus_branch') is-invalid @enderror">
                                    <label class="control-label" for="cus_branch">Branch Name
                                        <small>*</small></label>
                                    <select id="cus_branch" name="cus_branch" class="form-control"></select>
                                    @error('cus_branch')
                                        <span class="text-danger text-12">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group col-md-4 @error('product_name') is-invalid @enderror">
                                    <label class="control-label" for="product_name">Product / Service Name</label>
                                    <select id="product_name" name="product_name" class="form-control"></select>
                                    @error('product_name')
                                        <span class="text-danger text-12">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 py-4">
                    <div class="card shadow h-100">
                        <div class="card-top">
                            <div class="card-title mb-0">
                                <h5 class="font-weight-bold text-uppercase mb-0"><i class="fas fa-clipboard-list"></i></h5>
                            </div>
                        </div>
                        <div class="card-body pt-0">
                            <div class="row">
                                <div class="col-12">
                                    <h6 class="fw-700 text-uppercase">Ticket Details</h6>
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label" for="source">Source</label>
                                    <select class="form-control" id="source" name="source">
                                        <option disabled selected></option>
                                        <option value="CALL"> CALL </option>
                                        <option value="EMAIL"> EMAIL </option>
                                    </select>
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label" for="ticket_type">Ticket Type <small>*</small></label>
                                    <select class="form-control" id="ticket_type" name="ticket_type"></select>
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label" for="defect_code">Defect Code <small>*</small></label>
                                    <select class="form-control" id="defect_code" name="defect_code"></select>
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label" for="priority">Priority <small>*</small></label>
                                    <select class="form-control" id="priority" name="priority">
                                        <option disabled selected></option>
                                        <option value="P1"> P1 </option>
                                        <option value="P2"> P2 </option>
                                        <option value="P3"> P3 </option>
                                        <option value="P4"> P4 </option>
                                    </select>
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label" for="tat">Turn Around Time(TAT Hrs)</label>
                                    <input type="text" class="form-control" id="tat" name="tat">
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label" for="description">Problem Description
                                        <small>*</small></label>
                                    <textarea class="form-control mt-1" id="description" name="description"
                                        rows="1"></textarea>
                                </div>

                                <div class="col-md-12 mt-4">
                                    <h6 class="fw-700 text-uppercase">Caller Details</h6>
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label" for="caller_name">Caller/Sender Name</label>
                                    <input type="text" class="form-control" id="caller_name" name="caller_name">
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label" for="caller_email">Caller/Sender Email</label>
                                    <input type="text" class="form-control" id="caller_email" name="caller_email">
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label" for="caller_phone">Caller/Sender Phone</label>
                                    <input type="tel" class="form-control" id="caller_phone" name="caller_phone">
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label" for="assign_technician">Assign Technician
                                        <small>*</small></label>
                                    <select class="form-control" id="assign_technician" name="assign_technician"></select>
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label" for="assign_branch">Techinician Branch
                                        <small>*</small></label>
                                    <select class="form-control" id="assign_branch" name="assign_branch"></select>
                                </div>
                                <div class="col-12">
                                    <div class="row">
                                        <div class="form-group col-md-4 mt-4 pt-3">
                                            <h6 class="fw-700 text-uppercase">Charges</h6>
                                            <label class="control-label" for="service_charges">Service Charges</label>
                                            <input type="text" class="form-control" id="service_charges"
                                                name="service_charges">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-12 mt-4 pt-3">
                                    <h6 class="fw-700 text-uppercase">Record Meter Readings</h6>
                                </div>
                                <div class="form-group col-md-3">
                                    <label class="control-label" for="black_a3">Black A3</label>
                                    <input type="text" class="form-control" id="black_a3" name="black_a3">
                                </div>
                                <div class="form-group col-md-3">
                                    <label class="control-label" for="black_a4">Black A4</label>
                                    <input type="text" class="form-control" id="black_a4" name="black_a4">
                                </div>
                                <div class="form-group col-md-3">
                                    <label class="control-label" for="color_a3">Color A3</label>
                                    <input type="text" class="form-control" id="color_a3" name="color_a3">
                                </div>
                                <div class="form-group col-md-3">
                                    <label class="control-label" for="color_a4">Color A4</label>
                                    <input type="text" class="form-control" id="color_a4" name="color_a4">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <a href="/viewticket" type="submit" class="btn btn-outline-primary btn-lg btn-large mr-2">Cancel</a>
                <button type="submit" class="btn btn-primary btn-lg btn-large">Submit</button>
            </div>
        </form>
    </section>

    <div class="modal fade custom-modal" id="add_defect_code" tabindex="-1" role="dialog" aria-labelledby="add_defect_code"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content success">
                <div class="icon">
                    <span class="fa-stack fa-2x text-success">
                        <i class="fas fa-circle fa-stack-2x"></i>
                        <i class="fas fas fa-plus fa-stack-1x fa-inverse"></i>
                    </span>
                </div>
                <div class="modal-header justify-content-center">
                    <h3 class="modal-title text-capitalize font-weight-bolder mt-3">
                        Add Defect Code
                    </h3>
                </div>
                <form action="{{ url('addticketdefectcode') }}" method="POST">
                    @csrf
                    <div class="modal-body pb-0">
                        <div class="row justify-content-center">
                            <div class="col-md-11">
                                <div class="form-group">
                                    <label for="add_defect_code" class="col-form-label">Enter Defect Code</label>
                                    <input type="text" class="form-control" id="add_defect_code" name="add_defect_code">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer flex-nowrap">
                            <button type="button" class="btn btn-outline-success btn-block btn-lg mt-0"
                                data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-success btn-block btn-lg mt-0">Done</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade custom-modal" id="add_ticket_type" tabindex="-1" role="dialog" aria-labelledby="add_ticket_type"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content success">
                <div class="icon">
                    <span class="fa-stack fa-2x text-success">
                        <i class="fas fa-circle fa-stack-2x"></i>
                        <i class="fas fas fa-plus fa-stack-1x fa-inverse"></i>
                    </span>
                </div>
                <div class="modal-header justify-content-center">
                    <h3 class="modal-title text-capitalize font-weight-bolder mt-3">
                        Add Ticket Type
                    </h3>
                </div>
                <form action="{{ url('addtickettype') }}" method="POST">
                    @csrf
                    <div class="modal-body pb-0">
                        <div class="row justify-content-center">
                            <div class="col-md-11">
                                <div class="form-group">
                                    <label for="add_ticket_type" class="col-form-label">Enter Ticket Type Name</label>
                                    <input type="text" class="form-control" id="add_ticket_type" name="add_ticket_type">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer flex-nowrap">
                        <button type="button" class="btn btn-outline-success btn-block btn-lg mt-0"
                            data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success btn-block btn-lg mt-0">Done</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @include('components.alertmodal')
@endsection
@section('script')
    @if (session()->has('message'))
        <script>
            $(document).ready(function() {
                $('#successAlert').modal("show");
            });

        </script>
    @endif
    @if (session()->has('error'))
        <script>
            $(document).ready(function() {
                $('#errorAlert').modal("show");
            });

        </script>
    @endif
    <script>
        function valueChanged() {
            if ($('#sameascustomer').is(":checked")) {
                console.log('checked');
                var address = $("#cus_address").val();
                var city = $("#cus_city").val();
                var pincode = $("#cus_pincode").val();
                var state = $("#cus_state").val();
                console.log('address:', address);
                console.log('city:', city);
                console.log('pincode:', pincode);
                console.log('state:', state);

                $("#delivery_address").val(address);
                $("#delivery_city").val(city);
                $("#delivery_pincode").val(pincode);
                $("#delivery_state").val(state);
                $('.address_label').addClass('active');
            } else {
                console.log('unchecked');
                $("#delivery_address").val('');
                $("#delivery_city").val('');
                $("#delivery_pincode").val('');
                $("#delivery_state").val('');
                $('.address_label').removeClass('active');
            }
        }
        $(document).ready(function() {
            getBranch();
            loadProducts();
            loadTicketType();
            loadDefectCode();
            loadTechnicians();
            loadTechnicianBranch();

            $('#product_name').change(function() { //jQuery Change Function
                var opval = $(this).val(); //Get value from select element
                if (opval == "others") { //Compare it and if true
                    return window.location.href = "/addproduct";
                }
            });
            $('#cus_branch').change(function() { //jQuery Change Function
                var opval = $(this).val(); //Get value from select element
                if (opval == "others") { //Compare it and if true
                    return window.location.href = "/addorganization";
                }
            });
            $('#ticket_type').change(function() { //jQuery Change Function
                var opval = $(this).val(); //Get value from select element
                if (opval == "others") { //Compare it and if true
                    $('#add_ticket_type').modal("show"); //Open Modal
                }
            });
            $('#defect_code').change(function() { //jQuery Change Function
                var opval = $(this).val(); //Get value from select element
                if (opval == "others") { //Compare it and if true
                    $('#add_defect_code').modal("show"); //Open Modal
                }
            });
            $('#assign_branch').change(function() { //jQuery Change Function
                var opval = $(this).val(); //Get value from select element
                if (opval == "others") { //Compare it and if true
                    return window.location.href = "/addorganization";
                }
            });
        });

        function loadProducts() {
            return $.ajax({
                url: "/loadproducts",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    //console.log("branchname", data);
                    $('#product_name').empty();
                    $('#product_name').append('<option selected="true" disabled></option>');
                    $('#product_name').prop('selectedIndex', 0);
                    $.each(data, function(key, value) {
                        $('#product_name').append($('<option></option>').attr('value', value
                            .product_name).text(value.product_name));
                    });
                    $('#product_name').append('<option value="others">Others</option>');
                },
                error: function(error) {
                    console.log('product_name error:', error);
                }
            });
        }

        function getBranch() {
            return $.ajax({
                url: "/branchname",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    //console.log("branchname", data);
                    $('#cus_branch').empty();
                    $('#cus_branch').append('<option selected="true" disabled></option>');
                    $('#cus_branch').prop('selectedIndex', 0);
                    $.each(data, function(key, value) {
                        $('#cus_branch').append($('<option></option>').attr('value', value
                            .org_branch_name).text(value.org_branch_name));
                    });
                    $('#cus_branch').append('<option value="others">Others</option>');
                },
                error: function(error) {
                    console.log('cus_branch error:', error);
                }
            });
        }

        function loadTicketType() {
            return $.ajax({
                url: "/tickettype",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    //console.log("tickettype", data);
                    $('#ticket_type').empty();
                    $('#ticket_type').append('<option selected="true" disabled></option>');
                    $('#ticket_type').prop('selectedIndex', 0);
                    $.each(data, function(key, value) {
                        $('#ticket_type').append($('<option></option>').attr(
                            'value',
                            value.ticket_type).text(value.ticket_type));
                    });
                    $('#ticket_type').append('<option value="others">Others</option>');
                },
                error: function(error) {
                    console.log('ticket_type error:', error);
                }
            });
        }

        function loadDefectCode() {
            return $.ajax({
                url: "/ticketdefectcode",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    console.log("ticketdefectcode", data);
                    $('#defect_code').empty();
                    $('#defect_code').append('<option selected="true" disabled></option>');
                    $('#defect_code').prop('selectedIndex', 0);
                    $.each(data, function(key, value) {
                        $('#defect_code').append($('<option></option>').attr('value',
                            value.code_name).text(value.code_name));
                    });
                    $('#defect_code').append('<option value="others">Others</option>');
                },
                error: function(error) {
                    console.log('ticketdefectcode load error:', error);
                }
            });
        }

        function loadTechnicians() {
            return $.ajax({
                url: "/technicians",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    //console.log("Technicians", data);
                    $('#assign_technician').empty();
                    $('#assign_technician').append('<option selected="true" disabled></option>');
                    $('#assign_technician').prop('selectedIndex', 0);
                    $.each(data, function(key, value) {
                        var name = value.emp_firstname + value.emp_lastname;
                        $('#assign_technician').append($('<option></option>').attr('value',
                            name).text(name));
                    });
                },
                error: function(error) {
                    console.log('Technicians load error:', error);
                }
            });
        }

        function loadTechnicianBranch() {
            return $.ajax({
                url: "/branchname",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    //console.log("branchname", data);
                    $('#assign_branch').empty();
                    $('#assign_branch').append('<option selected="true" disabled></option>');
                    $('#assign_branch').prop('selectedIndex', 0);
                    $.each(data, function(key, value) {
                        $('#assign_branch').append($('<option></option>').attr('value', value
                            .org_branch_name).text(value.org_branch_name));
                    });
                    $('#assign_branch').append('<option value="others">Others</option>');
                },
                error: function(error) {
                    console.log('assign_branch error:', error);
                }
            });
        }

    </script>
@endsection
