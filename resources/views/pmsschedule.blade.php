@extends('layouts.app')
@section('mytitle', 'PM Schedule')
@section('head-links')
    <link href="{{ URL::asset('assets/css/datatables.min.css') }}" rel="stylesheet">
@endsection
@section('content')
    <div class="bg-top bg-gray p-4">
        <div class="d-flex">
            <h5 class="font-weight-bold text-uppercase mb-0">PM Schedule</h5>
        </div>
    </div>

    <div class="p-4 card-main">
        <section class="my-5">
            <div class="card shadow">
                <div class="card-top">
                    <div class="card-title mb-0">
                        <h5 class="font-weight-bold text-uppercase mb-0"><i class="fas fa-user-alt"></i></h5>
                    </div>
                    <div class="btn-right position-relative float-right">
                        <a href="#" class="btn btn-primary text-uppecase">Print</a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table" id="datatable">
                            <thead class="text-uppercase">
                                <tr>
                                    <th scope="col">MODEL NAME</th>
                                    <th scope="col">Machine Serial No.</th>
                                    <th scope="col">Customer Name</th>
                                    <th scope="col">LAST FOLLOWED DATE</th>
                                    <th scope="col">Follow Up DATE</th>
                                    <th scope="col">Status</th>
                                    <th scope="col" class="nosort">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (count($sql) >= 1)
                                    @foreach ($sql as $pms)
                                        <tr>
                                            <td>{{$pms->Item_Model_Name}}</td>
                                            <td>{{$pms->Machine_Serial_No}}</td>
                                            <td>{{$pms->customer_name}}</td>
                                            <td>{{$pms->Last_Followed_Date}}</td>
                                            <td>{{$pms->Follow_Up_Date}}</td>
                                            <td>
                                                @if ($pms->Status == 'open')
                                                    <span class="badge badge-pill badge-success text-12">
                                                        {{ $pms->Status }}
                                                    </span>
                                                @else
                                                    <span class="badge badge-pill badge-secondary text-12">
                                                        {{ $pms->Status }}
                                                    </span>
                                                @endif
                                            </td>
                                            <td class="d-flex flex-nowrap">
                                                <a href="/viewpms/{{$pms->Machine_Serial_No}}" class="btn btn-outline-info" data-toggle="tooltip"
                                                    data-placement="top" title="View PMS">
                                                    <i class="fas fa-user-alt"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                <tr>
                                    <td colspan="7">Sorry, No records found</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('script')
    <script src="{{ URL::asset('assets/js/datatables.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#datatable').DataTable({
                language: {
                    searchPlaceholder: "Search records",
                    search: "",
                    "lengthMenu": "_MENU_ Entries"
                },
                "lengthMenu": [
                    [8, 16, 32, -1],
                    [8, 16, 32, "All"]
                ],
                "pageLength": 8,
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': ['nosort']
                }]
            });
        });

    </script>
@endsection
