@extends('layouts.app')
@section('mytitle', 'View Contract')
@section('head-links')
    <link href="{{ URL::asset('assets/css/datatables.min.css') }}" rel="stylesheet">
@endsection
@section('content')
    <div class="bg-top bg-gray p-4">
        <div class="d-flex">
            <h5 class="font-weight-bold text-uppercase mb-0">View Contract</h5>
        </div>
    </div>

    <div class="p-4 card-main">
        <section class="my-5">
            <div class="card shadow">
                <div class="card-top ">
                    <div class="card-title mb-0">
                        <h5 class="font-weight-bold text-uppercase mb-0"><i class="fas fa-file-contract"></i></h5>
                    </div>
                    <div class="btn-right position-relative float-right">
                        <a href="#" class="btn btn-primary text-uppecase">Print</a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table" id="datatable">
                            <thead class="text-uppercase">
                                <tr>
                                    <th scope="col">Contract ID</th>
                                    <th scope="col">Contract Name</th>
                                    <th scope="col">Contract Type</th>
                                    <th scope="col">MACHINE SERIAL NO.</th>
                                    <th scope="col">Start DATE</th>
                                    <th scope="col">Expiry DATE</th>
                                    <th scope="col">Status</th>
                                    <th scope="col" class="nosort">ACTION</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (count($contract) >= 1)
                                    @foreach ($contract as $contract)
                                        <tr>
                                            <td>{{ $contract->Contract_Id }}</td>
                                            <td>{{ $contract->customer_name }}</td>
                                            <td>{{ $contract->Contract_type }}</td>
                                            <td>{{ $contract->Machine_Serial_No }}</td>
                                            <td>{{ $contract->Start_Date }}</td>
                                            <td>{{ $contract->End_Date }}</td>
                                            <td>
                                                @if ($contract->Contract_Status == 'open')
                                                    <span class="badge badge-pill badge-success text-12">
                                                        {{ $contract->Contract_Status }}
                                                    </span>
                                                @else
                                                    <span class="badge badge-pill badge-secondary text-12">
                                                        {{ $contract->Contract_Status }}
                                                    </span>
                                                @endif
                                            </td>
                                            <td class="d-flex flex-nowrap">
                                                <a href="{{ url('/viewcontracts', $contract->Contract_Id) }}"
                                                    class="btn btn-outline-info mr-1">
                                                    <i class="fas fa-external-link-alt"></i>
                                                </a>
                                                <a href="{{ url('/edit_contract', $contract->Contract_Id) }}"
                                                    class="btn btn-outline-success mr-1">
                                                    <i class="fas fa-edit"></i>
                                                </a>
                                                <span data-toggle="modal" data-target="#deletecontract"
                                                    data-id="{{ $contract->Contract_Id }}"
                                                    data-name="{{ $contract->customer_name }}"
                                                    data-productname="{{ $contract->product_name }}"
                                                    data-status="{{ $contract->Contract_Status }}">
                                                    <button type="button" class="btn btn-outline-danger"
                                                        data-toggle="tooltip" data-placement="top" title="Delete Contract">
                                                        <i class="fas fa-trash-alt"></i>
                                                    </button>
                                                </span>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="8">Sorry, No records found</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                        <!-- Modal Delete Contract -->
                        <div class="modal fade custom-modal" id="deletecontract" tabindex="-1" role="dialog"
                            aria-labelledby="deletecontract" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="icon">
                                        <span class="fa-stack fa-2x text-primary">
                                            <i class="fas fa-circle fa-stack-2x"></i>
                                            <i class="fas fa-trash-alt fa-stack-1x fa-inverse"></i>
                                        </span>
                                    </div>
                                    <div class="modal-header justify-content-center">
                                        <h3 class="modal-title text-capitalize font-weight-bolder mt-3" id="deletecontract">
                                            Delete
                                            Contract?
                                        </h3>
                                    </div>
                                    <div class="modal-body pb-0">
                                        <p class="font-weight-bold text-center">
                                            Are you sure you want to delete the Contract?
                                            <br>
                                            You cannot
                                            undo this operation!
                                        </p>
                                        <div class="row justify-content-center">
                                            <div class="col-md-11">
                                                <form>
                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <label for="delete_ticket_id" class="col-form-label">Contract
                                                                Id</label>
                                                            <input type="text" class="form-control" id="delete_ticket_id"
                                                                name="delete_ticket_id" readonly>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="delete_customer_name"
                                                                class="col-form-label">Customer
                                                                Name</label>
                                                            <input type="text" class="form-control"
                                                                id="delete_customer_name" readonly>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="delete_product_name" class="col-form-label">Product
                                                                Name</label>
                                                            <input type="text" class="form-control" id="delete_product_name"
                                                                readonly>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="delete_status" class="col-form-label">Status</label>
                                                            <input type="text" class="form-control" id="delete_status"
                                                                readonly>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer justify-content-center" id="delete_btn"></div>
                                </div>
                            </div>
                        </div>
                        <!-- End Delete Contract -->
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('script')
    <script src="{{ URL::asset('assets/js/datatables.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#datatable').DataTable({
                language: {
                    searchPlaceholder: "Search records",
                    search: "",
                    "lengthMenu": "_MENU_ Entries"
                },
                "lengthMenu": [
                    [8, 16, 32, -1],
                    [8, 16, 32, "All"]
                ],
                "pageLength": 8,
                "aaSorting": [
                    [0, 'desc']
                ],
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': ['nosort']
                }]
            });
            $("#deletecontract").on('show.bs.modal', function(event) {
                var val = $(event.relatedTarget);
                var id = val.data('id');
                var name = val.data('name');
                var prodName = val.data('productname');
                var status = val.data('status');

                $("#delete_ticket_id").val(id);
                $("#delete_customer_name").val(name);
                $("#delete_product_name").val(prodName);
                $("#delete_status").val(status);

                $('#delete_btn').html(
                    `<button type="button" class="btn btn-outline-primary btn-lg mt-0" data-dismiss="modal">Close</button><a href='deletecontract/${id}' class="btn btn-primary btn-lg mt-0">Delete Contract</a><a href='closecontract/${id}' class="btn btn-primary btn-lg mt-0">Close Contract</a>`
                );
            });
        });

    </script>
@endsection
