@extends('layouts.app')
@section('mytitle', 'View Ticket')
@section('head-links')
    <link href="{{ URL::asset('assets/css/datatables.min.css') }}" rel="stylesheet">
@endsection
@section('content')
    <div class="bg-top bg-gray p-4">
        <div class="d-flex">
            <h5 class="font-weight-bold text-uppercase mb-0">View Ticket</h5>
        </div>
    </div>

    <div class="p-4 card-main">
        <section class="my-5">
            <div class="card shadow">
                <div class="card-top ">
                    <div class="card-title mb-0">
                        <h5 class="font-weight-bold text-uppercase mb-0"><i class="fas fa-ticket-alt"></i></h5>
                    </div>
                    <div class="btn-right position-relative float-right">
                        <a href="#" class="btn btn-primary text-uppecase">Print</a>
                        <a href="/addticket" class="btn btn-primary text-uppecase" data-toggle="tooltip"
                            data-placement="top" title="Add Ticket">
                            <i class="fas fa-plus"></i> Add
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table" id="datatable">
                            <thead class="text-uppercase">
                                <tr>
                                    <th scope="col">TICKET ID</th>
                                    <th scope="col">STATUS</th>
                                    <th scope="col">CUSTOMER NAME</th>
                                    <th scope="col">MACHINE SERIAL NO.</th>
                                    <th scope="col">TECHNICIAN NAME</th>
                                    <th scope="col">PRIORITY</th>
                                    <th scope="col">CREATED DATE</th>
                                    <th scope="col">CLOSED DATE</th>
                                    <th scope="col">ACTION</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (count($select) >= 1)
                                    @foreach ($select as $ticket)
                                        <tr>
                                            <td>{{ $ticket->Ticket_id }}</td>
                                            <td>
                                                @if ($ticket->Ticket_Status == 'open')
                                                    <span
                                                        class="badge badge-pill badge-success text-12">{{ $ticket->Ticket_Status }}</span>
                                                @else
                                                    <span
                                                        class="badge badge-pill badge-secondary text-12">{{ $ticket->Ticket_Status }}</span>
                                                @endif
                                            </td>
                                            <td>{{ $ticket->customer_name }}</td>
                                            <td>{{ $ticket->Machine_Serial_No }}</td>
                                            <td>{{ $ticket->Assign_Technician_Name }}</td>
                                            <td>
                                                <span
                                                    class="badge badge-pill  badge-secondary text-white text-sm">{{ $ticket->Ticket_Priority }}</span>
                                            </td>
                                            <td>{{ $ticket->Created_Date }}</td>
                                            <td>{{ $ticket->Ticket_Closed_Date }}</td>
                                            <td class="d-flex flex-nowrap">
                                                <a href="{{ url('/viewtickets', [$ticket->Ticket_id]) }}"
                                                    class="btn btn-outline-info mr-1">
                                                    <i class="fas fa-user"></i>
                                                </a>
                                                <span data-toggle="modal" data-target="#deleteticket"
                                                    data-id="{{ $ticket->Ticket_id }}"
                                                    data-customer_name="{{ $ticket->customer_name }}"
                                                    data-product_name="{{ $ticket->product_model }}"
                                                    data-status="{{ $ticket->Ticket_Status }}">
                                                    <button type="button" class="btn btn-outline-danger"
                                                        data-toggle="tooltip" data-placement="top" title="Delete Product">
                                                        <i class="fas fa-trash-alt"></i>
                                                    </button>
                                                </span>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="9">Sorry, No records found</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                        <!-- Modal Delete Customer -->
                        <div class="modal fade custom-modal" id="deleteticket" tabindex="-1" role="dialog"
                            aria-labelledby="deleteticket" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="icon">
                                        <span class="fa-stack fa-2x text-primary">
                                            <i class="fas fa-circle fa-stack-2x"></i>
                                            <i class="fas fa-trash-alt fa-stack-1x fa-inverse"></i>
                                        </span>
                                    </div>
                                    <div class="modal-header justify-content-center">
                                        <h3 class="modal-title text-capitalize font-weight-bolder mt-3" id="deleteticket">
                                            Delete
                                            Ticket?
                                        </h3>
                                    </div>
                                    <div class="modal-body pb-0">
                                        <p class="font-weight-bold text-center">
                                            Are you sure you want to delete the Ticket?
                                            <br>
                                            You cannot
                                            undo this operation!
                                        </p>
                                        <div class="row justify-content-center">
                                            <div class="col-md-11">
                                                <form>
                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <label for="delete_ticket_id" class="col-form-label">Ticket
                                                                Id</label>
                                                            <input type="text" class="form-control" id="delete_ticket_id"
                                                                readonly placeholder="1">
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="delete_customer_name"
                                                                class="col-form-label">Customer
                                                                Name</label>
                                                            <input type="text" class="form-control"
                                                                id="delete_customer_name" readonly>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="delete_product_name" class="col-form-label">Product
                                                                Name</label>
                                                            <input type="text" class="form-control" id="delete_product_name"
                                                                readonly>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="delete_status" class="col-form-label">Customer
                                                                Ticket Status</label>
                                                            <input type="text" class="form-control" id="delete_status"
                                                                readonly>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer flex-nowrap" id="delete_btn"></div>
                                </div>
                            </div>
                        </div>
                        <!-- End Delete Customer -->
                    </div>
                </div>
            </div>
        </section>
    </div>
    @include('components.alertmodal')
@endsection
@section('script')
    @if (session()->has('message'))
        <script>
            $(document).ready(function() {
                $('#successAlert').modal("show");
            });

        </script>
    @endif
    @if (session()->has('update'))
        <script>
            $(document).ready(function() {
                $('#updateAlert').modal("show");
            });

        </script>
    @endif
    @if (session()->has('delete'))
        <script>
            $(document).ready(function() {
                $('#deleteAlert').modal("show");
            });

        </script>
    @endif
    @if (session()->has('error'))
        <script>
            $(document).ready(function() {
                $('#errorAlert').modal("show");
            });

        </script>
    @endif
    <script src="{{ URL::asset('assets/js/datatables.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#datatable').DataTable({
                language: {
                    searchPlaceholder: "Search records",
                    search: "",
                    "lengthMenu": "_MENU_ Entries"
                },
                "lengthMenu": [
                    [8, 16, 32, -1],
                    [8, 16, 32, "All"]
                ],
                "pageLength": 8,
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': ['nosort']
                }]
            });
            $("#deleteticket").on('show.bs.modal', function(event) {
                var ticket = $(event.relatedTarget);
                var id = ticket.data('id');
                var name = ticket.data('customer_name');
                var prodName = ticket.data('product_name');
                var status = ticket.data('status');

                $("#delete_product_id").val(id);
                $("#delete_customer_name").val(name);
                $("#delete_product_name").val(prodName);
                $("#delete_status").val(status);

                $('#delete_btn').html(
                    `<button type="button" class="btn btn-outline-primary btn-block btn-lg mt-0" data-dismiss="modal">Close</button><a href='deleteticket/${id}' class="btn btn-primary btn-block btn-lg mt-0">Delete</a>`
                );
            });
        });

    </script>
@endsection
