@extends('layouts.app')
@section('mytitle', 'Add Customer')
@section('content')
    <div class="bg-top bg-gray p-4">
        <div class="d-flex">
            <h5 class="font-weight-bold text-uppercase mb-0">Customer info</h5>
        </div>
    </div>
    <section class="my-4 p-4 card-main">
        <form action="addcus" method="post">
            @csrf
            <div class="row">
                <div class="col-md-6 py-4">
                    <div class="card shadow">
                        <div class="card-top ">
                            <div class="card-title mb-0">
                                <h5 class="font-weight-bold text-uppercase mb-0"><i class="fas fa-user-alt"></i></h5>
                            </div>
                        </div>
                        <div class="card-body pt-0">
                            <div class="form-group @error('customer_type') is-invalid @enderror">
                                <label class="control-label" for="customer_type">Type <small>*</small></label>
                                <select class="form-control" id="customer_type" name="customer_type">
                                    <option disabled selected></option>
                                    <option value="Organization"> Organization </option>
                                    <option value="Individual"> Individual </option>
                                </select>
                                @error('customer_type')
                                    <span class="text-danger text-12">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group @error('customer_name') is-invalid @enderror">
                                <label class="control-label" for="customer_name">Customer Name/Organization Name</label>
                                <input id="customer_name" name="customer_name" type="text" class="form-control">
                                @error('customer_name')
                                    <span class="text-danger text-12">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="add_product_gst">GST NO.</label>
                                <input type="text" class="form-control" id="add_product_gst" name="add_product_gst">
                                <span class="material-input"></span>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="customer_email">Email ID </label>
                                <input id="customer_email" name="customer_email" type="text" class="form-control">
                            </div>
                            <div class="form-group @error('customer_phoneno') is-invalid @enderror">
                                <label class="control-label" for="customer_phoneno">Phone No <small>*</small></label>
                                <input id="customer_phoneno" name="customer_phoneno" type="tel" class="form-control">
                                @error('customer_phoneno')
                                    <span class="text-danger text-12">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="customer_landline">Land Line
                                    No</label>
                                <input type="text" class="form-control" id="customer_landline" name="customer_landline">
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="customer_alterphoneno">Alternate Phone No</label>
                                <input type="tel" class="form-control" id="customer_alterphoneno"
                                    name="customer_alterphoneno">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 py-4">
                    <div class="card shadow h-100">
                        <div class="card-top ">
                            <div class="card-title mb-0">
                                <h5 class="font-weight-bold text-uppercase mb-0"><i class="fas fa-user-alt"></i></h5>
                            </div>
                        </div>
                        <div class="card-body mt-3">
                            <div class="custom-control custom-checkbox mb-2">
                                <input type="checkbox" class="custom-control-input" id="sameascus"
                                    onchange="valueChanged()">
                                <label class="custom-control-label font-weight-bold h6 mb-1 text-uppercase"
                                    for="sameascus">Same as Customer info</label>
                            </div>
                            <div class="form-group @error('customer_belongsto') is-invalid @enderror">
                                <label class="control-label" for="customer_belongsto">Belongs to branch
                                    <small>*</small></label>
                                <select id="customer_belongsto" name="customer_belongsto" class="form-control"></select>
                                @error('customer_belongsto')
                                    <span class="text-danger text-12">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="customer_section">Section</label>
                                <input type="text" class="form-control" id="customer_section" name="customer_section">
                            </div>
                            <div class="form-group @error('customer_primary_name') is-invalid @enderror">
                                <label class="control-label address_label" for="customer_primary_name">Primary Contact
                                    Person
                                    Name</label>
                                <input id="customer_primary_name" name="customer_primary_name" type="text"
                                    class="form-control">
                                @error('customer_primary_name')
                                    <span class="text-danger text-12">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label class="control-label address_label" for="customer_primary_email_id">Email ID</label>
                                <input id="customer_primary_email_id" name="customer_primary_email_id" type="email"
                                    class="form-control">
                            </div>
                            <div class="form-group @error('customer_phonenum') is-invalid @enderror">
                                <label class="control-label address_label" for="customer_phonenum">Phone No </label>
                                <input id="customer_phonenum" name="customer_phonenum" type="tel" class="form-control">
                                @error('customer_phonenum')
                                    <span class="text-danger text-12">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="lifetime">Life Time*</label>
                                <input id="lifetime" name="lifetime" type="date" placeholder="yyyy-mm-dd"
                                    class="flatpickr flatpickr-input form-control">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 py-4">
                    <div class="card shadow">
                        <div class="card-top ">
                            <div class="card-title mb-0">
                                <h5 class="font-weight-bold text-uppercase mb-0"><i class="fas fa-map-marker-alt"></i></h5>
                            </div>
                        </div>
                        <div class="card-body pt-0">
                            <div class="row">
                                <div class="form-group col-md-6 @error('customer_address') is-invalid @enderror">
                                    <label class="control-label" for="customer_address">Address <small>*</small></label>
                                    <input type="text" class="form-control" id="customer_address" name="customer_address">
                                    @error('customer_address')
                                        <span class="text-danger text-12">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6 @error('customer_city') is-invalid @enderror">
                                    <label class="control-label" for="customer_city">City <small>*</small></label>
                                    <input type="text" class="form-control" id="customer_city" name="customer_city">
                                    @error('customer_city')
                                        <span class="text-danger text-12">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6 @error('customer_state') is-invalid @enderror">
                                    <label class="control-label" for="customer_state">State <small>*</small></label>
                                    <input type="text" class="form-control" id="customer_state" name="customer_state">
                                    @error('customer_state')
                                        <span class="text-danger text-12">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6 @error('customer_postal') is-invalid @enderror">
                                    <label class="control-label" for="customer_postal">Postal Code <small>*</small></label>
                                    <input type="text" class="form-control" id="customer_postal" name="customer_postal">
                                    @error('customer_postal')
                                        <span class="text-danger text-12">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- <div class="col-md-6 py-4">
                    <div class="card shadow">
                        <div class="card-top ">
                            <div class="card-title mb-0">
                                <h5 class="font-weight-bold text-uppercase mb-0"><i class="far fa-building"></i></h5>
                            </div>
                        </div>
                        <div class="card-body pt-0">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label" for="product_brand_name">Brand Name
                                            <small>*</small></label>
                                        <select class="form-control" id="product_brand_name" name="product_brand_name"></select>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="product_model_name">Model Name<small>*</small></label>
                                        <select class="form-control" id="product_model_name" name="product_model_name"></select>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="customer_section">Section</label>
                                        <input type="text" class="form-control" id="customer_section" name="customer_section">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label" for="product_name">Product Name
                                            <small>*</small></label>
                                        <select class="form-control" id="product_name" name="product_name"></select>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="add_product_category">Product Category </label>
                                        <select class="form-control" id="add_product_category" name="add_product_category"></select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group mb-2">
                                        <label for="add_product_installed_date">Installed Date</label>
                                        <input id="add_product_installed_date" name="add_product_installed_date" type="date"
                                            placeholder="yyyy-mm-dd" class="flatpickr flatpickr-input form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> --}}
            </div>
            <div>
                <button type="submit" class="btn btn-primary btn-lg btn-large">Submit</button>
            </div>
        </form>
    </section>
    {{-- add_product_brand --}}
    <div class="modal fade custom-modal" id="add_product_brand" tabindex="-1" role="dialog"
        aria-labelledby="add_product_brand" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content success">
                <div class="icon">
                    <span class="fa-stack fa-2x text-success">
                        <i class="fas fa-circle fa-stack-2x"></i>
                        <i class="fas fas fa-plus fa-stack-1x fa-inverse"></i>
                    </span>
                </div>
                <div class="modal-header justify-content-center">
                    <h3 class="modal-title text-capitalize font-weight-bolder mt-3">
                        Add Product Brand
                    </h3>
                </div>
                <form action="addbrand" method="POST">
                    @csrf
                    <div class="modal-body pb-0">
                        <div class="row justify-content-center">
                            <div class="col-md-11">
                                <div class="form-group">
                                    <label for="add_brand_name" class="col-form-label">Enter Brand Name</label>
                                    <input type="text" class="form-control @error('add_brand_name') is-invalid @enderror"
                                        id="add_brand_name" name="add_brand_name">
                                    @error('add_brand_name')
                                        <span class="text-danger text-12">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer flex-nowrap">
                        <button type="button" class="btn btn-outline-success btn-block btn-lg mt-0"
                            data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success btn-block btn-lg mt-0">Done</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- add_product_category --}}
    <div class="modal fade custom-modal" id="add_product_category" tabindex="-1" role="dialog"
        aria-labelledby="add_product_category" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content success">
                <div class="icon">
                    <span class="fa-stack fa-2x text-success">
                        <i class="fas fa-circle fa-stack-2x"></i>
                        <i class="fas fas fa-plus fa-stack-1x fa-inverse"></i>
                    </span>
                </div>
                <div class="modal-header justify-content-center">
                    <h3 class="modal-title text-capitalize font-weight-bolder mt-3">
                        Add Product Category
                    </h3>
                </div>
                <form action="addprdcategory" method="POST">
                    @csrf
                    <div class="modal-body pb-0">
                        <div class="row justify-content-center">
                            <div class="col-md-11">
                                <div class="form-group">
                                    <label for="add_category" class="col-form-label">Enter New Product Category</label>
                                    <input type="text" class="form-control @error('add_category') is-invalid @enderror"
                                        id="add_category" name="add_category">
                                    @error('add_category')
                                        <span class="text-danger text-12">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer flex-nowrap">
                        <button type="button" class="btn btn-outline-success btn-block btn-lg mt-0"
                            data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success btn-block btn-lg mt-0">Done</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @include('components.alertmodal')
@endsection
@section('script')
    @if (session()->has('message'))
        <script>
            $(document).ready(function() {
                $('#successAlert').modal("show");
            });

        </script>
    @endif
    @if (session()->has('error'))
        <script>
            $(document).ready(function() {
                $('#errorAlert').modal("show");
            });

        </script>
    @endif
    <script>
        function valueChanged() {
            if ($('#sameascus').is(":checked")) {
                console.log('checked');
                var name = $("#customer_name").val();
                var email = $("#customer_email").val();
                var phone = $("#customer_phoneno").val();
                console.log('name:', name);
                console.log('email:', email);
                console.log('phone:', phone);

                $("#customer_primary_name").val(name);
                $("#customer_primary_email_id").val(email);
                $("#customer_phonenum").val(phone);
                $('.address_label').addClass('active');
            } else {
                console.log('unchecked');
                $("#customer_primary_name").val('');
                $("#customer_primary_email_id").val('');
                $("#customer_phonenum").val('');
                $('.address_label').removeClass('active');
            }
        }

        $(document).ready(function() {
            getBranch();
            loadBrandname();
            loadProductCategory();
            loadProductModel();
            loadProductName();
            $('#customer_belongsto').change(function() { //jQuery Change Function
                var opval = $(this).val(); //Get value from select element
                if (opval == "others") { //Compare it and if true
                    return window.location.href = "/addorganization";
                }
            });
            $('#product_brand_name').change(function() { //jQuery Change Function
                var opval = $(this).val(); //Get value from select element
                if (opval == "others") { //Compare it and if true
                    $('#add_product_brand').modal("show"); //Open Modal
                }
            });
            $('#product_model_name').change(function() { //jQuery Change Function
                var opval = $(this).val(); //Get value from select element
                if (opval == "others") { //Compare it and if true
                    return window.location = "addproduct";
                }
            });
            $('#product_name').change(function() { //jQuery Change Function
                var opval = $(this).val(); //Get value from select element
                if (opval == "others") { //Compare it and if true
                    return window.location = "addproduct";
                }
            });
        });

        function getBranch() {
            return $.ajax({
                url: "/branchname",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    //console.log("branchname", data);
                    $('#customer_belongsto').empty();
                    $('#customer_belongsto').append('<option selected="true" disabled></option>');
                    $('#customer_belongsto').prop('selectedIndex', 0);
                    $.each(data, function(key, value) {
                        $('#customer_belongsto').append($('<option></option>').attr('value', value
                            .org_branch_name).text(value.org_branch_name));
                    });
                    $('#customer_belongsto').append('<option value="others">Others</option>');
                },
                error: function(error) {
                    console.log('branchname error:', error);
                }
            });
        }

        function loadProductCategory() {
            return $.ajax({
                url: "/prdcategory",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    //console.log("prdhsn", data);
                    $('#add_product_category').empty();
                    $('#add_product_category').append('<option selected="true" disabled></option>');
                    $('#add_product_category').prop('selectedIndex', 0);
                    $.each(data, function(key, value) {
                        $('#add_product_category').append($('<option></option>').attr(
                            'value',
                            value.name).text(value.name));
                    });
                },
                error: function(error) {
                    console.log('branchname error:', error);
                }
            });
        }

        function loadBrandname() {
            return $.ajax({
                url: "/brand",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    //console.log("branchname", data);
                    $('#product_brand_name').empty();
                    $('#product_brand_name').append('<option selected="true" disabled></option>');
                    $('#product_brand_name').prop('selectedIndex', 0);
                    $.each(data, function(key, value) {
                        $('#product_brand_name').append($('<option></option>').attr('value', value
                            .name).text(value.name));
                    });
                    $('#product_brand_name').append('<option value="others">Others</option>');
                },
                error: function(error) {
                    console.log('branchname error:', error);
                }
            });
        }

        function loadProductModel() {
            return $.ajax({
                url: "/prdmodelname",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    //console.log("branchname", data);
                    $('#product_model_name').empty();
                    $('#product_model_name').append('<option selected="true" disabled></option>');
                    $('#product_model_name').prop('selectedIndex', 0);
                    $.each(data, function(key, value) {
                        $('#product_model_name').append($('<option></option>').attr('value', value
                            .product_model_name).text(value.product_model_name));
                    });
                    $('#product_model_name').append('<option value="others">Others</option>');
                },
                error: function(error) {
                    console.log('branchname error:', error);
                }
            });
        }

        function loadProductName() {
            return $.ajax({
                url: "/prdname",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    console.log("prdmodelname", data);
                    $('#product_name').empty();
                    $('#product_name').append('<option selected="true" disabled></option>');
                    $('#product_name').prop('selectedIndex', 0);
                    $.each(data, function(key, value) {
                        $('#product_name').append($('<option></option>').attr('value', value
                            .product_id).text(value.product_name));
                    });
                    $('#product_name').append('<option value="others">Others</option>');
                },
                error: function(error) {
                    console.log('branchname error:', error);
                }
            });
        }

    </script>
@endsection
