@extends('layouts.app')
@section('mytitle', 'Add Services')
@section('content')
    <div class="bg-top bg-gray p-4">
        <div class="d-flex">
            <h5 class="font-weight-bold text-uppercase mb-0">ADD Services</h5>
        </div>
    </div>
    <section class="my-4 p-4 card-main">
        <form action="addservice" method="POST">
            @csrf
            <div class="row">
                <div class="col-md-12 py-4">
                    <div class="card shadow h-100">
                        <div class="card-top ">
                            <div class="card-title mb-0">
                                <h5 class="font-weight-bold text-uppercase mb-0"><i class="far fa-building"></i></h5>
                            </div>
                        </div>
                        <div class="card-body pt-0">
                            <div class="row fw-90">
                                <div class="form-group col-md-6">
                                    <label class="control-label" for="service_hsn">HSN/SAC Number
                                        <small>*</small></label>
                                    <select id="service_hsn" name="service_hsn"
                                        class="form-control @error('service_hsn') is-invalid @enderror"></select>
                                    @error('service_hsn')
                                        <span class="text-danger text-12">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label" for="brand_name">Brand Name
                                        <small>*</small></label>
                                    <select id="brand_name" name="brand_name"
                                        class="form-control @error('brand_name') is-invalid @enderror"></select>
                                    @error('brand_name')
                                        <span class="text-danger text-12">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label" for="item_name">Item</label>
                                    <input id="item_name" name="item_name" type="text"
                                        class="form-control @error('item_name') is-invalid @enderror">
                                    @error('item_name')
                                        <span class="text-danger text-12">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label" for="unit_price">Unit Price <small>*</small></label>
                                    <input id="unit_price" name="unit_price" type="text"
                                        class="form-control @error('unit_price') is-invalid @enderror">
                                    @error('unit_price')
                                        <span class="text-danger text-12">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label" for="stock_unit">Unit</label>
                                    <select id="stock_unit" name="stock_unit"
                                        class="form-control @error('stock_unit') is-invalid @enderror"></select>
                                    @error('stock_unit')
                                        <span class="text-danger text-12">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label" for="belongs_to">Belongs To (branch name)</label>
                                    <select class="form-control @error('belongs_to') is-invalid @enderror" id="belongs_to"
                                        name="belongs_to"></select>
                                    @error('belongs_to')
                                        <span class="text-danger text-12">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label" for="gst">GST(In %)</label>
                                    <input id="gst" name="gst" type="text"
                                        class="form-control @error('gst') is-invalid @enderror">
                                    @error('gst')
                                        <span class="text-danger text-12">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label" for="toc">Type of Copies</label>
                                    <select class="form-control @error('toc') is-invalid @enderror" id="toc" name="toc">
                                        <option selected="true" disabled=""></option>
                                        <option value="Rental A3 Black Copies">Rental A3 Black Copies</option>
                                        <option value="Rental A4 Black Copies">Rental A4 Black Copies</option>
                                        <option value="Rental A3 Color Copies">Rental A3 Color Copies</option>
                                        <option value="Rental A4 Color Copies">Rental A4 Color Copies</option>
                                        <option value="4C A3 Black Copies">4C A3 Black Copies</option>
                                        <option value="4C A4 Black Copies">4C A4 Black Copies</option>
                                        <option value="4C A3 Color Copies">4C A3 Color Copies</option>
                                        <option value="4C A4 Color Copies">4C A4 Color Copies</option>
                                        <option value="Monthly Rental">Monthly Rental</option>
                                    </select>
                                    @error('toc')
                                        <span class="text-danger text-12">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <a href="/viewservices" type="submit" class="btn btn-outline-primary btn-lg btn-large mr-2">Cancel</a>
                <button type="submit" class="btn btn-primary btn-lg btn-large">Submit</button>
            </div>
        </form>
    </section>
    {{-- add_hsn/sac number --}}
    <div class="modal fade custom-modal" id="add_hsn" tabindex="-1" role="dialog" aria-labelledby="add_hsn"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content success">
                <div class="icon">
                    <span class="fa-stack fa-2x text-success">
                        <i class="fas fa-circle fa-stack-2x"></i>
                        <i class="fas fas fa-plus fa-stack-1x fa-inverse"></i>
                    </span>
                </div>
                <div class="modal-header justify-content-center">
                    <h3 class="modal-title text-capitalize font-weight-bolder mt-3">
                        Add HSN Code
                    </h3>
                </div>
                <form action="addservicehsn" method="POST">
                    @csrf
                    <div class="modal-body pb-0">
                        <div class="row justify-content-center">
                            <div class="col-md-11">
                                <div class="form-group">
                                    <label for="add_hsn" class="col-form-label">Enter New HSN Code</label>
                                    <input type="text" class="form-control @error('add_hsn') is-invalid @enderror"
                                        id="add_hsn" name="add_hsn">
                                    @error('add_hsn')
                                        <span class="text-danger text-12">{{ $message }}</span>
                                    @enderror
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer flex-nowrap">
                        <button type="button" class="btn btn-outline-success btn-block btn-lg mt-0"
                            data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success btn-block btn-lg mt-0">Done</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- add_brand --}}
    <div class="modal fade custom-modal" id="add_brand" tabindex="-1" role="dialog" aria-labelledby="add_brand"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content success">
                <div class="icon">
                    <span class="fa-stack fa-2x text-success">
                        <i class="fas fa-circle fa-stack-2x"></i>
                        <i class="fas fas fa-plus fa-stack-1x fa-inverse"></i>
                    </span>
                </div>
                <div class="modal-header justify-content-center">
                    <h3 class="modal-title text-capitalize font-weight-bolder mt-3">
                        Add Product Brand
                    </h3>
                </div>
                <form action="addbrand" method="POST">
                    @csrf
                    <div class="modal-body pb-0">
                        <div class="row justify-content-center">
                            <div class="col-md-11">
                                <div class="form-group">
                                    <label for="add_brand_name" class="col-form-label">Enter Brand Name</label>
                                    <input type="text" class="form-control @error('add_brand_name') is-invalid @enderror"
                                        id="add_brand_name" name="add_brand_name">
                                    @error('add_brand_name')
                                        <span class="text-danger text-12">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer flex-nowrap">
                        <button type="button" class="btn btn-outline-success btn-block btn-lg mt-0"
                            data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success btn-block btn-lg mt-0">Done</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- add_units --}}
    <div class="modal fade custom-modal" id="add_units" tabindex="-1" role="dialog" aria-labelledby="add_units"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content success">
                <div class="icon">
                    <span class="fa-stack fa-2x text-success">
                        <i class="fas fa-circle fa-stack-2x"></i>
                        <i class="fas fas fa-plus fa-stack-1x fa-inverse"></i>
                    </span>
                </div>
                <div class="modal-header justify-content-center">
                    <h3 class="modal-title text-capitalize font-weight-bolder mt-3">
                        Add Unit
                    </h3>
                </div>
                <form action="addunit" method="POST">
                    @csrf
                    <div class="modal-body pb-0">
                        <div class="row justify-content-center">
                            <div class="col-md-11">
                                <div class="form-group">
                                    <label for="add_units" class="col-form-label">Enter New Unit</label>
                                    <input type="text" class="form-control @error('add_units') is-invalid @enderror"
                                        id="add_units" name="add_units">
                                    @error('add_units')
                                        <span class="text-danger text-12">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer flex-nowrap">
                        <button type="button" class="btn btn-outline-success btn-block btn-lg mt-0"
                            data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success btn-block btn-lg mt-0">Done</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @include('components.alertmodal')
@endsection

@section('script')
    @if (session()->has('message'))
        <script>
            $(document).ready(function() {
                $('#successAlert').modal("show");
            });

        </script>
    @endif
    @if (session()->has('error'))
        <script>
            $(document).ready(function() {
                $('#errorAlert').modal("show");
            });

        </script>
    @endif
    <script>
        $(document).ready(function() {
            getBranch();
            loadBrandname();
            getHsn();
            loadUnit();
            $('#service_hsn').change(function() { //jQuery Change Function
                var opval = $(this).val(); //Get value from select element
                if (opval == "others") { //Compare it and if true
                    $('#add_hsn').modal("show"); //Open Modal
                }
            });
            $('#brand_name').change(function() { //jQuery Change Function
                var opval = $(this).val(); //Get value from select element
                if (opval == "others") { //Compare it and if true
                    $('#add_brand').modal("show"); //Open Modal
                }
            });
            $('#stock_unit').change(function() { //jQuery Change Function
                var opval = $(this).val(); //Get value from select element
                if (opval == "others") { //Compare it and if true
                    $('#add_units').modal("show"); //Open Modal
                }
            });
            $('#belongs_to').change(function() { //jQuery Change Function
                var opval = $(this).val(); //Get value from select element
                if (opval == "others") { //Compare it and if true
                    return window.location.href = "/addorganization";
                }
            });
        });

        function getBranch() {
            return $.ajax({
                url: "/branchname",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    //console.log("branchname", data);
                    $('#belongs_to').empty();
                    $('#belongs_to').append('<option selected="true" disabled></option>');
                    $('#belongs_to').prop('selectedIndex', 0);
                    $.each(data, function(key, value) {
                        $('#belongs_to').append($('<option></option>').attr('value', value
                            .org_branch_name).text(value.org_branch_name));
                    });
                    $('#belongs_to').append('<option value="others">Others</option>');
                },
                error: function(error) {
                    console.log('branchname error:', error);
                }
            });
        }

        function loadBrandname() {
            return $.ajax({
                url: "/brand",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    //console.log("brand_name", data);
                    $('#brand_name').empty();
                    $('#brand_name').append('<option selected="true" disabled></option>');
                    $('#brand_name').prop('selectedIndex', 0);
                    $.each(data, function(key, value) {
                        $('#brand_name').append($('<option></option>').attr('value', value
                            .name).text(value.name));
                    });
                    $('#brand_name').append('<option value="others">Others</option>');
                },
                error: function(error) {
                    console.log('brand_name error:', error);
                }
            });
        }

        function getHsn() {
            return $.ajax({
                url: "/servicehsn",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    //console.log("service_hsn", data);
                    $('#service_hsn').empty();
                    $('#service_hsn').append('<option selected="true" disabled></option>');
                    $('#service_hsn').prop('selectedIndex', 0);
                    $.each(data, function(key, value) {
                        $('#service_hsn').append($('<option></option>').attr(
                            'value',
                            value.Service_HSN_No).text(value.Service_HSN_No));
                    });
                    $('#service_hsn').append('<option value="others">Others</option>');
                },
                error: function(error) {
                    console.log('service_hsn error:', error);
                }
            });
        }

        function loadUnit() {
            return $.ajax({
                url: "/unit",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    //console.log("unit", data);
                    $('#stock_unit').empty();
                    $('#stock_unit').append('<option selected="true" disabled></option>');
                    $('#stock_unit').prop('selectedIndex', 0);
                    $.each(data, function(key, value) {
                        $('#stock_unit').append($('<option></option>').attr(
                            'value',
                            value.unit).text(value.unit));
                    });
                    $('#stock_unit').append('<option value="others">Others</option>');
                },
                error: function(error) {
                    console.log('unit error:', error);
                }
            });
        }

    </script>
@endsection
