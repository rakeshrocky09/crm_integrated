@extends('layouts.app')
@section('mytitle', 'View Customer')
@section('head-links')
    <link href="{{ URL::asset('assets/css/datatables.min.css') }}" rel="stylesheet">
@endsection
@section('content')
    <div class="bg-top bg-gray p-4">
        <div class="d-flex">
            <h5 class="font-weight-bold text-uppercase mb-0">View Customer</h5>
        </div>
    </div>

    <div class="p-4 card-main">
        <section class="my-5">
            <div class="card shadow">
                <div class="card-top">
                    <div class="card-title mb-0">
                        <h5 class="font-weight-bold text-uppercase mb-0"><i class="fas fa-user-alt"></i></h5>
                    </div>
                    <div class="btn-right position-relative float-right">
                        <a href="#" class="btn btn-primary text-uppecase">Print</a>
                        <a href="addcustomer" class="btn btn-primary text-uppecase" data-toggle="tooltip"
                            data-placement="top" title="Add Customer">
                            <i class="fas fa-plus"></i> Add
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table display" id="datatable">
                            <thead class="text-uppercase">
                                <tr>
                                    <th scope="col">ID</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Branch</th>
                                    <th scope="col">Contact person</th>
                                    <th scope="col">GST no</th>
                                    <th scope="col">Section</th>
                                    <th scope="col">Phone no</th>
                                    <th scope="col" class="nosort">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (count($cus) >= 1)
                                    @foreach ($cus as $cus)
                                        <tr>
                                            <td>{{ $cus->cus_const }}{{ $cus->cus_id }}</td>
                                            <td>{{ $cus->customer_name }}</td>
                                            <td>{{ $cus->cus_branch_name }}</td>
                                            <td>{{ $cus->primary_contact_name }}</td>
                                            <td>{{ $cus->cus_gst_percentage }}</td>
                                            <td>{{ $cus->cus_section }}</td>
                                            <td>{{ $cus->cus_phone_no }}</td>
                                            <td class="d-flex flex-nowrap">
                                                <a href="{{url('/view_customer',[$cus->cus_id])}}" class="btn btn-outline-info mr-1">
                                                    <i class="fas fa-user"></i>
                                                </a>
                                                <span data-toggle="modal" data-target="#editcustomer" class="mr-1"
                                                    data-id="{{ $cus->cus_id }}" data-cus_type="{{ $cus->cus_type }}"
                                                    data-name="{{ $cus->customer_name }}"
                                                    data-email="{{ $cus->cus_email_id }}"
                                                    data-phone="{{ $cus->cus_phone_no }}"
                                                    data-landline="{{ $cus->cus_landline }}"
                                                    data-branch="{{ $cus->cus_branch_name }}"
                                                    data-contact_name="{{ $cus->primary_contact_name }}"
                                                    data-contact_email="{{ $cus->primary_contact_email }}"
                                                    data-contact_phone="{{ $cus->primary_contact_phoneno }}"
                                                    data-alter_phone="{{ $cus->cus_alter_phone }}"
                                                    data-lifetime="{{ $cus->cus_lifetime }}"
                                                    data-address="{{ $cus->cus_address }}"
                                                    data-city="{{ $cus->cus_city }}"
                                                    data-state="{{ $cus->cus_state }}"
                                                    data-pincode="{{ $cus->cus_pincode }}"
                                                    data-gst="{{ $cus->cus_gst_percentage }}"
                                                    data-section="{{ $cus->cus_section }}">
                                                    <button type="button" class="btn btn-outline-success"
                                                        data-toggle="tooltip" data-placement="top" title="Edit Product">
                                                        <i class="fas fa-edit"></i>
                                                    </button>
                                                </span>
                                                <span data-toggle="modal" data-target="#deletecustomer"
                                                    data-id="{{ $cus->cus_id }}" data-name="{{ $cus->customer_name }}"
                                                    data-phone="{{ $cus->cus_phone_no }}"
                                                    data-email="{{ $cus->cus_email_id }}">
                                                    <button type="button" class="btn btn-outline-danger"
                                                        data-toggle="tooltip" data-placement="top" title="Delete Product">
                                                        <i class="fas fa-trash-alt"></i>
                                                    </button>
                                                </span>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <td>
                                    <td colspan="8">
                                        Sorry, No records found
                                    </td>
                                    </td>
                                @endif
                            </tbody>
                        </table>
                        <!-- Modal Edit Customer -->
                        <div class="modal fade custom-modal" id="editcustomer" tabindex="-1" role="dialog"
                            aria-labelledby="editcustomer" aria-hidden="true">
                            <div class="modal-dialog modal-xl modal-dialog-centered edit" role="document">
                                <form id="edit_form" method="POST">
                                    @csrf
                                    <div class="modal-content">
                                        <div class="icon">
                                            <span class="fa-stack fa-2x text-success">
                                                <i class="fas fa-circle fa-stack-2x"></i>
                                                <i class="far fa-edit fa-stack-1x fa-inverse"></i>
                                            </span>
                                        </div>
                                        <div class="modal-header justify-content-center">
                                            <h3 class="modal-title text-capitalize font-weight-bolder mt-3"
                                                id="editcustomer">
                                                Edit Customer Details
                                            </h3>
                                        </div>
                                        <div class="modal-body pb-0">
                                            <h6 class="fw-700 text-uppercase">
                                                Installation Report</h6>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label active" for="edit_type">Type
                                                                <small>*</small></label>
                                                            <select id="edit_type" name="edit_type" class="form-control">
                                                                <option disabled selected></option>
                                                                <option value="Organization"> Organization </option>
                                                                <option value="Individual"> Individual </option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label active" for="edit_cname">Customer
                                                                Name/Organization Name</label>
                                                            <input id="edit_cname" name="edit_cname" type="text"
                                                                class="form-control">
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label active" for="edit_email">Email
                                                                ID
                                                            </label>
                                                            <input id="edit_email" name="edit_email" type="email"
                                                                class="form-control">
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label active" for="edit_phone">Phone
                                                                No
                                                                <small>*</small></label>
                                                            <input id="edit_phone" name="edit_phone" type="tel"
                                                                class="form-control">
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label active" for="edit_landline">LandLine
                                                                No</label>
                                                            <input type="text" class="form-control" id="edit_landline"
                                                                name="edit_landline">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label active" for="edit_branch">Belongs to
                                                                branch<small>*</small></label>
                                                            <select id="edit_branch" name="edit_branch"
                                                                class="form-control"></select>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label active"
                                                                for="edit_contactname">Primary Contact Person Name</label>
                                                            <input id="edit_contactname" name="edit_contactname" type="text"
                                                                class="form-control">
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label active"
                                                                for="edit_contactemail">Email ID</label>
                                                            <input id="edit_contactemail" name="edit_contactemail"
                                                                type="text" class="form-control">
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label active"
                                                                for="edit_contactphone">Phone No </label>
                                                            <input id="edit_contactphone" name="edit_contactphone"
                                                                type="tel" class="form-control">
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label active"
                                                                for="edit_contact_altphone">Alternate
                                                                Phone No</label>
                                                            <input type="tel" class="form-control"
                                                                id="edit_contact_altphone" name="edit_contact_altphone">
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label active" for="edit_lifetime">Life
                                                                Time*</label>
                                                            <input id="edit_lifetime" name="edit_lifetime" type="date"
                                                                class="flatpickr form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label active" for="edit_address">Address
                                                                <small>*</small></label>
                                                            <input type="text" class="form-control" id="edit_address"
                                                                name="edit_address">
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label active" for="edit_city">City
                                                                <small>*</small></label>
                                                            <input type="text" class="form-control" id="edit_city"
                                                                name="edit_city">
                                                        </div>
                                                        <div class="form-group col-md-4 is-empty">
                                                            <label class="control-label active" for="edit_gst">GST
                                                                NO.</label>
                                                            <input type="text" class="form-control" id="edit_gst"
                                                                name="edit_gst">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label active" for="edit_state">State
                                                                <small>*</small></label>
                                                            <input type="text" class="form-control" id="edit_state"
                                                                name="edit_state">
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label active" for="edit_postal">Postal
                                                                Code <small>*</small></label>
                                                            <input type="text" class="form-control" id="edit_postal"
                                                                name="edit_postal">
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label active"
                                                                for="edit_section">Section</label>
                                                            <input type="text" class="form-control" id="edit_section"
                                                                name="edit_section">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer flex-nowrap">
                                            <button type="button" class="btn btn-outline-success btn-block btn-lg mt-0"
                                                data-dismiss="modal">Close</button>
                                            <button type="submit"
                                                class="btn btn-success btn-block btn-lg mt-0">Done</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- End Edit Customer -->
                        <!-- Modal Delete Customer -->
                        <div class="modal fade custom-modal" id="deletecustomer" tabindex="-1" role="dialog"
                            aria-labelledby="deletecustomer" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="icon">
                                        <span class="fa-stack fa-2x text-primary">
                                            <i class="fas fa-circle fa-stack-2x"></i>
                                            <i class="fas fa-trash-alt fa-stack-1x fa-inverse"></i>
                                        </span>
                                    </div>
                                    <div class="modal-header justify-content-center">
                                        <h3 class="modal-title text-capitalize font-weight-bolder mt-3" id="deletecustomer">
                                            Delete
                                            Customer?
                                        </h3>
                                    </div>
                                    <div class="modal-body pb-0">
                                        <p class="font-weight-bold text-center">
                                            Are you sure you want to delete the Customer?
                                            <br>
                                            You cannot
                                            undo this operation!
                                        </p>
                                        <div class="row justify-content-center">
                                            <div class="col-md-11">
                                                <form>
                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <label for="delete_customer_id" class="col-form-label">Machine
                                                                Customer Id</label>
                                                            <input type="text" class="form-control" id="delete_customer_id"
                                                                readonly>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="delete_customer_name"
                                                                class="col-form-label">Customer
                                                                Name</label>
                                                            <input type="text" class="form-control"
                                                                id="delete_customer_name" readonly>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="delete_phone" class="col-form-label">Customer Phone
                                                                Number</label>
                                                            <input type="text" class="form-control" id="delete_phone"
                                                                readonly>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="delete_email" class="col-form-label">Customer
                                                                Email-ID</label>
                                                            <input type="text" class="form-control" id="delete_email"
                                                                readonly>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer flex-nowrap" id="delete_btn"></div>
                                </div>
                            </div>
                        </div>
                        <!-- End Delete Customer -->
                    </div>
                </div>
            </div>
        </section>
    </div>
    @include('components.alertmodal')
@endsection
@section('script')
    @if (session()->has('message'))
        <script>
            $(document).ready(function() {
                $('#successAlert').modal("show");
            });

        </script>
    @endif
    @if (session()->has('update'))
        <script>
            $(document).ready(function() {
                $('#updateAlert').modal("show");
            });

        </script>
    @endif
    @if (session()->has('delete'))
        <script>
            $(document).ready(function() {
                $('#deleteAlert').modal("show");
            });

        </script>
    @endif
    @if (session()->has('error'))
        <script>
            $(document).ready(function() {
                $('#errorAlert').modal("show");
            });

        </script>
    @endif
    <script src="{{ URL::asset('assets/js/datatables.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            getBranch();
            $('#datatable').DataTable({
                language: {
                    searchPlaceholder: "Search records",
                    search: "",
                    "lengthMenu": "_MENU_ Entries"
                },
                "lengthMenu": [
                    [8, 16, 32, -1],
                    [8, 16, 32, "All"]
                ],
                "pageLength": 8,
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': ['nosort']
                }]
            });
            $("#editcustomer").on('show.bs.modal', function(event) {
                var cus = $(event.relatedTarget);
                var id = cus.data('id');
                var type = cus.data('cus_type');
                var name = cus.data('name');
                var email = cus.data('email');
                var phone = cus.data('phone');
                var landline = cus.data('landline');
                var branch = cus.data('branch');
                var contactName = cus.data('contact_name');
                var contactEmail = cus.data('contact_email');
                var contactPhone = cus.data('contact_phone');
                var alterPhone = cus.data('alter_phone');
                var lifetime = cus.data('lifetime');
                var address = cus.data('address');
                var city = cus.data('city');
                var state = cus.data('state');
                var pincode = cus.data('pincode');
                var gst = cus.data('gst');
                var section = cus.data('section');
                //console.log('alterPhone', alterPhone);

                $('#edit_form').attr('action', 'updatecus/' + id);
                $("#edit_type").val(type);
                $("#edit_cname").val(name);
                $("#edit_email").val(email);
                $("#edit_phone").val(phone);
                $("#edit_landline").val(landline);
                $("#edit_branch").val(branch);
                $("#edit_contactname").val(contactName);
                $("#edit_contactemail").val(contactEmail);
                $("#edit_contactphone").val(contactPhone);
                $("#edit_contact_altphone").val(alterPhone);
                $("#edit_lifetime").val(lifetime);
                $("#edit_address").val(address);
                $("#edit_city").val(city);
                $("#edit_state").val(state);
                $("#edit_postal").val(pincode);
                $("#edit_gst").val(gst);
                $("#edit_section").val(section);
            });

            $("#deletecustomer").on('show.bs.modal', function(event) {
                var cus = $(event.relatedTarget);
                var id = cus.data('id');
                var name = cus.data('name');
                var phone = cus.data('phone');
                var email = cus.data('email');

                $("#delete_customer_id").val(id);
                $("#delete_customer_name").val(name);
                $("#delete_phone").val(phone);
                $("#delete_email").val(email);

                $('#delete_btn').html(
                    `<button type="button" class="btn btn-outline-primary btn-block btn-lg mt-0" data-dismiss="modal">Close</button><a href='deletecustomer/${id}' class="btn btn-primary btn-block btn-lg mt-0">Delete</a>`
                );
            });
        });

        function getBranch() {
            return $.ajax({
                url: "/branchname",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    //console.log("branchname", data);
                    $('#edit_branch').empty();
                    $('#edit_branch').append('<option selected="true" disabled></option>');
                    $('#edit_branch').prop('selectedIndex', 0);
                    $.each(data, function(key, value) {
                        $('#edit_branch').append($('<option></option>').attr('value', value
                            .org_branch_name).text(value.org_branch_name));
                    });
                    $('#edit_branch').append('<option value="others">Others</option>');
                },
                error: function(error) {
                    console.log('branchname error:', error);
                }
            });
        }

    </script>
@endsection
