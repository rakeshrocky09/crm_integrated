@extends('layouts.app')
@section('mytitle', 'View Expenses')
@section('head-links')
    <link href="{{ URL::asset('assets/css/datatables.min.css') }}" rel="stylesheet">
@endsection
@section('content')
    <div class="bg-top bg-gray p-4">
        <div class="d-flex">
            <h5 class="font-weight-bold text-uppercase mb-0">View Expenses</h5>
        </div>
    </div>

    <div class="p-4 card-main">
        <section class="my-5">
            <div class="card shadow">
                <div class="card-top ">
                    <div class="card-title mb-0">
                        <h5 class="font-weight-bold text-uppercase mb-0"><i class="fas fa-user-alt"></i></h5>
                    </div>
                    <div class="btn-right position-relative float-right">
                        <a href="#" class="btn btn-primary text-uppecase">Print</a>
                        <a href="/addexpenses" class="btn btn-primary text-uppecase" data-toggle="tooltip"
                            data-placement="top" title="Add Expenses">
                            <i class="fas fa-plus"></i> Add
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table" id="datatable">
                            <thead class="text-uppercase">
                                <tr>
                                    <th scope="col">Expense ID</th>
                                    <th scope="col">Date.</th>
                                    <th scope="col">Amount</th>
                                    <th scope="col">Remarks</th>
                                    <th scope="col">Machine Serial No.</th>
                                    <th scope="col">Customer Name</th>
                                    <th scope="col" class="nosort">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (count($product) >= 1)
                                    @foreach ($product as $expenses)
                                        <tr>
                                            <td>{{ $expenses->expenses_id }}</td>
                                            <td>{{ $expenses->Expenses_Date }}</td>
                                            <td>{{ $expenses->Expenses_Amount }}</td>
                                            <td>{{ $expenses->Expenses_Description }}</td>
                                            <td>{{ $expenses->machine_id }}</td>
                                            <td>{{ $expenses->Customer_Name }}</td>
                                            <td class="d-flex flex-nowrap">
                                                <span data-toggle="modal" data-target="#viewexpense" class="mr-1"
                                                    data-id="{{ $expenses->expenses_id }}"
                                                    data-date="{{ $expenses->Expenses_Date }}"
                                                    data-amount="{{ $expenses->Expenses_Amount }}"
                                                    data-remarks="{{ $expenses->Expenses_Description }}"
                                                    data-customer="{{ $expenses->Customer_Name }}"
                                                    data-challan_id="{{ $expenses->challan_id }}"
                                                    data-branch="{{ $expenses->Branch }}"
                                                    data-machine_id="{{ $expenses->machine_id }}">
                                                    <button type="button" class="btn btn-outline-info" data-toggle="tooltip"
                                                        data-placement="top" title="View Expenses">
                                                        <i class="fas fa-user-alt"></i>
                                                    </button>
                                                </span>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="7">
                                            Sorry, No records found
                                        </td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                        <!-- Modal viewexpense -->
                        <div class="modal fade custom-modal" id="viewexpense" tabindex="-1" role="dialog"
                            aria-labelledby="viewexpense" aria-hidden="true">
                            <div class="modal-dialog modal-xl modal-dialog-centered edit" role="document">
                                <div class="modal-content">
                                    <div class="icon">
                                        <span class="fa-stack fa-2x text-success">
                                            <i class="fas fa-circle fa-stack-2x"></i>
                                            <i class="fas fa-table fa-stack-1x fa-inverse"></i>
                                        </span>
                                    </div>
                                    <div class="modal-header justify-content-center">
                                        <h3 class="modal-title text-capitalize font-weight-bolder mt-3" id="viewexpense">
                                            View Expenses
                                        </h3>
                                    </div>
                                    <div class="modal-body pb-0 text-sm">
                                        <div class="row text-sm mb-4">
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">Expense ID</label>
                                                <p class="mb-0"><span id="id"></span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">Expense Date</label>
                                                <p class="mb-0"><span id="date"></span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">Remarks</label>
                                                <p class="mb-0"><span id="remarks"></span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">Amount</label>
                                                <p class="mb-0"><span id="amount"></span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">Customer</label>
                                                <p class="mb-0"><span id="customer"></span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">Challan Id</label>
                                                <p class="mb-0"><span id="challan-id"></span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">Branch Name</label>
                                                <p class="mb-0"><span id="branch"></span></p>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="font-weight-bold">Machine Serial No.</label>
                                                <p class="mb-0"><span id="machine-id"></span></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer justify-content-center">
                                        <button type="button" class="btn btn-success btn-lg btn-large mt-0"
                                            data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End viewexpense -->
                    </div>
                </div>
            </div>
        </section>
    </div>
    @include('components.alertmodal')
@endsection
@section('script')
    @if (session()->has('message'))
        <script>
            $(document).ready(function() {
                $('#successAlert').modal("show");
            });

        </script>
    @endif
    <script src="{{ URL::asset('assets/js/datatables.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#datatable').DataTable({
                language: {
                    searchPlaceholder: "Search records",
                    search: "",
                    "lengthMenu": "_MENU_ Entries"
                },
                "lengthMenu": [
                    [8, 16, 32, -1],
                    [8, 16, 32, "All"]
                ],
                "pageLength": 8,
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': ['nosort']
                }]
            });
            //View Modal
            $("#viewexpense").on('show.bs.modal', function(event) {
                var expense = $(event.relatedTarget);
                var id = expense.data('id');
                var date = expense.data('date');
                var amount = expense.data('amount');
                var remarks = expense.data('remarks');
                var customer = expense.data('customer');
                var challanId = expense.data('challan_id');
                var branch = expense.data('branch');
                var machineId = expense.data('machine_id');
                //push retrieved data into mentioned id 
                $('#id').html(id);
                $('#date').html(date);
                $('#amount').html(amount);
                $('#remarks').html(remarks);
                $('#customer').html(customer);
                $('#challan-id').html(challanId);
                $('#branch').html(branch);
                $('#machine-id').html(machineId);
            });
        });

    </script>
@endsection
