@extends('layouts.app')
@section('mytitle', 'VIEW ENQUIRY')
@section('content')
    <div class="p-4">
        <section class="my-5">
            <div class="card shadow">
                <div class="card-top ">
                    <div class="card-title mb-0">
                        <h6 class="font-weight-bold text-capitalize mb-0">View ENQUIRY</h6>
                    </div>
                </div>
                <div class="card-body pt-0">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <h6 class="font-weight-bold mb-0 text-sm">Enquiry Id</h6>
                                <span class="text-13">{{ $enq[0]->enquiry_id }}</span>
                            </div>
                            <div class="form-group">
                                <h6 class="font-weight-bold mb-0 text-sm">Name</h6>
                                <span class="text-13">{{ $enq[0]->enquiry_cus_name }}</span>
                            </div>
                            <div class="form-group">
                                <h6 class="font-weight-bold mb-0 text-sm">Phone No</h6>
                                <span class="text-13">{{ $enq[0]->enquiry_cus_phone_no }}</span>
                            </div>
                            <div class="form-group">
                                <h6 class="font-weight-bold mb-0 text-sm">Email</h6>
                                <span class="text-13">{{ $enq[0]->email }}</span>
                            </div>
                            <div class="form-group">
                                <h6 class="font-weight-bold mb-0 text-sm">Branch Name</h6>
                                <span class="text-13">{{ $enq[0]->enquiry_branch_name }}</span>
                            </div>
                            <div class="form-group">
                                <h6 class="font-weight-bold mb-0 text-sm">Brand Name</h6>
                                <span class="text-13">{{ $enq[0]->enquiry_brand }}</span>
                            </div>
                            <div class="form-group">
                                <h6 class="font-weight-bold mb-0 text-sm">Address</h6>
                                <span class="text-13">{{ $enq[0]->enquiry_cus_address }}</span>
                            </div>
                            <div class="my-3">
                                <a href="{{ url('viewenquiry') }}" class="btn btn-primary">Cancel</a>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="table-responsive">
                                <table class="table table-bordered table-view">
                                    <thead>
                                        <tr>
                                            <th scope="col">Date</th>
                                            <th scope="col">Category</th>
                                            <th scope="col">Enquiry Throughe</th>
                                            <th scope="col">Details</th>
                                            <th scope="col">Reschedule Date</th>
                                            <th scope="col">Remarks</th>
                                            <th scope="col">Status</th>
                                            <th scope="col">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($enq as $enq)
                                            <tr>
                                                <td>{{ $enq->call_date }}</td>
                                                <td>{{ $enq->enquiry_category }}</td>
                                                <td>{{ $enq->enquiry_through }}</td>
                                                <td>{{ $enq->enquiry_details }}</td>
                                                <td>{{ $enq->enquiry_reschedule_date }}</td>
                                                <td>{{ $enq->enquiry_remarks }}</td>
                                                <td>
                                                    @if ($enq->enquiry_status == 'open')
                                                        <span
                                                            class="badge badge-pill badge-success text-12">{{ $enq->enquiry_status }}</span>
                                                    @else
                                                        <span
                                                            class="badge badge-pill badge-secondary text-12">{{ $enq->enquiry_status }}</span>
                                                    @endif
                                                </td>
                                                <td class="d-flex flex-nowrap">
                                                    <button type="button" class="btn btn-outline-info mr-1"
                                                        data-toggle="modal" data-target="#viewenquiry" title="View Enquiry"
                                                        data-date="{{ $enq->call_date }}"
                                                        data-category="{{ $enq->enquiry_category }}"
                                                        data-details="{{ $enq->enquiry_details }}"
                                                        data-status="{{ $enq->enquiry_status }}"
                                                        data-reschedule_date="{{ $enq->enquiry_reschedule_date }}"
                                                        data-enquiry_through="{{ $enq->enquiry_through }}">
                                                        <i class="fas fa-user-alt"></i>
                                                    </button>
                                                    <button type="button" class="btn btn-outline-success"
                                                        data-toggle="modal" data-target="#editenquiry" title="Edit Enquiry"
                                                        data-id="{{ $enq->enquiry_id }}"
                                                        data-reschedule_date="{{ $enq->enquiry_reschedule_date }}"
                                                        data-remarks="{{ $enq->enquiry_remarks }}"
                                                        data-details="{{ $enq->enquiry_details }}"
                                                        data-status="{{ $enq->enquiry_status }}"
                                                        data-enquiry_through="{{ $enq->enquiry_through }}">
                                                        <i class="fas fa-edit"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <!-- Modal viewenquiry -->
                                <div class="modal fade custom-modal" id="viewenquiry" tabindex="-1" role="dialog"
                                    aria-labelledby="viewenquiry" aria-hidden="true">
                                    <div class="modal-dialog modal-xl modal-dialog-centered edit" role="document">
                                        <div class="modal-content">
                                            <div class="icon">
                                                <span class="fa-stack fa-2x text-success">
                                                    <i class="fas fa-circle fa-stack-2x"></i>
                                                    <i class="fas fa-table fa-stack-1x fa-inverse"></i>
                                                </span>
                                            </div>
                                            <div class="modal-header justify-content-center">
                                                <h3 class="modal-title text-capitalize font-weight-bolder mt-3"
                                                    id="viewenquiry">
                                                    View Enquiry
                                                </h3>
                                            </div>
                                            <div class="modal-body pb-0">
                                                <div class="row text-sm mb-4">
                                                    <div class="form-group col-md-4">
                                                        <label class="font-weight-bold">Date</label>
                                                        <p class="mb-0" id="date"></p>
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label class="font-weight-bold">Category</label>
                                                        <p class="mb-0" id="category"></p>
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label class="font-weight-bold">Details</label>
                                                        <p class="mb-0" id="details"></p>
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label class="font-weight-bold">Ticket Status</label>
                                                        <p class="mb-0" id="status"></p>
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label class="font-weight-bold">Reschedule Date</label>
                                                        <p class="mb-0" id="reschedule_date"></p>
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label class="font-weight-bold">Enquiry Through</label>
                                                        <p class="mb-0" id="enquiry_through"></p>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="modal-footer justify-content-center">
                                                <button type="button" class="btn btn-success btn-lg btn-large mt-0"
                                                    data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End viewticket -->
                                <!-- Modal Edit Products -->
                                <div class="modal fade custom-modal" id="editenquiry" tabindex="-1" role="dialog"
                                    aria-labelledby="editenquiry" aria-hidden="true">
                                    <div class="modal-dialog modal-xl modal-dialog-centered edit" role="document">
                                        <form id="edit_form" method="POST">
                                            <div class="modal-content">
                                                @csrf
                                                <div class="icon">
                                                    <span class="fa-stack fa-2x text-success">
                                                        <i class="fas fa-circle fa-stack-2x"></i>
                                                        <i class="far fa-edit fa-stack-1x fa-inverse"></i>
                                                    </span>
                                                </div>
                                                <div class="modal-header justify-content-center">
                                                    <h3 class="modal-title text-capitalize font-weight-bolder mt-3"
                                                        id="editenquiry">
                                                        Edit Enquiry
                                                    </h3>
                                                </div>
                                                <div class="modal-body pb-0">
                                                    <div class="row">
                                                        <div class="form-group col-md-4">
                                                            <label for="edit_reschedule_date"
                                                                class="mb-0 control-label active">Last
                                                                Rescheduled Date</label>
                                                            <input id="edit_reschedule_date" name="edit_reschedule_date"
                                                                type="text" class="form-control" readonly>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label active"
                                                                for="edit_remarks">Remarks</label>
                                                            <input type="text" class="form-control" id="edit_remarks"
                                                                name="edit_remarks">
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label active" for="edit_details">Details
                                                                <small>*</small></label>
                                                            <textarea class="form-control mt-1" id="edit_details"
                                                                name="edit_details" rows="1"></textarea>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label for="edit_status"
                                                                class="mb-0 control-label active">Status
                                                                <small>*</small></label>
                                                            <select class="form-control" id="edit_status"
                                                                name="edit_status">
                                                                <option value="open">Open</option>
                                                                <option value="close">Close</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label for="edit_next_reschedule_date"
                                                                class="mb-0 control-label active">Next
                                                                Reschedule Date</label>
                                                            <input id="edit_next_reschedule_date"
                                                                name="edit_next_reschedule_date" type="date"
                                                                placeholder="yyyy-mm-dd" class="flatpickr form-control">
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label for="edit_enquiry_through"
                                                                class="mb-0 control-label active">Enquiry
                                                                Through</label>
                                                            <select class="form-control" id="edit_enquiry_through"
                                                                name="edit_enquiry_through"></select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer flex-nowrap">
                                                    <button type="button"
                                                        class="btn btn-outline-success btn-block btn-lg mt-0"
                                                        data-dismiss="modal">Close</button>
                                                    <button type="submit"
                                                        class="btn btn-success btn-block btn-lg mt-0">Done</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <!-- End Edit Products -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            loadEnquiryThrough()
            //View Modal
            $("#viewenquiry").on('show.bs.modal', function(event) {
                var ticket = $(event.relatedTarget);
                var date = ticket.data('date');
                var category = ticket.data('category');
                var details = ticket.data('details');
                var status = ticket.data('status');
                var reschedule_date = ticket.data('reschedule_date');
                var enquiry_through = ticket.data('enquiry_through');
                //console.log('colorA4', colorA4);

                //push retrieved data into mentioned id 
                $('#date').html(date);
                $('#category').html(category);
                $('#details').html(details);
                $('#status').html(status);
                $('#reschedule_date').html(reschedule_date);
                $('#enquiry_through').html(enquiry_through);
            });

            $("#editenquiry").on('show.bs.modal', function(event) {
                var ticket = $(event.relatedTarget);
                var id = ticket.data('id');
                var reschedule_date = ticket.data('reschedule_date');
                var remarks = ticket.data('remarks');
                var details = ticket.data('details');
                var status = ticket.data('status');
                var enquiry_through = ticket.data('enquiry_through');
                //console.log('details', details);

                //push retrieved data into mentioned id 
                var action = `{{ url('updateenq/${id}') }}`;
                $('#edit_form').attr('action', action);
                $('#edit_reschedule_date').val(reschedule_date);
                $('#edit_remarks').val(remarks);
                $('#edit_details').val(details);
                $('#edit_status').val(status);
                $('#edit_enquiry_through').val(enquiry_through);
            });

        });

        function loadEnquiryThrough() {
            return $.ajax({
                url: "/enqthrough",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    //console.log("enqthrough", data);
                    $('#edit_enquiry_through').empty();
                    $('#edit_enquiry_through').append('<option selected="true" disabled></option>');
                    $('#edit_enquiry_through').prop('selectedIndex', 0);
                    $.each(data, function(key, value) {
                        $('#edit_enquiry_through').append($('<option></option>').attr(
                            'value',
                            value.name).text(value.name));
                    });
                    $('#edit_enquiry_through').append('<option value="others">Others</option>');
                },
                error: function(error) {
                    console.log('branchname error:', error);
                }
            });
        }

    </script>
@endsection
