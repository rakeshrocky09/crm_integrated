<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{ URL::asset('assets/img/logo-sm.png') }}" type="image/x-icon">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>LOGIN</title>


    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css"
        integrity="sha512-HK5fgLBL+xu6dm/Ii3z4xhlSUyZgTT9tuc/hSrtw6uzJOvgRr2a9jyxxT1ely+B+xFAmJKVSTbpM/CuL7qxO8w=="
        crossorigin="anonymous" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ URL::asset('assets/css/app.css') }}" rel="stylesheet">
    <style>
        html,
        body {
            height: 100%;
            width: 100%;
        }

    </style>

</head>

<body>
    <div id="app" class="w-100 h-100">
        <div class="login_main">
            <div class="login-container" id="container">
                <div class="form-container sign-up-container">
                    <!-- Sign Up form code goes here -->
                    <form action="{{ url('/loginsubmit') }}" method="post">
                        @csrf
                        <h1>Create Account</h1>
                        <div class="input-grp">
                            <span class="pl-2">
                                <i class="far fa-user"></i>
                            </span>
                            <input type="text" placeholder="Name" />
                        </div>
                        <div class="input-grp">
                            <span class="pl-2">
                                <i class="far fa-envelope"></i>
                            </span>
                            <input type="email" placeholder="Email" />
                        </div>
                        <div class="input-grp">
                            <span class="pl-2">
                                <i class="fas fa-lock"></i>
                            </span>
                            <input type="password" placeholder="Password" />
                        </div>
                        <button class="btn btn-primary mt-4 btn-large">Sign Up</button>
                    </form>
                </div>
                <div class="form-container sign-in-container">
                    <!-- Sign In form code goes here -->
                    <form action="{{ url('/loginsubmit') }}" method="post">
                        @csrf
                        <h1>Sign in</h1>
                        <div class="input-grp">
                            <span class="pl-2">
                                <i class="far fa-user"></i>
                            </span>
                            <input type="text" id="username" name="username" placeholder="Username" />
                        </div>
                        <div class="input-grp">
                            <span class="pl-2">
                                <i class="fas fa-lock"></i>
                            </span>
                            <input type="password" id="password" name="password" placeholder="Password" />
                        </div>
                        <a href="#">Forgot your password?</a>
                        <button class="btn btn-primary mt-4 btn-large">Sign In</button>
                    </form>
                </div>
                <div class="overlay-container">
                    <!-- The overlay code goes here -->
                    <div class="overlay">
                        <div class="overlay-panel overlay-left">
                            <h1>Welcome Back!</h1>
                            <p>
                                To keep connected with us please login with your personal info
                            </p>
                            <button class="btn btn-outline-light btn-large" id="signIn">Sign In</button>
                        </div>
                        <div class="overlay-panel overlay-right">
                            <h1>Hello, Friend!</h1>
                            <p>Enter your personal details and start journey with us</p>
                            <button class="btn btn-outline-light btn-large" id="signUp">Sign Up</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Scripts -->
    <script src="{{ URL::asset('assets/js/app.js') }}"></script>
    <script>
        //login page
        const signUpButton = document.getElementById("signUp");
        const signInButton = document.getElementById("signIn");
        const container = document.getElementById("container");

        signUpButton.addEventListener("click", () => {
            container.classList.add("right-panel-active");
        });

        signInButton.addEventListener("click", () => {
            container.classList.remove("right-panel-active");
        });
    </script>
</body>

</html>
