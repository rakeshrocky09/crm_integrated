require("./bootstrap");

//sidebar menu toggle
$(".switch").on("click", function (e) {
    $("#app").toggleClass("sidebar-toggled"); //you can list several class names
    e.preventDefault();
});
//end of sidebar menu toggle

/* sidebar-dropdown */
$(".sidebar-dropdown .sidebar-submenu").hide();
$(".sidebar-dropdown a").on("click", function () {
    $(this)
        .parent(".sidebar-dropdown")
        .children(".sidebar-submenu")
        .slideToggle("100");
    $(this).toggleClass("show");
});
/* end of sidebar-dropdown */

// Add active class to the current nav menu (highlight it)
$(document).ready(function () {
    $(".menu-item.active").removeClass("active");
    var current = location.pathname;
    $(".menu-item").each(function () {
        var $this = $(this);
        // if the current path is like this link, make it active
        if ($this.attr("href").indexOf(current) !== -1) {
            $this.addClass("active");
        }
    });

    /* $('a[href="' + location.pathname + '"]')
        .closest(".menu-item")
        .addClass("active"); */
    /*  $(".menu-item.active").attr("aria-expanded", "true"); */
});
$(document).ready(function () {
    $(".submenu-item.active").removeClass("active");
    var current = location.pathname;
    $(".submenu-item").each(function () {
        var $this = $(this);
        // if the current path is like this link, make it active
        if ($this.attr("href").indexOf(current) !== -1) {
            $this
                .addClass("active")
                .addClass("active")
                .parents(".sidebar-submenu")
                .addClass("menu-open")
                .parents(".menu-list.dropdown")
                .addClass("active");
        }
    });

   /*  $('a[href="' + location.pathname + '"]')
        .closest(".submenu-item")
        .addClass("active")
        .parents(".sidebar-submenu")
        .addClass("menu-open")
        .parents(".menu-list.dropdown")
        .addClass("active"); */
});
//end

/* $(".sidebar-dropdowns > a").click(function () {
    $(".sidebar-submenu").slideUp(200);
    if ($(this).parent().hasClass("active")) {
        $(".sidebar-dropdowns").removeClass("active");
        $(this).parent().removeClass("active");
    } else {
        $(".sidebar-dropdowns").removeClass("active");
        $(this).next(".sidebar-submenu").slideDown(200);
        $(this).parent().addClass("active");
    }
}); */

//input label class toggle
jQuery(document).ready(function ($) {
    $(".form-control")
        .on("focus", function (e) {
            $(this).parent().addClass("focused");
        })
        .blur(function () {
            $(this).parent().removeClass("focused");
        });
});

$(".form-control").on("change", function () {
    var self = $(this),
        label = self.siblings("label");

    if (self.val() != "") {
        label.addClass("active");
    } /* else {
        label.removeClass("active");
    } */
});

function checkForInput(element) {
    // element is passed to the function ^

    const $label = $(element).siblings("label");

    if ($(element).val().length > 0) {
        $label.addClass("active");
    } /*  else {
      $label.removeClass('active');
    } */
}

// The lines below are executed on page load
$("input.form-control").each(function () {
    checkForInput(this);
});

// The lines below (inside) are executed on change & keyup
$("input.form-control").on("change keyup", function () {
    checkForInput(this);
});

$(function () {
    $('[data-toggle="tooltip"]').tooltip();
});

/* sidebar dropdown */
$.sidebarMenu = function (menu) {
    var animationSpeed = 300,
        subMenuSelector = ".sidebar-submenu";

    $(menu).on("click", "li a", function (e) {
        var $this = $(this);
        var checkElement = $this.next();

        if (checkElement.is(subMenuSelector) && checkElement.is(":visible")) {
            checkElement.slideUp(animationSpeed, function () {
                checkElement.removeClass("menu-open");
            });
            checkElement.parent("li").removeClass("active");
        }

        //If the menu is not visible
        else if (
            checkElement.is(subMenuSelector) &&
            !checkElement.is(":visible")
        ) {
            //Get the parent menu
            var parent = $this.parents("ul").first();
            //Close all open menus within the parent
            var ul = parent.find("ul:visible").slideUp(animationSpeed);
            //Remove the menu-open class from the parent
            ul.removeClass("menu-open");
            //Get the parent li
            var parent_li = $this.parent("li");

            //Open the target menu and add the menu-open class
            checkElement.slideDown(animationSpeed, function () {
                //Add the class active to the parent li
                checkElement.addClass("menu-open");
                parent.find("li.active").removeClass("active");
                parent_li.addClass("active");
            });
        }
        //if this isn't a link, prevent the page from being redirected
        if (checkElement.is(subMenuSelector)) {
            e.preventDefault();
        }
    });
};

$(document).on("click", ".browse", function () {
    var file = $(this).parents().find(".file");
    file.trigger("click");
});
$('input[type="file"]').change(function (e) {
    var fileName = e.target.files[0].name;
    $("#org_img").val(fileName);

    var reader = new FileReader();
    reader.onload = function (e) {
        // get loaded data and render thumbnail.
        document.getElementById("preview").src = e.target.result;
    };
    // read the image file as a data URL.
    reader.readAsDataURL(this.files[0]);
});