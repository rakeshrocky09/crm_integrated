<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;
use App\Http\Controllers\AuthAPIController;
use App\Http\Controllers\UsersecurityController;
use App\Http\Controllers\BranchController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\SpareController;
use App\Http\Controllers\ConsumableController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\CustomerProductController;
use App\Http\Controllers\ServicesController;
use App\Http\Controllers\ExpenseController;
use App\Http\Controllers\PaymentFollowUpController;
use App\Http\Controllers\PMSController;
use App\Http\Controllers\EnquiryController;
use App\Http\Controllers\ContractController;

use App\Http\Controllers\ReceiptController;
use App\Http\Controllers\TicketsController;
use App\Http\Controllers\InvoiceController;
use App\Http\Controllers\ReportController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('clear-cache', function () {
	Artisan::call('config:cache');
	Artisan::call('cache:clear');
	Artisan::call('route:clear');
	Artisan::call('view:clear');
	return "Cache is cleared";
});
Route::get('migrate', function () {
	Artisan::call('migrate');
	return "Migrated";
});

Route::post('adduser', [UsersecurityController::class, 'addUserDetails']);

Route::post('login', [AuthAPIController::class, 'login']);
Route::group(['middleware' => 'auth:api'], function () {
	Route::post('logout', [AuthAPIController::class, 'logout']);

	Route::post('branchs', [BranchController::class, 'getBranchs']);

	Route::post('addbranch', [BranchController::class, 'AddBranch']);
	Route::post('updatebranch', [BranchController::class, 'UpdateBranch']);
	Route::post('deletebranch', [BranchController::class, 'DeleteBranch']);
	Route::post('branch', [BranchController::class, 'getBranch']);
	Route::post('branchname', [BranchController::class, 'getAllBranchNames']);
	Route::post('cusname', [BranchController::class, 'getCustomerName']);
	Route::post('cusdetail', [BranchController::class, 'getcustomerdetail']);
	Route::post('cusinfo', [BranchController::class, 'getcustomerinfo']);
	Route::post('cusnum', [BranchController::class, 'getCustomerWithNum']);

	Route::post('cuscount', [DashboardController::class, 'getCustomerCount']);
	Route::post('activecontract', [DashboardController::class, 'getActiveContract']);
	Route::post('unresolveticket', [DashboardController::class, 'getUnresolvedTicket']);
	Route::post('resolveticket', [DashboardController::class, 'getResolvedTicket']);
	Route::post('pmsdue', [DashboardController::class, 'getPmsOverdue']);
	Route::post('contract', [DashboardController::class, 'getContract']);
	Route::post('enquiry', [DashboardController::class, 'getEnquiryCalls']);
	Route::post('follow', [DashboardController::class, 'getFollowupCount']);
	Route::post('ticket', [DashboardController::class, 'getTicketPriority']);
	Route::post('contractexp', [DashboardController::class, 'getContractExpiry']);
	Route::post('pmsschd', [DashboardController::class, 'getPmsNextSchedule']);
	Route::post('pmsdly', [DashboardController::class, 'getPmsDelaySchedule']);
	Route::post('payment', [DashboardController::class, 'getPayment']);

	Route::post('products', [ProductController::class, 'GetProducts']);
	Route::post('addproduct', [ProductController::class, 'AddProducts']);
	Route::post('updateproduct', [ProductController::class, 'UpdateProduct']);
	Route::post('deleteproduct', [ProductController::class, 'DeleteProduct']);
	Route::get('product', [ProductController::class, 'GetProduct']);
	Route::get('brand', [ProductController::class, 'getBrands']);
	Route::get('unit', [ProductController::class, 'getUnits']);
	Route::get('prdhsn', [ProductController::class, 'getProductHSN']);
	Route::get('prdcategory', [ProductController::class, 'getCategory']);
	Route::post('prdname', [ProductController::class, 'getProductname']);
	Route::post('prdmodelname', [ProductController::class, 'getModelname']);
	Route::post('addbrand', [ProductController::class, 'AddBrand']);
	Route::post('addunit', [ProductController::class, 'AddUnit']);
	Route::post('addprdhsn', [ProductController::class, 'AddProductHSN']);
	Route::post('addprdcategory', [ProductController::class, 'AddProductCategory']);

	Route::post('spares', [SpareController::class, 'GetSpares']);
	Route::post('addspare', [SpareController::class, 'AddSpare']);
	Route::post('updatespare', [SpareController::class, 'UpdateSpare']);
	Route::post('deletespare', [SpareController::class, 'DeleteSpare']);
	Route::get('spare', [SpareController::class, 'GetSpare']);
	Route::get('sparehsn', [SpareController::class, 'getSpareHSN']);
	Route::post('addsparehsn', [SpareController::class, 'AddSpareHSN']);

	Route::post('services', [ServicesController::class, 'GetServices']);
	Route::post('addservice', [ServicesController::class, 'AddService']);
	Route::post('updateservice', [ServicesController::class, 'UpdateService']);
	Route::post('deleteservice', [ServicesController::class, 'DeleteService']);
	Route::get('service', [ServicesController::class, 'GetService']);
	Route::get('servicehsn', [ServicesController::class, 'GetServiceHSN']);
	Route::post('addservicehsn', [ServicesController::class, 'AddSpareHSN']);

	Route::post('consumables', [ConsumableController::class, 'GetConsumables']);
	Route::post('addconsumable', [ConsumableController::class, 'AddConsumable']);
	Route::post('updateconsumable', [ConsumableController::class, 'UpdateConsumable']);
	Route::post('deleteconsumable', [ConsumableController::class, 'DeleteConsumable']);
	Route::get('consumable', [ConsumableController::class, 'GetConsumable']);
	Route::get('consumablehsn', [ConsumableController::class, 'getConsumableHSN']);
	Route::post('addconsumablehsn', [ConsumableController::class, 'AddConsumableHSN']);

	Route::post('customers', [CustomerController::class, 'getCustomers']);
	Route::post('addcus', [CustomerController::class, 'AddCustomer']);
	Route::post('updatecus', [CustomerController::class, 'UpdateCustomer']);
	Route::post('delcus', [CustomerController::class, 'DeleteCustomer']);
	Route::post('cusbasic', [CustomerController::class, 'GetCustomerBasic']);
	Route::post('cuslast', [CustomerController::class, 'GetLastCustomer']);
	Route::post('cusinv', [CustomerController::class, 'GetInvoice']);
	Route::post('custicket', [CustomerController::class, 'GetTicket']);

	Route::post('getcusticket', [CustomerController::class, 'GetCustomerTicket']);

	Route::post('cusprod', [CustomerController::class, 'GetCustomerProduct']);
	Route::post('cusspare', [CustomerController::class, 'GetCustomerSpare']);
	Route::post('cuscons', [CustomerController::class, 'GetCustomerConsumable']);
	Route::post('cusserv', [CustomerController::class, 'GetCustomerService']);
	Route::post('delspare', [CustomerController::class, 'GetDeliverySpare']);
	Route::post('delcons', [CustomerController::class, 'GetDeliveryConsumable']);
	Route::post('delserv', [CustomerController::class, 'GetDeliveryService']);
	Route::post('cusdel', [CustomerController::class, 'GetDelivery']);

	//employee
	Route::post('emps', [EmployeeController::class, 'GetEmployees']);
	Route::post('empdetail', [EmployeeController::class, 'GetEmployee']);
	Route::post('roles', [EmployeeController::class, 'GetRoles']);
	Route::post('tech', [EmployeeController::class, 'GetTechnicians']);
	Route::post('empname', [EmployeeController::class, 'GetEmpName']);
	Route::post('addemp', [EmployeeController::class, 'AddEmp']);
	Route::post('addrole', [EmployeeController::class, 'AddRole']);
	Route::post('updateemp', [EmployeeController::class, 'UpdateEmployee']);
	Route::post('deleteemp', [EmployeeController::class, 'DeleteEmployee']);

	Route::post('addcusprod', [CustomerProductController::class, 'AddCusProd']);
	Route::post('addcusproduct', [CustomerProductController::class, 'AddProduct']);
	Route::post('cusproduct', [CustomerProductController::class, 'GetCusProduct']);

	Route::post('updatecusproduct', [CustomerProductController::class, 'UpdateCusProductDetail']);

	Route::post('getcusprod', [CustomerProductController::class, 'GetCusPrd']);
	Route::post('updateins', [CustomerProductController::class, 'UpdateInstalled']);
	Route::post('delcusprod', [CustomerProductController::class, 'DeleteCusProduct']);

	Route::post('expenses', [ExpenseController::class, 'GetExpenses']);
	Route::post('exptype', [ExpenseController::class, 'GetExpType']);
	Route::post('addexp', [ExpenseController::class, 'AddExpense']);
	Route::post('addexptype', [ExpenseController::class, 'AddExpType']);
	Route::post('expense', [ExpenseController::class, 'GetExpense']);

	Route::post('folloups', [PaymentFollowUpController::class, 'GetPaymentFollowups']);
	Route::post('folloup', [PaymentFollowUpController::class, 'GetPaymentFollowup']);
	Route::post('addfolloup', [PaymentFollowUpController::class, 'AddPaymentFollowUp']);

	Route::post('pmss', [PMSController::class, 'GetPMSs']);
	Route::post('pms', [PMSController::class, 'GetPMS']);
	Route::post('updatepms', [PMSController::class, 'UpdatePMS']);

	Route::post('enquirys', [EnquiryController::class, 'GetEnquirys']);
	Route::post('addenquiry', [EnquiryController::class, 'AddEnquiry']);
	Route::post('addenqcategory', [EnquiryController::class, 'AddCategory']);
	Route::post('enqcategory', [EnquiryController::class, 'getCategory']);
	Route::post('addenqthrough', [EnquiryController::class, 'AddEnqThrough']);
	Route::post('enqthrough', [EnquiryController::class, 'getEnqThrough']);
	Route::post('enquiry', [EnquiryController::class, 'GetEnquiry']);
	Route::post('updateenq', [EnquiryController::class, 'UpdateEnquiry']);

	Route::post('contracts', [ContractController::class, 'GetContracts']);
	Route::post('addcontract', [ContractController::class, 'AddContract']);
	Route::post('closecontract', [ContractController::class, 'CloseContract']);
	Route::post('deletecontract', [ContractController::class, 'DeleteContract']);

	Route::post('contractview', [ContractController::class, 'GetContractView']);
	Route::post('contractdetails', [ContractController::class, 'GetContractDetails']);


	Route::post('addreceipt', [ReceiptController::class, 'AddReceipt']);
	Route::post('receipts', [ReceiptController::class, 'getAllReceipts']);
	Route::post('receiptdetails', [ReceiptController::class, 'getReceiptDetails']);


	Route::post('addticket', [TicketsController::class, 'AddTickets']);		
	Route::post('tickets', [TicketsController::class, 'getAllTickets']);
	Route::post('addcustickets', [TicketsController::class, 'addCustomerTickets']);
	Route::post('updateticket', [TicketsController::class, 'UpdateTicket']);
	Route::post('deleteticket', [TicketsController::class, 'DeleteTicket']);
	Route::post('tickethistory', [TicketsController::class, 'GetTicketHistory']);

	Route::post('ticketdefectcode', [TicketsController::class, 'GetTicketDefectCode']);
	Route::post('addticketdefectcode', [TicketsController::class, 'AddTicketDefectCode']);
	Route::post('tickettype', [TicketsController::class, 'GetTicketType']);
	Route::post('addtickettype', [TicketsController::class, 'AddTicketType']);

	Route::post('invoice', [InvoiceController::class, 'getAllInvoices']);
	Route::post('getchallan', [InvoiceController::class, 'getAllChallan']);
	Route::post('addchallan', [InvoiceController::class, 'AddChallan']);
	//invoices	
	Route::post('addinvoice', [InvoiceController::class, 'AddInvoice']);
	Route::post('invoices', [InvoiceController::class, 'getInvoices']);
	Route::post('editinvoices/{id}', [InvoiceController::class, 'editInvoice']);
	Route::post('deleteinvoice', [InvoiceController::class, 'DeleteInvoice']);
	Route::post('invoicedetail', [InvoiceController::class, 'getInvoiceDetails']);
	Route::post('invoicetotal', [InvoiceController::class, 'getInvoiceTotal']);
	Route::post('branchdetails', [BranchController::class, 'getBranchDetails']);
	Route::post('cusdetails', [BranchController::class, 'getCustomerDetails']);

	//Delivery challan
	Route::post('deliverychallan', [InvoiceController::class, 'getAlldc']);
	Route::post('deletedc', [InvoiceController::class, 'DeleteChallan']);
	Route::post('dcdetails', [InvoiceController::class, 'getChallanDetails']);
	Route::post('dcitems', [InvoiceController::class, 'getChallanItemDetails']);
	Route::post('dctotal', [InvoiceController::class, 'getChallanTotal']);

	Route::post('loaddc', [InvoiceController::class, 'editChallan']);
	
	//Report
	Route::post('report', [ReportController::class, 'getReportData']);

});
