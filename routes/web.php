<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;
use App\Http\Controllers\AuthController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|<h2>Hello {{session('username')}}</h2>
*/

Route::get('/clear-cache', function () {
	Artisan::call('cache:clear');
	Artisan::call('route:clear');
	Artisan::call('view:clear');
	Artisan::call('config:cache');
	return "Cache is cleared";
});

Route::view("login", 'login');

Route::post('loginsubmit', [AuthController::class, 'login']);
Route::get('/logout', function () {
	if (session()->has('username')) {
		session()->pull('username');
	}
	return redirect('login');
});
Route::get('/', function () {
	return redirect('login');
});

Route::get('dashboard', 'DashboardController@showDashboard');

//Organization
Route::get('vieworganization', 'BranchController@getBranchs');
Route::get('addorganization', function () {
	return view('addorganization');
});
Route::post('addorganizations', 'BranchController@AddBranch');
Route::post('updateorg/{id}', 'BranchController@UpdateBranch');
Route::get('deleteorg/{id}', 'BranchController@DeleteBranch');

//employee
Route::get('viewuser', 'EmployeeController@GetEmployees');
Route::get('loademployees', 'EmployeeController@GetAllEmployees');
Route::get('adduser', function () {
	return view('adduser');
});
Route::post('addemp', 'EmployeeController@AddEmp');
Route::post('addrole', 'EmployeeController@AddRole');
//Route::post('updateemp/{id}', 'BranchController@UpdateBranch');
//Route::get('empdetails', 'EmployeeController@GetEmployee');
Route::get('emproles', 'EmployeeController@GetRoles');
Route::get('branchname', 'BranchController@getAllBranchNames');

Route::get('branchinfo/{id}', 'BranchController@getBranch');
Route::get('cuslist/{name}', 'BranchController@getCustomerName');

Route::post('updateemp', 'EmployeeController@UpdateEmployee');
Route::get('deleteuser/{id}', 'EmployeeController@DeleteEmployee');
Route::get('technicians', 'EmployeeController@GetTechnicians');

//Inventory Management
Route::post('addunit', 'ProductController@AddUnit');
Route::post('addbrand', 'ProductController@AddBrand');
Route::get('brand', 'ProductController@getBrands');
Route::get('unit', 'ProductController@getUnits');
Route::get('prdmodelname', 'ProductController@getModelname');
Route::get('prdname', 'ProductController@getProductname');

//product
Route::get('viewproduct', 'ProductController@GetProducts');
Route::get('addproduct', function () {
	return view('addproduct');
});
Route::post('addproducts', 'ProductController@AddProduct');
Route::post('addprdhsn', 'ProductController@AddProductHSN');
Route::post('addprdcategory', 'ProductController@AddProductCategory');
Route::post('updateproduct/{id}', 'ProductController@UpdateProduct');
Route::get('prdhsn', 'ProductController@getProductHSN');
Route::get('prdcategory', 'ProductController@getCategory');
Route::get('deleteproduct/{id}', 'ProductController@DeleteProduct');

//spares
Route::get('viewspares', 'SpareController@GetSpares');
Route::get('loadspares', 'SpareController@GetAllSpares');
Route::get('addspares', function () {
	return view('addspares');
});
Route::post('addspare', 'SpareController@AddSpare');
Route::post('addsparehsn', 'SpareController@AddSpareHSN');
Route::post('addsparebrand', 'SpareController@AddSpareBrand');
Route::post('updatespare/{id}', 'SpareController@UpdateSpare');
Route::get('sparehsn', 'SpareController@getSpareHSN');
Route::get('sparebrand', 'SpareController@getSpareBrand');
Route::get('deletespare/{id}', 'SpareController@DeleteSpare');

//consumable
Route::get('viewconsumable', 'ConsumableController@GetConsumables');
Route::get('loadconsumable', 'ConsumableController@GetAllConsumables');
Route::get('addconsumable', function () {
	return view('addconsumable');
});
Route::post('addconsumables', 'ConsumableController@AddConsumable');
Route::post('addconsumablehsn', 'ConsumableController@AddConsumableHSN');
Route::post('updateconsumable/{id}', 'ConsumableController@UpdateConsumable');
Route::get('consumablehsn', 'ConsumableController@GetConsumableHSN');
Route::get('deleteconsumable/{id}', 'ConsumableController@DeleteConsumable');
//services
Route::get('viewservices', 'ServicesController@GetServices');
Route::get('loadservices', 'ServicesController@GetAllServices');
Route::get('addservices', function () {
	return view('addservices');
});
Route::post('addservice', 'ServicesController@AddService');
Route::post('addservicehsn', 'ServicesController@AddServiceHSN');
Route::post('updateservice/{id}', 'ServicesController@UpdateService');
Route::get('servicehsn', 'ServicesController@GetServiceHSN');
Route::get('deleteservice/{id}', 'ServicesController@DeleteService');

//Customer
Route::get('addcustomer', function () {
	return view('addcustomer');
});
Route::post('addcus', 'CustomerController@AddCustomer');

Route::get('view_customer/{id}', 'CustomerController@GetCustomerDetails');

Route::get('loadproducts', 'ProductController@GetAllProducts');
Route::post('addcusprod', 'CustomerProductController@AddCusProd');

Route::post('updatecusprod/{cusid}/{prodid}/{serial}', 'CustomerProductController@UpdateCusProductDetail');
//Route::post('customerticket/{cusid}/{prodid}/{serial}', 'CustomerProductController@UpdateCusProductDetail');
/* Route::get('customerticket/{cusid}/{prodid}/{serial}', function () {
	return view('customerticket');
}); */
Route::get('customerticket/{cusid}/{prodid}/{serial}', 'CustomerController@GetCustomerTicket');
Route::post('addcustickets', 'TicketsController@addCustomerTickets');

//Route::get('getcusprod/{id}', 'CustomerProductController@GetCusPrd');
Route::post('delcusprod/{id}', 'CustomerProductController@DeleteCusProduct');

Route::get('viewcustomer', 'CustomerController@getCustomers');
Route::post('updatecus/{id}', 'CustomerController@UpdateCustomer');
Route::get('deletecustomer/{id}', 'CustomerController@DeleteCustomer');

//Contract
Route::post('addcontract', 'ContractController@AddContract');
Route::get('addcontract/{id}', 'ContractController@AddContractView');
Route::post('addownership', 'CustomerController@AddOwnership');

Route::get('viewcontract', 'ContractController@GetContracts');
Route::get('closecontract/{id}', 'ContractController@CloseContract');
Route::get('deletecontract/{id}', 'ContractController@DeleteContract');

Route::get('viewcontracts/{id}', 'ContractController@GetContractDetails');
Route::get('view_contract/{id}', 'ContractController@GetContractView');
Route::get('addexpenses/{id}', function () {
	return view('addexpenses');
});
Route::get('edit_contract/{id}', 'ContractController@editContractView');
Route::post('updatecontract/{id}', 'ContractController@UpdateContract');


//Enquiry
Route::get('viewenquiry', 'EnquiryController@GetEnquirys');
Route::get('addenquiry', function () {
	return view('addenquiry');
});
Route::post('addenq', 'EnquiryController@AddEnquiry');
Route::post('addenqcategory', 'EnquiryController@AddCategory');
Route::get('enqcategory', 'EnquiryController@getCategory');
Route::post('addenqthrough', 'EnquiryController@AddEnqThrough');
Route::get('enqthrough', 'EnquiryController@getEnqThrough');
Route::get('viewenquirys/{id}', 'EnquiryController@GetEnquiry');
Route::post('updateenq/{id}', 'EnquiryController@UpdateEnquiry');

//Tickets
Route::get('addticket', function () {
	return view('addticket');
});
Route::post('addticket', 'TicketsController@AddTickets');

Route::get('viewticket', 'TicketsController@getAllTickets');
Route::get('deleteticket/{id}', 'TicketsController@DeleteTicket');
Route::get('viewtickets/{id}', 'TicketsController@GetTicketHistory');
Route::post('updateticket/{id}', 'TicketsController@UpdateTicket');
Route::get('setdisposition/{id}/{cusid}', 'TicketsController@LoadAddClosure');
Route::post('addclosure', 'TicketsController@AddClosure');

Route::get('tickettype', 'TicketsController@GetTicketType');
Route::post('addtickettype', 'TicketsController@AddTicketType');
Route::get('ticketdefectcode', 'TicketsController@GetTicketDefectCode');
Route::post('addticketdefectcode', 'TicketsController@AddTicketDefectCode');

//Datamigration
Route::get('datamigration', function () {
	return view('datamigration');
});
Route::post('uploadexcel', 'DatamigrationController@uploadExcel');

Route::prefix('/datamigration')->group(function () {
	Route::post('/customer', 'DatamigrationController@addCustomer');
	Route::post('/employee', 'DatamigrationController@addEmployee');
	Route::post('/products', 'DatamigrationController@addProducts');
	Route::post('/spares', 'DatamigrationController@addSpares');
	Route::post('/consumable', 'DatamigrationController@addConsumable');
	Route::post('/customerproduct', 'DatamigrationController@addCustomerProducts');
});



//Invoice
Route::post('addinvoice', 'InvoiceController@AddInvoice');
Route::get('viewinvoice', 'InvoiceController@getAllInvoices');
Route::get('deleteinvoice/{id}', 'InvoiceController@DeleteInvoice');
Route::get('printinvoice/{inv_id}/{branch_id}/{cus_id}', 'InvoiceController@printInvoice');

Route::get('addinvoice', function () {
	return view('addinvoice');
});
Route::get('edit_invoice/{id}', 'InvoiceController@editInvoice');

Route::prefix('/{user}')->group(function () {
	Route::get('/invoices', 'InvoiceController@getInvoices');
	Route::get('/invoicedetails', 'InvoiceController@getInvoiceDetails');
	Route::get('/invoicetotal', 'InvoiceController@getInvoiceTotal');
	Route::get('/branchdetails', 'BranchController@getBranchDetails');
	Route::get('/customerdetails', 'BranchController@getCustomerDetails');
});
Route::get('cusinfo/{cusname}', 'BranchController@getcustomerinfo');
Route::get('getcusprod/{id}', 'CustomerProductController@GetCusPrd');

Route::post('updateinvoice/{id}', 'InvoiceController@UpdateInvoice');

//DeliveryChallan
Route::get('viewdc', 'InvoiceController@getAllChallan');
Route::get('deletedc/{id}', 'InvoiceController@DeleteChallan');
Route::get('dcprint/{id}', 'InvoiceController@printChallan');
Route::get('adddc', function () {
	return view('adddc');
});
Route::post('addchallan', 'InvoiceController@AddChallan');
Route::post('updatechallan/{id}', 'InvoiceController@UpdateChallan');
Route::get('edit_dc/{id}', 'InvoiceController@editChallan');

Route::prefix('/{user}')->group(function () {
	//Route::get('/deliverychallan', 'InvoiceController@getAlldc');
	Route::get('/dcdetails', 'InvoiceController@getChallanDetails');
	Route::get('/challanitems', 'InvoiceController@getChallanItemDetails');
	Route::get('/challantotal', 'InvoiceController@getChallanTotal');
});

//Receipt
Route::get('addreceipt/{id}', 'ReceiptController@loadReceipt');
Route::post('addreceipt', 'ReceiptController@AddReceipt');
Route::get('viewreceipt', 'ReceiptController@getAllReceipts');
Route::get('viewreceipts/{id}', 'ReceiptController@getReceiptDetails');
Route::post('addpaymode', 'ReceiptController@AddPaymentMode');

//Expense
Route::get('addexpenses', function () {
	return view('addexpenses');
});

Route::post('addexpense', 'ExpenseController@AddExpense');
Route::get('viewexpenses', 'ExpenseController@GetExpenses');

//PmsSchedule
Route::get('pmsschedule', 'PMSController@GetPMSs');
Route::get('viewpms/{id}', 'PMSController@GetPMS');
Route::post('updatepms/{id}', 'PMSController@UpdatePMS');

//PaymentFollowUp
Route::get('paymentfollowup', 'PaymentFollowUpController@GetPaymentFollowups');
Route::get('viewpaymentfollowup/{id}', 'PaymentFollowUpController@GetPaymentFollowup');

//Reports
Route::get('reports', function () {
	return view('reports');
});
