<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\UsersecurityController;
use App\Http\Controllers\BranchController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\SpareController;
use App\Http\Controllers\ConsumableController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\CustomerProductController;
use App\Http\Controllers\ServicesController;
use App\Http\Controllers\ExpenseController;
use App\Http\Controllers\PaymentFollowUpController;
use App\Http\Controllers\PMSController;
use App\Http\Controllers\EnquiryController;
use App\Http\Controllers\ContractController;

use App\Http\Controllers\ReceiptController;
use App\Http\Controllers\TicketsController;
use App\Http\Controllers\InvoiceController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|<h2>Hello {{session('username')}}</h2>
*/

Route::get('/clear-cache', function () {
	Artisan::call('cache:clear');
	Artisan::call('route:clear');
	Artisan::call('view:clear');
	Artisan::call('config:cache');
	return "Cache is cleared";
});

Route::view("login", 'login');

Route::post('loginsubmit', [AuthController::class, 'login']);
Route::get('/logout', function () {
	if (session()->has('username')) {
		session()->pull('username');
	}
	return redirect('login');
});
Route::get('/', function () {
	return redirect('login');
});

Route::get('dashboard', 'DashboardController@showDashboard');

//Organization
Route::get('vieworganization', 'BranchController@getBranchs');
Route::get('addorganization', function () {
	return view('addorganization');
});
Route::post('addorganizations', 'BranchController@AddBranch');
Route::get('deleteorg/{id}', 'BranchController@DeleteBranch');
//employee
Route::get('viewuser', 'EmployeeController@GetEmployees');
//Route::get('adduser', 'EmployeeController@GetEmployees');
Route::get('adduser', function () {
	return view('adduser');
});
Route::post('addemp', 'EmployeeController@AddEmp');
Route::post('addrole', 'EmployeeController@AddRole');
Route::get('empdetails', 'EmployeeController@GetEmployee');
Route::get('emproles', 'EmployeeController@GetRoles');
Route::get('branchname', 'BranchController@getAllBranchNames');
Route::get('updateemp', 'BranchController@UpdateEmployee');
Route::get('deleteuser/{id}', 'EmployeeController@DeleteEmployee');

//Inventory Management
Route::post('addunit', 'ProductController@AddUnit');
Route::post('addbrand', 'ProductController@AddBrand');
Route::get('brand', 'ProductController@getBrands');
Route::get('unit', 'ProductController@getUnits');
//product
Route::get('viewproduct', 'ProductController@GetProducts');
Route::get('addproduct', function () {
	return view('addproduct');
});
Route::post('addproducts', 'ProductController@AddProduct');
Route::post('addprdhsn', 'ProductController@AddProductHSN');
Route::post('addprdcategory', 'ProductController@AddProductCategory');

Route::get('prdhsn', 'ProductController@getProductHSN');
Route::get('prdcategory', 'ProductController@getCategory');
Route::get('deleteproduct/{id}', 'ProductController@DeleteProduct');
//spares
Route::get('viewspares', 'SpareController@GetSpares');
Route::get('addspares', function () {
	return view('addspares');
});
Route::post('addspare', 'SpareController@AddSpare');
Route::post('addsparehsn', 'SpareController@AddSpareHSN');
Route::post('addsparebrand', 'SpareController@AddSpareBrand');
Route::get('sparehsn', 'SpareController@getSpareHSN');
Route::get('sparebrand', 'SpareController@getSpareBrand');
Route::get('deletespare/{id}', 'SpareController@DeleteSpare');

//consumable
Route::get('viewconsumable', 'ConsumableController@GetConsumables');
Route::get('addconsumable', function () {
	return view('addconsumable');
});
Route::post('addconsumables', 'ConsumableController@AddConsumable');
Route::post('addconsumablehsn', 'ConsumableController@AddConsumableHSN');
Route::get('consumablehsn', 'ConsumableController@GetConsumableHSN');
Route::get('deleteconsumable/{id}', 'ConsumableController@DeleteConsumable');
//services
Route::get('viewservices', 'ServicesController@GetServices');
Route::get('addservices', function () {
	return view('addservices');
});
Route::post('addservice', 'ServicesController@AddService');
Route::post('addservicehsn', 'ServicesController@AddServiceHSN');
Route::get('servicehsn', 'ServicesController@GetServiceHSN');
Route::get('deleteservice/{id}', 'ServicesController@DeleteService');

//Customer
Route::get('addcustomer', function () {
	return view('addcustomer');
});
Route::get('viewcustomer', 'CustomerController@getCustomers');
Route::get('deletecustomer/{id}', 'CustomerController@DeleteCustomer');
/* Route::get('view_customer', function () {
	return view('view_customer');
});
Route::get('customerticket', function () {
	return view('customerticket');
});
 */
//Contract
/* Route::get('addcontract', function () {
	return view('addcontract');
}); */
Route::get('viewcontract', 'ContractController@GetContracts');
Route::get('closecontract/{id}', 'ContractController@CloseContract');
Route::get('deletecontract/{id}', 'ContractController@DeleteContract');
/* Route::get('viewcontracts', function () {
	return view('viewcontracts');
});
Route::get('view_contract', function () {
	return view('view_contract');
});
Route::get('edit_contract', function () {
	return view('edit_contract');
}); */

//Enquiry
Route::get('viewenquiry', 'EnquiryController@GetEnquirys');
Route::get('addenquiry', function () {
	return view('addenquiry');
});
Route::get('viewenquirys', function () {
	return view('viewenquirys');
});

//Tickets
/* Route::get('addticket', function () {
	return view('addticket');
}); */
Route::get('viewticket', 'TicketsController@getAllTickets');
Route::get('deleteticket/{id}', 'TicketsController@DeleteTicket');
/* Route::get('viewtickets', function () {
	return view('viewtickets');
});
Route::get('setdisposition', function () {
	return view('setdisposition');
});

Route::get('datamigration', function () {
	return view('datamigration');
}); */

//Invoice
/* Route::get('addinvoice', function () {
	return view('addinvoice');
}); */
Route::get('viewinvoice', 'InvoiceController@getAllInvoices');
/* Route::get('edit_invoice', function () {
	return view('edit_invoice');
}); */
Route::get('deleteinvoice/{id}', 'InvoiceController@DeleteInvoice');
Route::get('printinvoice', 'InvoiceController@printInvoice');

Route::prefix('/{user}')->group(function () {
	Route::get('/invoices', 'InvoiceController@getInvoices');
	Route::get('/invoicedetails', 'InvoiceController@getInvoiceDetails');
	Route::get('/invoicetotal', 'InvoiceController@getInvoiceTotal');
	Route::get('/branchdetails', 'BranchController@getBranchDetails');
	Route::get('/customerdetails', 'BranchController@getCustomerDetails');
});

//DeliveryChallan
/* Route::get('adddc', function () {
	return view('adddc');
}); */
Route::get('viewdc', 'InvoiceController@getAllChallan');
/* Route::get('edit_dc', function () {
	return view('edit_dc');
}); */
Route::get('deletedc/{id}', 'InvoiceController@DeleteChallan');
Route::get('dcprint', 'InvoiceController@printChallan');
Route::prefix('/{user}')->group(function () {
	Route::get('/deliverychallan', 'InvoiceController@getAlldc');
	Route::get('/dcdetails', 'InvoiceController@getChallanDetails');
	Route::get('/challanitems', 'InvoiceController@getChallanItemDetails');
	Route::get('/challantotal', 'InvoiceController@getChallanTotal');
});

//Receipt
/* Route::get('addreceipt', function () {
	return view('addreceipt');
}); */
Route::get('viewreceipt', 'ReceiptController@getAllReceipts');
/* Route::get('viewreceipts', function () {
	return view('viewreceipts');
}); */

//Expense
/* Route::get('addexpenses', function () {
	return view('addexpenses');
}); */
Route::get('viewexpenses', 'ExpenseController@GetExpenses');

//PmsSchedule
Route::get('pmsschedule', 'PMSController@GetPMSs');
/* Route::get('viewpms', function () {
	return view('viewpms');
});
 */
//PaymentFollowUp
Route::get('paymentfollowup', 'PaymentFollowUpController@GetPaymentFollowups');
/* Route::get('viewpaymentfollowup', function () {
	return view('viewpaymentfollowup');
}); */

/* Route::get('reports', function () {
	return view('reports');
});
 */