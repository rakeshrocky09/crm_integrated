function form_to_json(form_id) {
    var jsonData = {};
    var formData = $("#" + form_id).serializeArray();
    $.each(formData, function () {
        if (jsonData[this.name]) {
            if (!jsonData[this.name].push) {
                jsonData[this.name] = [jsonData[this.name]];
            }
            jsonData[this.name].push(this.value || "");
        } else {
            jsonData[this.name] = this.value || "";
        }
    });

    return jsonData;
}
