<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class ExpenseController extends Controller
{
	public function GetExpenses(Request $request)
	{
		$input = $request->all();
		$select_company = DB::table('user_security')
			->select('company_name')
			->where('username', Auth::user()->username)
			->get();

		$product = DB::table('expenses_details')
			->where('Company_Name', $select_company[0]->company_name)
			->get();
		//echo json_encode($product);die;
		return view('viewexpenses', compact('product'));
	}

	public function GetExpType(Request $request)
	{
		$input = $request->all();

		$brand = DB::table('expense_type')
			->select('expense_type')
			->get()
			->unique('expense_type');

		if (count($brand) >= 1) {
			return response()->json(['success' => true, 'Expense Type' => $brand], 200);
		} else {
			return response()->json(['success' => false, 'message' => "No Data Found"], 200);
		}
	}

	public function AddExpense(Request $request)
	{
		$date = $request['date'];
		$amount = $request['amount'];
		$branch = $request['branch_name'];
		$remarks = $request['remarks'];
		$customer = $request['customer'];

		$username = Auth::user()->username;
		$expenses_const = "SP";
		$status = '1';

		$select_company = DB::table('user_security')
			->select('company_name')
			->where('username', $username)
			->get();

		$select_sql = DB::table('expenses_details')
			->select('id')
			->orderby('expenses_id', 'DESC')
			->take(1)
			->get();

		$id_number  = $select_sql[0]->id;
		$add_id_number = $id_number + 1;
		$const = "EXP" . $add_id_number;

		$exp_date = date('Y-m-d', strtotime($date));

		$insert = DB::table('expenses_details')
			->insert([
				'User_Name' => $username,
				'expenses_id' => $const,
				'Branch' => $branch,
				'Expenses_Date' => $exp_date,
				'Expenses_Description' => $remarks,
				'Expenses_Amount' => $amount,
				'Customer_Name' => $customer,
				//'machine_id' => $input['expenses_machine'],
				'Company_Name' => $select_company[0]->company_name
			]);

			return redirect('viewexpenses')->with('message', "Expenses");
	}

	public function AddExpType(Request $request)
	{
		$input = $request->all();

		$insert = DB::table('expense_type')->insert(['expense_type' => $input['add_expensetype']]);

		return response()->json(['success' => true, 'message' => 'Expense Type Added Successfully'], 200);
	}

	public function GetExpense(Request $request)
	{
		$input = $request->all();
		$validator = Validator::make(
			$request->all(),
			[
				'id' => 'required',
				'username' => 'required',
				'branch' => 'required'
			]
		);

		if ($validator->fails()) {
			$message = $validator->errors();
			return response()->json(['success' => false, 'message' => 'The given data was invalid.', 'errors' => $validator->errors()], 200);
		}

		$company = DB::table('user_security')
			->select('company_name')
			->where('username', $input['username'])
			->get();

		$sql = DB::table('expenses_details')
			->where('branch', $input['branch'])
			->where('Expense_ID', $input['id'])
			->where('User_Name', $input['username'])
			->get();

		if (count($sql) >= 1) {
			return response()->json(['success' => true, 'Expense Details' => $sql], 200);
		} else {
			return response()->json(['success' => true, 'message' => 'No Data Found'], 200);
		}
	}
}
