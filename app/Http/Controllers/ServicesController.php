<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class ServicesController extends Controller
{
	public function GetServices(Request $request)
	{
		$input = $request->all();

		$select_company = DB::table('user_security')
			->select('company_name')
			->where('username', Auth::user()->username)
			->get();

		$product = DB::table('service_details')
			->where('service_status', '=', '1')
			->where('company_name', $select_company[0]->company_name)
			->get();

		//echo json_encode($product);die;
		return view('viewservices', compact('product'));
	}
	public function GetAllServices(Request $request)
	{
		$select_company = DB::table('user_security')
			->select('company_name')
			->where('username', Auth::user()->username)
			->get();

		$product = DB::table('service_details')
			->where('service_status', '=', '1')
			->where('company_name', $select_company[0]->company_name)
			->get();

		return response()->json($product);
	}

	public function AddService(Request $request)
	{
		$validated = $request->validate([
			'service_hsn' => 'required',
			'brand_name' => 'required',
			'item_name' => 'required',
			'unit_price' => 'required',
			'stock_unit' => 'required',
			'belongs_to' => 'required',
			'gst' => 'required',
			'toc' => 'required',
		]);

		$hsn = $request['service_hsn'];
		$brand_name = $request['brand_name'];
		$item_name = $request['item_name'];
		$unit_price = $request['unit_price'];
		$stock_unit = $request['stock_unit'];
		$branch = $request['belongs_to'];
		$gst = $request['gst'];
		$toc = $request['toc'];

		$username = Auth::user()->username;
		$service_const = "SER";
		$status = '1';

		$select_company = DB::table('user_security')
			->select('company_name')
			->where('username', 	$username)
			->get();

		$select_sql = DB::table('service_details')
			->where('service_item_name', $item_name)
			->where('username', 	$username)
			->where('service_status', '=', '1')
			->count();

		if ($select_sql == 0) {
			$insert = DB::table('service_details')
				->insert([
					'username' => $username,
					'service_branch_name' => $branch,
					'service_const' => $service_const,
					'service_hsn' => $hsn,
					'service_brand_name' => $brand_name,
					'service_item_name' => $item_name,
					'service_parts_unit' => $stock_unit,
					'service_unit_price' => $unit_price,
					'Gst_Percentage' => $gst,
					'service_type' => $toc,
					'service_status' => $status,
					'Created_Date' => date('Y-m-d H:i:s'),
					'company_name' => $select_company[0]->company_name
				]);

			return redirect('viewservices')->with('message', "Service");
		} else {
			return redirect('viewservices')->with('error', "Service");
		}
	}

	public function UpdateService(Request $request, $id)
	{

		$brand = $request['edit_brand'];
		$hsn = $request['edit_hsn'];
		$price = $request['edit_price'];
		$unit = $request['edit_unit'];
		$typeofcopies = $request['edit_type_ofcopies'];
		$gst = $request['edit_gst'];
		$item = $request['edit_item'];

		$username = Auth::user()->username;
		$select_sql = DB::table('service_details')
			->where('service_id', $id)
			->where('username', $username)
			->count();

		if ($select_sql == 1) {
			$sql = DB::table('service_details')
				->where('service_id', $id)
				->where('username', $username)
				->update([
					//'service_branch_name' => $input['edit_service_branch_name'],
					'service_hsn' => $hsn,
					'service_brand_name' => $brand,
					'service_item_name' => $item,
					'service_parts_unit' => $unit,
					'service_unit_price' => $price,
					'Gst_Percentage' => $gst,
					'service_type' => $typeofcopies
				]);

			return redirect('viewservices')->with('update', "Services");
		} else {
			return redirect('viewservices')->with('error', 'Services Update Failed Contact Admin');
		}
	}

	public function DeleteService(Request $request, $id)
	{
		$username = Auth::user()->username;
		$company = DB::table('user_security')
			->select('company_name')
			->where('username', $username)
			->get();

		$select_sql = DB::table('service_details')
			->where('service_id', $id)
			->count();

		if ($select_sql == 1) {
			$sql = DB::table('service_details')
				->where('service_id', $id)
				->update(['service_status' => 0]);

			return redirect('viewservices');
		} else {
			return response()->json(['success' => true, 'message' => 'Invalid Value'], 200);
		}
	}

	public function GetService(Request $request)
	{
		$input = $request->all();
		$validator = Validator::make(
			$request->all(),
			[
				'id' => 'required',
				'username' => 'required'
			]
		);

		if ($validator->fails()) {
			$message = $validator->errors();
			return response()->json(['success' => false, 'message' => 'The given data was invalid.', 'errors' => $validator->errors()], 200);
		}

		$company = DB::table('user_security')
			->select('company_name')
			->where('username', $input['username'])
			->get();

		$sql = DB::table('service_details')
			->where('service_status', 1)
			->where('service_id', $input['id'])
			->where('company_name', $company[0]->company_name)
			->get();

		if (count($sql) >= 1) {
			return response()->json(['success' => true, 'Service Details' => $sql], 200);
		} else {
			return response()->json(['success' => true, 'message' => 'No Data Found'], 200);
		}
	}

	public function GetServiceHSN(Request $request)
	{
		$hsn = DB::table('service_hsn_code')
			->select('Service_HSN_No')
			->where(['flag' => 1])
			->get()
			->unique('Service_HSN_No');

		if (count($hsn) >= 1) {
			return response()->json($hsn);
		} else {
			return response()->json(['success' => false, 'message' => "No Data Found"], 200);
		}
	}

	public function AddServiceHSN(Request $request)
	{
		$validated = $request->validate([
			'add_hsn' => 'required',
		]);

		$hsn = $request['add_hsn'];
		$flag = 1;
		$select_sql = DB::table('service_hsn_code')
			->where('Service_HSN_No', $hsn)
			->where('flag', '=', '1')
			->count();

		if ($select_sql == 0) {
			$insert = DB::table('service_hsn_code')->insert(['Service_HSN_No' => $hsn, 'flag' => $flag]);
			return redirect()->back()->with('message', $hsn);
		} else {
			return redirect()->back()->with('error', $hsn);
		}
	}
}
