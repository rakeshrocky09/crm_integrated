<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class PaymentFollowUpController extends Controller
{
	public function GetPaymentFollowups(Request $request)
	{
		$input = $request->all();

		$select_company = DB::table('user_security')
			->select('company_name')
			->where('username', Auth::user()->username)
			->get();

		$sql = DB::select("SELECT max(Amount_Received_Date) as Amount from payment_follow_up  group by Invoice_ID");
		$amount = array_column($sql, 'Amount');

		$payment = DB::table('payment_follow_up')
			->whereIn('Amount_Received_Date', $amount)
			->where('username', Auth::user()->username)
			->get()
			->unique('Invoice_ID');

		//echo json_encode($payment);die;
		return view('paymentfollowup', compact('payment'));
	}

	public function GetPaymentFollowup(Request $request, $id)
	{

		$username = Auth::user()->username;
		$sql = DB::table('payment_follow_up')
			->where('Payment_Id', $id)
			->where('username', $username)
			->orderby('Payment_Id', 'DESC')
			->get();

		if (count($sql) >= 1) {
			return view('viewpaymentfollowup', compact('sql'));
		} else {
			return response()->json(['success' => true, 'message' => 'No Data Found'], 200);
		}
	}

	public function AddPaymentFollowUp(Request $request)
	{
		$input = $request->all();
		$validator = Validator::make(
			$request->all(),
			[
				'username' => 'required'
			]
		);

		if ($validator->fails()) {
			$message = $validator->errors();
			return response()->json(['success' => false, 'message' => 'The given data was invalid.', 'errors' => $validator->errors()], 200);
		}

		$select_company = DB::table('user_security')
			->select('company_name')
			->where('username', $input['username'])
			->get();

		$select_sql = DB::table('payment_follow_up')
			->select('Payment_Id')
			->orderby('Payment_Id', 'DESC')
			->take(1)
			->get();

		$Payment_Id  = $select_sql[0]->Payment_Id;
		$add_Payment_id = $Payment_Id + 1;
		$follow_date = date('Y-m-d', strtotime($input['edit_followup_date']));

		$insert = DB::table('payment_follow_up')
			->insert([
				'Username' => $input['username'],
				'Customer_Name' => $input['edit_custo_name'],
				'Customer_Phone_No' => $input['edit_custo_phone'],
				'Invoice_ID' => $input['edit_payment_invoice_id'],
				'Total_Due' => $input['edit_total_due'],
				'Followup_Date' => $follow_date,
				'Amount_Received_Date' => $input['edit_follow_up_amount'],
				'Due_Date' => $input['edit_followup_duedate'],
				'Comments' => $input['edit_followup_message'],
				'Last_Followed_Date' => $input['edit_followup_lastfollowed_date'],
				'Payment_Id' => $add_Payment_id,
				'address' => $input['edit_payment_address']
			]);

		return response()->json(['success' => true, 'message' => 'Payment Added Successfully'], 200);
	}
}
