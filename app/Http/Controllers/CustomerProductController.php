<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class CustomerProductController extends Controller
{
	public function AddCusProd(Request $request)
	{
		$input = $request->all();
		$username = Auth::user()->username;
		$cusid = $input['cus_id'];
		$branch = $input['cus_branch'];

		$select_company = DB::table('user_security')
			->select('company_name')
			->where('username', $username)
			->get();

		foreach ($input['lineItems'] as $linetiemkey => $lineItem) {
			//print_r($linetiemvalue['serialno']);
			$products = DB::table('cus_prod_details')
				->insert([
					'company_name' => $select_company[0]->company_name,
					'username' => $username,
					'cus_branch_name' => $branch,
					'customer_id' => $cusid,
					'product_id' => $lineItem['product_id'],
					'Machine_Serial_No' => $lineItem['serialno'],
					'Serial_Key_No' => $lineItem['serialkey'],
					'section' => $lineItem['section'],
					'Contract_Type' => $lineItem['contractype'],
					'Flag' => 1,
					'Created_Date' => date('Y-m-d')
				]);
		}
		return response()->json('success');
	}

	public function AddProduct(Request $request)
	{
		$input = $request->all();
		$validator = Validator::make(
			$request->all(),
			[
				'username' => 'required'
			]
		);

		if ($validator->fails()) {
			$message = $validator->errors();
			return response()->json(['success' => false, 'message' => 'The given data was invalid.', 'errors' => $validator->errors()], 200);
		}

		$inst_date = date("Y-m-d", strtotime($input['edit_install_date']));
		$date = date("Y-m-d", strtotime($input['date']));

		$sql = DB::table('cus_prod_details')
			->where('customer_id', $input['cus_ids'])
			->where('product_id', $input['prod_ids'])
			->where('Machine_Serial_No', $input['edit_machine'])
			->update([
				'installation_date' => $inst_date,
				'technician_id' => $input['edit_install_assigned_tech'],
				'product_sold_by' => $input['edit_install_sold_by'],
				'section' => $input['edit_section'],
				'Contract_Type' => $input['edit_type'],
				'Black_A3' => $input['edit_install_blacka3'],
				'Black_A4' => $input['edit_install_blacka4'],
				'Color_A3' => $input['edit_install_colora3'],
				'Color_A4' => $input['edit_install_colora4'],
				'Date' => $date,
				'Serial_Key_No' => $input['edit_serial']
			]);

		$insert = DB::table('installation_history')
			->insert([
				'cus_id' => $input['cus_ids'],
				'mahine_serial_no' => $input['edit_machine'],
				'installed_date' => $inst_date,
				'blacka3' => $input['edit_install_blacka3'],
				'blacka4' => $input['edit_install_blacka4'],
				'colora3' => $input['edit_install_colora3'],
				'colora4' => $input['edit_install_colora4'],
				'date' => $date
			]);


		return response()->json(['success' => true, 'message' => 'Added Successfully'], 200);
	}
	public function UpdateCusProductDetail(Request $request, $cusid, $prodid, $serial)
	{
		$serial_key = $request['edit_serial_key'];
		$model = $request['edit_model'];
		$contract_type = $request['edit_contract_type'];
		$section = $request['edit_section'];
		$sold_by = $request['edit_sold_by'];
		$technician_id = $request['edit_technician_id'];
		$installation_date = $request['edit_installation_date'];
		$techinician_branch = $request['edit_techinician_branch'];
		$blacka3 = $request['edit_blacka3'];
		$blacka4 = $request['edit_blacka4'];
		$colora3 = $request['edit_colora3'];
		$colora4 = $request['edit_colora4'];
		$editdate = $request['edit_date'];


		$select_sql = DB::table('cus_prod_details')
			->where('customer_id', $cusid)
			->where('product_id', $prodid)
			->where('Machine_Serial_No', $serial)
			->count();
		if ($select_sql == 1) {

			$inst_date = date("Y-m-d", strtotime($installation_date));
			$date = date("Y-m-d", strtotime($editdate));

			$sql = DB::table('cus_prod_details')
				->where('customer_id', $cusid)
				->where('product_id', $prodid)
				->where('Machine_Serial_No', $serial)
				->update([
					'installation_date' => $inst_date,
					'technician_id' => $technician_id,
					'product_sold_by' => $sold_by,
					'section' => $section,
					'Contract_Type' => $contract_type,
					'Black_A3' => $blacka3,
					'Black_A4' => $blacka4,
					'Color_A3' => $colora3,
					'Color_A4' => $colora4,
					'Date' => $date,
					'Serial_Key_No' => $serial_key
				]);

			$insert = DB::table('installation_history')
				->insert([
					'cus_id' => $cusid,
					'mahine_serial_no' => $serial,
					'installed_date' => $inst_date,
					'blacka3' => $blacka3,
					'blacka4' => $blacka4,
					'colora3' => $colora3,
					'colora4' => $colora4,
					'date' => $date
				]);

			return redirect('viewcustomer')->with('update', "Customer Product");
		} else {
			return redirect('viewcustomer')->with('error', "Customer Product Update Failed Contact Admin");
		}
	}

	public function GetCusProduct(Request $request)
	{
		$input = $request->all();
		$validator = Validator::make(
			$request->all(),
			[
				'id' => 'required',
				'username' => 'required'
			]
		);

		if ($validator->fails()) {
			$message = $validator->errors();
			return response()->json(['success' => false, 'message' => 'The given data was invalid.', 'errors' => $validator->errors()], 200);
		}

		$company = DB::table('user_security')
			->select('company_name')
			->where('username', $input['username'])
			->get();

		$sql = DB::table('cus_prod_details')
			->leftjoin('product_details', 'product_details.product_id', '=', 'cus_prod_details.product_id')
			->where('cus_prod_details.Flag', 1)
			->where('cus_prod_details.customer_id', $input['id'])
			->where('cus_prod_details.company_name', $company[0]->company_name)
			->get();

		if (count($sql) >= 1) {
			return response()->json(['success' => true, 'Customer Product Details' => $sql], 200);
		} else {
			return response()->json(['success' => true, 'message' => 'No Data Found'], 200);
		}
	}

	public function GetCusPrd(Request $request, $id)
	{
		$username = Auth::user()->username;
		$company = DB::table('user_security')
			->select('company_name')
			->where('username', $username)
			->get();

		$sql = DB::table('cus_prod_details')
			->where('customer_id', $id)
			->leftJoin('product_details', 'product_details.product_id', '=', 'cus_prod_details.product_id')
			->where('cus_prod_details.company_name', $company[0]->company_name)
			->get();

		if (count($sql) >= 1) {
			return response()->json($sql);
		} else {
			return response()->json('');
		}
	}

	public function UpdateInstalled(Request $request)
	{
		$input = $request->all();
		$validator = Validator::make(
			$request->all(),
			[
				'id' => 'required',
				'username' => 'required'
			]
		);

		if ($validator->fails()) {
			$message = $validator->errors();
			return response()->json(['success' => false, 'message' => 'The given data was invalid.', 'errors' => $validator->errors()], 200);
		}

		$company = DB::table('user_security')
			->select('company_name')
			->where('username', $input['username'])
			->get();

		$sql = DB::table('cus_prod_details')
			->where('Machine_Serial_No', $input['id'])
			->where('company_name', $company[0]->company_name)
			->count();

		if ($input['install_blacka3']) {
			$blacka3 = $input['install_blacka3'];
		} else {
			$blacka3 = 0;
		}
		if ($input['install_blacka4']) {
			$blacka4 = $input['install_blacka4'];
		} else {
			$blacka4 = 0;
		}
		if ($input['install_colora3']) {
			$colora3 = $input['install_colora3'];
		} else {
			$colora3 = 0;
		}
		if ($input['install_colora4']) {
			$colora4 = $input['install_colora4'];
		} else {
			$colora4 = 0;
		}
		$inst_date = date("Y-m-d", strtotime($input['install_date']));
		if ($sql >= 1) {
			$sql = DB::table('cus_prod_details')
				->where('Machine_Serial_No', $input['id'])
				->where('company_name', $company[0]->company_name)
				->update([
					'product_sold_by' => $input['install_sold_by'],
					'installation_date' =>  $inst_date,
					'technician_id' => $input['install_assigned_tech'],
					'Techinician_Branch' => $input['install_tech_brach'],
					'Black_A3' => $blacka3,
					'Black_A4' => $blacka4,
					'Color_A3' => $colora3,
					'Color_A4' => $colora4
				]);
			return response()->json(['success' => true, 'message' => 'Success'], 200);
		} else {
			return response()->json(['success' => true, 'message' => 'unavailable please contact admin.'], 200);
		}
	}

	public function DeleteCusProduct(Request $request, $id)
	{
		/* $input = $request->all();
		$validator = Validator::make(
			$request->all(),
			[
				'id' => 'required',
				'username' => 'required'
			]
		);

		if ($validator->fails()) {
			$message = $validator->errors();
			return response()->json(['success' => false, 'message' => 'The given data was invalid.', 'errors' => $validator->errors()], 200);
		} */

		$company = DB::table('user_security')
			->select('company_name')
			->where('username', Auth::user()->username)
			->get();

		$select_sql = DB::table('cus_prod_details')
			->where('Machine_Serial_No', $id)
			->where('company_name', $company[0]->company_name)
			->count();

		if ($select_sql == 1) {
			$sql = DB::table('cus_prod_details')
				->where('Machine_Serial_No', $id)
				->where('company_name', $company[0]->company_name)
				->update(['Flag' => 0]);

			return back()->with('delete', 'Product');
		} else {
			return redirect('viewcustomer')->with('error', 'Product Delete Failed Contact Admin');
		}
	}
}
