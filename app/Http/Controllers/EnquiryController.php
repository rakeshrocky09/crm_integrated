<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class EnquiryController extends Controller
{
	public function GetEnquirys(Request $request)
	{
		$input = $request->all();
		$select_company = DB::table('user_security')
			->select('company_name')
			->where('username', Auth::user()->username)
			->get();

		$sql = DB::select("SELECT max(enquiry_reschedule_date) as date from enq_details  group by enquiry_id");
		$amount = array_column($sql, 'date');

		$select = DB::table('enq_details')
			->leftjoin('enquiry_details', 'enquiry_details.enquiry_id', '=', 'enq_details.enquiry_id')
			->where('enquiry_details.username', Auth::user()->username)
			->get()
			->unique('enquiry_id');
		return view('viewenquiry', compact('select'));
		//return response()->json($select);
	}

	public function AddEnquiry(Request $request)
	{
		$validated = $request->validate([
			'customer_name' => 'required',
			'customer_phoneno' => 'required',
			'customer_address' => 'required',
			'branch_name' => 'required',
			'date' => 'required',
			'brand_name' => 'required',
			'enquiry_category' => 'required',
			'details' => 'required',
		]);

		$name = $request['customer_name'];
		$phone = $request['customer_phoneno'];
		$email = $request['customer_email'];
		$address = $request['customer_address'];
		$branch = $request['branch_name'];
		$date = $request['date'];
		$brand = $request['brand_name'];
		$category = $request['enquiry_category'];
		$details = $request['details'];
		$reschedule_date = $request['reschedule_date'];
		$enquiry_through = $request['enquiry_through'];
		$remarks = $request['remarks'];

		$username = Auth::user()->username;
		$enquiry_const = "ENQ";
		$status = "open";

		$select_company = DB::table('user_security')
			->select('company_name')
			->where('username', $username)
			->get();

		$insert = DB::table('enquiry_details')
			->insert([
				'company_name' => $select_company[0]->company_name,
				'username' => $username,
				'enquiry_branch_name' => $branch,
				'enquiry_const' => $enquiry_const,
				'enquiry_cus_name' => $name,
				'enquiry_cus_phone_no' => $phone,
				'enquiry_cus_address' => $address,
				'email' => $email,
				'enquiry_brand' => $brand
			]);

		$last = DB::table('enquiry_details')->orderBy('enquiry_id', 'desc')->first();

		$enquiry_date_no = date('Y-m-d', strtotime($date));
		$enquiry_date = date('Y-m-d', strtotime($reschedule_date));

		$insert = DB::table('enq_details')
			->insert([
				'company_name' => $select_company[0]->company_name,
				'enquiry_id' => $last->enquiry_id,
				'call_date' => $enquiry_date_no,
				'enquiry_category' => $category,
				'enquiry_details' => $details,
				'enquiry_reschedule_date' => $enquiry_date,
				'enquiry_status' => $status,
				'enquiry_through' => $enquiry_through,
				'enquiry_remarks' => $remarks
			]);

		return redirect('viewenquiry')->with('message', "Product");
	}

	public function AddCategory(Request $request)
	{
		$validated = $request->validate([
			'add_category' => 'required',
		]);

		$category = $request['add_category'];
		$flag = 1;
		$select_sql = DB::table('enquiry_category')
			->where('name', $category)
			->where('flag', '=', '1')
			->count();

		if ($select_sql == 0) {
			$insert = DB::table('enquiry_category')->insert(['name' => $category, "flag" => $flag]);
			return redirect()->back()->with('message', $category);
		} else {
			return redirect()->back()->with('error', $category);
		}
	}

	public function getCategory(Request $request)
	{
		$sql = DB::table('enquiry_category')
			->select('name')
			->where('flag', '=', '1')
			->get()
			->unique('name');

		if (count($sql) >= 1) {
			return response()->json($sql);
		} else {
			return response()->json(['success' => false, 'message' => "No Data Found"], 200);
		}
	}

	public function AddEnqThrough(Request $request)
	{
		$validated = $request->validate([
			'add_enqthrough' => 'required',
		]);

		$enq = $request['add_enqthrough'];
		$flag = 1;
		$select_sql = DB::table('enquiry_through')
			->where('name', $enq)
			->where('flag', '=', '1')
			->count();

		if ($select_sql == 0) {
			$insert = DB::table('enquiry_through')->insert(['name' => $enq, "flag" => $flag]);
			return redirect()->back()->with('message', $enq);
		} else {
			return redirect()->back()->with('error', $enq);
		}
	}

	public function getEnqThrough(Request $request)
	{
		$sql = DB::table('enquiry_through')
			->select('name')
			->where('flag', '=', '1')
			->get()
			->unique('name');

		if (count($sql) >= 1) {
			return response()->json($sql);
		} else {
			return response()->json(['success' => false, 'message' => "No Data Found"], 200);
		}
	}

	public function GetEnquiry(Request $request, $id)
	{
		/* $input = $request->all();
		$validator = Validator::make(
			$request->all(),
			[
				'id' => 'required',
				'username' => 'required'
			]
		);

		if ($validator->fails()) {
			$message = $validator->errors();
			return response()->json(['success' => false, 'message' => 'The given data was invalid.', 'errors' => $validator->errors()], 200);
		} */
		$username = Auth::user()->username;
		$company = DB::table('user_security')
			->select('company_name')
			->where('username', $username)
			->get();

		$enq = DB::table('enq_details')
			->leftjoin('enquiry_details', 'enquiry_details.enquiry_id', '=', 'enq_details.enquiry_id')
			->where('enq_details.enquiry_id', $id)
			->where('enquiry_details.username', $username)
			->orderby('enq_details.call_date', 'DESC')
			->get();
		//->unique('enq_details.enquiry_id');
		return view('viewenquirys', compact('enq'));
	}

	public function UpdateEnquiry(Request $request, $id)
	{

		$remarks = $request['edit_remarks'];
		$details = $request['edit_details'];
		$status = $request['edit_status'];
		$next_reschedule_date = $request['edit_next_reschedule_date'];
		$enquiry_through = $request['edit_enquiry_through'];

		$username = Auth::user()->username;
		$company = DB::table('user_security')
			->select('company_name')
			->where('username', $username)
			->get();

		$select_sql = DB::table('enq_details')
			->where('enquiry_id', $id)
			->count();

		if ($select_sql == 1) {
			$sql = DB::table('enq_details')
				->where('enquiry_id', $id)
				->where('company_name', $company[0]->company_name)
				->update([
					'enquiry_remarks' => $remarks,
					'enquiry_details' => $details,
					'enquiry_status' => $status,
					'enquiry_reschedule_date' => $next_reschedule_date,
					'enquiry_through' => $enquiry_through,
				]);

			return redirect('viewenquiry')->with('update', "Enquiry");
		} else {
			return redirect('viewenquiry')->with('error', "Product Update Failed Contact Admin");
		}
	}

	public function AddReschedule(Request $request)
	{
		$input = $request->all();
		$validator = Validator::make(
			$request->all(),
			[
				'username' => 'required',
				'id' => 'required'
			]
		);

		if ($validator->fails()) {
			$message = $validator->errors();
			return response()->json(['success' => false, 'message' => 'The given data was invalid.', 'errors' => $validator->errors()], 200);
		}

		$select_company = DB::table('user_security')
			->select('company_name')
			->where('username', $input['username'])
			->get();

		$enquiry_date_no = date('Y-m-d', strtotime($input['edit_enquiry_date']));
		$enquiry_date = date('Y-m-d', strtotime($input['edit_enquiry_reschedule_date']));

		$insert = DB::table('enq_details')
			->insert([
				'company_name' => $select_company[0]->company_name,
				'enquiry_id' => $input['id'],
				'call_date' => $enquiry_date_no,
				'enquiry_category' => $input['edit_enquiry_category'],
				'enquiry_details' => $input['edit_enquiry_message'],
				'enquiry_reschedule_date' => $enquiry_date,
				'enquiry_status' => $input['edit_enquiry_status'],
				'enquiry_through' => $input['enquiry_through'],
				'enquiry_remarks' => $input['edit_enquiry_remarks']
			]);

		return response()->json(['success' => true, 'message' => 'Enquiry Added Successfully'], 200);
	}
}
