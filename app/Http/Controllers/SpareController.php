<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class SpareController extends Controller
{
	public function GetSpares(Request $request)
	{
		$input = $request->all();

		$select_company = DB::table('user_security')
			->select('company_name')
			->where('username', Auth::user()->username)
			->get();

		$product = DB::table('spare_details')
			->where('spare_status', '=', '1')
			->where('company_name', $select_company[0]->company_name)
			->orderByDesc('spare_id')
			->get();

		//return response()->json($product);
		return view('viewspares', compact('product'));
	}
	
	public function GetAllSpares(Request $request)
	{
		$select_company = DB::table('user_security')
			->select('company_name')
			->where('username', Auth::user()->username)
			->get();

		$product = DB::table('spare_details')
			->where('spare_status', '=', '1')
			->where('company_name', $select_company[0]->company_name)
			->get();

		return response()->json($product);
	}

	public function AddSpare(Request $request)
	{
		$validated = $request->validate([
			'spare_belongs_to' => 'required',
			'spare_codename' => 'required',
			'spare_brand_name' => 'required',
			'spare_model_name' => 'required',
			'spare_partname' => 'required',
			'spare_unit' => 'required',
			'spare_unitprice' => 'required',
			'spare_stock_unit' => 'required',
			'spare_gst_percentage' => 'required',
			'msl' => 'required',
			'chargeable' => 'required',
		]);

		$branch = $request['spare_belongs_to'];
		$codename = $request['spare_codename'];
		$brand = $request['spare_brand_name'];
		$model = $request['spare_model_name'];
		$partname = $request['spare_partname'];
		//$unit = $request['spare_unit'];
		$unitprice = $request['spare_unitprice'];
		$stock = $request['spare_stock_unit'];
		$gst_percentage = $request['spare_gst_percentage'];
		$msl = $request['msl'];
		$chargeable = $request['chargeable'];


		$username = Auth::user()->username;
		$spare_const = "SP";
		$status = '1';

		$select_company = DB::table('user_security')
			->select('company_name')
			->where('username', $username)
			->get();

		$select_sql = DB::table('spare_details')
			->where('spare_model_name', $model)
			->where('username', $username)
			->where('spare_status', '=', '1')
			->count();

		if ($select_sql == 0) {
			$insert = DB::table('spare_details')
				->insert([
					'username' => $username,
					'spare_branch_name' => $branch,
					'spare_const' => $spare_const,
					'spare_code_name' => $codename,
					'spare_brand_name' => $brand,
					'spare_model_name' => $model,
					'spare_parts_name' => $partname,
					//'spare_parts_unit' => $unit,
					'spare_unit_price' => $unitprice,
					'spare_stock_unit' => $stock,
					'spare_status' => $status,
					'spare_msl' => $msl,
					'spare_chargeable' => $chargeable,
					'Gst_Percentage' => $gst_percentage,
					'company_name' => $select_company[0]->company_name,
					'Created_Date' => date('Y-m-d H:i:s'),
				]);

			return redirect('viewspares')->with('message', 'Spare');
		} else {
			return redirect('viewspares')->with('error', 'Spare');
		}
	}

	public function UpdateSpare(Request $request, $id)
	{
		$brand = $request['edit_brand'];
		$hsn = $request['edit_hsn'];
		$partno = $request['edit_partno'];
		$item = $request['edit_item'];
		$price = $request['edit_price'];
		$unit = $request['edit_unit'];
		$gst = $request['edit_gst'];
		$msl = $request['msl'];
		$chargeable = $request['chargeable'];
		$username = Auth::user()->username;

		$select_sql = DB::table('spare_details')
			->where('spare_id', $id)
			->where('username', $username)
			->count();

		if ($select_sql == 1) {
			$sql = DB::table('spare_details')
				->where('spare_id', $id)
				->update([
					'spare_code_name' => $hsn,
					'spare_brand_name' => $brand,
					'spare_model_name' => $item,
					'spare_parts_name' => $partno,
					'spare_stock_unit' => $unit,
					'spare_unit_price' => $price,
					'spare_msl' => $msl,
					'spare_chargeable' => $chargeable,
					'Gst_Percentage' => $gst
				]);

			return redirect('viewspares')->with('update', "Spares");
		} else {
			return redirect('viewspares')->with('error', 'Spares Update Failed Contact Admin');
		}
	}

	public function DeleteSpare(Request $request, $id)
	{
		$username = Auth::user()->username;
		$company = DB::table('user_security')
			->select('company_name')
			->where('username', $username)
			->get();

		$select_sql = DB::table('spare_details')
			->where('spare_id', $id)
			->count();

		if ($select_sql == 1) {
			$sql = DB::table('spare_details')
				->where('spare_id', $id)
				->update(['spare_status' => 0]);

			return redirect('viewspares');
		} else {
			return response()->json(['success' => true, 'message' => 'Invalid Value'], 200);
		}
	}

	public function GetSpare(Request $request)
	{
		$input = $request->all();
		$validator = Validator::make(
			$request->all(),
			[
				'id' => 'required',
				'username' => 'required'
			]
		);

		if ($validator->fails()) {
			$message = $validator->errors();
			return response()->json(['success' => false, 'message' => 'The given data was invalid.', 'errors' => $validator->errors()], 200);
		}

		$company = DB::table('user_security')
			->select('company_name')
			->where('username', $input['username'])
			->get();

		$sql = DB::table('spare_details')
			->where('spare_status', 1)
			->where('spare_id', $input['id'])
			->where('company_name', $company[0]->company_name)
			->get();

		if (count($sql) >= 1) {
			return	redirect('viewspares');
		} else {
			return response()->json(['success' => true, 'message' => 'No Data Found'], 200);
		}
	}

	public function getSpareHSN(Request $request)
	{
		$input = $request->all();

		$hsn = DB::table('spare_hsn_code')
			->select('Spare_HSN_No')
			->where(['flag' => 1])
			->get()
			->unique('Spare_HSN_No');

		if (count($hsn) >= 1) {
			return response()->json($hsn);
		} else {
			return response()->json(['success' => false, 'message' => "No Data Found"], 200);
		}
	}
	public function getSpareBrand(Request $request)
	{
		$input = $request->all();

		$brand = DB::table('spare_brand_category')
			->select('name')
			->where(['flag' => 1])
			->get()
			->unique('name');

		if (count($brand) >= 1) {
			return response()->json($brand);
		} else {
			return response()->json(['success' => false, 'message' => "No Data Found"], 200);
		}
	}
	public function AddSpareHSN(Request $request)
	{
		$validated = $request->validate([
			'add_hsn' => 'required',
		]);

		$hsn = $request['add_hsn'];
		$flag = 1;

		$select_sql = DB::table('spare_hsn_code')
			->where('Spare_HSN_No', $hsn)
			->where('flag', '=', '1')
			->count();
		if ($select_sql == 0) {
			$insert = DB::table('spare_hsn_code')->insert(['Spare_HSN_No' => $hsn, 'flag' => $flag]);
			return redirect()->back()->with('message', $hsn);
		} else {
			return redirect()->back()->with('error', $hsn);
		}
	}
	public function AddSpareBrand(Request $request)
	{
		$validated = $request->validate([
			'add_brand_name' => 'required',
		]);

		$brand = $request['add_brand_name'];
		$flag = 1;

		$select_sql = DB::table('spare_brand_category')
			->where('name', $brand)
			->where('flag', '=', '1')
			->count();

		if ($select_sql == 0) {
			$insert = DB::table('spare_brand_category')->insert(['name' => $brand, 'flag' => $flag]);
			return redirect()->back()->with('message', $brand);
		} else {
			return redirect()->back()->with('error', $brand);
		}
	}
}
