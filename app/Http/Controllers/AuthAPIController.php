<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Validator;
use DB;
use App\Models\User;

class AuthAPIController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth', ['except' => ['login']]);
    }

    /*login details*/
	public function login(Request $request)
	{
		$input = $request->all();
		$validator = Validator::make($request->all(), 
		[
			'userName' => 'required |string',
			'password' => 'required |string'

		]);

		if ($validator->fails()) 
		{
            $message = $validator->errors();
            return response()->json(['success' => false, 'message' => 'The given data was invalid.', 'errors' => $validator->errors()], 200);
        }


        $username = $input['userName'];
        $password = $input['password'];
        
        $result = DB::table('user_security')->where('username', $username)->first();

        $credentials = array(
            'username' => $input['userName'],
            'password' => $input['password']
        );

        if($result)
       	{
        	if(Hash::check($password,$result->password))
       		{
       			$resp = $this->verify_user($username);
       			if($resp=='Y')
       			{
       				if(Auth::attempt($input))
			        {
			            $user = Auth::user();
						       	
						$array['login'] = true;
						$array['token'] =  $user->createToken('crm')->accessToken;
			       		$array['user'] = $result;
						return response()->json($array, 200);
			        }
       				
       				else
       				{
       					$array['message'] = "Your Account Is Not Valid";
		     				return response()->json($array, 200);
       				}
       			}
       			else
			    {
			    	$array['message'] = "Incorrect Credentials";
		     		return response()->json($array, 200);
			    }
       		}
       		else
       		{
       			$array['login'] = false;
       			$array['message'] = "The username or password you entered is not valid";
       			return response()->json($array, 200);
       		}

        	return response()->json(['success' => true,'Branch Details' => $result], 200);
    	}
    }

    /*Verify if the login Credentials is engineer */
	public function verify_user($username)
	{
		$verifydetails = DB::table('user_security')
			->where('username',$username)
			->count();
		if($verifydetails>=1){
			$data = 'Y';
		}else{
			$data = 'N';
		}

		return $data;
	}

	public function logout()
	{
		if(Auth::check()){
			Auth::user()->token()->revoke();
			//Auth::user()->AauthAccessToken()->delete();
			return response()->json(['success' => true, 'message'=>'Logged out successfully'], 200);
		}
	}
}
