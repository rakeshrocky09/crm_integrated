<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{

    public function showDashboard()
    {
        $title = 'Dashboard';

        $day = date('Y-m-d');
        $date = date('Y-m-d', strtotime($day . " +7 days"));
        $dates = date('Y-m-d', strtotime($day . " +15 days"));

        $company = DB::table('user_security')
            ->select('company_name')
            ->where('username', Auth::user()->username)
            ->get();

        $cus_count = DB::table('customer_detail')
            ->where('cus_status', '=', '1')
            ->where('company_name', $company[0]->company_name)
            ->count();

        $active_contract_count = DB::table('contract_details')
            ->where('Contract_Status', 'open')
            ->where('company_name', $company[0]->company_name)
            ->count();

        $unresolved = array('open', 'Processing On-Hold');

        $unresolved_count = DB::table('closure_details')
            ->whereIn('Closure_Type', $unresolved)
            ->where('company_name', $company[0]->company_name)
            ->count();

        $resolved = array('Resolved', 'Cancelled by Customer', 'Closed with Discreation');

        $resolved_count = DB::table('closure_details')
            ->whereIn('Closure_Type', $resolved)
            ->where('company_name', $company[0]->company_name)
            ->count();

        $pms_overdue = DB::table('pms_details')
            ->whereBetween('Follow_Up_Date', [$day, $date])
            ->where('company_name', $company[0]->company_name)
            ->count();

        $contract_count = DB::table('cus_prod_details')
            ->whereNull('contract_id')
            ->where('company_name', $company[0]->company_name)
            ->count();

        $enquiry_count = DB::table('enq_details')
            ->where('Enquiry_Status', 'open')
            ->whereBetween('call_date', [$day, $dates])
            ->where('company_name', $company[0]->company_name)
            ->count();

        $pending_payment = DB::table('payment_follow_up_detail')
            ->where('Payment_Status', 'pending')
            ->where('company_name', $company[0]->company_name)
            ->count();

        $ticket =  array('open', 'pending');
        $recent_ticket = DB::table('customer_detail')
            ->leftjoin('ticket_details', 'ticket_details.cus_id', '=', 'customer_detail.cus_id')
            ->leftjoin('tick_details', 'tick_details.Ticket_id', '=', 'ticket_details.ticket_id')
            ->leftjoin('product_details', 'product_details.product_name', '=', 'ticket_details.product_model')
            ->select('tick_details.Assign_Technician_Name', 'ticket_details.Machine_Serial_No', 'tick_details.Ticket_Created_Date', 'tick_details.Ticket_Defect_Code', 'tick_details.Ticket_Priority', 'customer_detail.customer_name', 'ticket_details.product_model', 'ticket_details.ticket_id')
            ->where('ticket_details.flag', '1')
            ->whereIn('tick_details.Ticket_Status', $ticket)
            ->where('ticket_details.company_name', $company[0]->company_name)
            ->orderby('tick_details.Ticket_Priority', 'asc')
            ->get();

        $payment = DB::table('payment_follow_up')
            ->where('company_name', $company[0]->company_name)
            ->get();

        if (count($payment) > 0) {
            $pay_day = $payment[0]->Followup_Date;
            $pay_date = date('Y-m-d', strtotime($pay_day . " +3 month"));

            $delayed_payment_alerts = DB::table('payment_follow_up')
                ->where('Status', '=', 'open')
                ->where('company_name', $company[0]->company_name)
                ->where('Receipt_Id', $payment[0]->Receipt_Id)
                ->whereNotNull('Followup_Date')
                ->whereBetween('Followup_Date', [$pay_day, $pay_date])
                ->get()
                ->unique('Receipt_Id');
        }else {
            $delayed_payment_alerts = '';
        }

        $delayed_pms = DB::table('pms_details')
            ->leftjoin('customer_detail', 'customer_detail.cus_id', '=', 'pms_details.Customer_Id')
            ->where('pms_details.Follow_Up_Date', '<=', $day)
            ->where('pms_details.status', 'open')
            ->where('pms_details.company_name', $company[0]->company_name)
            ->get();

        $next_pms = DB::table('pms_details')
            ->leftjoin('customer_detail', 'customer_detail.cus_id', '=', 'pms_details.Customer_Id')
            ->whereBetween('pms_details.Follow_Up_Date', [$day, $date])
            ->where('pms_details.status', 'open')
            ->where('pms_details.company_name', $company[0]->company_name)
            ->get();

        /*print_r(count($next_pms));
die;*/
        return view('dashboard', compact('title', 'cus_count', 'active_contract_count', 'unresolved_count', 'resolved_count', 'pms_overdue', 'contract_count', 'enquiry_count', 'pending_payment', 'recent_ticket', 'delayed_payment_alerts', 'delayed_pms', 'next_pms'));
    }

    public function getCustomerCount(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make(
            $request->all(),
            [
                'username' => 'required'
            ]
        );

        if ($validator->fails()) {
            $message = $validator->errors();
            return response()->json(['success' => false, 'message' => 'The given data was invalid.', 'errors' => $validator->errors()], 200);
        }

        $company = DB::table('user_security')
            ->select('company_name')
            ->where('username', $input['username'])
            ->get();

        $sql = DB::table('customer_detail')
            ->where('cus_status', '=', '1')
            ->where('company_name', $company[0]->company_name)
            ->get();

        $data = count($sql);

        return response()->json(['success' => true, 'Customer Count' => $data], 200);
    }

    public function getActiveContract(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make(
            $request->all(),
            [
                'username' => 'required'
            ]
        );

        if ($validator->fails()) {
            $message = $validator->errors();
            return response()->json(['success' => false, 'message' => 'The given data was invalid.', 'errors' => $validator->errors()], 200);
        }

        $company = DB::table('user_security')
            ->select('company_name')
            ->where('username', $input['username'])
            ->get();

        $sql = DB::table('contract_details')
            ->where('Contract_Status', 'open')
            ->where('company_name', $company[0]->company_name)
            ->get();

        $data = count($sql);

        return response()->json(['success' => true, 'Active Contract Count' => $data], 200);
    }

    public function getUnresolvedTicket(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make(
            $request->all(),
            [
                'username' => 'required'
            ]
        );

        if ($validator->fails()) {
            $message = $validator->errors();
            return response()->json(['success' => false, 'message' => 'The given data was invalid.', 'errors' => $validator->errors()], 200);
        }

        $company = DB::table('user_security')
            ->select('company_name')
            ->where('username', $input['username'])
            ->get();
        $array = array('open', 'Processing On-Hold');

        $sql = DB::table('closure_details')
            ->whereIn('Closure_Typ', $array)
            ->where('company_name', $company[0]->company_name)
            ->get();

        $data = count($sql);

        return response()->json(['success' => true, 'Unresolved Ticket Count' => $data], 200);
    }

    public function getResolvedTicket(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make(
            $request->all(),
            [
                'username' => 'required'
            ]
        );

        if ($validator->fails()) {
            $message = $validator->errors();
            return response()->json(['success' => false, 'message' => 'The given data was invalid.', 'errors' => $validator->errors()], 200);
        }

        $company = DB::table('user_security')
            ->select('company_name')
            ->where('username', $input['username'])
            ->get();
        $array = array('Resolved', 'Cancelled by Customer', 'Closed with Discreation');

        $sql = DB::table('closure_details')
            ->whereIn('Closure_Type', $array)
            ->where('company_name', $company[0]->company_name)
            ->get();

        $data = count($sql);

        return response()->json(['success' => true, 'Resolved Ticket Count' => $data], 200);
    }

    public function getPmsOverdue(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make(
            $request->all(),
            [
                'username' => 'required'
            ]
        );

        if ($validator->fails()) {
            $message = $validator->errors();
            return response()->json(['success' => false, 'message' => 'The given data was invalid.', 'errors' => $validator->errors()], 200);
        }

        $day = date('Y-m-d');
        $date = date('Y-m-d', strtotime($day . " +7 days"));

        $company = DB::table('user_security')
            ->select('company_name')
            ->where('username', $input['username'])
            ->get();

        $sql = DB::table('pms_details')
            ->whereBetween('Follow_Up_Date', [$day, $date])
            ->where('company_name', $company[0]->company_name)
            ->get();

        $data = count($sql);

        return response()->json(['success' => true, 'Active Contract Count' => $data], 200);
    }

    public function getContract(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make(
            $request->all(),
            [
                'username' => 'required'
            ]
        );

        if ($validator->fails()) {
            $message = $validator->errors();
            return response()->json(['success' => false, 'message' => 'The given data was invalid.', 'errors' => $validator->errors()], 200);
        }

        $day = date('Y-m-d');
        $date = date('Y-m-d', strtotime($day . " +7 days"));

        $company = DB::table('user_security')
            ->select('company_name')
            ->where('username', $input['username'])
            ->get();

        $sql = DB::table('cus_prod_details')
            ->where('contract_id', '!=', '')
            ->where('company_name', $company[0]->company_name)
            ->get();

        $data = count($sql);

        return response()->json(['success' => true, 'Contract Count' => $data], 200);
    }

    public function getEnquiryCalls(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make(
            $request->all(),
            [
                'username' => 'required'
            ]
        );

        if ($validator->fails()) {
            $message = $validator->errors();
            return response()->json(['success' => false, 'message' => 'The given data was invalid.', 'errors' => $validator->errors()], 200);
        }

        $day = date('Y-m-d');
        $date = date('Y-m-d', strtotime($day . " +7 days"));

        $company = DB::table('user_security')
            ->select('company_name')
            ->where('username', $input['username'])
            ->get();

        $sql = DB::table('enq_details')
            ->where('Enquiry_Status', 'open')
            ->where('company_name', $company[0]->company_name)
            ->get();

        $data = count($sql);

        return response()->json(['success' => true, 'Enquiry Count' => $data], 200);
    }

    public function getFollowupCount(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make(
            $request->all(),
            [
                'username' => 'required'
            ]
        );

        if ($validator->fails()) {
            $message = $validator->errors();
            return response()->json(['success' => false, 'message' => 'The given data was invalid.', 'errors' => $validator->errors()], 200);
        }

        $day = date('Y-m-d');
        $date = date('Y-m-d', strtotime($day . " +7 days"));

        $company = DB::table('user_security')
            ->select('company_name')
            ->where('username', $input['username'])
            ->get();

        $sql = DB::table('payment_follow_up_detail')
            ->where('Payment_Status', 'pending')
            ->where('company_name', $company[0]->company_name)
            ->get();

        $data = count($sql);

        return response()->json(['success' => true, 'FOLLOWUP Count' => $data], 200);
    }
    public function getTicketPriority(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make(
            $request->all(),
            [
                'username' => 'required'
            ]
        );

        if ($validator->fails()) {
            $message = $validator->errors();
            return response()->json(['success' => false, 'message' => 'The given data was invalid.', 'errors' => $validator->errors()], 200);
        }

        $day = date('Y-m-d');
        $date = date('Y-m-d', strtotime($day . " +7 days"));

        $company = DB::table('user_security')
            ->select('company_name')
            ->where('username', $input['username'])
            ->get();

        $array =  array('open', 'pending');
        $sql = DB::table('customer_detail')
            ->leftjoin('ticket_details', 'ticket_details.cus_id', '=', 'customer_detail.cus_id')
            ->leftjoin('tick_details', 'tick_details.Ticket_id', '=', 'ticket_details.ticket_id')
            ->leftjoin('product_details', 'product_details.product_name', '=', 'ticket_details.product_model')
            ->where('ticket_details.flag', '1')
            ->whereIn('tick_details.Ticket_Status', $array)
            ->where('customer_detail.company_name', $company[0]->company_name)
            ->orderby('tick_details.Ticket_Priority', 'asc')
            ->get()
            ->unique('tick_details.Ticket_id');

        return response()->json(['success' => true, 'Ticket Priority Count' => $sql], 200);
    }
    public function getContractExpiry(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make(
            $request->all(),
            [
                'username' => 'required'
            ]
        );

        if ($validator->fails()) {
            $message = $validator->errors();
            return response()->json(['success' => false, 'message' => 'The given data was invalid.', 'errors' => $validator->errors()], 200);
        }

        $day = date('Y-m-d');
        $date = date('Y-m-d', strtotime($day . " +7 days"));

        $company = DB::table('user_security')
            ->select('company_name')
            ->where('username', $input['username'])
            ->get();

        $sql = DB::table('product_details')
            ->leftjoin('contract_details', 'contract_details.Product_Id', '=', 'product_details.product_id')
            ->leftjoin('contract_history_details', 'contract_history_details.Contract_ID', '=', 'contract_details.Contract_ID')
            ->leftjoin('customer_detail', 'customer_detail.cus_id', '=', 'contract_details.Customer_Id')
            ->whereBetween('contract_history_details.End_date', [$day, $date])
            ->where('contract_details.company_name', $company[0]->company_name)
            ->get();

        return response()->json(['success' => true, 'FOLLOWUP Count' => $sql], 200);
    }
    public function getPmsNextSchedule(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make(
            $request->all(),
            [
                'username' => 'required'
            ]
        );

        if ($validator->fails()) {
            $message = $validator->errors();
            return response()->json(['success' => false, 'message' => 'The given data was invalid.', 'errors' => $validator->errors()], 200);
        }

        $day = date('Y-m-d');
        $date = date('Y-m-d', strtotime($day . " +7 days"));

        $company = DB::table('user_security')
            ->select('company_name')
            ->where('username', $input['username'])
            ->get();

        $sql = DB::table('pms_details')
            ->leftjoin('customer_detail', 'customer_detail.cus_id', '=', 'pms_details.Customer_Id')
            ->whereBetween('pms_details.Follow_Up_Date', [$day, $date])
            ->where('pms_details.status', 'open')
            ->where('pms_details.company_name', $company[0]->company_name)
            ->get();

        return response()->json(['success' => true, 'PMS Next Schedule' => $sql], 200);
    }
    public function getPmsDelaySchedule(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make(
            $request->all(),
            [
                'username' => 'required'
            ]
        );

        if ($validator->fails()) {
            $message = $validator->errors();
            return response()->json(['success' => false, 'message' => 'The given data was invalid.', 'errors' => $validator->errors()], 200);
        }

        $day = date('Y-m-d');
        $date = date('Y-m-d', strtotime($day . " +7 days"));

        $company = DB::table('user_security')
            ->select('company_name')
            ->where('username', $input['username'])
            ->get();

        $sql = DB::table('pms_details')
            ->leftjoin('customer_detail', 'customer_detail.cus_id', '=', 'pms_details.Customer_Id')
            ->where('pms_details.Follow_Up_Date', '<=', $day)
            ->where('pms_details.status', 'open')
            ->where('pms_details.company_name', $company[0]->company_name)
            ->get();


        return response()->json(['success' => true, 'FOLLOWUP Count' => $sql], 200);
    }
    public function getPayment(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make(
            $request->all(),
            [
                'username' => 'required'
            ]
        );

        if ($validator->fails()) {
            $message = $validator->errors();
            return response()->json(['success' => false, 'message' => 'The given data was invalid.', 'errors' => $validator->errors()], 200);
        }

        $company = DB::table('user_security')
            ->select('company_name')
            ->where('username', $input['username'])
            ->get();

        $payment = DB::table('payment_follow_up')
            ->where('company_name', $company[0]->company_name)
            ->get();

        $day = $payment[0]->Followup_Date;
        $date = date('Y-m-d', strtotime($day . " +3 month"));

        $sql = DB::table('payment_follow_up')
            ->where('Status', '=', 'open')
            ->where('company_name', $company[0]->company_name)
            ->where('Receipt_Id', $payment[0]->Receipt_Id)
            ->whereBetween('Followup_Date', [$day, $date])
            ->get()
            ->unique('Receipt_Id');

        $rows = array();
        if ($sql[0]->Followup_Date != null) {
            array_push($rows, array('Customer_Name' => $sql[0]->Customer_Name, 'Invoice_ID' => $sql[0]->Invoice_ID, 'Invoice_Date' => $sql[0]->Invoice_Date, 'Recieved_Amount' => $sql[0]->Recieved_Amount, 'Followup_Date' => $sql[0]->Followup_Date));
        }

        return response()->json(['success' => true, 'FOLLOWUP Count' => $rows], 200);
    }
}
