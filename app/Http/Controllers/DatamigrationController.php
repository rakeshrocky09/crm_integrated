<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class DatamigrationController extends Controller
{
	public function uploadExcel(Request $request)
	{
		$upload_dir = "excels/";
		if (isset($_FILES["excelFile"])) { // it is recommended to check file type and size here
			if ($_FILES["excelFile"]["error"] > 0) {
				echo "Error: " . $_FILES["file"]["error"] . "<br>";
			} else {
				move_uploaded_file($_FILES["excelFile"]["tmp_name"], $upload_dir . $_FILES["excelFile"]["name"]);
				echo $_FILES["excelFile"]["name"];
			}
		}
	}

	public function addCustomer(Request $request)
	{
		$input = $request->all();
		$username = Auth::user()->username;
		$status = '1';
		$cus_const = "CUS";
		//get company name
		$select_company = DB::table('user_security')
			->select('company_name')
			->where('username', $username)
			->get();


		$data = [];

		foreach ($input as $key => $value) {

			if (isset($value['Customer_Branch_Name'])) {
				$CustomerBranchName = $value['Customer_Branch_Name'];
			} else {
				$CustomerBranchName = '';
			}
			if (isset($value['Customer_Name'])) {
				$CustomerName = $value['Customer_Name'];
			} else {
				$CustomerName = '';
			}
			if (isset($value['Customer_Email_Id'])) {
				$CustomerEmailId = $value['Customer_Email_Id'];
			} else {
				$CustomerEmailId = '';
			}
			if (isset($value['Customer_Phone_No'])) {
				$CustomerPhoneNo = $value['Customer_Phone_No'];
			} else {
				$CustomerPhoneNo = '';
			}
			if (isset($value['Customer_Alternate_Phone_No'])) {
				$CustomerAlternatePhoneNo = $value['Customer_Alternate_Phone_No'];
			} else {
				$CustomerAlternatePhoneNo = '';
			}
			if (isset($value['Primary_Contact_Name'])) {
				$PrimaryContactName = $value['Primary_Contact_Name'];
			} else {
				$PrimaryContactName = '';
			}
			if (isset($value['Primary_Contact_Email_Id'])) {
				$PrimaryContactEmailId = $value['Primary_Contact_Email_Id'];
			} else {
				$PrimaryContactEmailId = '';
			}
			if (isset($value['Primary_Contact_Phone_No'])) {
				$PrimaryContactPhoneNo = $value['Primary_Contact_Phone_No'];
			} else {
				$PrimaryContactPhoneNo = '';
			}
			if (isset($value['Customer_Address'])) {
				$CustomerAddress = $value['Customer_Address'];
			} else {
				$CustomerAddress = '';
			}
			if (isset($value['City'])) {
				$City = $value['City'];
			} else {
				$City = '';
			}
			if (isset($value['State'])) {
				$State = $value['State'];
			} else {
				$State = '';
			}
			if (isset($value['Pincode'])) {
				$Pincode = $value['Pincode'];
			} else {
				$Pincode = '';
			}
			if (isset($value['Customer_Type'])) {
				$CustomerType = $value['Customer_Type'];
			} else {
				$CustomerType = '';
			}
			$cus_phone = $CustomerPhoneNo;
			$cus_alterphone = $CustomerAlternatePhoneNo;
			$cus_prim_phone = $PrimaryContactPhoneNo;
			$customer_phoneno = floatval($cus_phone);
			$customer_alterphoneno = floatval($cus_alterphone);
			$customer_primary_phoneno = floatval($cus_prim_phone);

			$get_cusid = DB::table('customer_detail')
				->select('cus_id')
				->where('company_name', $select_company[0]->company_name)
				->orderByDesc('cus_id')
				->limit(1)
				->get();
			$cus_id  = $get_cusid[0]->cus_id;
			$add_cus_id = $cus_id + 1;

			$invoice_items_insert_sql = DB::table('customer_detail')
				->insert([
					'company_name' => $select_company[0]->company_name,
					'username' => $username,
					'cus_branch_name' => $CustomerBranchName,
					'cus_const' => $cus_const,
					'cus_id' => $add_cus_id,
					'customer_name' => $CustomerName,
					'cus_email_id' => $CustomerEmailId,
					'cus_phone_no' => $customer_phoneno,
					'cus_alter_phone' => $customer_alterphoneno,
					'primary_contact_name' => $PrimaryContactName,
					'primary_contact_email' => $PrimaryContactEmailId,
					'primary_contact_phoneno' => $customer_primary_phoneno,
					'cus_address' => $CustomerAddress,
					'cus_city' => $City,
					'cus_state' => $State,
					'cus_pincode' => $Pincode,
					'cus_type' => $CustomerType,
					'cus_status' => $status,
					'Customer_Create_Date' =>  date('Y-m-d'),
				]);
			$data[$key] = $value;
		}
		if (!($data == null)) {
			return response()->json('Customer Success');
		} else {
			return response()->json('Error');
		}
	}

	public function addEmployee(Request $request)
	{
		$input = $request->all();
		$username = Auth::user()->username;
		$status = '1';
		$emp_const = "EMP";
		//get company name
		$select_company = DB::table('user_security')
			->select('company_name')
			->where('username', $username)
			->get();


		$data = [];

		foreach ($input as $key => $value) {

			if (isset($value['Employee_Firstname'])) {
				$EmployeeFirstname = $value['Employee_Firstname'];
			} else {
				$EmployeeFirstname = '';
			}
			if (isset($value['Employee_Lastname'])) {
				$EmployeeLastname = $value['Employee_Lastname'];
			} else {
				$EmployeeLastname = '';
			}
			if (isset($value['Employee_Role'])) {
				$EmployeeRole = $value['Employee_Role'];
			} else {
				$EmployeeRole = '';
			}
			if (isset($value['Employee_Branch_Name'])) {
				$EmployeeBranchName = $value['Employee_Branch_Name'];
			} else {
				$EmployeeBranchName = '';
			}
			if (isset($value['Employee_Reports_To'])) {
				$EmployeeReportsTo = $value['Employee_Reports_To'];
			} else {
				$EmployeeReportsTo = '';
			}
			if (isset($value['Employee_Phone_No'])) {
				$EmployeePhoneNo = $value['Employee_Phone_No'];
			} else {
				$EmployeePhoneNo = '';
			}
			if (isset($value['Alternate_Phone_No'])) {
				$AlternatePhoneNo = $value['Alternate_Phone_No'];
			} else {
				$AlternatePhoneNo = '';
			}
			if (isset($value['Employee_Address'])) {
				$EmployeeAddress = $value['Employee_Address'];
			} else {
				$EmployeeAddress = '';
			}
			if (isset($value['Employee_City'])) {
				$EmployeeCity = $value['Employee_City'];
			} else {
				$EmployeeCity = '';
			}
			if (isset($value['Employee_Pincode'])) {
				$EmployeePincode = $value['Employee_Pincode'];
			} else {
				$EmployeePincode = '';
			}
			if (isset($value['Employee_State'])) {
				$EmployeeState = $value['Employee_State'];
			} else {
				$EmployeeState = '';
			}
			if (isset($value['Employee_Aadhar_No'])) {
				$EmployeeAadharNo = $value['Employee_Aadhar_No'];
			} else {
				$EmployeeAadharNo = '';
			}
			if (isset($value['Employee_Pan_No'])) {
				$EmployeePanNo = $value['Employee_Pan_No'];
			} else {
				$EmployeePanNo = '';
			}
			if (isset($value['Employee_Joining_Date'])) {
				$EmployeeJoiningDate = $value['Employee_Joining_Date'];
			} else {
				$EmployeeJoiningDate = '';
			}
			if (isset($value['Employee_Resignation_Date'])) {
				$EmployeeResignationDate = $value['Employee_Resignation_Date'];
			} else {
				$EmployeeResignationDate = '';
			}
			$emp_phone = $EmployeePhoneNo;
			$emp_alterphone = $AlternatePhoneNo;
			$employee_phone = floatval($emp_phone);
			$employee_alter_phone = floatval($emp_alterphone);
			$aadhar = $EmployeeAadharNo;
			$emp_aadharno = floatval($aadhar);

			//check if already emp exist
			$select_sql = DB::table('emp_details')
				->where('emp_phone_no', $employee_phone)
				->where('company_name', $select_company[0]->company_name)
				->get();

			if (count($select_sql) == 0) {

				$sql = DB::table('emp_details')
					->insert([
						'company_name' => $select_company[0]->company_name,
						'username' => $username,
						'emp_branch_name' => $EmployeeBranchName,
						'emp_const' => $emp_const,
						'emp_firstname' => $EmployeeFirstname,
						'emp_lastname' => $EmployeeLastname,
						'emp_role' => $EmployeeRole,
						'emp_reports_to' => $EmployeeReportsTo,
						'emp_phone_no' => $employee_phone,
						'alter_phone_no' => $employee_alter_phone,
						'emp_address' => $EmployeeAddress,
						'emp_city' => $EmployeeCity,
						'emp_pincode' => $EmployeePincode,
						'emp_state' => $EmployeeState,
						'emp_aadhar_no' => $emp_aadharno,
						'emp_pan_no' => $EmployeePanNo,
						'emp_join_date' => $EmployeeJoiningDate,
						'emp_resign_date' => $EmployeeResignationDate,
						'emp_status' => $status,
					]);
			}

			$data[$key] = $value;
		}
		if (!($data == null)) {
			return response()->json('Employee Success');
		} else {
			return response()->json('Error');
		}
	}

	public function addProducts(Request $request)
	{
		$input = $request->all();
		$username = Auth::user()->username;
		$product_const = "PROD";
		$status = '1';
		//get company name
		$select_company = DB::table('user_security')
			->select('company_name')
			->where('username', $username)
			->get();

		$data = [];

		foreach ($input as $key => $value) {

			if (isset($value['Product_Branch_Name'])) {
				$ProductBranchName = $value['Product_Branch_Name'];
			} else {
				$ProductBranchName = '';
			}
			if (isset($value['Product_HSN_SAC_CodeNo'])) {
				$ProductHSNSACCodeNo = $value['Product_HSN_SAC_CodeNo'];
			} else {
				$ProductHSNSACCodeNo = '';
			}
			if (isset($value['Product_Name'])) {
				$ProductName = $value['Product_Name'];
			} else {
				$ProductName = '';
			}
			if (isset($value['Product_Brand_Name'])) {
				$ProductBrandName = $value['Product_Brand_Name'];
			} else {
				$ProductBrandName = '';
			}
			if (isset($value['Manufacturer_Name'])) {
				$ManufacturerName = $value['Manufacturer_Name'];
			} else {
				$ManufacturerName = '';
			}
			if (isset($value['Mfg_Part_No'])) {
				$MfgPartNo = $value['Mfg_Part_No'];
			} else {
				$MfgPartNo = '';
			}
			if (isset($value['Product_Description'])) {
				$ProductDescription = $value['Product_Description'];
			} else {
				$ProductDescription = '';
			}
			if (isset($value['Product_Color '])) {
				$ProductColor = $value['Product_Color '];
			} else {
				$ProductColor = '';
			}
			if (isset($value['Product_Model_Name'])) {
				$ProductModel = $value['Product_Model_Name'];
			} else {
				$ProductModel = '';
			}
			if (isset($value['Product_Weight'])) {
				$ProductWeight = $value['Product_Weight'];
			} else {
				$ProductWeight = '';
			}
			if (isset($value['Product_Life_Time'])) {
				$ProductLifeTime = $value['Product_Life_Time'];
			} else {
				$ProductLifeTime = '';
			}
			if (isset($value['Product_Stock_Unit'])) {
				$ProductStockUnit = $value['Product_Stock_Unit'];
			} else {
				$ProductStockUnit = '';
			}
			if (isset($value['Product_Price'])) {
				$ProductPrice = $value['Product_Price'];
			} else {
				$ProductPrice = '';
			}
			if (isset($value['Product_Total_Hours'])) {
				$ProductTotalHours = $value['Product_Total_Hours'];
			} else {
				$ProductTotalHours = '';
			}
			if (isset($value['Product_Warranty'])) {
				$ProductWarranty = $value['Product_Warranty'];
			} else {
				$ProductWarranty = '';
			}
			if (isset($value['Product_Service_Charges'])) {
				$ProductServiceCharges = $value['Product_Service_Charges'];
			} else {
				$ProductServiceCharges = '';
			}
			if (isset($value['Product_Installation_Charge'])) {
				$ProductInstallationCharge = $value['Product_Installation_Charge'];
			} else {
				$ProductInstallationCharge = '';
			}
			if (isset($value['Product_Visit_charge'])) {
				$visitcharge = $value['Product_Visit_charge'];
			} else {
				$visitcharge = '';
			}

			//check if already product exist
			$select_sql = DB::table('product_details')
				->where('product_name', $ProductName)
				->where('product_brand_name', $ProductBrandName)
				->where('username', $username)
				->where('product_status', '=', '1')
				->get();

			if (count($select_sql) == 0) {

				$sql = DB::table('product_details')
					->insert([
						'company_name' => $select_company[0]->company_name,
						'username' => $username,
						'product_branch_name' => $ProductBranchName,
						'product_const' => $product_const,
						'product_code' => $ProductHSNSACCodeNo,
						'product_name' => $ProductName,
						'product_brand_name' => $ProductBrandName,
						'product_manufacturer_name' => $ManufacturerName,
						'product_part_num' => $MfgPartNo,
						'product_description' => $ProductDescription,
						'product_color' => $ProductColor,
						'product_model_name' => $ProductModel,
						'product_weight' => $ProductWeight,
						'product_life_time' => $ProductLifeTime,
						'product_stock_unit' => $ProductStockUnit,
						'product_price' => $ProductPrice,
						'product_tat_hrs' => $ProductTotalHours,
						'product_warranty_months' => $ProductWarranty,
						'product_service_charge' => $ProductServiceCharges,
						'product_installation_charge' => $ProductInstallationCharge,
						'product_visit_charge' => $visitcharge,
						'product_status' => $status,
						'Created_Date' => date('Y-m-d')
					]);

				$data[$key] = $value;
			} else {
				return response()->json('exist');
			}
		}
		if (!($data == null)) {
			return response()->json('Product Success');
		} else {
			return response()->json('Error');
		}
	}

	public function addSpares(Request $request)
	{
		$input = $request->all();
		$username = Auth::user()->username;
		$spare_const = "SP";
		$status = '1';
		//get company name
		$select_company = DB::table('user_security')
			->select('company_name')
			->where('username', $username)
			->get();


		$data = [];

		foreach ($input as $key => $value) {
			if (isset($value['Spare_Brand_Name'])) {
				$SpareBrandName = $value['Spare_Brand_Name'];
			} else {
				$SpareBrandName = '';
			}
			if (isset($value['Spare_Model_Name'])) {
				$SpareModelName = $value['Spare_Model_Name'];
			} else {
				$SpareModelName = '';
			}
			if (isset($value['Spare_Branch_Name'])) {
				$SpareBranchName = $value['Spare_Branch_Name'];
			} else {
				$SpareBranchName = '';
			}
			if (isset($value['Spare_HSN_SAC_CodeNo'])) {
				$SpareHSNSACCodeNo = $value['Spare_HSN_SAC_CodeNo'];
			} else {
				$SpareHSNSACCodeNo = '';
			}
			if (isset($value['Spare_Parts_Name'])) {
				$SparePartsName = $value['Spare_Parts_Name'];
			} else {
				$SparePartsName = '';
			}
			if (isset($value['Spare_Parts_Unit'])) {
				$SparePartsUnit = $value['Spare_Parts_Unit'];
			} else {
				$SparePartsUnit = '';
			}
			if (isset($value['Spare_Unit_Price'])) {
				$SpareUnitPrice = $value['Spare_Unit_Price'];
			} else {
				$SpareUnitPrice = '';
			}
			if (isset($value['Spare_MSL'])) {
				$SpareMSL = $value['Spare_MSL'];
			} else {
				$SpareMSL = '';
			}
			if (isset($value['Spare_Chargeable'])) {
				$SpareChargeable = $value['Spare_Chargeable'];
			} else {
				$SpareChargeable = '';
			}
			if (isset($value['Spare_Stock_Unit'])) {
				$SpareStockUnit = $value['Spare_Stock_Unit'];
			} else {
				$SpareStockUnit = '';
			}
			if (isset($value['Spare_Manufacturer'])) {
				$SpareManufacturer = $value['Spare_Manufacturer'];
			} else {
				$SpareManufacturer = '';
			}
			if (isset($value['Spare_Mfg_Part_No'])) {
				$SpareMfgPartNo = $value['Spare_Mfg_Part_No'];
			} else {
				$SpareMfgPartNo = '';
			}

			//check if already product exist
			$select_sql = DB::table('spare_details')
				->where('spare_brand_name', $SpareBrandName)
				->where('spare_model_name', $SpareModelName)
				->where('company_name', $select_company[0]->company_name)
				->where('spare_status', '=', '1')
				->get();

			if (count($select_sql) == 0) {

				$sql = DB::table('spare_details')
					->insert([
						'company_name' => $select_company[0]->company_name,
						'username' => $username,
						'spare_const' => $spare_const,
						'spare_branch_name' => $SpareBranchName,
						'spare_brand_name' => $SpareBrandName,
						'spare_model_name' => $SpareModelName,
						'spare_code_name' => $SpareHSNSACCodeNo,
						'spare_parts_name' => $SparePartsName,
						'spare_parts_unit' => $SparePartsUnit,
						'spare_unit_price' => $SpareUnitPrice,
						'spare_msl' => $SpareMSL,
						'spare_chargeable' => $SpareChargeable,
						'spare_stock_unit' => $SpareStockUnit,
						'spare_manufacturer' => $SpareManufacturer,
						'spare_mfg_part_no' => $SpareMfgPartNo,
						'spare_status' => $status,
						'Created_Date' => date('Y-m-d')
					]);

				$data[$key] = $value;
			} else {
				return response()->json('exist');
			}
		}
		if (!($data == null)) {
			return response()->json('Spares Success');
		} else {
			return response()->json('Error');
		}
	}

	public function addConsumable(Request $request)
	{
		$input = $request->all();
		$username = Auth::user()->username;
		$consumable_const = "CONSUM";
		$status = '1';
		//get company name
		$select_company = DB::table('user_security')
			->select('company_name')
			->where('username', $username)
			->get();


		$data = [];

		foreach ($input as $key => $value) {
			if (isset($value['Consumable_Brand_Name'])) {
				$ConsumableBrandName = $value['Consumable_Brand_Name'];
			} else {
				$ConsumableBrandName = '';
			}
			if (isset($value['Consumable_Model_Name'])) {
				$ConsumableModelName = $value['Consumable_Model_Name'];
			} else {
				$ConsumableModelName = '';
			}
			if (isset($value['Consumable_Branch_Name'])) {
				$ConsumableBranchName = $value['Consumable_Branch_Name'];
			} else {
				$ConsumableBranchName = '';
			}
			if (isset($value['Consumable_Code_Name'])) {
				$ConsumableCodeName = $value['Consumable_Code_Name'];
			} else {
				$ConsumableCodeName = '';
			}
			if (isset($value['Consumable_HSN_SAC_CodeNo'])) {
				$ConsumableHSNSACCodeNo = $value['Consumable_HSN_SAC_CodeNo'];
			} else {
				$ConsumableHSNSACCodeNo = '';
			}
			if (isset($value['Consumable_Name'])) {
				$ConsumableName = $value['Consumable_Name'];
			} else {
				$ConsumableName = '';
			}
			if (isset($value['Consumable_Unit'])) {
				$ConsumableUnit = $value['Consumable_Unit'];
			} else {
				$ConsumableUnit = '';
			}
			if (isset($value['Consumable_Unit_Price'])) {
				$ConsumableUnitPrice = $value['Consumable_Unit_Price'];
			} else {
				$ConsumableUnitPrice = '';
			}
			if (isset($value['Consumable_MSL'])) {
				$ConsumableMSL = $value['Consumable_MSL'];
			} else {
				$ConsumableMSL = '';
			}
			if (isset($value['Consumable_Chargeable'])) {
				$ConsumableChargeable = $value['Consumable_Chargeable'];
			} else {
				$ConsumableChargeable = '';
			}
			if (isset($value['Consumable_Manufacturer'])) {
				$ConsumableManufacturer = $value['Consumable_Manufacturer'];
			} else {
				$ConsumableManufacturer = '';
			}
			if (isset($value['Consumable_Mfg_Part_No'])) {
				$ConsumableMfgPartNo = $value['Consumable_Mfg_Part_No'];
			} else {
				$ConsumableMfgPartNo = '';
			}
			if (isset($value['Consumable_Copies'])) {
				$ConsumableCopies = $value['Consumable_Copies'];
			} else {
				$ConsumableCopies = '';
			}

			//check if already product exist
			$select_sql = DB::table('consumable_details')
				->where('consumable_brand_name', $ConsumableBrandName)
				->where('consumable_model_name', $ConsumableModelName)
				->where('company_name', $select_company[0]->company_name)
				->where('consumable_status', '=', '1')
				->get();

			if (count($select_sql) == 0) {

				$sql = DB::table('consumable_details')
					->insert([
						'company_name' => $select_company[0]->company_name,
						'username' => $username,
						'consumable_branch_name' => $ConsumableBranchName,
						'consumable_brand_name' => $ConsumableBrandName,
						'consumable_model_name' => $ConsumableModelName,
						'consumable_const' => $consumable_const,
						'consumable_code_name' => $ConsumableCodeName,
						'consumable_name' => $ConsumableName,
						'consumable_unit' => $ConsumableUnit,
						'consumable_unit_price' => $ConsumableUnitPrice,
						'consumable_msl' => $ConsumableMSL,
						'consumable_chargeable' => $ConsumableChargeable,
						'consumable_hsn' => $ConsumableHSNSACCodeNo,
						'consumable_manufacturer' => $ConsumableManufacturer,
						'consumable_mfg_part_no' => $ConsumableMfgPartNo,
						'consumable_copies' => $ConsumableCopies,
						'consumable_status' => $status,
						'Created_Date' => date('Y-m-d')
					]);

				$data[$key] = $value;
			} else {
				return response()->json('exist');
			}
		}
		if (!($data == null)) {
			return response()->json('Consumables Success');
		} else {
			return response()->json('Error');
		}
	}

	public function addCustomerProducts(Request $request)
	{
		$input = $request->all();
		$username = Auth::user()->username;
		$status = '1';
		$cus_const = "CUS";
		//get company name
		$select_company = DB::table('user_security')
			->select('company_name')
			->where('username', $username)
			->get();


		$data = [];

		foreach ($input as $key => $value) {
			if (isset($value['Branch_Name'])) {
				$BranchName = $value['Branch_Name'];
			} else {
				$BranchName = '';
			}
			if (isset($value['Customer_Name'])) {
				$CustomerName = $value['Customer_Name'];
			} else {
				$CustomerName = '';
			}
			if (isset($value['Customer_Email_id'])) {
				$CustomerEmailid = $value['Customer_Email_id'];
			} else {
				$CustomerEmailid = '';
			}
			if (isset($value['Customer_Phone_No'])) {
				$CustomerPhoneNo = $value['Customer_Phone_No'];
			} else {
				$CustomerPhoneNo = '';
			}
			if (isset($value['Customer_Address'])) {
				$CustomerAddress = $value['Customer_Address'];
			} else {
				$CustomerAddress = '';
			}
			if (isset($value['Customer_City'])) {
				$CustomerCity = $value['Customer_City'];
			} else {
				$CustomerCity = '';
			}
			if (isset($value['Customer_State'])) {
				$CustomerState = $value['Customer_State'];
			} else {
				$CustomerState = '';
			}
			if (isset($value['Customer_Pincode'])) {
				$CustomerPincode = $value['Customer_Pincode'];
			} else {
				$CustomerPincode = '';
			}
			if (isset($value['Product_Id'])) {
				$ProductId = $value['Product_Id'];
			} else {
				$ProductId = '';
			}
			if (isset($value['Product_Code_No'])) {
				$ProductCodeNo = $value['Product_Code_No'];
			} else {
				$ProductCodeNo = '';
			}
			if (isset($value['Product_Name'])) {
				$ProductName = $value['Product_Name'];
			} else {
				$ProductName = '';
			}
			if (isset($value['Brand_Name'])) {
				$BrandName = $value['Brand_Name'];
			} else {
				$BrandName = '';
			}
			if (isset($value['Order_date'])) {
				$Orderdate = $value['Order_date'];
			} else {
				$Orderdate = '';
			}
			if (isset($value['Contract_Id'])) {
				$ContractId = $value['Contract_Id'];
			} else {
				$ContractId = '';
			}
			if (isset($value['Ticket_Id'])) {
				$TicketId = $value['Ticket_Id'];
			} else {
				$TicketId = '';
			}
			if (isset($value['Machine_Serial_No'])) {
				$MachineSerialNo = $value['Machine_Serial_No'];
			} else {
				$MachineSerialNo = '';
			}
			if (isset($value['Serial_Key_No'])) {
				$SerialKeyNo = $value['Serial_Key_No'];
			} else {
				$SerialKeyNo = '';
			}
			if (isset($value['Contract_Type'])) {
				$ContractType = $value['Contract_Type'];
			} else {
				$ContractType = '';
			}
			if (isset($value['Section'])) {
				$Section = $value['Section'];
			} else {
				$Section = '';
			}
			$cus_phone = $CustomerPhoneNo;
			$customer_phoneno = floatval($cus_phone);

			//check if already customer_detail exist
			$select_sql = DB::table('customer_detail')
				->where('cus_phone_no', $customer_phoneno)
				->where('company_name', $select_company[0]->company_name)
				->get();

			if (count($select_sql) == 0) {

				$sql = DB::table('customer_detail')
					->insert([
						'company_name' => $select_company[0]->company_name,
						'username' => $username,
						'cus_branch_name' => $BranchName,
						'cus_const' => $cus_const,
						'customer_name' => $CustomerName,
						'cus_email_id' => $CustomerEmailid,
						'cus_phone_no' => $customer_phoneno,
						'cus_address' => $CustomerAddress,
						'cus_city' => $CustomerCity,
						'cus_state' => $CustomerState,
						'cus_pincode' => $CustomerPincode,
						'cus_status' => $status,
					]);

				$select_customerid = DB::table('customer_detail')
					->where('cus_phone_no', $customer_phoneno)
					->where('company_name', $select_company[0]->company_name)
					->select('cus_id')
					->get();
				$customer_id = $select_customerid[0]->cus_id;

				$insert_cus_prod = DB::table('cus_prod_details')
					->insert([
						'company_name' => $select_company[0]->company_name,
						'username' => $username,
						'cus_branch_name' => $BranchName,
						'customer_id' => $customer_id,
						'product_id' => $ProductId,
						'Machine_Serial_No' => $MachineSerialNo,
						'Serial_Key_No' => $SerialKeyNo,
						'Contract_Type' => $ContractType,
						'section' => $Section,
						//'customer_name' => $CustomerName,
						//'cus_phoneno' => $customer_phoneno,
						//'product_code' => $ProductCodeNo,
						//'product_name' => $ProductName,
						//'product_brand_name' => $BrandName,
						'order_date' => $Orderdate,
						//'contract_id' => $ContractId,
						//'ticket_id' => $TicketId,
						'Flag' => 1,
						'Created_Date' => date('Y-m-d')
					]);


				$data[$key] = 'CustomerProduct Success';
			} else {
				$select_customerid = DB::table('customer_detail')
					->where('cus_phone_no', $customer_phoneno)
					->where('company_name', $select_company[0]->company_name)
					->select('cus_id')
					->get();
				$customer_id = $select_customerid[0]->cus_id;

				$insert_cus_prod = DB::table('cus_prod_details')
					->insert([
						'company_name' => $select_company[0]->company_name,
						'username' => $username,
						'cus_branch_name' => $BranchName,
						'customer_id' => $customer_id,
						'product_id' => $ProductId,
						'Machine_Serial_No' => $MachineSerialNo,
						'Serial_Key_No' => $SerialKeyNo,
						'Contract_Type' => $ContractType,
						'section' => $Section,
						'order_date' => $Orderdate,
						'Flag' => 1,
						'Created_Date' => date('Y-m-d')
					]);

				$data[$key] = 'CustomerProduct Success';
			}
		}
		if (!($data == null)) {
			return response()->json($data);
		} else {
			return response()->json('Error');
		}
	}
}
