<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
	public function getReportData(Request $request)
	{
		/* 		$type = DB::table('ticket_type')
			->select('ticket_type')
			->where('flag', 1)
			->get(); */

		/* 		if (count($type) >= 1) {
			return response()->json($type);
		} else {
			return response()->json(['success' => false, 'message' => "No Data Found"], 200);
		} */
		$input = $request->all();
		$type = $input['type'];

		switch ($type) {
			case 'customer':
				return $this->getCustomerData($input);
				break;
			case 'ticket':
				return $this->getTicketData($input);
				break;
			case 'contract':
				return $this->getContractData($input);
				break;
			case 'invoice':
				return $this->getInvoiceData($input);
				break;
			case 'dc':
				return $this->getChallanData($input);
				break;
			case 'pm':
				return $this->getPMSChallanData($input);
				break;
			case 'enquiry':
				return $this->getEnquiryData($input);
				break;
			case 'products':
				return $this->getProductData($input);
				break;
			case 'spares':
				return $this->getSpareData($input);
				break;
			case 'consumables':
				return $this->getConsumableData($input);
				break;
			case 'services':
				return $this->getServiceData($input);
				break;
			case 'employee':
				return $this->getEmployeeData($input);
				break;
			case 'employee':
				return $this->getMachineData($input);
				break;
			default:
				return response()->json("default data");
		}
	}

	public function getCustomerData($input)
	{
		return response()->json(['success' => false, 'message' => "success getCustomerData"], 200);
	}
	//Returns data if Ticket Category is selected 
	public function getTicketData($input)
	{
		$category = $input['category'];
		switch ($category) {
			case 'open':
				return $this->getTicketOpenData($input);
				break;
			case 'closed':
				return $this->getTicketClosedData($input);
				break;
			case 'broken':
				return $this->getTicketPendingData($input);
				break;
			default:
				return response()->json("default ticket data");
		}
	}

	public function getContractData($input)
	{
		$category = $input['category'];
		switch ($category) {
			case 'warranty':
				return $this->getWarrantyData($input);
				break;
			case 'amc':
				return $this->getAMCData($input);
				break;
			case '4c':
				return $this->get4cData($input);
				break;
			case 'rental':
				return $this->getRentalData($input);
				break;
			default:
				return response()->json("default ticket data");
		}
	}
	public function getInvoiceData($input)
	{
		return response()->json(['success' => false, 'message' => "success getInvoiceData"], 200);
	}
	public function getChallanData($input)
	{
		return response()->json(['success' => false, 'message' => "success getChallanData"], 200);
	}
	public function getPMSChallanData($input)
	{
		return response()->json(['success' => false, 'message' => "success getPMSChallanData"], 200);
	}
	public function getEnquiryData($input)
	{
		return response()->json(['success' => false, 'message' => "success getEnquiryData"], 200);
	}
	public function getProductData($input)
	{
		return response()->json(['success' => false, 'message' => "success getProductData"], 200);
	}
	public function getSpareData($input)
	{
		return response()->json(['success' => false, 'message' => "success getSpareData"], 200);
	}
	public function getConsumableData($input)
	{
		return response()->json(['success' => false, 'message' => "success getConsumableData"], 200);
	}
	public function getServiceData($input)
	{
		return response()->json(['success' => false, 'message' => "success getServiceData"], 200);
	}
	public function getEmployeeData($input)
	{
		return response()->json(['success' => false, 'message' => "success getEmployeeData"], 200);
	}
	public function getMachineData($input)
	{
		return response()->json(['success' => false, 'message' => "success getMachineData"], 200);
	}

	//Returns data form TICKET-OPEN type-category selection.
	public function getTicketOpenData($input)
	{
		$username = Auth::user()->username;
		$fromDate = $input['fromDate'];
		$fromDateconvert = date("Y-m-d", strtotime($fromDate));
		$toDate = $input['toDate'];
		$toDateconvert = date("Y-m-d", strtotime($toDate));

		if ((empty($fromDate)) && (empty($toDate))) {
			$query = DB::table('ticket_details')
				->leftJoin('tick_details', 'tick_details.Ticket_id', '=', 'ticket_details.ticket_id')
				->leftJoin('cus_prod_details', 'cus_prod_details.ticket_id', '=', 'tick_details.ticket_id')
				->leftJoin('product_details', 'product_details.product_id', '=', 'cus_prod_details.product_id')
				->where('cus_prod_details.Flag', 1)
				->where('tick_details.Ticket_Status', 'open')
				->where('ticket_details.username', $username)
				->where('ticket_details.flag', 1)
				//->groupBy('tick_details.ticket_id')
				->get();
			return response()->json($query);
		} else {
			$query = DB::table('ticket_details')
				->leftJoin('tick_details', 'tick_details.Ticket_id', '=', 'ticket_details.ticket_id')
				->leftJoin('cus_prod_details', 'cus_prod_details.ticket_id', '=', 'tick_details.ticket_id')
				->leftJoin('product_details', 'product_details.product_id', '=', 'cus_prod_details.product_id')
				->where('cus_prod_details.Flag', 1)
				->where('tick_details.Ticket_Status', 'open')
				->whereBetween('tick_details.Ticket_Created_Date', [$fromDateconvert, $toDateconvert])
				->where('ticket_details.username', $username)
				->where('ticket_details.flag', 1)
				//->groupBy('tick_details.ticket_id')
				->get();
			return response()->json($query);
		}
	}
	//Returns data form TICKET-CLOSED type-category selection.
	public function getTicketClosedData($input)
	{
		$username = Auth::user()->username;
		$fromDate = $input['fromDate'];
		$fromDateconvert = date("Y-m-d", strtotime($fromDate));
		$toDate = $input['toDate'];
		$toDateconvert = date("Y-m-d", strtotime($toDate));

		if ((empty($fromDate)) && (empty($toDate))) {
			/* 		$query = "select cus.Contract_Type as contract,tid.delivery_address_1 as address, tid.Caller_Sender_Name as name,tid.Caller_Sender_Phone as phone,tid.customer_name customer_name, cus.Machine_Serial_No machine_id, prod.product_model_name product_model,td.Ticket_Type ticket_type, td.Assign_Technician_Name assign_name,td.Problem_Description defect_code,td.Ticket_Closed_Date created_date, td.Ticket_Status ticket_status, td.ticket_priority ticket_priority from ticket_details tid, tick_details td, cus_prod_details cus, product_details prod where prod.product_id = cus.product_id and cus.customer_id=tid.cus_id and cus.Machine_Serial_No=tid.Machine_Serial_No and tid.username = '$inputUser' and tid.ticket_id = td.Ticket_id and td.Ticket_Status = 'close' and cus.Flag='1' and tid.flag='1'  group by td.ticket_id";
 			*/

			$query = DB::table('ticket_details')
				->leftJoin('tick_details', 'tick_details.Ticket_id', '=', 'ticket_details.ticket_id')
				->leftJoin('cus_prod_details', 'cus_prod_details.Machine_Serial_No', '=', 'ticket_details.Machine_Serial_No')
				->leftJoin('product_details', 'product_details.product_id', '=', 'cus_prod_details.product_id')
				->where('cus_prod_details.Flag', 1)
				->where('tick_details.Ticket_Status', 'close')
				->where('ticket_details.username', $username)
				->where('ticket_details.flag', 1)
				//->where('cus_prod_details.customer_id', '=', 'ticket_details.cus_id')
				->select(
					'cus_prod_details.Contract_Type as contract',
					'cus_prod_details.Machine_Serial_No as machine_id',
					'ticket_details.delivery_address_1 as address',
					'ticket_details.Caller_Sender_Name as name',
					'ticket_details.Caller_Sender_Phone as phone',
					'ticket_details.customer_name',
					'product_details.product_model_name as product_model',
					'tick_details.Ticket_Type as ticket_type',
					'tick_details.Assign_Technician_Name as assign_name',
					'tick_details.Problem_Description as defect_code',
					'tick_details.Ticket_Closed_Date as created_date',
					'tick_details.Ticket_Status as ticket_status',
					'tick_details.ticket_priority as ticket_priority',
				)
				->get();
			return response()->json($query);
		} else {

			$query = DB::table('ticket_details')
				->leftJoin('tick_details', 'tick_details.Ticket_id', '=', 'ticket_details.ticket_id')
				->leftJoin('cus_prod_details', 'cus_prod_details.Machine_Serial_No', '=', 'ticket_details.Machine_Serial_No')
				->leftJoin('product_details', 'product_details.product_id', '=', 'cus_prod_details.product_id')
				->where('cus_prod_details.Flag', 1)
				//->where('cus_prod_details.customer_id', '=', 'ticket_details.cus_id')
				->where('tick_details.Ticket_Status', 'close')
				->whereBetween('tick_details.Ticket_Closed_Date', [$fromDateconvert, $toDateconvert])
				->where('ticket_details.username', $username)
				->where('ticket_details.flag', 1)
				->select(
					'cus_prod_details.Contract_Type as contract',
					'cus_prod_details.Machine_Serial_No as machine_id',
					'ticket_details.delivery_address_1 as address',
					'ticket_details.Caller_Sender_Name as name',
					'ticket_details.Caller_Sender_Phone as phone',
					'ticket_details.customer_name',
					'product_details.product_model_name as product_model',
					'tick_details.Ticket_Type as ticket_type',
					'tick_details.Assign_Technician_Name as assign_name',
					'tick_details.Problem_Description as defect_code',
					'tick_details.Ticket_Closed_Date as created_date',
					'tick_details.Ticket_Status as ticket_status',
					'tick_details.ticket_priority as ticket_priority',
				)
				->get();

			return response()->json($query);
		}
	}
	//Returns data form BROKEN-CALLS type-category selection.
	public function getTicketPendingData($input)
	{
		$username = Auth::user()->username;
		$fromDate = $input['fromDate'];
		$fromDateconvert = date("Y-m-d", strtotime($fromDate));
		$toDate = $input['toDate'];
		$toDateconvert = date("Y-m-d", strtotime($toDate));

		if ((empty($fromDate)) && (empty($toDate))) {
			$query = DB::table('ticket_details')
				->leftJoin('tick_details', 'tick_details.Ticket_id', '=', 'ticket_details.ticket_id')
				->leftJoin('cus_prod_details', 'cus_prod_details.ticket_id', '=', 'tick_details.ticket_id')
				->leftJoin('product_details', 'product_details.product_id', '=', 'cus_prod_details.product_id')
				->where('cus_prod_details.Flag', 1)
				->where('tick_details.Ticket_Status', 'pending')
				->where('ticket_details.username', $username)
				->where('ticket_details.flag', 1)
				->get();
			return response()->json($query);
		} else {
			$query = DB::table('ticket_details')
				->leftJoin('tick_details', 'tick_details.Ticket_id', '=', 'ticket_details.ticket_id')
				->leftJoin('cus_prod_details', 'cus_prod_details.ticket_id', '=', 'tick_details.ticket_id')
				->leftJoin('product_details', 'product_details.product_id', '=', 'cus_prod_details.product_id')
				->where('cus_prod_details.Flag', 1)
				->where('tick_details.Ticket_Status', 'pending')
				->whereBetween('tick_details.Ticket_Closed_Date', [$fromDateconvert, $toDateconvert])
				->where('ticket_details.username', $username)
				->where('ticket_details.flag', 1)
				->get();
			return response()->json($query);
		}
	}


	//Returns data form Contract warranty category selection.
	public function getWarrantyData($input)
	{
		$username = Auth::user()->username;
		$fromDate = $input['fromDate'];
		$fromDateconvert = date("Y-m-d", strtotime($fromDate));
		$toDate = $input['toDate'];
		$toDateconvert = date("Y-m-d", strtotime($toDate));

		if ((empty($fromDate)) && (empty($toDate))) {
			/* 
			 $query = "SELECT d.installation_date as date, b.Product_Ownership, a.Machine_Serial_No, a.Contract_ID, b.Contract_type, b.Start_Date, b. End_Date, b.Contract_Status, c.customer_name 
			 
			 FROM contract_details a, contract_history_details b, customer_detail c, cus_prod_details d WHERE d.contract_id = a.Contract_ID and a.Contract_ID=b.Contract_Id and a.Customer_Id=c.cus_id and b.Contract_Status in ('open','close') and a.Contract_Status in ('open','close') and b.Contract_type = 'warranty' and a.User_Name = '$inputUser'";
			*/

			$status =  array('open', 'pending');
			$query = DB::table('contract_details')
				//->leftJoin('tick_details', 'tick_details.Ticket_id', '=', 'ticket_details.ticket_id')
				//->leftJoin('cus_prod_details', 'cus_prod_details.ticket_id', '=', 'tick_details.ticket_id')
				//->leftJoin('product_details', 'product_details.product_id', '=', 'cus_prod_details.product_id')
				//->where('cus_prod_details.Flag', 1)
				//->where('tick_details.Ticket_Status', 'open')
				//->where('ticket_details.username', $username)
				//->where('ticket_details.flag', 1)
				//->groupBy('tick_details.ticket_id')
				->get();
			return response()->json($query);
		} else {
			$query = DB::table('ticket_details')
				->leftJoin('tick_details', 'tick_details.Ticket_id', '=', 'ticket_details.ticket_id')
				->leftJoin('cus_prod_details', 'cus_prod_details.ticket_id', '=', 'tick_details.ticket_id')
				->leftJoin('product_details', 'product_details.product_id', '=', 'cus_prod_details.product_id')
				->where('cus_prod_details.Flag', 1)
				->where('tick_details.Ticket_Status', 'open')
				->whereBetween('tick_details.Ticket_Created_Date', [$fromDateconvert, $toDateconvert])
				->where('ticket_details.username', $username)
				->where('ticket_details.flag', 1)
				//->groupBy('tick_details.ticket_id')
				->get();
			return response()->json($query);
		}
	}








	public function uploadExcel(Request $request)
	{
		return response()->json(['success' => false, 'message' => "success getInvoiceData"], 200);
	}
}
