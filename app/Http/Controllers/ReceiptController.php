<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class ReceiptController extends Controller
{
	public function getAllReceipts(Request $request)
	{
		/* $input = $request->all();
		$validator = Validator::make(
			$request->all(),
			[
				'username' => 'required'
			]
		);

		if ($validator->fails()) {
			$message = $validator->errors();
			return response()->json(['success' => false, 'message' => 'The given data was invalid.', 'errors' => $validator->errors()], 200);
		} */
		$select_company = DB::table('user_security')
			->select('company_name')
			->where('username', Auth::user()->username)
			->get();

		$sql = DB::select("SELECT max(Paid_Date) as date from receipt_item_details  group by Invoice_Id");
		$amount = array_column($sql, 'date');

		/* $sql = DB::select("SELECT max(Paid_Date) as date from receipt_item_details  group by Invoice_Id"); */

		$select = DB::table('receipt_item_details')
			->leftjoin('invoice_billing_details', 'invoice_billing_details.invoice_number', '=', 'receipt_item_details.invoice_number')
			->leftjoin('receipt_details', 'receipt_details.Invoice_Id', '=', 'receipt_item_details.Invoice_Id')
			->where('receipt_details.User_Name', Auth::user()->username)
			->leftjoin('customer_detail', 'customer_detail.cus_id', '=', 'receipt_details.Customer_Id')
			->get()
			->unique('Invoice_Id');

		/* $select_company = DB::table('user_security')
			->select('company_name')
			->where('username', $input['username'])
			->get(); */

		/* $receipt = DB::table('customer_detail')
			->where('cus_status', '=', '1')
			->where('company_name', $select_company[0]->company_name)
			->orderby('cus_id')
			->get(); */

		/* if (count($select) >= 1) {
			return response()->json(['success' => true, 'Branch Details' => $select], 200);
		} else {
			return response()->json(['success' => false, 'message' => "No Data Found"], 200);
		} */

		//echo json_encode($select);die;
		return view('viewreceipt', compact('select'));
	}
	public function getReceiptDetails(Request $request, $id)
	{
		$username = Auth::user()->username;
		$sql = DB::select("SELECT max(Paid_Date) as date from receipt_item_details  group by Invoice_Id");
		$amount = array_column($sql, 'date');

		$select = DB::table('receipt_details')
			->where('receipt_details.User_Name',  $username)
			->where('receipt_details.Invoice_Id',  $id)
			->leftJoin('receipt_item_details', 'receipt_item_details.Invoice_Id', '=', 'receipt_details.Invoice_Id')
			->leftjoin('customer_detail', 'customer_detail.cus_id', '=', 'receipt_details.Customer_Id')
			->get();
		//echo json_encode($select);die;
		return view('viewreceipts', compact('select'));
	}

	public function loadReceipt(Request $request, $id)
	{
		$username = Auth::user()->username;
		$select_company = DB::table('user_security')
			->select('company_name')
			->where('username', $username)
			->get();
		//loads modes of payment list
		$receipt_payment = DB::table('payment_mode')->get();

		$sql = DB::table('invoice_item_details')
			->leftjoin('invoice_billing_details', 'invoice_billing_details.Invoice_ID', '=', 'invoice_item_details.Invoice_ID')
			->where('invoice_billing_details.Invoice_ID', $id)
			->leftjoin('customer_detail', 'customer_detail.cus_id', '=', 'invoice_billing_details.Customer_Id')
			->where('invoice_billing_details.Company_Name', $select_company[0]->company_name)
			->limit(1)
			->get();

		$select_receipt = DB::table('invoice_item_details')
			->leftjoin('invoice_billing_details', 'invoice_billing_details.Invoice_ID', '=', 'invoice_item_details.Invoice_ID')
			->leftjoin('receipt_item_details', 'receipt_item_details.Invoice_ID', '=', 'invoice_billing_details.Invoice_ID')
			->where('invoice_billing_details.Invoice_ID', $id)
			->leftjoin('customer_detail', 'customer_detail.cus_id', '=', 'invoice_billing_details.Customer_Id')
			->where('invoice_billing_details.company_name', $select_company[0]->company_name)
			->orderByDesc('receipt_item_details.Receipt_Id')
			->limit(1)
			->get();

		$rows = array();
		array_push($rows, array('cus_branch_name' => $sql[0]->cus_branch_name, 'Item_Id' => $sql[0]->Item_Id, 'Customer_Id' => $sql[0]->Customer_Id, 'Invoice_Amount' => $sql[0]->Invoice_Amount, 'Invoice_Date' => $sql[0]->Invoice_Date, 'Invoice_Status' => $sql[0]->Invoice_Status, 'Paid_Date' => $select_receipt[0]->Paid_Date, 'Total_Paid_Amount' => $select_receipt[0]->Total_Paid_Amount, 'invoice_number' => $select_receipt[0]->invoice_number, 'invoice_id' => $select_receipt[0]->Invoice_ID));

		$cus = DB::table('customer_detail')
			->where('company_name', $select_company[0]->company_name)
			->where('cus_id', $sql[0]->Customer_Id)
			->get();

		/* $invoice = DB::table('invoice_item_details')
			->leftjoin('invoice_billing_details', 'invoice_billing_details.Invoice_ID', '=', 'invoice_item_details.Invoice_ID')
			->where('invoice_billing_details.Invoice_ID', $id)
			->where('invoice_billing_details.Company_Name', $select_company[0]->company_name)
			->limit(1)
			->get();
		$select_receipt = DB::table('invoice_item_details')
			->leftjoin('invoice_billing_details', 'invoice_billing_details.Invoice_ID', '=', 'invoice_item_details.Invoice_ID')
			->leftjoin('receipt_item_details', 'receipt_item_details.Invoice_ID', '=', 'invoice_billing_details.Invoice_ID')
			->leftjoin('customer_detail', 'customer_detail.cus_id', '=', 'invoice_billing_details.Customer_Id')
			->where('invoice_billing_details.Invoice_ID', $id)
			->where('invoice_billing_details.Company_Name', $select_company[0]->company_name)
			->orderByDesc('receipt_item_details.Receipt_Id')
			->limit(1)
			->get(); */

		foreach ($rows as $key => $value) {
			$data = $value;
		}
		//return response()->json($data);
		return view('addreceipt', compact('data', 'cus', 'receipt_payment'));
	}

	public function AddReceipt(Request $request)
	{
		$input = $request->all();
		$username = Auth::user()->username;
		$receipt_const = "RECEP";
		$receipt_amount_date = date("Y-m-d", strtotime($input['receipt_amount_date']));
		$receipt_pending_date = date("Y-m-d", strtotime($input['receipt_pending_date']));
		$invoice_date = date("Y-m-d", strtotime($input['receipt_invoice_date']));
		if ($input['receipt_amount_pending'] > 0.00) {
			$status = "open";
		} else {
			$status = "close";
		}
		//get company name
		$select_company = DB::table('user_security')
			->select('company_name')
			->where('username', $username)
			->get();
		//already exists or not
		$select_sql = DB::table('receipt_details')
			->where('Invoice_Id', $input['receipt_invoice_id'])
			->where('company_name', $select_company[0]->company_name)
			->get();
		$select_cus = DB::table('customer_detail')
			->where('cus_id', $input['receipt_cus_id'])
			->get();

		$get_paymentid = DB::table('payment_follow_up')
			->where('company_name', $select_company[0]->company_name)
			->select('Payment_Id')
			->orderby('Payment_Id', 'DESC')
			->limit(1)
			->get();
		$payment_id  = $get_paymentid[0]->Payment_Id;
		$add_payment_id = $payment_id + 1;
		$total_paid = $input['receipt_total_paid'] + $input['receipt_amount_paid'];
		$tot_paid = number_format((float)$total_paid, 2, '.', '');

		if (count($select_sql) == 0) {
			//ID increment
			$get_recepid = DB::table('receipt_item_details')
				->select('Receipt_Id')
				->orderby('Receipt_Id', 'DESC')
				->limit(1)
				->get();
			$recep_id  = $get_recepid[0]->Receipt_Id;
			$add_recep_id = $recep_id + 1;

			$sql = DB::table('receipt_details')
				->insert([
					'Company_Name' => $select_company[0]->company_name,
					'User_Name' => $username,
					'Customer_Id' => $input['receipt_cus_id'],
					'Invoice_Id' => $input['receipt_invoice_id'],
					'invoice_number' => $input['receipt_invoice_number'],
					'Invoice_Amount' => $input['receipt_acutual_amount'],
					'Receipt_Status' => $status,
				]);

			$recep = DB::table('receipt_item_details')
				->insert([
					'Company_Name' => $select_company[0]->company_name,
					'Receipt_Id' => $add_recep_id,
					'Invoice_Id' => $input['receipt_invoice_id'],
					'invoice_number' => $input['receipt_invoice_number'],
					'Paid_Amount' => $input['receipt_amount_paid'],
					'Paid_Date' => $receipt_amount_date,
					'Pending_Amount' => $input['receipt_amount_pending'],
					'Status' => $status,
					'Next_Followup_Date' => $receipt_pending_date,
					'Total_Paid_Amount' => $tot_paid,
					'Ref_no' => $input['receipt_ref_no'],
					'payment_mode' => $input['receipt_payment'],
				]);

			$insert_follow = DB::table('payment_follow_up')
				->insert([
					'Company_Name' => $select_company[0]->company_name,
					'Username' => $username,
					'Branch_Name' => $select_cus[0]->cus_branch_name,
					'Customer_Name' => $select_cus[0]->customer_name,
					'Customer_Phone_No' => $select_cus[0]->cus_phone_no,
					'Receipt_Id' => $add_recep_id,
					'Invoice_ID' => $input['receipt_invoice_id'],
					'Total_Due' => $input['receipt_amount_pending'],
					'Followup_Date' => $receipt_pending_date,
					'Amount_Received_Date' => $receipt_amount_date,
					'Payment_Id' => $add_payment_id,
					'Recieved_Amount' => $input['receipt_amount_paid'],
					'Status' => $status,
					'Invoice_Date' => $invoice_date,
				]);
			$update = DB::table('invoice_billing_details')
				->where('Invoice_ID', $input['receipt_invoice_id'])
				->update([
					'Status' => $status
				]);

			return redirect('viewreceipt');
		} else {
			$get_recepid = DB::table('receipt_item_details')
				->orderBy('Receipt_Id', 'DESC')
				->limit(1)
				->get();
			$recep_id  = $get_recepid[0]->Receipt_Id;
			$add_recep_id = $recep_id + 1;

			$recep = DB::table('receipt_item_details')
				->insert([
					'Company_Name' => $select_company[0]->company_name,
					'Receipt_Id' => $add_recep_id,
					'Invoice_Id' => $input['receipt_invoice_id'],
					'invoice_number' => $input['receipt_invoice_number'],
					'Paid_Amount' => $input['receipt_amount_paid'],
					'Paid_Date' => $receipt_amount_date,
					'Pending_Amount' => $input['receipt_amount_pending'],
					'Status' => $status,
					'Next_Followup_Date' => $receipt_pending_date,
					'Total_Paid_Amount' => $tot_paid,
					'Ref_no' => $input['receipt_ref_no'],
					'payment_mode' => $input['receipt_payment'],
				]);

			$insert_follow = DB::table('payment_follow_up')
				->insert([
					'Company_Name' => $select_company[0]->company_name,
					'Username' => $username,
					'Branch_Name' => $select_cus[0]->cus_branch_name,
					'Customer_Name' => $select_cus[0]->customer_name,
					'Customer_Phone_No' => $select_cus[0]->cus_phone_no,
					'Receipt_Id' => $add_recep_id,
					'Invoice_ID' => $input['receipt_invoice_id'],
					'Total_Due' => $input['receipt_amount_pending'],
					'Followup_Date' => $receipt_pending_date,
					'Amount_Received_Date' => $receipt_amount_date,
					'Payment_Id' => $add_payment_id,
					'Recieved_Amount' => $input['receipt_amount_paid'],
					'Status' => $status,
					'Invoice_Date' => $invoice_date,
				]);

			$update = DB::table('invoice_billing_details')
				->where('Invoice_ID', $input['receipt_invoice_id'])
				->update([
					'Status' => $status
				]);
			return redirect('viewreceipt');
		}

		//return response()->json($input);
		return view('addreceipt', compact('select_receipt', 'receipt_payment'));
	}

	public function AddPaymentMode(Request $request)
	{
		$validated = $request->validate([
			'new_mode' => 'required',
		]);

		$mode = $request['new_mode'];
		$flag = 1;

		$select_sql = DB::table('payment_mode')
			->where('mode_of_payment', $mode)
			->where('flag', '=', '1')
			->count();
		if ($select_sql == 0) {
			$insert = DB::table('payment_mode')->insert(['mode_of_payment' => $mode, 'flag' => $flag]);
			return redirect()->back()->with('message', $mode);
		} else {
			return redirect()->back()->with('error', $mode);
		}
	}
}
