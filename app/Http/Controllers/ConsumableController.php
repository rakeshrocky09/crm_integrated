<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class ConsumableController extends Controller
{
	public function GetConsumables(Request $request)
	{
		$input = $request->all();

		$select_company = DB::table('user_security')
			->select('company_name')
			->where('username', Auth::user()->username)
			->get();

		$product = DB::table('consumable_details')
			->where('consumable_status', '=', '1')
			->where('company_name', $select_company[0]->company_name)
			->get();

		//echo json_encode($product);die;
		return view('viewconsumable', compact('product'));
	}
	public function GetAllConsumables(Request $request)
	{
		$input = $request->all();

		$select_company = DB::table('user_security')
			->select('company_name')
			->where('username', Auth::user()->username)
			->get();

		$product = DB::table('consumable_details')
			->where('consumable_status', '=', '1')
			->where('company_name', $select_company[0]->company_name)
			->get();

		return response()->json($product);
	}

	public function AddConsumable(Request $request)
	{
		$validated = $request->validate([
			'consumable_code_name' => 'required',
			'consumable_stock_unit' => 'required',
			'consumable_belongs_to' => 'required',
			'noc' => 'required',
			'gst' => 'required',
			'product_name' => 'required',
			'consumable_hsn' => 'required',
			'unit_price' => 'required',
			'brand_name' => 'required',
			'msl' => 'required',
			'chargeable' => 'required',
		]);

		$code_name = $request['consumable_code_name'];
		$stock_unit = $request['consumable_stock_unit'];
		$branch = $request['consumable_belongs_to'];
		$noc = $request['noc'];
		$gst = $request['gst'];
		$product_name = $request['product_name'];
		$hsn = $request['consumable_hsn'];
		$unit_price = $request['unit_price'];
		$brand_name = $request['brand_name'];
		$msl = $request['msl'];
		$chargeable = $request['chargeable'];

		$username = Auth::user()->username;
		$consumable_const = "CONSUM";
		$status = '1';

		$select_company = DB::table('user_security')
			->select('company_name')
			->where('username', $username)
			->get();

		$select_sql = DB::table('consumable_details')
			->where('consumable_name', $product_name)
			->where('company_name', $select_company[0]->company_name)
			->where('consumable_status', '=', '1')
			->count();

		if ($select_sql == 0) {
			$insert = DB::table('consumable_details')
				->insert([
					'username' => $username,
					'consumable_branch_name' => $branch,
					'consumable_const' => $consumable_const,
					'consumable_brand_name' => $brand_name,
					'consumable_code_name' => $code_name,
					'consumable_name' => $product_name,
					'consumable_unit' => $stock_unit,
					'consumable_unit_price' => $unit_price,
					'consumable_msl' => $msl,
					'consumable_chargeable' => $chargeable,
					'consumable_hsn' => $hsn,
					'consumable_status' => $status,
					'company_name' => $select_company[0]->company_name,
					'consumable_copies' => $noc,
					'consumable_gst_percentage' => $gst,
					'Created_Date' => date('Y-m-d H:i:s'),
				]);

			return redirect('viewconsumable')->with('message', "Consumable");
		} else {
			return redirect('viewconsumable')->with('error', 'Consumable');
		}
	}

	public function UpdateConsumable(Request $request, $id)
	{
		$brand = $request['edit_brand'];
		$hsn = $request['edit_hsn'];
		$productname = $request['edit_productname'];
		$codename = $request['edit_codename'];
		$price = $request['edit_price'];
		$unit = $request['edit_unit'];
		$noCopies = $request['edit_no_ofcopies'];
		$code_name = $request['edit_codename'];
		$gst = $request['edit_gst'];
		$msl = $request['msl'];
		$chargeable = $request['chargeable'];

		$username = Auth::user()->username;
		$select_sql = DB::table('consumable_details')
			->where('consumable_id', $id)
			->where('username', $username)
			->count();

		if ($select_sql == 1) {
			$sql = DB::table('consumable_details')
				->where('consumable_id', $id)
				->update([
					//'consumable_branch_name' => $input['edit_consumable_id'],
					'consumable_brand_name' => $brand,
					'consumable_code_name' => $codename,
					'consumable_name' => $productname,
					'consumable_unit' => $unit,
					'consumable_unit_price' => $price,
					'consumable_msl' => $msl,
					'consumable_chargeable' => $chargeable,
					'consumable_hsn' => $hsn,
					'consumable_copies' => $noCopies,
					'consumable_gst_percentage' => $gst
				]);

			return redirect('viewconsumable')->with('update', "Consumable");
		} else {
			return redirect('viewconsumable')->with('error', 'Consumable Update Failed Contact Admin');
		}
	}

	public function DeleteConsumable(Request $request, $id)
	{
		$username = Auth::user()->username;

		$company = DB::table('user_security')
			->select('company_name')
			->where('username', $username)
			->get();

		$select_sql = DB::table('consumable_details')
			->where('consumable_id', $id)
			->count();

		if ($select_sql == 1) {
			$sql = DB::table('consumable_details')
				->where('consumable_id', $id)
				->update(['consumable_status' => 0]);

			return redirect('viewconsumable');
		} else {
			return response()->json(['success' => true, 'message' => 'Invalid Value'], 200);
		}
	}

	public function GetConsumable(Request $request)
	{
		$input = $request->all();
		$validator = Validator::make(
			$request->all(),
			[
				'id' => 'required',
				'username' => 'required'
			]
		);

		if ($validator->fails()) {
			$message = $validator->errors();
			return response()->json(['success' => false, 'message' => 'The given data was invalid.', 'errors' => $validator->errors()], 200);
		}

		$company = DB::table('user_security')
			->select('company_name')
			->where('username', $input['username'])
			->get();

		$sql = DB::table('consumable_details')
			->where('consumable_status', 1)
			->where('consumable_id', $input['id'])
			->where('company_name', $company[0]->company_name)
			->get();

		if (count($sql) >= 1) {
			return response()->json(['success' => true, 'Consumable Details' => $sql], 200);
		} else {
			return response()->json(['success' => true, 'message' => 'No Data Found'], 200);
		}
	}

	public function getConsumableHSN(Request $request)
	{
		$hsn = DB::table('consumable_hsn_code')
			->select('Consumable_HSN_No')
			->where(['flag' => 1])
			->get()
			->unique('Consumable_HSN_No');

		if (count($hsn) >= 1) {
			return response()->json($hsn);
		} else {
			return response()->json(['success' => false, 'message' => "No Data Found"], 200);
		}
	}

	public function AddConsumableHSN(Request $request)
	{
		$validated = $request->validate([
			'add_hsn' => 'required',
		]);

		$hsn = $request['add_hsn'];
		$flag = 1;

		$select_sql = DB::table('consumable_hsn_code')
			->where('Consumable_HSN_No', $hsn)
			->where('flag', '=', '1')
			->count();

		if ($select_sql == 0) {
			$insert = DB::table('consumable_hsn_code')->insert(['Consumable_HSN_No' => $hsn, "flag" => $flag]);
			return redirect()->back()->with('message', $hsn);
		} else {
			return redirect()->back()->with('error', $hsn);
		}
	}
}
