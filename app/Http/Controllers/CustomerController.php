<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class CustomerController extends Controller
{
	public function getCustomers(Request $request)
	{
		$input = $request->all();
		$select_company = DB::table('user_security')
			->select('company_name')
			->where('username', Auth::user()->username)
			->get();

		$cus = DB::table('customer_detail')
			->where('cus_status', '=', '1')
			->where('company_name', $select_company[0]->company_name)
			->orderby('cus_id')
			->get();
		//echo json_encode($cus);die;
		return view('viewcustomer', compact('cus'));
	}

	public function AddCustomer(Request $request)
	{
		$validated = $request->validate([
			'customer_type' => 'required',
			'customer_name' => 'required',
			'customer_phoneno' => 'required',
			'customer_belongsto' => 'required',
			'customer_primary_name' => 'required',
			'customer_phonenum' => 'required',
			'customer_address' => 'required',
			'customer_city' => 'required',
			'customer_state' => 'required',
			'customer_postal' => 'required',
		]);

		$input = $request->all();
		$username = Auth::user()->username;
		$status = '1';
		$cus_const = "CUS";
		$cus_phone = $input['customer_phoneno'];
		$cus_alterphone = $input['customer_alterphoneno'];
		$cus_prim_phone = $input['customer_phonenum'];
		$customer_phoneno = floatval($cus_phone);
		$customer_alterphoneno = floatval($cus_alterphone);
		$customer_primary_phoneno = floatval($cus_prim_phone);
		//$installation_date = date("Y-m-d H:i:s", strtotime($input['add_product_installed_date']));

		//get company name
		$select_company = DB::table('user_security')
			->select('company_name')
			->where('username', $username)
			->get();

		//ID increment
		$get_cusid = DB::table('customer_detail')
			->select('cus_id')
			->orderby('cus_id', 'DESC')
			->take(1)
			->get();
		$cus_id  = $get_cusid[0]->cus_id;
		$add_cus_id = $cus_id + 1;

		$select_sql = DB::table('customer_detail')
			->where('customer_name', $input['customer_name'])
			->where('cus_phone_no', $customer_phoneno)
			->where('company_name', $select_company[0]->company_name)
			->where('cus_email_id', $input['customer_email'])
			->get();

		if ($input['lifetime']) {
			$life_time = date("Y-m-d H:i:s", strtotime($input['lifetime']));
		} else {
			$life_time = "";
		}

		if (count($select_sql) == 0) {
			$insert = DB::table('customer_detail')
				->insert([
					'company_name' => $select_company[0]->company_name,
					'username' => $username,
					'cus_branch_name' => $input['customer_belongsto'],
					'cus_const' => $cus_const,
					'cus_id' => $add_cus_id,
					'customer_name' => $input['customer_name'],
					'cus_email_id' => $input['customer_email'],
					'cus_phone_no' => $customer_phoneno,
					'cus_alter_phone' => $customer_alterphoneno,
					'primary_contact_name' => $input['customer_primary_name'],
					'primary_contact_email' => $input['customer_primary_email_id'],
					'primary_contact_phoneno' => $customer_primary_phoneno,
					'cus_address' => $input['customer_address'],
					'cus_city' => $input['customer_city'],
					'cus_state' => $input['customer_state'],
					'cus_pincode' => $input['customer_postal'],
					'cus_type' => $input['customer_type'],
					'cus_status' => $status,
					'Customer_Create_Date' => date('Y-m-d'),
					'cus_landline' => $input['customer_landline'],
					'cus_gst_percentage' => $input['add_product_gst'],
					'cus_section' => $input['customer_section'],
					'cus_lifetime' => $life_time
				]);

			/* 	$sql1 = DB::table('cus_prod_details')
				->insert([
					'company_name' => $select_company[0]->company_name,
					'username' => $username,
					'cus_branch_name' => $input['customer_belongsto'],
					'customer_id' => $add_cus_id,
					'product_id' => $input['product_name'],					
					'Machine_Serial_No' => $input['add_product_machine_no'],
					'Contract_Type' => $input['add_product_warranty'],

					//'Machine_Serial_No' => $input['add_product_machine_no'],
					//'Contract_Type' => $input['add_product_warranty'],
					'installation_date' => $installation_date,
				]); */

			return redirect('viewcustomer')->with('message', "Customer");
		} else {
			return redirect('viewcustomer')->with('error', 'Customer Already Exist');
		}
	}

	public function UpdateCustomer(Request $request, $id)
	{
		$type = $request['edit_type'];
		$cname = $request['edit_cname'];
		$email = $request['edit_email'];
		$phone = $request['edit_phone'];
		$landline = $request['edit_landline'];
		$branch = $request['edit_branch'];
		$contactName = $request['edit_contactname'];
		$contactemail = $request['edit_contactemail'];
		$contactphone = $request['edit_contactphone'];
		$alterPhone = $request['edit_contact_altphone'];
		$lifetime = $request['edit_lifetime'];
		$address = $request['edit_address'];
		$city = $request['edit_city'];
		$state = $request['edit_state'];
		$pincode = $request['edit_postal'];
		$gst = $request['edit_gst'];
		$section = $request['edit_section'];

		$username = Auth::user()->username;
		$select_company = DB::table('user_security')
			->select('company_name')
			->where('username', $username)
			->get();

		$select_sql = DB::table('customer_detail')
			->where('cus_id', $id)
			->where('company_name', $select_company[0]->company_name)
			->count();

		if ($lifetime) {
			$life_time = date("Y-m-d", strtotime($lifetime));
		} else {
			$life_time = "";
		}

		//$installation_date = date("Y-m-d", strtotime($input['add_product_installed_date']));

		if ($select_sql == 1) {
			$sql = DB::table('customer_detail')
				->where('cus_id', $id)
				->update([
					'cus_branch_name' => $branch,
					'customer_name' => $cname,
					'cus_email_id' => $email,
					'cus_landline' => $landline,
					'cus_phone_no' => $phone,
					'cus_alter_phone' => $alterPhone,
					'primary_contact_name' => $contactName,
					'primary_contact_email' => $contactemail,
					'primary_contact_phoneno' => $contactphone,
					'cus_address' => $address,
					'cus_city' => $city,
					'cus_state' => $state,
					'cus_pincode' => $pincode,
					'cus_type' => $type,
					'cus_gst_percentage' => $gst,
					'cus_section' => $section,
					'cus_lifetime' => $life_time
				]);

			return redirect('viewcustomer')->with('update', "Customer");
		} else {
			return redirect('viewcustomer')->with('error', 'Customer Update Failed Contact Admin');
		}
	}

	public function DeleteCustomer(Request $request, $id)
	{
		$username = Auth::user()->username;
		$select_sql = DB::table('customer_detail')
			->where('cus_id', $id)
			->where('username', $username)
			->count();

		if ($select_sql == 1) {
			$sql = DB::table('customer_detail')
				->where('cus_id', $id)
				->where('username', $username)
				->update(['cus_status' => 0]);

			return redirect('viewcustomer');
		} else {
			return response()->json(['success' => true, 'message' => 'Invalid Value'], 200);
		}
	}

	public function GetCustomerBasic(Request $request)
	{
		$input = $request->all();
		$validator = Validator::make(
			$request->all(),
			[
				'id' => 'required',
				'username' => 'required'
			]
		);

		if ($validator->fails()) {
			$message = $validator->errors();
			return response()->json(['success' => false, 'message' => 'The given data was invalid.', 'errors' => $validator->errors()], 200);
		}

		$company = DB::table('user_security')
			->select('company_name')
			->where('username', $input['username'])
			->get();

		$sql = DB::table('customer_detail')
			->where('cus_id', $input['id'])
			->where('company_name', $company[0]->company_name)
			->get();

		if (count($sql) >= 1) {
			return response()->json(['success' => true, 'Customer Basic Details' => $sql], 200);
		} else {
			return response()->json(['success' => true, 'message' => 'No Data Found'], 200);
		}
	}

	public function GetLastCustomer(Request $request)
	{
		$input = $request->all();
		$validator = Validator::make(
			$request->all(),
			[
				'id' => 'required',
				'username' => 'required'
			]
		);

		if ($validator->fails()) {
			$message = $validator->errors();
			return response()->json(['success' => false, 'message' => 'The given data was invalid.', 'errors' => $validator->errors()], 200);
		}

		$company = DB::table('user_security')
			->select('company_name')
			->where('username', $input['username'])
			->get();

		$sql = DB::table('customer_detail')
			->select('cus_id')
			->orderby('Customer_Create_Date', 'DESC')
			->take(1)
			->get();

		if (count($sql) >= 1) {
			return response()->json(['success' => true, 'Customer Details' => $sql], 200);
		} else {
			return response()->json(['success' => true, 'message' => 'No Data Found'], 200);
		}
	}

	public function GetInvoice(Request $request)
	{
		$input = $request->all();

		$company = DB::table('user_security')
			->select('company_name')
			->where('username', Auth::user()->username)
			->get();

		$sql = DB::table('invoice_billing_details')
			->where('Customer_Id', $input['id'])
			->where('flag', 1)
			->where('Company_Name', $company[0]->company_name)
			->orderby('Invoice_Date', 'DESC')
			->get()
			->unique('Invoice_ID');

		return response()->json(['success' => true, 'Delivery Details' => $sql], 200);
		//return view('viewinvoice', compact('cus'));

	}

	public function GetTicket(Request $request)
	{
		$input = $request->all();

		$company = DB::table('user_security')
			->select('company_name')
			->where('username', Auth::user()->username)
			->get();

		$sql = DB::table('ticket_details')
			->leftjoin('tick_details', 'tick_details.Ticket_id', '=', 'ticket_details.ticket_id')
			->where('ticket_details.cus_id', $input['id'])
			->where('ticket_details.Company_Name', $company[0]->company_name)
			->orderby('tick_details.ticket_id', 'DESC')
			->get();

		return response()->json(['success' => true, 'Customer Product Details' => $sql], 200);
		//return view('viewticket', compact('sql'));
	}

	public function AddOwnership(Request $request)
	{
		$product_list = DB::table('product_ownership')->insert([
			'ownership' => $request['new_ownership']
		]);
		return redirect()->back()->with('message', 'Product Ownership');

		//return response()->json($request->all());
	}

	public function GetCustomerSpare(Request $request)
	{
		$input = $request->all();
		$validator = Validator::make(
			$request->all(),
			[
				'id' => 'required',
				'username' => 'required'
			]
		);

		if ($validator->fails()) {
			$message = $validator->errors();
			return response()->json(['success' => false, 'message' => 'The given data was invalid.', 'errors' => $validator->errors()], 200);
		}

		$company = DB::table('user_security')
			->select('company_name')
			->where('username', $input['username'])
			->get();

		$sql = DB::table('invoice_billing_details')
			->leftjoin('invoice_item_details', 'invoice_item_details.Invoice_ID', '=', 'invoice_billing_details.Invoice_ID')
			->leftjoin('spare_details', 'spare_details.spare_id', '=', 'invoice_item_details.Item_Id')
			->where('invoice_billing_details.Customer_Id', $input['id'])
			->where('invoice_item_details.Item_Type', 'spare')
			->where('invoice_billing_details.company_name', $company[0]->company_name)
			->orderby('invoice_billing_details.Invoice_Date', 'DESC')
			->get();

		if (count($sql) >= 1) {
			return response()->json(['success' => true, 'Customer Spare Details' => $sql], 200);
		} else {
			return response()->json(['success' => true, 'message' => 'No Data Found'], 200);
		}
	}

	public function GetCustomerConsumable(Request $request)
	{
		$input = $request->all();
		$validator = Validator::make(
			$request->all(),
			[
				'id' => 'required',
				'username' => 'required'
			]
		);

		if ($validator->fails()) {
			$message = $validator->errors();
			return response()->json(['success' => false, 'message' => 'The given data was invalid.', 'errors' => $validator->errors()], 200);
		}

		$company = DB::table('user_security')
			->select('company_name')
			->where('username', $input['username'])
			->get();

		$sql = DB::table('invoice_billing_details')
			->leftjoin('invoice_item_details', 'invoice_item_details.Invoice_ID', '=', 'invoice_billing_details.Invoice_ID')
			->leftjoin('consumable_details', 'consumable_details.consumable_id', '=', 'invoice_item_details.Item_Id')
			->where('invoice_billing_details.Customer_Id', $input['id'])
			->where('invoice_item_details.Item_Type', 'consumable')
			->where('invoice_billing_details.company_name', $company[0]->company_name)
			->where('invoice_billing_details.flag', 1)
			->orderby('invoice_billing_details.Invoice_Date', 'DESC')
			->get();

		if (count($sql) >= 1) {
			return response()->json(['success' => true, 'Customer Consumable Details' => $sql], 200);
		} else {
			return response()->json(['success' => true, 'message' => 'No Data Found'], 200);
		}
	}

	public function GetCustomerService(Request $request)
	{
		$input = $request->all();
		$validator = Validator::make(
			$request->all(),
			[
				'id' => 'required',
				'username' => 'required'
			]
		);

		if ($validator->fails()) {
			$message = $validator->errors();
			return response()->json(['success' => false, 'message' => 'The given data was invalid.', 'errors' => $validator->errors()], 200);
		}

		$company = DB::table('user_security')
			->select('company_name')
			->where('username', $input['username'])
			->get();

		$sql = DB::table('invoice_billing_details')
			->leftjoin('invoice_item_details', 'invoice_item_details.Invoice_ID', '=', 'invoice_billing_details.Invoice_ID')
			->leftjoin('service_details', 'service_details.service_id', '=', 'invoice_item_details.Item_Id')
			->where('invoice_billing_details.Customer_Id', $input['id'])
			->where('invoice_item_details.Item_Type', 'service')
			->where('invoice_billing_details.company_name', $company[0]->company_name)
			->where('invoice_billing_details.flag', 1)
			->orderby('invoice_billing_details.Invoice_Date', 'DESC')
			->get();

		if (count($sql) >= 1) {
			return response()->json(['success' => true, 'Customer Service Details' => $sql], 200);
		} else {
			return response()->json(['success' => true, 'message' => 'No Data Found'], 200);
		}
	}

	public function GetDeliverySpare(Request $request)
	{
		$input = $request->all();
		$validator = Validator::make(
			$request->all(),
			[
				'id' => 'required',
				'username' => 'required'
			]
		);

		if ($validator->fails()) {
			$message = $validator->errors();
			return response()->json(['success' => false, 'message' => 'The given data was invalid.', 'errors' => $validator->errors()], 200);
		}

		$company = DB::table('user_security')
			->select('company_name')
			->where('username', $input['username'])
			->get();

		$sql = DB::table('delivery_challan_details')
			->leftjoin('delivery_item_details', 'delivery_item_details.Challan_ID', '=', 'delivery_challan_details.Challan_ID')
			->leftjoin('spare_details', 'spare_details.spare_id', '=', 'delivery_item_details.Item_Id')
			->where('delivery_challan_details.Customer_Id', $input['id'])
			->where('delivery_item_details.Item_Type', 'spare')
			->where('delivery_challan_details.flag', 1)
			->where('delivery_challan_details.company_name', $company[0]->company_name)
			->orderby('delivery_challan_details.Delivery_Date', 'DESC')
			->get();

		if (count($sql) >= 1) {
			return response()->json(['success' => true, 'Delivery Spare Details' => $sql], 200);
		} else {
			return response()->json(['success' => true, 'message' => 'No Data Found'], 200);
		}
	}

	public function GetDeliveryConsumable(Request $request)
	{
		$input = $request->all();
		$validator = Validator::make(
			$request->all(),
			[
				'id' => 'required',
				'username' => 'required'
			]
		);

		if ($validator->fails()) {
			$message = $validator->errors();
			return response()->json(['success' => false, 'message' => 'The given data was invalid.', 'errors' => $validator->errors()], 200);
		}

		$company = DB::table('user_security')
			->select('company_name')
			->where('username', $input['username'])
			->get();

		$sql = DB::table('delivery_challan_details')
			->leftjoin('delivery_item_details', 'delivery_item_details.Challan_ID', '=', 'delivery_challan_details.Challan_ID')
			->leftjoin('consumable_details', 'consumable_details.consumable_id', '=', 'delivery_item_details.Item_Id')
			->where('delivery_challan_details.Customer_Id', $input['id'])
			->where('delivery_item_details.Item_Type', 'consumable')
			->where('delivery_challan_details.company_name', $company[0]->company_name)
			->where('delivery_challan_details.flag', 1)
			->orderby('delivery_challan_details.Delivery_Date', 'DESC')
			->get();

		if (count($sql) >= 1) {
			return response()->json(['success' => true, 'Delivery Consumable Details' => $sql], 200);
		} else {
			return response()->json(['success' => true, 'message' => 'No Data Found'], 200);
		}
	}

	public function GetDeliveryService(Request $request)
	{
		$input = $request->all();
		$validator = Validator::make(
			$request->all(),
			[
				'id' => 'required',
				'username' => 'required'
			]
		);

		if ($validator->fails()) {
			$message = $validator->errors();
			return response()->json(['success' => false, 'message' => 'The given data was invalid.', 'errors' => $validator->errors()], 200);
		}

		$company = DB::table('user_security')
			->select('company_name')
			->where('username', $input['username'])
			->get();

		$sql = DB::table('delivery_challan_details')
			->leftjoin('delivery_item_details', 'delivery_item_details.Challan_ID', '=', 'delivery_challan_details.Challan_ID')
			->leftjoin('service_details', 'service_details.service_id', '=', 'delivery_item_details.Item_Id')
			->where('delivery_challan_details.Customer_Id', $input['id'])
			->where('delivery_item_details.Item_Type', 'service')
			->where('delivery_challan_details.company_name', $company[0]->company_name)
			->where('delivery_challan_details.flag', 1)
			->orderby('delivery_challan_details.Delivery_Date', 'DESC')
			->get();

		if (count($sql) >= 1) {
			return response()->json(['success' => true, 'Customer Service Details' => $sql], 200);
		} else {
			return response()->json(['success' => true, 'message' => 'No Data Found'], 200);
		}
	}

	public function GetDelivery(Request $request)
	{
		$input = $request->all();
		$validator = Validator::make(
			$request->all(),
			[
				'id' => 'required',
				'username' => 'required'
			]
		);

		if ($validator->fails()) {
			$message = $validator->errors();
			return response()->json(['success' => false, 'message' => 'The given data was invalid.', 'errors' => $validator->errors()], 200);
		}

		$company = DB::table('user_security')
			->select('company_name')
			->where('username', $input['username'])
			->get();

		$sql = DB::table('delivery_challan_details')
			->where('Customer_Id', $input['id'])
			->where('flag', 1)
			->where('Company_Name', $company[0]->company_name)
			->orderby('Delivery_Date', 'DESC')
			->get()
			->unique('Challan_ID');

		if (count($sql) >= 1) {
			return response()->json(['success' => true, 'Delivery Details' => $sql], 200);
		} else {
			return response()->json(['success' => true, 'message' => 'No Data Found'], 200);
		}
	}

	/* public function GetCustomerProduct(Request $request)
	{
		$input = $request->all();
		$validator = Validator::make(
			$request->all(),
			[
				'id' => 'required',
				'username' => 'required'
			]
		);

		if ($validator->fails()) {
			$message = $validator->errors();
			return response()->json(['success' => false, 'message' => 'The given data was invalid.', 'errors' => $validator->errors()], 200);
		}

		$company = DB::table('user_security')
			->select('company_name')
			->where('username', $input['username'])
			->get();

		$sql = DB::table('delivery_challan_details')
			->where('Customer_Id', $input['id'])
			->where('flag', 1)
			->where('Company_Name', $company[0]->company_name)
			->orderby('Delivery_Date', 'DESC')
			->get()
			->unique('Challan_ID');

		if (count($sql) >= 1) {
			return response()->json(['success' => true, 'Delivery Details' => $sql], 200);
		} else {
			return response()->json(['success' => true, 'message' => 'No Data Found'], 200);
		}
	} */


	public function GetCustomerDetails(Request $request, $id)
	{
		/* $input = $request->all();
		$validator = Validator::make(
			$request->all(),
			[
				'id' => 'required',
				'username' => 'required'
			]
		);

		if ($validator->fails()) {
			$message = $validator->errors();
			return response()->json(['success' => false, 'message' => 'The given data was invalid.', 'errors' => $validator->errors()], 200);
		} */

		$username = Auth::user()->username;
		$company = DB::table('user_security')
			->select('company_name')
			->where('username', $username)
			->get();

		$cus = DB::table('customer_detail')
			->where('cus_id', $id)
			->where('company_name', $company[0]->company_name)
			->get();

		$product = DB::table('cus_prod_details')
			->leftjoin('product_details', 'product_details.product_id', '=', 'cus_prod_details.product_id')
			->where('cus_prod_details.Flag', 1)
			->where('cus_prod_details.customer_id', $id)
			->where('cus_prod_details.company_name', $company[0]->company_name)
			->get();

		$invoice = DB::table('invoice_billing_details')
			->where('Customer_Id', $id)
			->where('flag', 1)
			->where('Company_Name', $company[0]->company_name)
			->orderby('Invoice_ID', 'DESC')
			->get()
			->unique('Invoice_ID');

		$ticket = DB::table('ticket_details')
			->leftjoin('tick_details', 'tick_details.Ticket_id', '=', 'ticket_details.ticket_id')
			->where('ticket_details.cus_id', $id)
			->where('ticket_details.Company_Name', $company[0]->company_name)
			->orderby('tick_details.ticket_id', 'DESC')
			->get();

		$delivery = DB::table('delivery_challan_details')
			->where('Customer_Id', $id)
			->where('flag', 1)
			->where('Company_Name', $company[0]->company_name)
			->orderby('Challan_ID', 'DESC')
			->get()
			->unique('Challan_ID');

		$consumables = DB::table('delivery_challan_details')
			->leftjoin('delivery_item_details', 'delivery_item_details.Challan_ID', '=', 'delivery_challan_details.Challan_ID')
			->leftjoin('consumable_details', 'consumable_details.consumable_id', '=', 'delivery_item_details.Item_Id')
			->where('delivery_challan_details.Customer_Id', $id)
			->where('delivery_item_details.Item_Type', 'consumable')
			->where('delivery_challan_details.company_name', $company[0]->company_name)
			->where('delivery_challan_details.flag', 1)
			->orderby('delivery_challan_details.Delivery_Date', 'DESC')
			->get();

		$spares = DB::table('delivery_challan_details')
			->leftjoin('delivery_item_details', 'delivery_item_details.Challan_ID', '=', 'delivery_challan_details.Challan_ID')
			->leftjoin('spare_details', 'spare_details.spare_id', '=', 'delivery_item_details.Item_Id')
			->where('delivery_challan_details.Customer_Id', $id)
			->where('delivery_item_details.Item_Type', 'spare')
			->where('delivery_challan_details.flag', 1)
			->where('delivery_challan_details.company_name', $company[0]->company_name)
			->orderby('delivery_challan_details.Delivery_Date', 'DESC')
			->get();

		$services = DB::table('invoice_billing_details')
			->leftjoin('invoice_item_details', 'invoice_item_details.Invoice_ID', '=', 'invoice_billing_details.Invoice_ID')
			->leftjoin('service_details', 'service_details.service_id', '=', 'invoice_item_details.Item_Id')
			->where('invoice_billing_details.Customer_Id', $id)
			->where('invoice_item_details.Item_Type', 'service')
			->where('invoice_billing_details.company_name', $company[0]->company_name)
			->where('invoice_billing_details.flag', 1)
			->orderby('service_details.service_id', 'DESC')
			->get();

		$product_list = DB::table('product_details')
			->where('product_status', '=', '1')
			->where('company_name', $company[0]->company_name)
			->get();

		if (count($cus) >= 1) {
			return view('view_customer', compact('cus', 'product_list', 'product', 'invoice', 'ticket', 'delivery', 'services', 'spares', 'consumables'));
			//return response()->json(['success' => true, 'Customer Basic Details' => $sql], 200);
		} else {
			return response()->json(['success' => true, 'message' => 'No Data Found'], 200);
		}
	}

	public function GetCustomerTicket(Request $request, $id, $prodid, $machineid)
	{
		/* $input = $request->all();
		$validator = Validator::make(
			$request->all(),
			[
				'id' => 'required',
				'username' => 'required',
				'machineid' => 'required',
				'prodid' => 'required',
			]
		);

		if ($validator->fails()) {
			$message = $validator->errors();
			return response()->json(['success' => false, 'message' => 'The given data was invalid.', 'errors' => $validator->errors()], 200);
		} */

		$username = Auth::user()->username;
		$company = DB::table('user_security')
			->select('company_name')
			->where('username', $username)
			->get();

		$cus = DB::table('customer_detail')
			->where('cus_id', $id)
			->where('company_name', $company[0]->company_name)
			->get();

		$product = DB::table('cus_prod_details')
			->leftjoin('product_details', 'product_details.product_id', '=', 'cus_prod_details.product_id')
			->where('cus_prod_details.Machine_Serial_No', $machineid)
			->where('cus_prod_details.customer_id', $id)
			->where('cus_prod_details.product_id', $prodid)
			->where('cus_prod_details.Flag', 1)
			->where('cus_prod_details.company_name', $company[0]->company_name)
			->get();


		return view('customerticket', compact('cus', 'product'));
		//return response()->json(['success' => true, 'Customer Basic Details' => $cus], 200);
	}
}
