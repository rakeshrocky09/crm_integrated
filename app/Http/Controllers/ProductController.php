<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
	public function GetProducts(Request $request)
	{
		$input = $request->all();

		$select_company = DB::table('user_security')
			->select('company_name')
			->where('username', Auth::user()->username)
			->get();

		$product = DB::table('product_details')
			->where('product_status', '=', '1')
			->where('company_name', $select_company[0]->company_name)
			->get();

		//return response()->json($product);
		return view('viewproduct', compact('product'));
	}
	public function GetAllProducts(Request $request)
	{
		$select_company = DB::table('user_security')
			->select('company_name')
			->where('username', Auth::user()->username)
			->get();

		$product = DB::table('product_details')
			->where('product_status', '=', '1')
			->where('company_name', $select_company[0]->company_name)
			->get();

		//echo json_encode($product);die;	
		return response()->json($product);
		//return view('viewproduct', compact('product'));
	}

	public function AddProduct(Request $request)
	{
		$validated = $request->validate([
			'product_belongs_to' => 'required',
			'product_brand_name' => 'required',
			'product_model_name' => 'required',
			'product_description' => 'required',
			'product_code_name' => 'required',
			'product_name' => 'required',
			'product_category' => 'required',
			'product_gst_percentage' => 'required',
			'product_color' => 'required',
			'product_stock_unit' => 'required',
			'prod_part_number' => 'required',
			'product_price' => 'required'
		]);

		$product_belongs_to = $request['product_belongs_to'];
		$brand_name = $request['product_brand_name'];
		$model_name = $request['product_model_name'];
		$description = $request['product_description'];
		$code_name = $request['product_code_name'];
		$product_name = $request['product_name'];
		$product_category = $request['product_category'];
		$gst_percentage = $request['product_gst_percentage'];
		$color = $request['product_color'];
		$stock_unit = $request['product_stock_unit'];
		$part_number = $request['prod_part_number'];
		$price = $request['product_price'];

		$username = Auth::user()->username;
		$status = '1';
		$product_const = "PROD";

		$select_company = DB::table('user_security')
			->select('company_name')
			->where('username', $username)
			->get();

		$select_sql = DB::table('product_details')
			->where('product_name', $product_name)
			->where('username', $username)
			->where('product_status', '=', '1')
			->count();

		if ($select_sql == 0) {
			$insert = DB::table('product_details')
				->insert([
					'username' => $username,
					'product_branch_name' => $product_belongs_to,
					'product_brand_name' => $brand_name,
					'product_model_name' => $model_name,
					'product_description' => $description,
					'product_code' => $code_name,
					'product_name' => $product_name,
					'Product_Category' => $product_category,
					'product_gst_percentage' => $gst_percentage,

					'product_color' => $color,
					'product_stock_unit' => $stock_unit,
					'product_part_num' => $part_number,
					'product_price' => $price,

					'product_const' => $product_const,
					'product_status' => $status,
					'Created_Date' => date('Y-m-d'),
					'company_name' => $select_company[0]->company_name
				]);

			return redirect('viewproduct')->with('message', "Product");
		} else {
			return redirect('viewproduct')->with('error', 'Product');
		}
	}

	public function UpdateProduct(Request $request, $id)
	{
		$hsn = $request['edit_hsn'];
		$brand = $request['edit_brand_name'];
		$model = $request['edit_model'];
		$prod_name = $request['edit_prod_name'];
		$prod_category = $request['edit_prod_category'];
		$description = $request['edit_description'];
		$part_no = $request['edit_part_no'];
		$gst = $request['edit_gst'];
		$color = $request['edit_color'];
		$price = $request['edit_price'];
		$unit = $request['edit_unit'];

		$username = Auth::user()->username;
		/* $product_const = "PROD";
		$status = '1';

		$company = DB::table('user_security')
			->select('company_name')
			->where('username', $username)
			->get(); */

		$select_sql = DB::table('product_details')
			->where('product_id', $id)
			->where('username', $username)
			->count();

		if ($select_sql == 1) {
			$sql = DB::table('product_details')
				->where('product_id', $id)
				->update([
					'product_part_num' => $part_no,
					'product_code' => $hsn,
					'product_name' => $prod_name,
					'product_brand_name' => $brand,
					'product_model_name' => $model,
					'Product_Category' => $prod_category,
					'product_description' => $description,
					'product_color' => $color,
					'product_stock_unit' => $unit,
					'product_price' => $price,
					'product_gst_percentage' => $gst
				]);

			return redirect('viewproduct')->with('update', "Product");
		} else {
			return redirect('viewproduct')->with('error', 'Product Update Failed Contact Admin');
		}
	}

	public function DeleteProduct(Request $request, $id)
	{
		$username = Auth::user()->username;
		/* $validator = Validator::make(
			$request->all(),
			[
				'id' => 'required',
				'username' => 'required'
			]
		);

		if ($validator->fails()) {
			$message = $validator->errors();
			return response()->json(['success' => false, 'message' => 'The given data was invalid.', 'errors' => $validator->errors()], 200);
		} */

		$company = DB::table('user_security')
			->select('company_name')
			->where('username', $username)
			->get();

		$select_sql = DB::table('product_details')
			->where('product_id', $id)
			->count();

		if ($select_sql == 1) {
			$sql = DB::table('product_details')
				->where('product_id', $id)
				->where('company_name', $company[0]->company_name)
				->update(['product_status' => 0]);


			return redirect('viewproduct');
			//return response()->json(['success' => true, 'message' => 'Product Deleted Successfully'], 200);
		} else {
			return response()->json(['success' => true, 'message' => 'Invalid Value'], 200);
		}
	}

	public function GetProduct(Request $request)
	{
		$input = $request->all();
		$validator = Validator::make(
			$request->all(),
			[
				'id' => 'required',
				'username' => 'required'
			]
		);

		if ($validator->fails()) {
			$message = $validator->errors();
			return response()->json(['success' => false, 'message' => 'The given data was invalid.', 'errors' => $validator->errors()], 200);
		}

		$company = DB::table('user_security')
			->select('company_name')
			->where('username', $input['username'])
			->get();

		$sql = DB::table('product_details')
			->where('product_status', 1)
			->where('product_id', $input['id'])
			->where('company_name', $company[0]->company_name)
			->get();

		if (count($sql) >= 1) {
			return response()->json(['success' => true, 'Product Details' => $sql], 200);
		} else {
			return response()->json(['success' => true, 'message' => 'No Data Found'], 200);
		}
	}

	public function getBrands(Request $request)
	{
		$input = $request->all();

		$brand = DB::table('brand_category')
			->select('name')
			->where(['flag' => 1])
			->get()
			->unique('name');

		if (count($brand) >= 1) {
			return response()->json($brand);
		} else {
			return response()->json(['success' => false, 'message' => "No Data Found"], 200);
		}
	}

	public function getUnits(Request $request)
	{
		$input = $request->all();

		$unit = DB::table('unit_types')
			->select('unit')
			->where(['flag' => 1])
			->get()
			->unique('unit');

		if (count($unit) >= 1) {
			return response()->json($unit);
		} else {
			return response()->json(['success' => false, 'message' => "No Data Found"], 200);
		}
	}

	public function getProductHSN(Request $request)
	{
		$hsn = DB::table('product_hsn_code')
			->select('Product_HSN_No')
			->where(['flag' => 1])
			->get()
			->unique('Product_HSN_No');

		if (count($hsn) >= 1) {
			return response()->json($hsn);
		} else {
			return response()->json(['success' => false, 'message' => "No Data Found"], 200);
		}
	}

	public function getCategory(Request $request)
	{
		$input = $request->all();

		$hsn = DB::table('product_category')
			->select('name')
			->where(['flag' => 1])
			->get()
			->unique('name');

		if (count($hsn) >= 1) {
			return response()->json($hsn);
		} else {
			return response()->json(['success' => false, 'message' => "No Data Found"], 200);
		}
	}

	public function getProductname(Request $request)
	{
		$username = Auth::user()->username;
		$select_company = DB::table('user_security')
			->select('company_name')
			->where('username', $username)
			->get();

		$product = DB::table('product_details')
			->where('product_status', '=', '1')
			->where('company_name', $select_company[0]->company_name)
			->select('product_name', 'product_id')
			->get();

		if (count($product) >= 1) {
			return response()->json($product);
		} else {
			return response()->json(['success' => false, 'message' => "No Data Found"], 200);
		}
	}

	public function getModelname(Request $request)
	{
		$username = Auth::user()->username;
		$select_company = DB::table('user_security')
			->select('company_name')
			->where('username', $username)
			->get();

		$product = DB::table('product_details')
			->where('product_status', '=', '1')
			->where('company_name', $select_company[0]->company_name)
			->select('product_model_name')
			->get();

		if (count($product) >= 1) {
			return response()->json($product);
		} else {
			return response()->json(['success' => false, 'message' => "No Data Found"], 200);
		}
	}

	public function AddBrand(Request $request)
	{
		$validated = $request->validate([
			'add_brand_name' => 'required',
		]);

		$brand = $request['add_brand_name'];
		$flag = 1;

		$select_sql = DB::table('brand_category')
			->where('name', $brand)
			->where('flag', '=', '1')
			->count();

		if ($select_sql == 0) {
			$insert = DB::table('brand_category')->insert(['name' => $brand, "flag" => $flag]);
			return redirect()->back()->with('message', $brand);
		} else {
			return redirect()->back()->with('error', $brand);
		}
	}

	public function AddUnit(Request $request)
	{
		$validated = $request->validate([
			'add_units' => 'required',
		]);

		$unit = $request['add_units'];
		$flag = 1;

		$select_sql = DB::table('unit_types')
			->where('unit', $unit)
			->where('flag', '=', '1')
			->count();
		if ($select_sql == 0) {
			$insert = DB::table('unit_types')->insert(['unit' => $unit, 'flag' => $flag]);
			return redirect()->back()->with('message', $unit);
		} else {
			return redirect()->back()->with('error', $unit);
		}
	}

	public function AddProductHSN(Request $request)
	{
		$validated = $request->validate([
			'add_hsn' => 'required',
		]);

		$hsn = $request['add_hsn'];
		$flag = 1;
		$select_sql = DB::table('Product_HSN_Code')
			->where('Product_HSN_No', $hsn)
			->where('flag', '=', '1')
			->count();

		if ($select_sql == 0) {
			$insert = DB::table('Product_HSN_Code')->insert(['Product_HSN_No' => $hsn, 'flag' => $flag]);
			return redirect()->back()->with('message', $hsn);
		} else {
			return redirect()->back()->with('error', $hsn);
		}
	}

	public function AddProductCategory(Request $request)
	{
		$validated = $request->validate([
			'add_category' => 'required',
		]);

		$category = $request['add_category'];
		$flag = 1;

		$select_sql = DB::table('product_category')
			->where('name', $category)
			->where('flag', '=', '1')
			->count();

		if ($select_sql == 0) {
			$insert = DB::table('product_category')->insert(['name' => $category, 'flag' => $flag]);
			return redirect()->back()->with('message', $category);
		} else {
			return redirect()->back()->with('error', $category);
		}
	}
}
