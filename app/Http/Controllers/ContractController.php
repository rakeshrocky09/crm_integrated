<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class ContractController extends Controller
{
	public function GetContracts(Request $request)
	{
		$input = $request->all();

		$select_company = DB::table('user_security')
			->select('company_name')
			->where('username', Auth::user()->username)
			->get();

		$array = array('open', 'close');

		$contract = DB::table('contract_details')
			->leftjoin('product_details', 'product_details.product_id', '=', 'contract_details.Product_Id')
			->leftjoin('contract_history_details', 'contract_history_details.Contract_Id', '=', 'contract_details.Contract_ID')
			->leftjoin('customer_detail', 'customer_detail.cus_id', '=', 'contract_details.Customer_Id')
			->whereIn('contract_history_details.Contract_Status', $array)
			->whereIn('contract_details.Contract_Status', $array)
			->where('contract_details.Company_Name', $select_company[0]->company_name)
			->get();

		//echo json_encode($contract);die;
		return view('viewcontract', compact('contract'));
	}

	public function GetContractDetails(Request $request, $id)
	{
		$username = Auth::user()->username;
		$contract = DB::table('contract_details')
			->leftjoin('contract_history_details', 'contract_history_details.Contract_Id', '=', 'contract_details.Contract_ID')
			->leftjoin('customer_detail', 'customer_detail.cus_id', '=', 'contract_details.Customer_Id')
			->where('contract_history_details.Contract_ID', $id)
			->where('contract_details.User_Name', $username)
			->orderby('contract_details.Created_Date', 'DESC')
			->get();

		//return response()->json($contract);
		return view('viewcontracts', compact('contract'));
	}

	public function GetContractView(Request $request, $id)
	{
		$input = request()->all();
		$username = Auth::user()->username;
		$select_company = DB::table('user_security')
			->select('company_name')
			->where('username', Auth::user()->username)
			->get();

		$contract = DB::table('contract_history_details')
			->leftjoin('contract_details', 'contract_details.Contract_Id', '=', 'contract_history_details.Contract_ID')
			->leftjoin('customer_detail', 'customer_detail.cus_id', '=', 'contract_details.Customer_Id')
			->leftjoin('product_details', 'product_details.product_id', '=', 'contract_details.Product_Id')
			->where('contract_history_details.Contract_ID', $id)
			->where('contract_details.User_Name', $username)
			->orderby('contract_details.Created_Date', 'DESC')
			->get();

		$product = DB::table('contract_details')
			->leftjoin('product_details', 'product_details.product_id', '=', 'contract_details.Product_Id')
			->leftjoin('cus_prod_details', 'cus_prod_details.contract_id', '=', 'contract_details.Contract_ID')
			//->where('cus_prod_details.customer_id', '=', 'contract_details.Customer_Id')
			->where('contract_details.Contract_ID', $id)
			->where('contract_details.Company_Name', $select_company[0]->company_name)
			->select(
				'contract_details.Contract_ID',
				'product_details.Product_Id',
				'product_details.product_name',
				'product_details.Product_Category',
				'product_details.product_model_name',
				'product_details.product_brand_name',
				'cus_prod_details.section',
				'cus_prod_details.installation_date',
				'cus_prod_details.Machine_Serial_No',
			)
			->get();

		$spares = DB::table('contract_item_details')
			->leftJoin('contract_details', 'contract_details.Contract_id', "=", "contract_item_details.Contract_ID")
			->where('contract_details.Contract_id', $id)
			->where('contract_item_details.Item_Type', 'spare')
			->get();

		$consumables = DB::table('contract_item_details')
			->leftJoin('contract_details', 'contract_details.Contract_id', "=", "contract_item_details.Contract_ID")
			->where('contract_details.Contract_id', $id)
			->where('contract_item_details.Item_Type', 'consumable')
			->get();

		$pms = DB::table('pms_details')
			->where('pms_details.Contract_ID', $id)
			->get();

		//return response()->json($spares);
		return view('view_contract', compact('contract', 'product', 'spares', 'consumables', 'pms'));
	}

	public function AddContractView(Request $request, $id)
	{
		$username = Auth::user()->username;
		$company = DB::table('user_security')
			->select('company_name')
			->where('username', $username)
			->get();

		$sql = DB::table('customer_detail')
			->leftjoin('cus_prod_details', 'cus_prod_details.customer_id', '=', 'customer_detail.cus_id')
			->leftjoin('product_details', 'product_details.product_id', '=', 'cus_prod_details.product_id')
			->where('cus_prod_details.Flag', '1')
			->where('cus_prod_details.Machine_Serial_No', $id)
			->where('cus_prod_details.company_name', $company[0]->company_name)
			->get();
		$product_list = DB::table('product_ownership')->select('ownership')->groupBy('ownership')->get();

		if (count($sql) >= 1) {
			//return response()->json(['success' => true, 'Customer Product Details' => $sql], 200);
			return view('addcontract', compact('sql', 'product_list'));
		} else {
			return response()->json(['success' => true, 'message' => 'No Data Found'], 200);
		}
	}
	public function AddContract(Request $request)
	{
		$input = $request->all();
		$username = Auth::user()->username;
		$status = 'open';
		$Contract_const = "Contract";

		$select_company = DB::table('user_security')
			->select('company_name')
			->where('username', $username)
			->get();

		$get_contractid = DB::table('contract_details')
			->select('Contract_ID')
			->orderby('Contract_ID', 'DESC')
			->take(1)
			->get();

		$contract_id  = $get_contractid[0]->Contract_ID;
		$add_contract_id = $contract_id + 1;

		$contract_dtl = $input["contract"];


		if ($contract_dtl['warranty_month']) {
			$warrantymonth = $contract_dtl['warranty_month'];
		} else {
			$warrantymonth = 0;
		}

		if ($contract_dtl['amc_month']) {
			$amcmonth = $contract_dtl['amc_month'];
		} else {
			$amcmonth = 0;
		}

		/* 	if ($contract_dtl['c4_pages']) {
			$c4pages = $contract_dtl['c4_pages'];
		} else {
			$c4pages = 0;
		} */

		if ($contract_dtl['contract_type'] == "4c") {
			$c4_blacka3 = $contract_dtl['fourc_install_blacka3'];
			$c4_blacka4 = $contract_dtl['fourc_install_blacka4'];
			$c4_black = $contract_dtl['fourc_black_copies'];
			$c4_colora3 = $contract_dtl['fourc_install_colora3'];
			$c4_colora4 = $contract_dtl['fourc_install_colora4'];
			$c4_color = $contract_dtl['fourc_color_copies'];
			$c4_scanner3 = $contract_dtl['fourc_install_scanner3'];
			$c4_scanner4 = $contract_dtl['fourc_install_scanner4'];
			$c4_scanner = $contract_dtl['fourc_scanner_copes'];
		} else {
			$c4_blacka3 = 0;
			$c4_blacka4 = 0;
			$c4_scanner3 = 0;
			$c4_colora3 = 0;
			$c4_colora4 = 0;
			$c4_scanner4 = 0;
			$c4_black = 0;
			$c4_color = 0;
			$c4_scanner = 0;
		}
		/* 	if ($contract_dtl['contract_type']) {
			$rentalpages = $contract_dtl['rental_pages'];
		} else {
			$rentalpages = 0;
		} */

		if ($contract_dtl['inputval'] == "rental") {
			$rentalcopies_blacka3 = $contract_dtl['rental_install_blacka3'];
			$rentalcopies_blacka4 = $contract_dtl['rental_install_blacka4'];
			$rentalcopies_scanner3 = $contract_dtl['rental_install_scanner3'];
			$rentalcopies_colora3 = $contract_dtl['rental_install_colora3'];
			$rentalcopies_colora4 = $contract_dtl['rental_install_colora4'];
			$rentalcopies_scanner4 = $contract_dtl['rental_install_scanner4'];
			$rentalcopies_black = $contract_dtl['rental_black_copies'];
			$rentalcopies_color = $contract_dtl['rental_color_copies'];
			$rentalcopies_scanner = $contract_dtl['rental_scanner_copes'];
		} else {
			$rentalcopies_blacka3 = 0;
			$rentalcopies_blacka4 = 0;
			$rentalcopies_scanner3 = 0;
			$rentalcopies_colora3 = 0;
			$rentalcopies_colora4 = 0;
			$rentalcopies_scanner4 = 0;
			$rentalcopies_black = 0;
			$rentalcopies_color = 0;
			$rentalcopies_scanner = 0;
		}

		/* 		if ($contract_dtl['c4_rental_charges']) {
			$c4_rentalcharges = $contract_dtl['c4_rental_charges'];
		} else {
			$c4_rentalcharges = 0;
		} */

		if ($contract_dtl['c4_credit_period']) {
			$c4_credit_period = $contract_dtl['c4_credit_period'];
		} else {
			$c4_credit_period = 0;
		}

		if ($contract_dtl['rent_credit_period']) {
			$rent_credit_period = $contract_dtl['rent_credit_period'];
		} else {
			$rent_credit_period = 0;
		}

		if ($contract_dtl['rent_rental_charges']) {
			$rent_rentalcharges = $contract_dtl['rent_rental_charges'];
		} else {
			$rent_rentalcharges = 0;
		}

		$startdate = date("Y-m-d", strtotime($contract_dtl['startdate']));
		$end_date = date("Y-m-d", strtotime($contract_dtl['end_date']));


		$month = $contract_dtl['maintenance_month'];

		$schedule_date = date('Y-m-d', strtotime("+$month months", strtotime($startdate)));

		if ($schedule_date >= $end_date) {
			$end = $end_date;
		} else {
			$end = $schedule_date;
		}

		$contract_details_insert_sql = DB::table('contract_details')
			->insert([
				'Company_Name' => $select_company[0]->company_name,
				'Branch_Name' => $input['branch_name'],
				'User_Name' => $username,
				'Contract_ID' => $add_contract_id,
				'Customer_Id' => $input['customer_id'],
				'Product_Id' => $input['product_id'],
				'Machine_Serial_No' => $input['machine_serialno'],
				'Created_Date' => date('Y-m-d'),
				'Contract_Status' => $status,
			]);

		$contract_details_sql = DB::table('contract_history_details')
			->insert([
				'Contract_Id' => $add_contract_id,
				'Contract_type' => $contract_dtl['contract_type'],
				'Product_Ownership' => $contract_dtl['product_ownership'],
				'Start_Date' => $startdate,
				'End_date' => $end_date,
				'Contract_Billing_Date' => $contract_dtl['billdate'],
				'Machine_Serial_No' => $input['machine_serialno'],
				//'rental_noofpages' => $rentalpages,
				//'c4_rental_charges' => $c4_rentalcharges,
				'rental_rentalcharges' => $rent_rentalcharges,
				'warranty_noof_months' => $warrantymonth,
				'amc_noofmonths' => $amcmonth,
				//'c4_noof_pages' => $c4pages,
				'Service_Charge' => $contract_dtl['service_charge'],
				'Installation_Charge' => $contract_dtl['install_charge'],
				'Visit_Charge' => $contract_dtl['visit_charge'],
				'Contract_Status' => $status,
				'Contract_Amount' => $contract_dtl['contract_amount'],
				'Last_Updated_Date' => date('Y-m-d'),
				'rental_blacka3' => $rentalcopies_blacka3,
				'rental_blacka4' => $rentalcopies_blacka4,
				'rental_colora3' => $rentalcopies_colora3,
				'rental_colora4' => $rentalcopies_colora4,
				'rental_scannera3' => $rentalcopies_scanner3,
				'rental_scannera4' => $rentalcopies_scanner4,
				'c4_blacka3' => $c4_blacka3,
				'c4_blacka4' => $c4_blacka4,
				'c4_colora3' => $c4_colora3,
				'c4_colora4' => $c4_colora4,
				'c4_scannera3' => $c4_scanner3,
				'c4_scannera4' => $c4_scanner4,
				'c4_black_copies' => $c4_black,
				'c4_color_copies' => $c4_color,
				'c4_scanner_copies' => $c4_scanner,
				'rental_black_copies' => $rentalcopies_black,
				'rental_color_copies' => $rentalcopies_color,
				'rental_scanner_copies' => $rentalcopies_scanner,
				'C4_Credit_Period' => $c4_credit_period,
				'Rental_Credit_Period' => $rent_credit_period,
				'Maintenance_Month' => $month,
			]);

		$insert_pm = DB::table('pms_details')
			->insert([
				'company_name' => $select_company[0]->company_name,
				'Username' => $username,
				'Customer_Id' => $input['customer_id'],
				'Item_Id' => $input['product_id'],
				'Item_Model_Name' => $input['product_name'],
				'Machine_Serial_No' => $input['machine_serialno'],
				'Contract_ID' => $add_contract_id,
				'Last_Followed_Date' => $startdate,
				'Follow_Up_Date' => $end,
				'Status' => 'open',
				'Maintenance_Period' => $month,
				'End_Date' => $end_date,
			]);

		foreach ($input['lineItems']['spares'] as $linetiemkey => $spare) {
			$invoice_items_insert_sql = DB::table('contract_item_details')
				->insert([
					'Contract_ID' => $add_contract_id,
					'Customer_Id' => $input['customer_id'],
					'Item_Type' => 'spare',
					'Item_Id' => $spare['id'],
					'Item_Code_Name' => $spare['codename'],
					'Item_Name' => $spare['name'],
					'Item_Units' => $spare['units'],
					'Item_Price' => $spare['price'],
					'Item_Reading' => $spare['reading'],
				]);
		}
		foreach ($input['lineItems']['consumables'] as $linetiemkey => $consumable) {
			$invoice_items_insert_sql = DB::table('contract_item_details')
				->insert([
					'Contract_ID' => $add_contract_id,
					'Customer_Id' => $input['customer_id'],
					'Item_Type' => 'Consumable',
					'Item_Id' => $consumable['cons_id'],
					'Item_Code_Name' => $consumable['cons_codename'],
					'Item_Name' => $consumable['cons_name'],
					'Item_Units' => $consumable['cons_units'],
					'Item_Price' => $consumable['cons_price'],
					'Item_Reading' => $consumable['cons_reading'],
				]);
		}

		$invoice_items_insert_sql = DB::table('cus_prod_details')
			->where('Machine_Serial_No', $input['machine_serialno'])
			->update([
				'contract_id' => $add_contract_id,
			]);

		return response()->json('success');
	}

	public function editContractView(Request $request, $id)
	{
		$username = Auth::user()->username;
		$company = DB::table('user_security')
			->select('company_name')
			->where('username', $username)
			->get();

		$contract = DB::table('contract_history_details')
			->where('contract_history_details.Contract_Id', $id)
			->leftjoin('contract_details', 'contract_details.Contract_ID', '=', 'contract_history_details.Contract_Id')
			->where('contract_details.User_Name', $username)
			->leftjoin('customer_detail', 'customer_detail.cus_id', '=', 'contract_details.Customer_Id')
			->leftjoin('product_details', 'product_details.product_id', '=', 'contract_details.Product_Id')
			->orderBy('contract_details.Created_Date', 'DESC')
			->get();

		$macserial = $contract[0]->Machine_Serial_No;

		$product = DB::table('contract_details')
			->leftjoin('cus_prod_details', 'cus_prod_details.customer_id', '=', 'contract_details.Customer_Id')
			->leftjoin('product_details', 'product_details.product_id', '=', 'contract_details.Product_Id')
			->where('contract_details.Company_Name', $company[0]->company_name)
			->where('contract_details.Contract_ID', $id)
			//->where('contract_details.Contract_ID', '=', 'cus_prod_details.contract_id')
			->where('cus_prod_details.Flag', '1')
			->where('cus_prod_details.Machine_Serial_No', $macserial)
			->select(
				'contract_details.Contract_ID',
				'product_details.Product_Id',
				'product_details.product_name',
				'product_details.Product_Category',
				'product_details.product_model_name',
				'product_details.product_brand_name',
				'cus_prod_details.section',
				'cus_prod_details.installation_date',
				'cus_prod_details.Machine_Serial_No'
			)
			->get();

		$product_list = DB::table('product_ownership')->select('ownership')->groupBy('ownership')->get();

		$spare = DB::table('contract_item_details')
			->leftjoin('contract_details', 'contract_details.Contract_ID', '=', 'contract_item_details.Contract_ID')
			->where('contract_details.Contract_id', $id)
			->where('contract_item_details.Item_Type', 'spare')
			->get();

		$consumable = DB::table('contract_item_details')
			->leftjoin('contract_details', 'contract_details.Contract_ID', '=', 'contract_item_details.Contract_ID')
			->where('contract_details.Contract_id', $id)
			->where('contract_item_details.Item_Type', 'consumable')
			->get();

		if (count($contract) >= 1) {
			$contract = $contract[0];
			//return response()->json($product);
			return view('edit_contract', compact('contract', 'product', 'product_list', 'spare', 'consumable'));
		} else {
			return response()->json(['success' => true, 'message' => 'No Data Found'], 200);
		}
	}
	public function UpdateContract(Request $request, $id)
	{
		$input = $request->all();
		$username = Auth::user()->username;
		$status = 'open';
		$Contract_const = "Contract";

		$select_company = DB::table('user_security')
			->select('company_name')
			->where('username', $username)
			->get();

		$get_contractid = DB::table('contract_details')
			->select('Contract_ID')
			->orderby('Contract_ID', 'DESC')
			->take(1)
			->get();

		$contract_id  = $get_contractid[0]->Contract_ID;
		$add_contract_id = $contract_id + 1;

		$contract_dtl = $input["contract"];


		if ($contract_dtl['warranty_month']) {
			$warrantymonth = $contract_dtl['warranty_month'];
		} else {
			$warrantymonth = 0;
		}

		if ($contract_dtl['amc_month']) {
			$amcmonth = $contract_dtl['amc_month'];
		} else {
			$amcmonth = 0;
		}

		if ($contract_dtl['contract_type'] == "4c") {
			$c4_blacka3 = $contract_dtl['fourc_install_blacka3'];
			$c4_blacka4 = $contract_dtl['fourc_install_blacka4'];
			$c4_black = $contract_dtl['fourc_black_copies'];
			$c4_colora3 = $contract_dtl['fourc_install_colora3'];
			$c4_colora4 = $contract_dtl['fourc_install_colora4'];
			$c4_color = $contract_dtl['fourc_color_copies'];
			$c4_scanner3 = $contract_dtl['fourc_install_scanner3'];
			$c4_scanner4 = $contract_dtl['fourc_install_scanner4'];
			$c4_scanner = $contract_dtl['fourc_scanner_copes'];
		} else {
			$c4_blacka3 = 0;
			$c4_blacka4 = 0;
			$c4_scanner3 = 0;
			$c4_colora3 = 0;
			$c4_colora4 = 0;
			$c4_scanner4 = 0;
			$c4_black = 0;
			$c4_color = 0;
			$c4_scanner = 0;
		}
		if ($contract_dtl['inputval'] == "rental") {
			$rentalcopies_blacka3 = $contract_dtl['rental_install_blacka3'];
			$rentalcopies_blacka4 = $contract_dtl['rental_install_blacka4'];
			$rentalcopies_scanner3 = $contract_dtl['rental_install_scanner3'];
			$rentalcopies_colora3 = $contract_dtl['rental_install_colora3'];
			$rentalcopies_colora4 = $contract_dtl['rental_install_colora4'];
			$rentalcopies_scanner4 = $contract_dtl['rental_install_scanner4'];
			$rentalcopies_black = $contract_dtl['rental_black_copies'];
			$rentalcopies_color = $contract_dtl['rental_color_copies'];
			$rentalcopies_scanner = $contract_dtl['rental_scanner_copes'];
		} else {
			$rentalcopies_blacka3 = 0;
			$rentalcopies_blacka4 = 0;
			$rentalcopies_scanner3 = 0;
			$rentalcopies_colora3 = 0;
			$rentalcopies_colora4 = 0;
			$rentalcopies_scanner4 = 0;
			$rentalcopies_black = 0;
			$rentalcopies_color = 0;
			$rentalcopies_scanner = 0;
		}

		if ($contract_dtl['c4_credit_period']) {
			$c4_credit_period = $contract_dtl['c4_credit_period'];
		} else {
			$c4_credit_period = 0;
		}

		if ($contract_dtl['rent_credit_period']) {
			$rent_credit_period = $contract_dtl['rent_credit_period'];
		} else {
			$rent_credit_period = 0;
		}

		if ($contract_dtl['rent_rental_charges']) {
			$rent_rentalcharges = $contract_dtl['rent_rental_charges'];
		} else {
			$rent_rentalcharges = 0;
		}

		$startdate = date("Y-m-d", strtotime($contract_dtl['startdate']));
		$end_date = date("Y-m-d", strtotime($contract_dtl['end_date']));


		$month = $contract_dtl['maintenance_month'];

		$schedule_date = date('Y-m-d', strtotime("+$month months", strtotime($startdate)));

		if ($schedule_date >= $end_date) {
			$end = $end_date;
		} else {
			$end = $schedule_date;
		}

		$contract_details_sql = DB::table('contract_history_details')
			->where('Contract_Id', $id)
			->update([
				'Contract_type' => $contract_dtl['contract_type'],
				'Product_Ownership' => $contract_dtl['product_ownership'],
				'Start_Date' => $startdate,
				'End_date' => $end_date,
				'Contract_Billing_Date' => $contract_dtl['billdate'],
				'Rental_Credit_Period' => $rent_credit_period,
				'C4_Credit_Period' => $c4_credit_period,
				'rental_rentalcharges' => $rent_rentalcharges,
				'warranty_noof_months' => $warrantymonth,
				'amc_noofmonths' => $amcmonth,
				'Service_Charge' => $contract_dtl['service_charge'],
				'Installation_Charge' => $contract_dtl['install_charge'],
				'Visit_Charge' => $contract_dtl['visit_charge'],
				'Contract_Status' => $status,
				'Contract_Amount' => $contract_dtl['contract_amount'],
				'rental_blacka3' => $rentalcopies_blacka3,
				'rental_blacka4' => $rentalcopies_blacka4,
				'rental_colora3' => $rentalcopies_colora3,
				'rental_colora4' => $rentalcopies_colora4,
				'rental_scannera3' => $rentalcopies_scanner3,
				'rental_scannera4' => $rentalcopies_scanner4,
				'c4_blacka3' => $c4_blacka3,
				'c4_blacka4' => $c4_blacka4,
				'c4_colora3' => $c4_colora3,
				'c4_colora4' => $c4_colora4,
				'c4_scannera3' => $c4_scanner3,
				'c4_scannera4' => $c4_scanner4,
				'c4_black_copies' => $c4_black,
				'c4_color_copies' => $c4_color,
				'c4_scanner_copies' => $c4_scanner,
				'Maintenance_Month' => $month,
				'rental_black_copies' => $rentalcopies_black,
				'rental_color_copies' => $rentalcopies_color,
				'rental_scanner_copies' => $rentalcopies_scanner,
			]);

		$count = DB::table('pms_details')
			->where('Contract_ID', $id)
			->where('Status', 'open')
			->get();

		if (count($count) >= 1) {
			$update = DB::table('pms_details')
				->where('Contract_ID', $id)
				->update([
					'Last_Followed_Date' => $startdate,
					'Follow_Up_Date' => $end,
					'Maintenance_Period' => $month,
					'End_Date' => $end_date,
				]);
		} else {
			$insert_pm = DB::table('pms_details')
				->insert([
					'company_name' => $select_company[0]->company_name,
					'Username' => $username,
					'Customer_Id' => $input['customer_id'],
					'Item_Id' => $input['product_id'],
					'Item_Model_Name' => $input['product_name'],
					'Machine_Serial_No' => $input['machine_serialno'],
					'Contract_ID' => $id,
					'Last_Followed_Date' => $startdate,
					'Follow_Up_Date' => $end,
					'Status' => 'open',
					'Maintenance_Period' => $month,
					'End_Date' => $end_date,
				]);
		}


		if (!($input['lineItems']['spares'] == 'empty')) {
			foreach ($input['lineItems']['spares'] as $linetiemkey => $spare) {
				$invoice_items_insert_sql = DB::table('contract_item_details')
					->insert([
						'Contract_ID' => $id,
						'Customer_Id' => $input['customer_id'],
						'Item_Type' => 'spare',
						'Item_Id' => $spare['id'],
						'Item_Code_Name' => $spare['codename'],
						'Item_Name' => $spare['name'],
						'Item_Units' => $spare['units'],
						'Item_Price' => $spare['price'],
						'Item_Reading' => $spare['reading'],
					]);
			}
		}

		if (!($input['lineItems']['consumables'] == 'empty')) {
			foreach ($input['lineItems']['consumables'] as $linetiemkey => $consumable) {
				$invoice_items_insert_sql = DB::table('contract_item_details')
					->insert([
						'Contract_ID' => $id,
						'Customer_Id' => $input['customer_id'],
						'Item_Type' => 'Consumable',
						'Item_Id' => $consumable['cons_id'],
						'Item_Code_Name' => $consumable['cons_codename'],
						'Item_Name' => $consumable['cons_name'],
						'Item_Units' => $consumable['cons_units'],
						'Item_Price' => $consumable['cons_price'],
						'Item_Reading' => $consumable['cons_reading'],
					]);
			}
		}

		return response()->json('success');
	}

	public function CloseContract(Request $request, $id)
	{
		$username = Auth::user()->username;
		$select_sql = DB::table('contract_details')
			->where('User_Name', $username)
			->where('Contract_id', $id)
			->count();

		if ($select_sql == 1) {
			$sql = DB::table('contract_details')
				->where('User_Name', $username)
				->where('Contract_id', $id)
				->update(['Contract_Status' => 'close']);
			if ($sql) {
				$sql1 = DB::table('contract_history_details')
					->where('Contract_id', $id)
					->update(['Contract_Status' => 'close']);
				if ($sql1) {
					return redirect('viewcontract');
				} else {

					return "Error contract_history_details";
				}
			} else {

				return "Error contract_details";
			}
		} else {
			return response()->json(['success' => true, 'message' => 'Invalid Value'], 200);
		}
	}
	public function DeleteContract(Request $request, $id)
	{
		$username = Auth::user()->username;

		$select_sql = DB::table('contract_details')
			->where('User_Name', $username)
			->where('Contract_id', $id)
			->count();

		if ($select_sql == 1) {
			$sql = DB::table('contract_details')
				->where('User_Name', $username)
				->where('Contract_id', $id)
				->update(['Contract_Status' => 'closed']);

			return redirect('viewcontract');
		} else {
			return response()->json(['success' => true, 'message' => 'Invalid Value'], 200);
		}
	}
}
