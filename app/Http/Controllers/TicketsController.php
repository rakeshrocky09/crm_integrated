<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class TicketsController extends Controller
{
	public function getAllTickets(Request $request)
	{
		$select_company = DB::table('user_security')
			->select('company_name')
			->where('username', Auth::user()->username)
			->get();

		$sql = DB::select("SELECT max(Created_Date) as date from tick_details  group by Ticket_id");
		$amount = array_column($sql, 'date');

		$select = DB::table('tick_details')
			->where('ticket_details.Flag', 1)
			->leftjoin('ticket_details', 'ticket_details.ticket_id', '=', 'tick_details.Ticket_id')
			->where('ticket_details.username', Auth::user()->username)
			->get()
			->unique('Ticket_id');

		return view('viewticket', compact('select'));
	}
	public function UpdateTicket(Request $request, $id)
	{
		/* $input = $request->all();
		$validator = Validator::make(
			$request->all(),
			[
				'id' => 'required',
			]
		);

		if ($validator->fails()) {
			$message = $validator->errors();
			return response()->json(['success' => false, 'message' => 'The given data was invalid.', 'errors' => $validator->errors()], 200);
		} */

		$technician = $request['edit_technician'];
		$closed_date = $request['edit_closed_date'];
		$source = $request['edit_source'];
		$type = $request['edit_ticket_type'];
		$defectcode = $request['edit_defectcode'];
		$priority = $request['edit_ticket_priority'];
		$tat = $request['edit_tat'];
		$description = $request['edit_description'];
		$servicecharge = $request['edit_servicecharge'];
		$blacka3 = $request['edit_ticket_blacka3'];
		$colora3 = $request['edit_ticket_colora3'];
		$blacka4 = $request['edit_ticket_blacka4'];
		$colora4 = $request['edit_ticket_colora4'];
		$status = $request['edit_status'];

		$select_sql = DB::table('tick_details')
			->where('Ticket_id', $id)
			->count();

		if ($select_sql >= 1) {
			$sql = DB::table('tick_details')
				->where('Ticket_id', $id)
				->update([
					'Assign_Technician_Name' =>  $technician,
					'Ticket_Closed_Date' => $closed_date,
					'Ticket_Source' => $source,
					'Ticket_Type' => $type,
					'Ticket_Defect_Code' => $defectcode,
					'Ticket_Priority' => $priority,
					'Total_Hrs' => $tat,
					'Problem_Description' => $description,
					'Service_Charges' => $servicecharge,
					'Count_Black_A3' => $blacka3,
					'Count_Color_A3' => $colora3,
					'Count_Black_A4' => $blacka4,
					'Count_Color_A4' => $colora4,
					'Ticket_Status' => $status,
					'Updated_Date' => date('Y-m-d'),
				]);
			//return response()->json(['success' => true, 'message' => $select_sql], 200);
			return redirect('viewticket')->with('update', 'Ticket');
		} else {
			return redirect('viewticket')->with('error', 'Product Update Failed Contact Admin');
		}
	}
	public function UpdateCustomerTicket(Request $request, $id)
	{
		/* $input = $request->all();
		$validator = Validator::make(
			$request->all(),
			[
				'id' => 'required',
			]
		);

		if ($validator->fails()) {
			$message = $validator->errors();
			return response()->json(['success' => false, 'message' => 'The given data was invalid.', 'errors' => $validator->errors()], 200);
		} */

		$technician = $request['edit_technician'];
		$closed_date = $request['edit_closed_date'];
		$source = $request['edit_source'];
		$type = $request['edit_ticket_type'];
		$defectcode = $request['edit_defectcode'];
		$priority = $request['edit_ticket_priority'];
		$tat = $request['edit_tat'];
		$description = $request['edit_description'];
		$servicecharge = $request['edit_servicecharge'];
		$blacka3 = $request['edit_ticket_blacka3'];
		$colora3 = $request['edit_ticket_colora3'];
		$blacka4 = $request['edit_ticket_blacka4'];
		$colora4 = $request['edit_ticket_colora4'];
		$status = $request['edit_status'];

		$select_sql = DB::table('tick_details')
			->where('Ticket_id', $id)
			->count();

		if ($select_sql >= 1) {
			$sql = DB::table('tick_details')
				->where('Ticket_id', $id)
				->update([
					'Assign_Technician_Name' =>  $technician,
					'Ticket_Closed_Date' => $closed_date,
					'Ticket_Source' => $source,
					'Ticket_Type' => $type,
					'Ticket_Defect_Code' => $defectcode,
					'Ticket_Priority' => $priority,
					'Total_Hrs' => $tat,
					'Problem_Description' => $description,
					'Service_Charges' => $servicecharge,
					'Count_Black_A3' => $blacka3,
					'Count_Color_A3' => $colora3,
					'Count_Black_A4' => $blacka4,
					'Count_Color_A4' => $colora4,
					'Ticket_Status' => $status,
					'Updated_Date' => date('Y-m-d'),
				]);
			//return response()->json(['success' => true, 'message' => $select_sql], 200);
			return redirect('viewticket')->with('update', 'Ticket');
		} else {
			return redirect('viewticket')->with('error', 'Product Update Failed Contact Admin');
		}
	}

	public function DeleteTicket(Request $request, $id)
	{/* 
		$input = $request->all();
		$validator = Validator::make(
			$request->all(),
			[
				'id' => 'required',
				'username' => 'required'
			]
		);
		if ($validator->fails()) {
			$message = $validator->errors();
			return response()->json(['success' => false, 'message' => 'The given data was invalid.', 'errors' => $validator->errors()], 200);
		} */
		$username = Auth::user()->username;

		$select_sql = DB::table('ticket_details')
			->where('username', $username)
			->where('ticket_id',  $id)
			->count();

		if ($select_sql == 1) {
			$update_sql = DB::table('ticket_details')
				->where('username', 	$username)
				->where('ticket_id',  $id)
				->update(['flag' => 0]);
			if ($update_sql == 1) {
				$sql = DB::table('cus_prod_details')
					->where('ticket_id',  $id)
					->update(['ticket_id' => '']);
				//return response()->json(['success' => true, 'message' => 'ticket deleted'], 200);
				return redirect('viewticket');
			} else {
				return response()->json(['success' => true, 'message' => 'failed cus_prod_details'], 200);;
			}
			//return redirect('viewconsumable');
		} else {
			return response()->json(['success' => true, 'message' => 'Invalid Value'], 200);
		}
	}

	public function GetTicketHistory(Request $request, $id)
	{
		/* $input = $request->all();
		$validator = Validator::make(
			$request->all(),
			[
				'id' => 'required',
			]
		);

		if ($validator->fails()) {
			$message = $validator->errors();
			return response()->json(['success' => false, 'message' => 'The given data was invalid.', 'errors' => $validator->errors()], 200);
		} */
		$select_company = DB::table('user_security')
			->select('company_name')
			->where('username', Auth::user()->username)
			->get();

		$ticket = DB::table('tick_details')
			->leftjoin('ticket_details', 'ticket_details.ticket_id', '=', 'tick_details.Ticket_id')
			->where('tick_details.Ticket_id', $id)
			->where('tick_details.company_name',  $select_company[0]->company_name)
			->get();

		return view('viewtickets', compact('ticket'));
		/* if (count($ticket) >= 1) {
			return response()->json(['success' => true, 'message' => $ticket], 200);
		} else {
			return response()->json(['success' => false, 'message' => "No Data Found"], 200);
		} */
	}

	public function GetTicketDefectCode(Request $request)
	{
		$select = DB::table('ticket_defect_code')
			->select('code_name')
			->where('flag', 1)
			->get();

		//return view('viewtickets', compact('ticket'));
		if (count($select) >= 1) {
			return response()->json($select);
		} else {
			return response()->json(['success' => false, 'message' => "No Data Found"], 200);
		}
	}

	public function GetTicketType(Request $request)
	{
		$type = DB::table('ticket_type')
			->select('ticket_type')
			->where('flag', 1)
			->get();

		//return view('viewtickets', compact('ticket'));
		if (count($type) >= 1) {
			return response()->json($type);
		} else {
			return response()->json(['success' => false, 'message' => "No Data Found"], 200);
		}
	}

	public function AddTicketType(Request $request)
	{
		/* $input = $request->all();
		$validator = Validator::make(
			$request->all(),
			[
				'ticket_type' => 'required',
			]
		);

		if ($validator->fails()) {
			$message = $validator->errors();
			return response()->json(['success' => false, 'message' => 'The given data was invalid.', 'errors' => $validator->errors()], 200);
		} */

		$type = $request['add_ticket_type'];
		$flag = 1;

		$select_sql = DB::table('ticket_type')
			->where('ticket_type', $type)
			->where('flag', '=', '1')
			->count();

		if ($select_sql == 0) {
			$insert = DB::table('ticket_type')->insert(['ticket_type' => $type, "flag" => $flag]);
			return redirect()->back()->with('message', $type);
			//return response()->json(['success' => true, 'message' => "success"], 200);
		} else {
			//return response()->json(['success' => false, 'message' => "ticket_type already exist"], 200);
			return redirect()->back()->with('error', $type);
		}
	}

	public function AddTicketDefectCode(Request $request)
	{
		/* $input = $request->all();
		$validator = Validator::make(
			$request->all(),
			[
				'add_defect_code' => 'required',
			]
		);

		if ($validator->fails()) {
			$message = $validator->errors();
			return response()->json(['success' => false, 'message' => 'The given data was invalid.', 'errors' => $validator->errors()], 200);
		} */

		$code = $request['add_defect_code'];
		$flag = 1;

		$select_sql = DB::table('ticket_defect_code')
			->where('code_name', $code)
			->where('flag', '=', '1')
			->count();

		if ($select_sql == 0) {
			$insert = DB::table('ticket_defect_code')->insert(['code_name' => $code, "flag" => $flag]);
			return redirect()->back()->with('message', $code);
			//return response()->json(['success' => true, 'message' => "success"], 200);
		} else {
			//return response()->json(['success' => false, 'message' => "Ticket Defect Code already exist"], 200);
			return redirect()->back()->with('error', $code);
		}
	}

	public function addCustomerTickets(Request $request)
	{
		$cus_name = $request['customer_name'];
		$cus_id = $request['cus_id'];
		$cus_branch_name = $request['cus_branch_name'];
		$address = $request['address'];
		$city = $request['city'];
		$state = $request['state'];
		$pincode = $request['pincode'];
		//$prod_name = $request['prod_name'];
		$brand_name = $request['brand_name'];
		$mac_serial = $request['mac_serial'];
		$source = $request['source'];
		$defect_code = $request['defect_code'];
		$tat = $request['tat'];
		$ticket_type = $request['ticket_type'];
		$priority = $request['priority'];
		$description = $request['description'];
		$sender_name = $request['sender_name'];
		$sender_email = $request['sender_email'];
		$sender_phone = $request['sender_phone'];
		$assign_technician = $request['assign_technician'];
		$assign_branch = $request['assign_branch'];
		$service_charges = $request['service_charges'];
		$created_date = $request['created_date'];
		$closed_date = $request['closed_date'];
		$black_a3 = $request['black_a3'];
		$black_a4 = $request['black_a4'];
		$color_a3 = $request['color_a3'];
		$color_a4 = $request['color_a4'];


		$username = Auth::user()->username;
		$flag = 1;
		$status = 'open';

		$ticket_caller_phone_no = $sender_phone;
		$ticket_caller_phone_int = floatval($ticket_caller_phone_no);

		/* $created_date = date("Y-m-d H:i:s", strtotime($created_date));
		if ($closed_date) {
			$closed_date = date("Y-m-d", strtotime($closed_date));
		} else {
			$closed_date = "";
		} */


		$get_ticketid = DB::table('ticket_details')
			->select('ticket_id')
			->orderByDesc('ticket_id', 'DESC')->limit(1)->get();

		$ticket_id  = $get_ticketid[0]->ticket_id;
		$add_tick_id = $ticket_id + 1;

		$select_company = DB::table('user_security')
			->select('company_name')
			->where('username', $username)
			->get();

		$sql = DB::table('ticket_details')
			->insert([
				'company_name' => $select_company[0]->company_name,
				'username' => $username,
				'branch_name' => $cus_branch_name,
				'ticket_id' => $add_tick_id,
				'cus_id' => $cus_id,
				'customer_name' => $cus_name,
				'phone_no' => $ticket_caller_phone_int,
				'product_model' => $brand_name,
				'Machine_Serial_No' => $mac_serial,
				'delivery_address_1' => $address,
				'delivery_city' => $city,
				'delivery_state' => $state,
				'delivery_pincode' => $pincode,
				'Caller_Sender_Name' => $sender_name,
				'Caller_Sender_Phone' => $ticket_caller_phone_int,
				'Caller_Sender_Email' => $sender_email,
				'Flag' => $flag,
			]);

		$tick = DB::table('tick_details')
			->insert([
				'Company_Name' => $select_company[0]->company_name,
				'Ticket_id' => $add_tick_id,
				'Assign_Technician_Name' => $assign_technician,
				'Technician_Branch' => $assign_branch,
				'Ticket_Source' => $source,
				'Ticket_Type' => $ticket_type,
				'Ticket_Defect_Code' => $defect_code,
				'Ticket_Priority' => $priority,
				'Total_Hrs' => $tat,
				'Problem_Description' => $description,
				'Service_Charges' => $service_charges,
				'Count_Black_A3' => $black_a3,
				'Count_Black_A4' => $black_a4,
				'Count_Color_A3' => $color_a3,
				'Count_Color_A4' => $color_a4,
				'Created_Date' => date('Y-m-d'),
				'Ticket_Status' => $status,
				//'Ticket_Created_Date' => $created_date,
				//'Ticket_Closed_Date' => $closed_date,
			]);
		$cussql = DB::table('cus_prod_details')
			->where('Machine_Serial_No', '=', $mac_serial)
			->update([
				'ticket_id' => $add_tick_id,
			]);

		return redirect('viewticket')->with('message', "CustomerTicket");

		//return response()->json(['success' => true, 'message' => "outside if sql"], 200);
	}

	public function AddTickets(Request $request)
	{
		$validated = $request->validate([
			'cus_name' => 'required',
			'cus_phone' => 'required',
			'mac_serial' => 'required',
			'cus_address' => 'required',
			'cus_city' => 'required',
			'cus_state' => 'required',
			'cus_branch' => 'required',
			'product_name' => 'required',
		]);

		$cus_name = $request['cus_name'];
		$cus_phone = $request['cus_phone'];
		$mac_serial = $request['mac_serial'];
		$cus_address = $request['cus_address'];
		$cus_city = $request['cus_city'];
		$cus_state = $request['cus_state'];
		$cus_pincode = $request['cus_pincode'];
		$delivery_address = $request['delivery_address'];
		$delivery_city = $request['delivery_city'];
		$delivery_state = $request['delivery_state'];
		$delivery_pincode = $request['delivery_pincode'];
		$cus_branch = $request['cus_branch'];
		$product_name = $request['product_name'];
		$source = $request['source'];
		$ticket_type = $request['ticket_type'];
		$defect_code = $request['defect_code'];
		$priority = $request['priority'];
		$tat = $request['tat'];
		$description = $request['description'];
		$caller_name = $request["caller_name"];
		$caller_email = $request['caller_email'];
		$caller_phone = $request['caller_phone'];
		$assign_technician = $request['assign_technician'];
		$assign_branch = $request['assign_branch'];
		$service_charges = $request['service_charges'];
		$black_a3 = $request['black_a3'];
		$black_a4 = $request['black_a4'];
		$color_a3 = $request['color_a3'];
		$color_a4 = $request['color_a4'];


		$input = $request->all();
		$username = Auth::user()->username;

		$ticket_phone_no = $cus_phone;
		$ticket_phone_no_int = floatval($ticket_phone_no);
		$ticket_caller_phone_no = $caller_phone;
		$ticket_caller_phone_no_int = floatval($ticket_caller_phone_no);

		$status = 'open';
		$tkt_const = "TKT";
		$cus_const = "CUS";

		$select_company = DB::table('user_security')
			->select('company_name')
			->where('username', $username)
			->get();

		$get_cusid = DB::table('customer_detail')
			->select('cus_id')
			->where('company_name', $select_company[0]->company_name)
			->orderByDesc('cus_id', 'DESC')
			->limit(1)->get();
		$cus_id  = $get_cusid[0]->cus_id;
		$add_cus_id = $cus_id + 1;

		$insert_sql = DB::table('customer_detail')
			->insert([
				'company_name' => $select_company[0]->company_name,
				'username' => $username,
				'cus_branch_name' => $cus_branch,
				'cus_const' => $cus_const,
				'cus_id' => $add_cus_id,
				'customer_name' => $cus_name,
				'cus_phone_no' => $cus_phone,
				'cus_address' => $cus_address,
				'cus_city' => $cus_city,
				'cus_state' => $cus_state,
				'cus_pincode' => $cus_pincode,
				'cus_status' => 1,
				'Customer_Create_Date' =>  date('Y-m-d'),
			]);

		$get_ticketid = DB::table('ticket_details')
			->select('ticket_id')
			->orderByDesc('ticket_id', 'DESC')
			->limit(1)->get();
		$ticket_id  = $get_ticketid[0]->ticket_id;
		$add_tick_id = $ticket_id + 1;

		$sql = DB::table('ticket_details')
			->insert([
				'company_name' => $select_company[0]->company_name,
				'username' => $username,
				'branch_name' => $cus_branch,
				'customer_name' => $cus_name,
				'ticket_id' => $add_tick_id,
				'cus_id' => $add_cus_id,
				'phone_no' => $ticket_phone_no_int,
				'product_model' => $product_name,
				'Machine_Serial_No' => $mac_serial,
				'delivery_address_1' => $delivery_address,
				'delivery_city' => $delivery_city,
				'delivery_state' => $delivery_state,
				'delivery_pincode' => $delivery_pincode,
				'Caller_Sender_Name' => $caller_name,
				'Caller_Sender_Phone' => $ticket_caller_phone_no_int,
				'Caller_Sender_Email' => $caller_email,
				'Flag' => 1,
				'Ticket_Status_Closure' => $status,
			]);


		$tick = DB::table('tick_details')
			->insert([
				'Company_Name' => $select_company[0]->company_name,
				'Ticket_id' => $add_tick_id,
				'Assign_Technician_Name' => $assign_technician,
				'Technician_Branch' => $assign_branch,
				'Ticket_Source' => $source,
				'Ticket_Type' => $ticket_type,
				'Ticket_Defect_Code' => $defect_code,
				'Ticket_Priority' => $priority,
				'Total_Hrs' => $tat,
				'Problem_Description' => $description,
				'Service_Charges' => $service_charges,
				'Count_Black_A3' => $black_a3,
				'Count_Black_A4' => $black_a4,
				'Count_Color_A3' => $color_a3,
				'Count_Color_A4' => $color_a4,
				'Created_Date' => date('Y-m-d'),
				'Ticket_Status' => $status,
			]);

		//return response()->json(['success' => true, 'message' => 	'tick inserted'], 200);
		return redirect('viewticket')->with('message', "Ticket");
	}
	public function LoadAddClosure(Request $request, $id, $cusid)
	{
		$select_company = DB::table('user_security')
			->select('company_name')
			->where('username', Auth::user()->username)
			->get();

		$ticket = DB::table('tick_details')
			->leftjoin('ticket_details', 'ticket_details.ticket_id', '=', 'tick_details.Ticket_id')
			->where('tick_details.Ticket_id', $id)
			->where('tick_details.company_name',  $select_company[0]->company_name)
			->get();
		//return response()->json($ticket);
		return view('setdisposition', compact('id', 'cusid', 'ticket'));
	}
	public function AddClosure(Request $request)
	{
		$input = $request->all();
		$username = Auth::user()->username;
		$company = DB::table('user_security')
			->select('company_name')
			->where('username', $username)
			->get();

		$closure_dtl = $input["closure"];
		$Ticket_Closure_Type = '';
		$Ticket_Status = '';

		if ($closure_dtl['closure_disposition']) {
			$closure_disposition = $closure_dtl['closure_disposition'];
		} else {
			$closure_disposition = $closure_dtl['broken_closure_disposition'];
		}
		if ($closure_dtl['closure_type'] == "BrokenCall") {
			$Ticket_Closure_Type = $closure_dtl['closure_type'];
			$Ticket_Status = "pending";
		} elseif ($closure_dtl['closure_type'] == "Resolved") {
			$Ticket_Closure_Type = $closure_dtl['closure_type'];
			$Ticket_Status = "Resolved";
		} elseif ($closure_dtl['closure_type'] == "Cancelled by Customer") {
			$Ticket_Closure_Type = $closure_dtl['closure_type'];
			$Ticket_Status = "close";
		} elseif ($closure_dtl['closure_type'] == "Closed with Discreation") {
			$Ticket_Closure_Type = $closure_dtl['closure_type'];
			$Ticket_Status = "close";
		}
		if ($closure_dtl['closure_services_charges']) {
			$servicecharges = $closure_dtl['closure_services_charges'];
		} else {
			$servicecharges = 0;
		}
		if ($closure_dtl['closure_black_a3']) {
			$blacka3 = $closure_dtl['closure_black_a3'];
		} else {
			$blacka3 = 0;
		}
		if ($closure_dtl['closure_color_a3']) {
			$colora3 = $closure_dtl['closure_color_a3'];
		} else {
			$colora3 = 0;
		}
		if ($closure_dtl['closure_black_a4']) {
			$blacka4 = $closure_dtl['closure_black_a4'];
		} else {
			$blacka4 = 0;
		}
		if ($closure_dtl['closure_color_a4']) {
			$colora4 = $closure_dtl['closure_color_a4'];
		} else {
			$colora4 = 0;
		}
		if (!($closure_dtl['reassigned_technician'] == '')) {
			$assigned_technician = $closure_dtl['reassigned_technician'];
		} else {
			$assigned_technician = $closure_dtl['assigned_technician'];
		}

		$select = DB::table('closure_details')->where('Ticket_id', $input['ticket_id'])->get();

		if (count($select) >= 1) {
			$closure_details_insert_sql = DB::table('closure_details')
				->where('Ticket_id', $input['ticket_id'])
				->where('Customer_Id', $input['customer_id'])
				->update([
					'Closure_Type' => $closure_dtl['closure_type'],
					'Closure_Description' => $closure_dtl['closure_remarks'],
					'Resolution_Type' => $closure_disposition,
					'Assign_Technician_Name' => $assigned_technician,
					'technician_assigned_date' => $closure_dtl['assigned_date'],
					'comments' => $closure_dtl['comments'],
					'Service_Charge' => $servicecharges,
					'Black_A3' => $blacka3,
					'Color_A3' => $colora3,
					'Black_A4' => $blacka4,
					'Color_A4' => $colora4,
				]);
		} else {
			$closure_details_insert_sql = DB::table('closure_details')
				->where('Ticket_id', $input['ticket_id'])
				->where('Customer_Id', $input['customer_id'])
				->update([
					'Company_Name' =>  $company[0]->company_name,
					'User_Name' => $username,
					'Ticket_id' => $input['ticket_id'],
					'Customer_Id' => $input['customer_id'],
					'Closure_Type' => $closure_dtl['closure_type'],
					'Closure_Description' => $closure_dtl['closure_remarks'],
					'Resolution_Type' => $closure_disposition,
					'Assign_Technician_Name' => $assigned_technician,
					'technician_assigned_date' => $closure_dtl['assigned_date'],
					'comments' => $closure_dtl['comments'],
					'Service_Charge' => $servicecharges,
					'Black_A3' => $blacka3,
					'Color_A3' => $colora3,
					'Black_A4' => $blacka4,
					'Color_A4' => $colora4,
				]);
		}

		$closure_type_insert_sql = DB::table('ticket_details')
			->where('ticket_id', $input['ticket_id'])
			->where('company_name',  $company[0]->company_name)
			->update([
				'Ticket_Status_Closure' => $Ticket_Closure_Type,
			]);

		$closure_insert_sql = DB::table('tick_details')
			->where('Ticket_id', $input['ticket_id'])
			->where('company_name',  $company[0]->company_name)
			->update([
				'Ticket_Status' => $Ticket_Status,
				'Ticket_Disposition' => $closure_disposition,
				'Assign_Technician_Name' => $assigned_technician,
			]);

		$update = DB::table('cus_prod_details')
			->where('ticket_id', $input['ticket_id'])
			->update([
				'ticket_id' => '',
			]);

		if (!($input['lineItems']['spares'] == 'empty')) {
			foreach ($input['lineItems']['spares'] as $linetiemkey => $spare) {
				$invoice_items_insert_sql = DB::table('closure_item_details')
					->insert([
						'Ticket_Id' => $input['ticket_id'],
						'Customer_Id' => $input['customer_id'],
						'Item_Type' => 'spare',
						'Item_Id' => $spare['id'],
						'Item_Code_Name' => $spare['codename'],
						'Item_Name' => $spare['name'],
						'Item_Units' => $spare['units'],
						'Item_Price' => $spare['price'],
					]);
			}
		}

		if (!($input['lineItems']['consumables'] == 'empty')) {
			foreach ($input['lineItems']['consumables'] as $linetiemkey => $consumable) {
				$invoice_items_insert_sql = DB::table('closure_item_details')
					->insert([
						'Ticket_Id' => $input['ticket_id'],
						'Customer_Id' => $input['customer_id'],
						'Item_Type' => 'Consumable',
						'Item_Id' => $consumable['cons_id'],
						'Item_Code_Name' => $consumable['cons_codename'],
						'Item_Name' => $consumable['cons_name'],
						'Item_Units' => $consumable['cons_units'],
						'Item_Price' => $consumable['cons_price'],
					]);
			}
		}

		return response()->json('success');
	}
}
