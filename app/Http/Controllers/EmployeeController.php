<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class EmployeeController extends Controller
{
	public function GetEmployees(Request $request)
	{
		$input = $request->all();

		$select_company = DB::table('user_security')
			->select('company_name')
			->where('username', Auth::user()->username)
			->get();

		$product = DB::table('emp_details')
			->where('emp_status', '=', '1')
			->where('company_name', $select_company[0]->company_name)
			->get();

		//echo json_encode($product);die;
		return view('viewuser', compact('product'));
	}
	public function GetAllEmployees(Request $request)
	{
		$select_company = DB::table('user_security')
			->select('company_name')
			->where('username', Auth::user()->username)
			->get();

		$product = DB::table('emp_details')
			->where('emp_status', '=', '1')
			->where('company_name', $select_company[0]->company_name)
			->get();

		return response()->json($product);
	}

	public function GetRoles(Request $request)
	{
		$sql = DB::table('emp_role')
			->where('flag', '=', '1')
			->select('employee_role')
			->get();

		if (count($sql) >= 1) {
			return response()->json($sql);
		} else {
			return response()->json(['success' => false, 'message' => "No Data Found"], 200);
		}
	}

	public function GetTechnicians(Request $request)
	{
		$select_company = DB::table('user_security')
			->select('company_name')
			->where('username', Auth::user()->username)
			->get();

		$sql = DB::table('emp_details')
			->select('emp_firstname', 'emp_lastname', 'emp_branch_name')
			->where('emp_status', 1)
			->where('company_name', $select_company[0]->company_name)
			->get();

		if (count($sql) >= 1) {
			return response()->json($sql);
		} else {
			return response()->json(['success' => false, 'message' => "No Data Found"], 200);
		}
	}

	public function GetEmpName(Request $request)
	{
		$input = $request->all();
		$validator = Validator::make(
			$request->all(),
			[
				'username' => 'required'
			]
		);

		if ($validator->fails()) {
			$message = $validator->errors();
			return response()->json(['success' => false, 'message' => 'The given data was invalid.', 'errors' => $validator->errors()], 200);
		}

		$select_company = DB::table('user_security')
			->select('company_name')
			->where('username', $input['username'])
			->get();

		$sql = DB::table('emp_details')
			->select('emp_firstname', 'emp_lastname')
			->where('emp_status', 1)
			->where('company_name', $select_company[0]->company_name)
			->get();

		if (count($sql) >= 1) {
			return response()->json(['success' => true, 'Employee Details' => $sql], 200);
		} else {
			return response()->json(['success' => false, 'message' => "No Data Found"], 200);
		}
	}

	public function AddEmp(Request $request)
	{
		$validated = $request->validate([
			'fname' => 'required',
			'lname' => 'required',
			'emp_role' => 'required',
			'user_belongs_to' => 'required',
			'joining_date' => 'required',
			'phoneno' => 'required',
			'emp_address' => 'required',
			'emp_city' => 'required',
			'emp_state' => 'required',
			'emp_postal' => 'required',
			'aadhar' => 'required',
		]);

		$fname = $request['fname'];
		$lname = $request['lname'];
		$role = $request['emp_role'];
		$branch = $request['user_belongs_to'];
		$joining_date = $request['joining_date'];
		$resignation_date = $request['resignation_date'];
		$phoneno = $request['phoneno'];
		$alterphoneno = $request['alterphoneno'];
		$address = $request['emp_address'];
		$city = $request['emp_city'];
		$state = $request['emp_state'];
		$postal = $request['emp_postal'];
		$aadhar = $request['aadhar'];
		$pancard = $request['pancard'];

		$username = Auth::user()->username;
		$status = '1';
		$emp_const = "EMP";

		$select_company = DB::table('user_security')
			->select('company_name')
			->where('username', $username)
			->get();

		$select_sql = DB::table('emp_details')
			->where('emp_phone_no', $phoneno)
			->where('emp_branch_name', $branch)
			->where('company_name', $select_company[0]->company_name)
			->where('emp_status', '=', '1')
			->count();

		$emp_join_date = date('Y-m-d', strtotime($joining_date));
		$emp_res_date = date('Y-m-d', strtotime($resignation_date));

		if ($select_sql == 0) {
			$insert = DB::table('emp_details')
				->insert([
					'username' => $username,
					'emp_branch_name' => $branch,
					'emp_const' => $emp_const,
					'emp_firstname' => $fname,
					'emp_lastname' => $lname,
					'emp_role' => $role,
					//'emp_reports_to' => $input['emp_user_report'],
					'emp_phone_no' => $phoneno,
					'alter_phone_no' => $alterphoneno,
					'emp_address' => $address,
					'emp_city' => $city,
					'emp_state' => $state,
					'emp_pincode' => $postal,
					'emp_aadhar_no' => $aadhar,
					'emp_pan_no' => $pancard,
					'emp_join_date' => $emp_join_date,
					'emp_resign_date' => $emp_res_date,
					'emp_status' => $status,
					'company_name' => $select_company[0]->company_name
				]);
			return redirect('viewuser')->with('message', "Employee");
		} else {
			return redirect('viewuser')->with('error', 'Employee');
		}
	}

	public function AddRole(Request $request)
	{
		$validated = $request->validate([
			'add_role' => 'required',
		]);

		$role = $request['add_role'];
		$flag = 1;

		$select_sql = DB::table('emp_role')
			->where('employee_role', $role)
			->where('flag', '=', '1')
			->count();
		if ($select_sql == 0) {
			$insert = DB::table('emp_role')->insert(['employee_role' => $role, 'flag' => $flag]);
			return redirect()->back()->with('message', $role);
		} else {
			return redirect()->back()->with('error', $role);
		}
	}

	public function GetEmployee(Request $request)
	{
		$input = $request->all();
		/* $validator = Validator::make(
			$request->all(),
			[
				'branch' => 'required'
			]
		);

		if ($validator->fails()) {
			$message = $validator->errors();
			return response()->json(['success' => false, 'message' => 'The given data was invalid.', 'errors' => $validator->errors()], 200);
		} */

		$username = Auth::user()->username;
		$select_company = DB::table('user_security')
			->select('company_name')
			->where('username', $username)
			->get();

		$product = DB::table('emp_details')
			->where('emp_status', '=', '1')
			->where('company_name', $select_company[0]->company_name)
			//->where('emp_branch_name', $input['branch'])
			->where('emp_id', $input['id'])
			->get();

		if (count($product) >= 1) {
			return response()->json($product);
		} else {
			return response()->json(['success' => false, 'message' => "No Data Found"], 200);
		}
	}

	public function UpdateEmployee(Request $request)
	{
		$id = $request['edit_emp_id'];
		$fname = $request['edit_emp_fname'];
		$lname = $request['edit_emp_lname'];
		$branch = $request['emp_edit_user_belongs'];
		$role = $request['emp_edit_user_role'];
		$address = $request['edit_emp_address'];
		$city = $request['edit_emp_city'];
		$state = $request['edit_emp_state'];
		$postal = $request['edit_postal'];
		$phone = $request['edit_emp_phone'];
		$alterphoneno = $request['edit_alterphoneno'];
		$aadhar = $request['edit_emp_aadhar'];
		$pan = $request['edit_emp_pan'];
		$jdate = $request['edit_jdate'];
		$rdate = $request['edit_rdate'];

		$username = Auth::user()->username;
		$company = DB::table('user_security')
			->select('company_name')
			->where('username', $username)
			->get();

		$select_sql = DB::table('emp_details')
			->where('emp_id', $id)
			->where('company_name', $company[0]->company_name)
			->count();

		$emp_join_date = date('Y-m-d', strtotime($jdate));
		$emp_res_date = date('Y-m-d', strtotime($rdate));

		if ($select_sql == 1) {
			$sql = DB::table('emp_details')
				->where('emp_id', $id)
				->update([
					'emp_branch_name' => $branch,
					'emp_firstname' => $fname,
					'emp_lastname' => $lname,
					'emp_role' => $role,
					//'emp_reports_to' => $input['emp_edit_user_report'],
					'emp_phone_no' => $phone,
					'alter_phone_no' => $alterphoneno,
					'emp_address' => $address,
					'emp_city' => $city,
					'emp_pincode' => $postal,
					'emp_state' => $state,
					'emp_aadhar_no' => $aadhar,
					'emp_pan_no' => $pan,
					'emp_join_date' => $emp_join_date,
					'emp_resign_date' => $emp_res_date
				]);

			return redirect('viewuser')->with('update', "Employee");
		} else {
			return redirect('viewuser')->with('error', "Employee Update Failed Contact Admin");
		}
	}

	public function DeleteEmployee(Request $request, $id)
	{
		$username = Auth::user()->username;

		$company = DB::table('user_security')
			->select('company_name')
			->where('username', $username)
			->get();

		$select_sql = DB::table('emp_details')
			->where('emp_id', $id)
			->where('company_name', $company[0]->company_name)
			->count();

		if ($select_sql == 1) {
			$sql = DB::table('emp_details')
				->where('emp_id', $id)
				->where('company_name', $company[0]->company_name)
				->update(['emp_status' => 0]);

			//return response()->json(['success' => true, 'message' => 'Employee Deleted Successfully'], 200);
			return redirect('viewuser');
		} else {
			return response()->json(['success' => true, 'message' => 'Invalid Value'], 200);
		}
	}
}
