<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class PMSController extends Controller
{
	public function GetPMSs(Request $request)
	{
		$input = $request->all();

		$select_company = DB::table('user_security')
			->select('company_name')
			->where('username', Auth::user()->username)
			->get();

		$sqls = DB::select("SELECT max(Follow_Up_Date) as date from pms_details group by Machine_Serial_No");
		$amount = array_column($sqls, 'date');
		/* $sqls = DB::select("SELECT max(Follow_Up_Date) as amount from pms_details group by Machine_Serial_No");
		$amount = array_column($sqls, 'amount'); */

		$sql = DB::table('pms_details')
			->select('pms_details.Machine_Serial_No', 'pms_details.Follow_Up_Date', 'pms_details.Last_Followed_Date', 'Status', 'pms_details.Item_Model_Name', 'customer_detail.customer_name')
			->leftjoin('customer_detail', 'customer_detail.cus_id', '=', 'pms_details.Customer_Id')
			->leftjoin('contract_details', 'contract_details.Contract_ID', '=', 'pms_details.Contract_ID')
			->where('contract_details.Contract_Status', 'open')
			->whereIn('pms_details.Follow_Up_Date', $amount)
			->where('pms_details.company_name', $select_company[0]->company_name)
			->get();
		//->unique('pms_details.Machine_Serial_No');

		//return response()->json(['success' => true, 'pmsschedule' => $sql], 200);
		return view('pmsschedule', compact('sql'));
	}

	public function GetPMS(Request $request, $id)
	{
		$username = Auth::user()->username;
		$company = DB::table('user_security')
			->select('company_name')
			->where('username', $username)
			->get();

		$sql = DB::table('pms_details')
			->leftjoin('customer_detail', 'customer_detail.cus_id', '=', 'pms_details.Customer_Id')
			->where('pms_details.Machine_Serial_No', $id)
			->where('pms_details.company_name', $company[0]->company_name)
			->orderby('pms_details.Follow_Up_Date', 'DESC')
			->get();

		if (count($sql) >= 1) {
			return view('viewpms', compact('sql'));
		} else {
			return response()->json(['success' => true, 'message' => 'No Data Found'], 200);
		}
	}

	public function UpdatePMS(Request $request, $id)
	{
		$status = $request['edit_status'];
		$technician = $request['edit_technician'];

		$username = Auth::user()->username;
		$company = DB::table('user_security')
			->select('company_name')
			->where('username', $username)
			->get();

		$select_sql = DB::table('pms_details')
			->where('Status', 'open')
			->where('Contract_ID', $id)
			->where('company_name', $company[0]->company_name)
			->update([
				'Status' => $status,
				'Follow_Up_Date' => date('Y-m-d'),
				'assigned_technician' => $technician
			]);

		$select = DB::table('pms_details')
			->where('Contract_ID', $id)
			->orderby('Follow_Up_Date', 'DESC')
			->take(1)
			->get();

		$month = $select[0]->Maintenance_Period;

		$schedule_date = date('Y-m-d', strtotime("+$month months", strtotime($select[0]->Follow_Up_Date)));

		if ($schedule_date >= $select[0]->End_Date) {
			$end = $select[0]->End_Date;
		} else {
			$end = $schedule_date;
		}

		if ($select[0]->Status == 'close') {
			$insert_pm = DB::table('pms_details')
				->insert([
					'company_name' => $select[0]->company_name,
					'Username' => $username,
					'Customer_Id' => $select[0]->Customer_Id,
					'Item_Id' => $select[0]->Item_Id,
					'Item_Model_Name' => $select[0]->Item_Model_Name,
					'Machine_Serial_No' => $select[0]->Machine_Serial_No,
					'Contract_ID' => $select[0]->Contract_ID,
					'Last_Followed_Date' => $select[0]->Last_Followed_Date,
					'Follow_Up_Date' => $schedule_date,
					'Status' => 'open',
					'Maintenance_Period' => $select[0]->Maintenance_Period
				]);


			return redirect('pmsschedule')->with('update', "Pms_details");
		} else {
			return redirect('pmsschedule')->with('error', 'Pms_details Update Failed Contact Admin');
		}
	}
}
