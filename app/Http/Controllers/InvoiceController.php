<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\Date;

use function PHPUnit\Framework\returnArgument;

class InvoiceController extends Controller
{
	public function getAllInvoices(Request $request)
	{
		$select_company = DB::table('user_security')
			->select('company_name')
			->where('username', Auth::user()->username)
			->get();

		$invoicelist = DB::table('invoice_billing_details')
			->where('invoice_billing_details.Flag', 1)
			->leftjoin('customer_detail', 'customer_detail.cus_id', '=', 'invoice_billing_details.Customer_Id')
			->where('invoice_billing_details.company_name', $select_company[0]->company_name)
			->orderby('Invoice_ID', 'DESC')
			->get();
		//return response()->json($invoicelist);
		return view('viewinvoice', compact('invoicelist'));
	}

	public function AddInvoice(Request $request)
	{
		$input = $request->all();
		$username = Auth::user()->username;
		//$invoice = $input['invoice']['invoice_serial'];

		//get company name
		$select_company = DB::table('user_security')
			->select('company_name')
			->where('username', $username)
			->get();

		//ID increment
		$get_invoice = DB::table('invoice_billing_details')
			->select('Invoice_ID', 'Order_No')
			->orderby('Invoice_ID', 'DESC')
			->take(1)
			->get();
		$invoice_number  = $get_invoice[0]->Invoice_ID;
		$order_number  = $get_invoice[0]->Order_No;
		$add_invoice_number = $invoice_number + 1;
		$add_order_number = $order_number + 1;
		$flag = 1;

		if ($input['invoice']['invoice_serial'] == "others") {
			$serial = $input['invoice']['invoice_serial_no'];
		} else {
			$serial = $input['invoice']['invoice_serial'];
		}

		$invoice_billing_insert_sql = DB::table('invoice_billing_details')
			->insert([
				'Company_Name' => $select_company[0]->company_name,
				'Username' => $username,
				'Invoice_ID' => $add_invoice_number,
				'Seller_Branch_Id' => $input['branch_id'],
				'Customer_Id' => $input['cus_id'],
				'Customer_Phone_No' => $input['cust_name'],
				'Invoice_Date' => $input['invoice']['invoice_date'],
				'Invoice_Amount' => $input['invoice_amount'],
				'Invoice_Status' => 'pending',
				'Amount_Received' => '',
				'partial_amount' => '',
				'total_amount' => $input['invoice_amount'],
				'Order_No' => $add_order_number,
				'Delivery_Note' => $input['invoice']['delivery_note'],
				'Delivery_Note_Date' => $input['invoice']['del_note_dt'],
				'Supplier_Reference' => $input['invoice']['sup_ref_num'],
				'Other_Reference' => $input['invoice']['other_ref'],
				'Dispatch_Document_No' => $input['invoice']['dispatch_doc_no'],
				'Dispatched_Through' => $input['invoice']['dispatch_thru'],
				'Terms_Of_Delivery' => $input['invoice']['del_terms'],
				'Mode_Terms_Payment' => $input['invoice']['pymt_term_mode'],
				'Destination' => $input['invoice']['destination'],
				'delivery_address' => $input['invoice']['delivery_address'],
				'delivery_city' => $input['invoice']['delivery_city'],
				'delivery_state' => $input['invoice']['delivery_state'],
				'delivery_pincode' => $input['invoice']['delivery_postalcode'],
				'Contract_Id' => '',
				'invoice_number' => $input['invoice']['invoice_no'],
				'Invoice_amount_words' => $input['invoice_amount_words'],
				'Invoice_Created_Date' => date('Y-m-d'),
				'delivery_name' => $input['invoice']['delivery_name'],
				'delivery_no' => $input['invoice']['delivery_no'],
				'Status' => 'open',
				'Machine_Serial_No' => $serial,
				'flag' => $flag,
				'Buyer_Order_No' => $input['invoice']['buyer_no'],
				'Buyer_Order_Date' => $input['invoice']['buyer_date'],
				'Serial_Key_No' => '',
			]);

		foreach ($input['lineItems'] as $linetiemkey => $lineItem) {
			$tax = $lineItem["tax"];


			if ($lineItem['type'] == "PROD") {
				$type = "product";
				$productid = $lineItem['IDs'];
				//gets product_stock_unit value
				$select_product = DB::table('product_details')
					->where('product_id', $productid)
					->where('company_name', $select_company[0]->company_name)
					->select('product_stock_unit')
					->get();
				$product_stock_unit = $select_product[0]->product_stock_unit;
				$stock = $product_stock_unit - 1;

				//reduce product stock_unit value 
				if (count($select_product) == 1) {
					$sql = DB::table('product_details')
						->where('product_id', $productid)
						->update([
							'product_stock_unit' => $stock,
						]);

					//$data[$linetiemkey] = array("New product added" => $productid);
				}
			} else if ($lineItem['type'] == "SP") {
				$type = "spare";
				$spareid = $lineItem['IDs'];
				//gets spare_stock_unit value
				$select_spare = DB::table('spare_details')
					->where('spare_id', $spareid)
					->where('username', $username)
					->select('spare_stock_unit')
					->get();
				$spare_stock_unit = $select_spare[0]->spare_stock_unit;
				$stock = $spare_stock_unit - 1;

				//reduce spare stock_unit value 
				if (count($select_spare) == 1) {
					$sql = DB::table('spare_details')
						->where('spare_id', $spareid)
						->update([
							'spare_stock_unit' => $stock,
						]);
					//$data[$linetiemkey] = array("New spare added" => $spareid);
				}
			} else if ($lineItem['type'] == "CONSUM") {
				$type = "consumable";
				$consumableid = $lineItem['IDs'];
				//gets consumable_unit value
				$select_consumablet = DB::table('consumable_details')
					->where('consumable_id', $consumableid)
					->where('company_name', $select_company[0]->company_name)
					->select('consumable_unit')
					->get();
				$consumable_unit = $select_consumablet[0]->consumable_unit;
				$stock = $consumable_unit - 1;

				//reduce consumable stock_unit value 
				if (count($select_consumablet) == 1) {
					$sql = DB::table('consumable_details')
						->where('consumable_id', $consumableid)
						->update([
							'consumable_unit' => $stock,
						]);
					//$data[$linetiemkey] = array("New consumable added" => $consumableid);
				}
			} else if ($lineItem['type'] == "SER") {
				$type = "service";
			}

			$flag = 1;
			$invoice_items_insert_sql = DB::table('invoice_item_details')
				->insert([
					'Invoice_ID' => $add_invoice_number,
					'Customer_Phone_No' => $input['cust_name'],
					'description' => $lineItem['descriptions'],
					'Item_Type' => $type,
					'Item_Id' => $lineItem['IDs'],
					'Item_Name' => $lineItem['name'],
					'HSN_SAC' => $lineItem['hsn'],
					'GST' => $lineItem['gst'],
					'Unit_Price' => $lineItem['price'],
					'Quantity' => $lineItem['quantity'],
					'Discount' => $lineItem['discount'],
					'Item_Amount' => $lineItem['item_total'],
					'Total_Item_Amount' => '',
					'CGST_Percentage' => $tax['cgst'],
					'CGST_Amount' => $tax['cgst_amt'],
					'SGST_Percentage' => $tax['sgst'],
					'SGST_Amount' => $tax['sgst_amt'],
					'Tax_Amount' => $tax['tax_total'],
					'Total_Tax_Amount' => '',
					'Total_Bill_Amount' => $input['invoice_amount'],
					'Tax_Amount_Words' => $input['invoice_amount_words'],
				]);


			$select_serialno = DB::table('cus_prod_details')
				->where('Machine_Serial_No', $serial)
				->where('company_name', $select_company[0]->company_name)
				->count();

			if ($select_serialno != 0) {
				$update_invoice = DB::table('cus_prod_details')
					->where('Machine_Serial_No', $serial)
					->where('company_name', $select_company[0]->company_name)
					->update([
						'invoice_id' => $add_invoice_number,
					]);
			} else {
				$insert_machine = DB::table('cus_prod_details')
					->insert([
						'company_name' => $select_company[0]->company_name,
						'username' => $username,
						'cus_branch_name' => $input['branch_name'],
						'customer_id' => $input['cus_id'],
						'product_id' => $lineItem['IDs'],
						'Machine_Serial_No' => $serial,
						'invoice_id' => $add_invoice_number,
						'Flag' => $flag,
					]);
			}
		}

		return response()->json('success');
	}
	public function editInvoice(Request $request, $id)
	{
		$input = $request->all();
		$username = Auth::user()->username;
		$company = DB::table('user_security')
			->select('company_name')
			->where('username', $username)
			->get();
		$invoice_details = DB::table('invoice_billing_details')
			->where('invoice_billing_details.Invoice_ID', $id)
			->where('invoice_billing_details.Company_Name', $company[0]->company_name)
			->leftjoin('org_details', 'org_details.org_branch_id', '=', 'invoice_billing_details.Seller_Branch_Id')
			->leftjoin('customer_detail', 'customer_detail.cus_id', '=', 'invoice_billing_details.Customer_Id')
			->select(
				'org_details.*',
				'customer_detail.*',
				'invoice_billing_details.Buyer_Order_No',
				'invoice_billing_details.Buyer_Order_Date',
				'invoice_billing_details.Customer_Phone_No as cus_name',
				'invoice_billing_details.Machine_Serial_No as serial',
				'invoice_billing_details.Invoice_ID',
				'invoice_billing_details.Company_Name as company_name',
				'invoice_billing_details.Customer_Id as cus_id',
				'invoice_billing_details.Invoice_Date as invoice_date',
				'invoice_billing_details.Invoice_Amount as invoice_amount',
				'invoice_billing_details.Order_No as order_num',
				'invoice_billing_details.Delivery_Note as delivery_note',
				'invoice_billing_details.Delivery_Note_Date as del_note_dt',
				'invoice_billing_details.Supplier_Reference as sup_ref_num',
				'invoice_billing_details.Other_Reference as other_ref',
				'invoice_billing_details.Dispatch_Document_No as dispatch_doc_no',
				'invoice_billing_details.Dispatched_Through as dispatch_thru',
				'invoice_billing_details.Terms_Of_Delivery as del_terms',
				'invoice_billing_details.Mode_Terms_Payment as pymt_term_mode',
				'invoice_billing_details.destination',
				'invoice_billing_details.delivery_address',
				'invoice_billing_details.delivery_city',
				'invoice_billing_details.delivery_state',
				'invoice_billing_details.delivery_name',
				'invoice_billing_details.delivery_no',
				'invoice_billing_details.delivery_pincode',
				'invoice_billing_details.invoice_number as invoice_number',
				'invoice_billing_details.Invoice_amount_words as invoice_amount_words',
				'invoice_billing_details.Delivery_Note',
				'invoice_billing_details.Delivery_Note',
				'invoice_billing_details.Delivery_Note',
				'invoice_billing_details.Delivery_Note',
			)
			->get();

		$name = $invoice_details[0]->org_branch_name;

		$branch = DB::table('org_details')
			->where('org_status', '=', '1')
			->where('company_name', $company[0]->company_name)
			->select('org_branch_name', 'org_branch_id')
			->get();

		$cuslist = DB::table('customer_detail')
			->where('cus_status', '=', '1')
			->where('cus_branch_name', $name)
			->where('company_name', $company[0]->company_name)
			->get();

		//return  response()->json($invoice_details);
		return view('edit_invoice', compact('invoice_details', 'branch', 'cuslist'));
	}

	public function UpdateInvoice(Request $request, $id)
	{
		$input = $request->all();
		$username = Auth::user()->username;
		//get company name
		$select_company = DB::table('user_security')
			->select('company_name')
			->where('username', $username)
			->get();

		$flag = 1;
		if ($input['invoice']['invoice_serial'] == "others") {
			$serial = $input['invoice']['invoice_serial_no'];
		} else {
			$serial = $input['invoice']['invoice_serial'];
		}
		$date = date("Y-m-d", strtotime($input['invoice']['invoice_date']));

		$invoice_billing_insert_sql = DB::table('invoice_billing_details')
			->where('Invoice_ID', $id)
			->update([
				'Machine_Serial_No' => $serial,
				'Invoice_Date' => $date,
				'Buyer_Order_No' => $input['invoice']['buyer_no'],
				'Buyer_Order_Date' => $input['invoice']['buyer_date'],
				'Invoice_Amount' => $input['invoice_amount'],
				'total_amount' => $input['invoice_amount'],
				'Delivery_Note' => $input['invoice']['delivery_note'],
				'Delivery_Note_Date' => $input['invoice']['del_note_dt'],
				'Supplier_Reference' => $input['invoice']['sup_ref_num'],
				'Other_Reference' => $input['invoice']['other_ref'],
				'Dispatch_Document_No' => $input['invoice']['dispatch_doc_no'],
				'Dispatched_Through' => $input['invoice']['dispatch_thru'],
				'Terms_Of_Delivery' => $input['invoice']['del_terms'],
				'Mode_Terms_Payment' => $input['invoice']['pymt_term_mode'],
				'Destination' => $input['invoice']['destination'],
				'delivery_address' => $input['invoice']['delivery_address'],
				'delivery_city' => $input['invoice']['delivery_city'],
				'delivery_state' => $input['invoice']['delivery_state'],
				'delivery_pincode' => $input['invoice']['delivery_postalcode'],
				'invoice_number' => $input['invoice']['invoice_no'],
				'Invoice_amount_words' => $input['invoice_amount_words'],
				'delivery_name' => $input['invoice']['delivery_name'],
				'delivery_no' => $input['invoice']['delivery_no'],
			]);

		$data = [];
		foreach ($input['lineItems'] as $linetiemkey => $lineItem) {
			$tax = $lineItem["tax"];

			if ($lineItem['type'] == "PROD") {
				$type = "product";
				$productid = $lineItem['IDs'];
				//check if product already exist
				$product = DB::table('invoice_item_details')
					->where('Invoice_ID', $id)
					->where('Item_Id', $productid)
					->get();
				if (count($product) == 1) {
					$data[$linetiemkey] = array("product already exist" => $productid);
				} // Execute If product does not exist 
				else {
					//gets product_stock_unit value
					$select_product = DB::table('product_details')
						->where('product_id', $productid)
						->where('company_name', $select_company[0]->company_name)
						->select('product_stock_unit')
						->get();
					$product_stock_unit = $select_product[0]->product_stock_unit;
					$stock = $product_stock_unit - 1;

					//reduce product stock_unit value 
					if (count($select_product) == 1) {
						$sql = DB::table('product_details')
							->where('product_id', $productid)
							->update([
								'product_stock_unit' => $stock,
							]);

						$data[$linetiemkey] = array("New product added" => $productid);
					}
				}
			} else if ($lineItem['type'] == "SP") {
				$type = "spare";
				$spareid = $lineItem['IDs'];
				//check if spare already exist
				$spare = DB::table('invoice_item_details')
					->where('Invoice_ID', $id)
					->where('Item_Id', $spareid)
					->get();
				if (count($spare) == 1) {
					$data[$linetiemkey] = array("Spare already exist" => $spareid);
				} // Execute If spare does not exist 
				else {
					//gets spare_stock_unit value
					$select_spare = DB::table('spare_details')
						->where('spare_id', $spareid)
						->where('username', $username)
						->select('spare_stock_unit')
						->get();
					$spare_stock_unit = $select_spare[0]->spare_stock_unit;
					$stock = $spare_stock_unit - 1;

					//reduce spare stock_unit value 
					if (count($select_spare) == 1) {
						$sql = DB::table('spare_details')
							->where('spare_id', $spareid)
							->update([
								'spare_stock_unit' => $stock,
							]);
						$data[$linetiemkey] = array("New spare added" => $spareid);
					}
				}
			} else if ($lineItem['type'] == "CONSUM") {
				$type = "consumable";
				$consumableid = $lineItem['IDs'];
				//check if consumable already exist
				$consumable = DB::table('invoice_item_details')
					->where('Invoice_ID', $id)
					->where('Item_Id', $consumableid)
					->get();
				if (count($consumable) == 1) {
					$data[$linetiemkey] = array("consumable already exist" => $consumable);
				} // Execute If consumable does not exist 
				else {
					//gets consumable_unit value
					$select_consumablet = DB::table('consumable_details')
						->where('consumable_id', $consumableid)
						->where('company_name', $select_company[0]->company_name)
						->select('consumable_unit')
						->get();
					$consumable_unit = $select_consumablet[0]->consumable_unit;
					$stock = $consumable_unit - 1;

					//reduce consumable stock_unit value 
					if (count($select_consumablet) == 1) {
						$sql = DB::table('consumable_details')
							->where('consumable_id', $consumableid)
							->update([
								'consumable_unit' => $stock,
							]);
						$data[$linetiemkey] = array("New consumable added" => $consumableid);
					}
				}
			} else if ($lineItem['type'] == "SER") {
				$type = "service";
			}
			$flag = 1;
			$select = DB::table('invoice_item_details')
				->where('Invoice_ID', $id)
				->where('Item_Id', $lineItem['IDs'])
				->where('Item_Type', $type)
				->where('Item_Name', $lineItem['name'])
				->count();

			if ($select == 1) {
				$invoice_items_insert_sql = DB::table('invoice_item_details')
					->where('Invoice_ID', $id)
					->where('Item_Id', $lineItem['IDs'])
					->where('Item_Type', $type)
					->where('Item_Name', $lineItem['name'])
					->update([
						'description' => $lineItem['descriptions'],
						'Item_Type' => $type,
						'Item_Id' => $lineItem['IDs'],
						'Item_Name' => $lineItem['name'],
						'HSN_SAC' => $lineItem['hsn'],
						'GST' => $lineItem['gst'],
						'Unit_Price' => $lineItem['price'],
						'Quantity' => $lineItem['quantity'],
						'Discount' => $lineItem['discount'],
						'Item_Amount' => $lineItem['item_total'],
						'CGST_Percentage' => $tax['cgst'],
						'CGST_Amount' => $tax['cgst_amt'],
						'SGST_Percentage' => $tax['sgst'],
						'SGST_Amount' => $tax['sgst_amt'],
						'Tax_Amount' => $tax['tax_total'],
						'Total_Bill_Amount' => $input['invoice_amount'],
						'Tax_Amount_Words' => $input['invoice_amount_words'],
					]);
			} else {
				$invoice_items_insert_sql = DB::table('invoice_item_details')
					->insert([
						'Invoice_ID' => $id,
						'Customer_Phone_No' => $input['cust_name'],
						'description' => $lineItem['descriptions'],
						'Item_Type' => $type,
						'Item_Id' => $lineItem['IDs'],
						'Item_Name' => $lineItem['name'],
						'HSN_SAC' => $lineItem['hsn'],
						'GST' => $lineItem['gst'],
						'Unit_Price' => $lineItem['price'],
						'Quantity' => $lineItem['quantity'],
						'Discount' => $lineItem['discount'],
						'Item_Amount' => $lineItem['item_total'],
						'Total_Item_Amount' => '',
						'CGST_Percentage' => $tax['cgst'],
						'CGST_Amount' => $tax['cgst_amt'],
						'SGST_Percentage' => $tax['sgst'],
						'SGST_Amount' => $tax['sgst_amt'],
						'Tax_Amount' => $tax['tax_total'],
						'Total_Tax_Amount' => '',
						'Total_Bill_Amount' => $input['invoice_amount'],
						'Tax_Amount_Words' => $input['invoice_amount_words'],
					]);
			}
			if ($serial != "") {
				if ($type == "product") {
					$select_serialno = DB::table('cus_prod_details')
						->where('Machine_Serial_No', $serial)
						->where('company_name', $select_company[0]->company_name)
						->count();

					if ($select_serialno != 0) {
						$update_invoice = DB::table('cus_prod_details')
							->where('Machine_Serial_No', $serial)
							->where('company_name', $select_company[0]->company_name)
							->update([
								'invoice_id' => $id,
							]);
					} else {
						$insert_machine = DB::table('cus_prod_details')
							->insert([
								'company_name' => $select_company[0]->company_name,
								'username' => $username,
								'cus_branch_name' => $input['branch_name'],
								'customer_id' => $input['cus_id'],
								'product_id' => $lineItem['IDs'],
								'Machine_Serial_No' => $serial,
								'invoice_id' => $id,
								'Flag' => $flag,
							]);
					}
				}
			}
		}
		//return response()->json($data);
		return response()->json('success');
	}

	public function AddChallan(Request $request)
	{
		$input = $request->all();
		$username = Auth::user()->username;
		//$invoice = $input['invoice']['invoice_serial'];

		$select_company = DB::table('user_security')
			->select('company_name')
			->where('username', $username)
			->get();

		$get_challan = DB::table('delivery_challan_details')
			->select('Challan_ID')
			->orderby('Challan_ID', 'DESC')
			->take(1)
			->get();
		$get_expenses = DB::table('expenses_details')
			->select('id')
			->orderby('id', 'DESC')
			->take(1)
			->get();


		$challan_number  = $get_challan[0]->Challan_ID;
		$add_challan_number = $challan_number + 1;
		$const = "DC";
		$flag = 1;

		$expense_number  = $get_expenses[0]->id;
		$add_expense_number = $expense_number + 1;
		$expense_id = "EXP" . $add_expense_number;

		if ($input['invoice']['invoice_serial'] == "others") {
			$serial = $input['invoice']['invoice_serial_no'];
		} else {
			$serial = $input['invoice']['invoice_serial'];
		}

		/* 	$invoice_billing_insert_sql = DB::table('delivery_challan_details')
			->insert([
				'Company_Name' => $select_company[0]->company_name,
				'Username' => $username,
				'Challan_Const' => $const,
				'Challan_ID' => $add_challan_number,
				'Seller_Branch_Id' => $input['branch_id'],
				'Customer_Id' => $input['cus_id'],
				'Customer_Name' => $input['cust_name'],
				'Delivery_Note' => $input['delivery_note'],
				'Delivery_Date' => $input['delivery_date'],
				'Buyer_Order_No' => $input['buyer_no'],
				'Buyer_Order_Date' => $input['buyer_date'],
				'Supplier_Reference' => $input['sup_ref_num'],
				'Other_Reference' => $input['other_ref'],
				'Dispatch_Document_No' => $input['dispatch_doc_no'],
				'Dispatched_Through' => $input['dispatch_thru'],
				'Terms_Of_Delivery' => $input['del_terms'],
				'Mode_Terms_Payment' => $input['pymt_term_mode'],
				'Destination' => $input['destination'],
				'delivery_address' => $input['delivery_address'],
				'delivery_city' => $input['delivery_city'],
				'delivery_state' => $input['delivery_state'],
				'delivery_pincode' => $input['delivery_postalcode'],
				'Amount' => $input['invoice_amount'],
				'Created_Date' => date('Y-m-d'),
				'delivery_name' => $input['delivery_name'],
				'delivery_no' => $input['delivery_no'],
				'Machine_Serial_No' => $input['serial'],
				'flag' => $flag,
			]); */
		//POSTMAN



		$invoice_billing_insert_sql = DB::table('delivery_challan_details')
			->insert([
				'Company_Name' => $select_company[0]->company_name,
				'Username' => $username,
				'Challan_Const' => $const,
				'Challan_ID' => $add_challan_number,
				'Seller_Branch_Id' => $input['branch_id'],
				'Customer_Id' => $input['cus_id'],
				'Customer_Name' => $input['cust_name'],
				'Delivery_Note' => $input['invoice']['delivery_note'],
				'Delivery_Date' => $input['invoice']['delivery_date'],
				'Buyer_Order_No' => $input['invoice']['buyer_no'],
				'Buyer_Order_Date' => $input['invoice']['buyer_date'],
				'Supplier_Reference' => $input['invoice']['sup_ref_num'],
				'Other_Reference' => $input['invoice']['other_ref'],
				'Dispatch_Document_No' => $input['invoice']['dispatch_doc_no'],
				'Dispatched_Through' => $input['invoice']['dispatch_thru'],
				'Terms_Of_Delivery' => $input['invoice']['del_terms'],
				'Mode_Terms_Payment' => $input['invoice']['pymt_term_mode'],
				'Destination' => $input['invoice']['destination'],
				'delivery_address' => $input['invoice']['delivery_address'],
				'delivery_city' => $input['invoice']['delivery_city'],
				'delivery_state' => $input['invoice']['delivery_state'],
				'delivery_pincode' => $input['invoice']['delivery_postalcode'],
				'Amount' => $input['invoice_amount'],
				'amount_words' => $input['invoice_amount_words'],
				'Created_Date' => date('Y-m-d'),
				'delivery_name' => $input['invoice']['delivery_name'],
				'delivery_no' => $input['invoice']['delivery_no'],
				'Machine_Serial_No' => $serial,
				'flag' => $flag,
			]);

		$sum = 0;
		foreach ($input['lineItems'] as $linetiemkey => $lineItem) {
			$tax = $lineItem["tax"];
			if ($lineItem['type'] == "PROD") {
				$type = "product";
			} else if ($lineItem['type'] == "SP") {
				$type = "spare";
			} else if ($lineItem['type'] == "CONSUM") {
				$type = "consumable";
			} else if ($lineItem['type'] == "SER") {
				$type = "service";
			}
			$flag = 1;
			$sum += $lineItem['unit'];

			$invoice_items_insert_sql = DB::table('delivery_item_details')
				->insert([
					'Challan_ID' => $add_challan_number,
					'Customer_Name' => $input['cust_name'],
					'description' => $lineItem['descriptions'],
					'Item_Type' => $type,
					'Item_Id' => $lineItem['IDs'],
					'Item_Name' => $lineItem['name'],
					'HSN_SAC' => $lineItem['hsn'],
					'GST' => $lineItem['gst'],
					'Unit_Price' => $lineItem['price'],
					'Quantity' => $lineItem['quantity'],
					'Discount' => $lineItem['discount'],
					'Item_Amount' => $lineItem['item_total'],
					'Total_Item_Amount' => '',
					'CGST_Percentage' => $tax['cgst'],
					'CGST_Amount' => $tax['cgst_amt'],
					'SGST_Percentage' => $tax['sgst'],
					'SGST_Amount' => $tax['sgst_amt'],
					'Tax_Amount' => $tax['tax_total'],
					'Total_Tax_Amount' => $tax['tot_amount'],
					'Total_Bill_Amount' => $input['invoice_amount'],
					'Tax_Amount_Words' => $input['invoice_amount_words'],
				]);

			if ($type == "product") {
				$select_serialno = DB::table('cus_prod_details')
					->where('Machine_Serial_No', $serial)
					->where('company_name', $select_company[0]->company_name)
					->count();

				if ($select_serialno != 0) {
					$update_invoice = DB::table('cus_prod_details')
						->where('Machine_Serial_No', $serial)
						->where('company_name', $select_company[0]->company_name)
						->update([
							'challan_id' => $add_challan_number,
						]);
				} else {
					$insert_machine = DB::table('cus_prod_details')
						->insert([
							'company_name' => $select_company[0]->company_name,
							'username' => $username,
							'cus_branch_name' => $input['branch_name'],
							'customer_id' => $input['cus_id'],
							'product_id' => $lineItem['IDs'],
							'Machine_Serial_No' => $serial,
							'challan_id' => $add_challan_number,
							'Flag' => $flag,
							'Created_Date' => date('Y-m-d'),
						]);
				}
			} else {
				return response()->json("DB Error");
				break;
			}
		}
		if ($sum > 0) {
			$insert = DB::table('expenses_details')
				->insert([
					'expenses_id' => $expense_id,
					'Expenses_Date' => $input['invoice']['delivery_date'],
					'Expenses_Amount' => $sum,
					'Customer_Name' => $input['cust_name'],
					'challan_id' => $add_challan_number,
					'Branch' => $input['branch_name'],
					'Company_Name' => $select_company[0]->company_name,
					'User_Name' => $username,
					'machine_id' => $serial,
					'Expenses_Type' => $type,
				]);
		}

		return response()->json('success');
		//return view('viewinvoice', compact('invoicelist'));
	}

	public function editChallan(Request $request, $id)
	{
		$input = $request->all();
		$username = Auth::user()->username;
		$company = DB::table('user_security')
			->select('company_name')
			->where('username', $username)
			->get();

		$challan_details = DB::table('delivery_challan_details')
			->where('delivery_challan_details.Challan_ID', $id)
			->where('delivery_challan_details.Company_Name', $company[0]->company_name)
			->leftjoin(
				'delivery_item_details',
				'delivery_item_details.Challan_ID',
				'=',
				'delivery_challan_details.Challan_ID'
			)
			->leftjoin(
				'customer_detail',
				'customer_detail.cus_id',
				'=',
				'delivery_challan_details.Customer_Id'
			)
			->leftjoin(
				'org_details',
				'org_details.org_branch_id',
				'=',
				'delivery_challan_details.Seller_Branch_Id'
			)
			->select(
				'delivery_challan_details.*',
				'customer_detail.*',
				'delivery_item_details.*',
				'org_details.*'
			)
			->first();

		$name = $challan_details->org_branch_name;

		$branch = DB::table('org_details')
			->where('org_status', '=', '1')
			->where('company_name', $company[0]->company_name)
			->select('org_branch_name', 'org_branch_id')
			->get();

		$cuslist = DB::table('customer_detail')
			->where('cus_status', '=', '1')
			->where('cus_branch_name', $name)
			->where('company_name', $company[0]->company_name)
			->get();

		//return  response()->json($sql);
		return view('edit_dc', compact('challan_details', 'branch', 'cuslist'));
	}

	public function UpdateChallan(Request $request, $id)
	{
		$input = $request->all();
		$username = Auth::user()->username;
		$select_company = DB::table('user_security')
			->select('company_name')
			->where('username', $username)
			->get();

		$flag = 1;
		if ($input['invoice']['invoice_serial'] == "others") {
			$serial = $input['invoice']['invoice_serial_no'];
		} else {
			$serial = $input['invoice']['invoice_serial'];
		}
		$date = date("Y-m-d", strtotime($input['invoice']['delivery_date']));

		$invoice_billing_insert_sql = DB::table('delivery_challan_details')
			->where('Challan_ID', $id)
			->update([
				'Machine_Serial_No' => $serial,
				'Buyer_Order_No' => $input['invoice']['buyer_no'],
				'Buyer_Order_Date' => $input['invoice']['buyer_date'],
				'Amount' => $input['invoice_amount'],
				'Delivery_Note' => $input['invoice']['delivery_note'],
				'Delivery_Date' => $date,
				'Supplier_Reference' => $input['invoice']['sup_ref_num'],
				'Other_Reference' => $input['invoice']['other_ref'],
				'Dispatch_Document_No' => $input['invoice']['dispatch_doc_no'],
				'Dispatched_Through' => $input['invoice']['dispatch_thru'],
				'Terms_Of_Delivery' => $input['invoice']['del_terms'],
				'Mode_Terms_Payment' => $input['invoice']['pymt_term_mode'],
				'Destination' => $input['invoice']['destination'],
				'delivery_address' => $input['invoice']['delivery_address'],
				'delivery_city' => $input['invoice']['delivery_city'],
				'delivery_state' => $input['invoice']['delivery_state'],
				'delivery_pincode' => $input['invoice']['delivery_postalcode'],
				'delivery_name' => $input['invoice']['delivery_name'],
				'delivery_no' => $input['invoice']['delivery_no'],
			]);

		foreach ($input['lineItems'] as $linetiemkey => $lineItem) {
			$tax = $lineItem["tax"];
			if ($lineItem['type'] == "PROD") {
				$type = "product";
			} else if ($lineItem['type'] == "SP") {
				$type = "spare";
			} else if ($lineItem['type'] == "CONSUM") {
				$type = "consumable";
			} else if ($lineItem['type'] == "SER") {
				$type = "service";
			}
			$flag = 1;
			$select = DB::table('delivery_item_details')
				->where('Challan_ID', $id)
				->where('Item_Id', $lineItem['IDs'])
				->where('Item_Type', $type)
				->where('Item_Name', $lineItem['name'])
				->count();

			if ($select == 1) {
				$invoice_items_insert_sql = DB::table('delivery_item_details')
					->where('Challan_ID', $id)
					->where('Item_Id', $lineItem['IDs'])
					->where('Item_Type', $type)
					->where('Item_Name', $lineItem['name'])
					->insert([
						'description' => $lineItem['descriptions'],
						'Item_Type' => $type,
						'Item_Id' => $lineItem['IDs'],
						'Item_Name' => $lineItem['name'],
						'HSN_SAC' => $lineItem['hsn'],
						'GST' => $lineItem['gst'],
						'Unit_Price' => $lineItem['price'],
						'Quantity' => $lineItem['quantity'],
						'Discount' => $lineItem['discount'],
						'Item_Amount' => $lineItem['item_total'],
						'CGST_Percentage' => $tax['cgst'],
						'CGST_Amount' => $tax['cgst_amt'],
						'SGST_Percentage' => $tax['sgst'],
						'SGST_Amount' => $tax['sgst_amt'],
						'Tax_Amount' => $tax['tax_total'],
						'Total_Bill_Amount' => $input['invoice_amount'],
						'Tax_Amount_Words' => $input['invoice_amount_words'],
					]);
			} else {
				$invoice_items_insert_sql = DB::table('delivery_item_details')
					->insert([
						'Challan_ID' => $id,
						'Customer_Name' => $input['cust_name'],
						'description' => $lineItem['descriptions'],
						'Item_Type' => $type,
						'Item_Id' => $lineItem['IDs'],
						'Item_Name' => $lineItem['name'],
						'HSN_SAC' => $lineItem['hsn'],
						'GST' => $lineItem['gst'],
						'Unit_Price' => $lineItem['price'],
						'Quantity' => $lineItem['quantity'],
						'Discount' => $lineItem['discount'],
						'Item_Amount' => $lineItem['item_total'],
						'Total_Item_Amount' => '',
						'CGST_Percentage' => $tax['cgst'],
						'CGST_Amount' => $tax['cgst_amt'],
						'SGST_Percentage' => $tax['sgst'],
						'SGST_Amount' => $tax['sgst_amt'],
						'Tax_Amount' => $tax['tax_total'],
						'Total_Tax_Amount' => $tax['tot_amount'],
						'Total_Bill_Amount' => $input['invoice_amount'],
						'Tax_Amount_Words' => $input['invoice_amount_words'],
					]);
			}
			if ($serial != "") {
				if ($type == "product") {
					$select_serialno = DB::table('cus_prod_details')
						->where('Machine_Serial_No', $serial)
						->where('company_name', $select_company[0]->company_name)
						->count();

					if ($select_serialno != 0) {
						$update_invoice = DB::table('cus_prod_details')
							->where('Machine_Serial_No', $serial)
							->where('company_name', $select_company[0]->company_name)
							->update([
								'challan_id' => $id,
							]);
					} else {
						$insert_machine = DB::table('cus_prod_details')
							->insert([
								'company_name' => $select_company[0]->company_name,
								'username' => $username,
								'cus_branch_name' => $input['branch_name'],
								'customer_id' => $input['cus_id'],
								'product_id' => $lineItem['IDs'],
								'Machine_Serial_No' => $serial,
								'challan_id' => $id,
								'Flag' => $flag,
							]);
					}
				}
			}
		}
		return response()->json('success');
	}


	//AJAX call for Invoice
	public function getInvoices(Request $request) //gets all invoice list
	{
		$select_company = DB::table('user_security')
			->select('company_name')
			->where('username', Auth::user()->username)
			->get();

		$select = DB::table('invoice_billing_details')
			->where('invoice_billing_details.Flag', 1)
			->leftjoin('customer_detail', 'customer_detail.cus_id', '=', 'invoice_billing_details.Customer_Id')
			->where('invoice_billing_details.company_name', $select_company[0]->company_name)
			->orderby('Invoice_ID', 'DESC')
			->get();
		return json_encode($select);
	}
	public function getInvoiceDetails(Request $request) //gets invoice order summary
	{
		$input = $request->all();
		$username = Auth::user()->username;
		$select_company = DB::table('user_security')
			->select('company_name')
			->where('username', $username)
			->get();

		$sqls = DB::table('org_details')
			->select('org_branch_id')
			->where('org_branch_id', 'Seller_Branch_Id')
			->get();

		$select = DB::table('invoice_billing_details')
			->where('invoice_billing_details.Invoice_ID', $input['id'])
			->where('invoice_billing_details.Company_Name', $select_company[0]->company_name)
			->leftjoin('invoice_item_details', 'invoice_item_details.Invoice_ID', '=', 'invoice_billing_details.Invoice_ID')
			->whereRaw('Item_Type <> ""')
			->select(
				'invoice_item_details.Item_Type as type',
				'invoice_item_details.description as descriptions',
				'invoice_item_details.Invoice_ID',
				'invoice_item_details.Item_Id',
				'invoice_item_details.Item_Name as name',
				'invoice_item_details.HSN_SAC as hsn',
				'invoice_item_details.GST as gst',
				'invoice_item_details.Unit_Price as price',
				'invoice_item_details.Quantity as quantity',
				'invoice_item_details.Discount as discount',
				'invoice_item_details.Item_Amount as item_total',
				'invoice_item_details.CGST_Percentage as cgst',
				'invoice_item_details.CGST_Amount as cgst_amount',
				'invoice_item_details.SGST_Percentage as sgst',
				'invoice_item_details.SGST_Amount as sgst_amount',
				'invoice_item_details.Tax_Amount as tax_amount',
			)
			->get();
		return response()->json($select);
	}
	public function getInvoiceTotal(Request $request) //gets invoice item total and tax total
	{
		$input = $request->all();
		$username = Auth::user()->username;
		$select_company = DB::table('user_security')
			->select('company_name')
			->where('username', $username)
			->get();
		$select = DB::table('invoice_billing_details')
			->where('invoice_billing_details.Invoice_ID', $input['id'])
			->where('invoice_billing_details.Company_Name', $select_company[0]->company_name)
			->leftjoin('invoice_item_details', 'invoice_item_details.Invoice_ID', '=', 'invoice_billing_details.Invoice_ID')
			->whereRaw('Item_Type <> ""')
			->select([
				DB::raw("SUM(Item_Amount) as items_total"),
				DB::raw("SUM(sgst_amount + cgst_amount) as total_tax"),
			])
			->get();

		return response()->json($select);
	} //End of AJAX CALL


	public function getAllChallan(Request $request)
	{

		$select_company = DB::table('user_security')
			->select('company_name')
			->where('username', Auth::user()->username)
			->get();

		$challan = DB::table('delivery_challan_details')
			->select('delivery_challan_details.Delivery_Note', 'delivery_challan_details.Challan_Const', 'delivery_challan_details.Challan_ID', 'delivery_challan_details.Delivery_Date', 'delivery_challan_details.Customer_Name', 'customer_detail.cus_phone_no', 'customer_detail.cus_id')
			->where('delivery_challan_details.Flag', 1)
			->leftjoin('customer_detail', 'customer_detail.cus_id', '=', 'delivery_challan_details.Customer_Id')
			->where('delivery_challan_details.company_name', $select_company[0]->company_name)
			->orderby('Delivery_Date', 'DESC')
			->get();

		//echo json_encode($select);die;
		return view('viewdc', compact('challan'));
	}
	//AJAX call for Delivery Challan
	public function getAlldc(Request $request) //gets all Delivery Challan list
	{

		$select_company = DB::table('user_security')
			->select('company_name')
			->where('username', Auth::user()->username)
			->get();

		$select = DB::table('delivery_challan_details')
			->select('delivery_challan_details.Delivery_Note', 'delivery_challan_details.Challan_Const', 'delivery_challan_details.Challan_ID', 'delivery_challan_details.Delivery_Date', 'delivery_challan_details.Customer_Name', 'customer_detail.cus_phone_no', 'customer_detail.cus_id')
			->where('delivery_challan_details.Flag', 1)
			->leftjoin('customer_detail', 'customer_detail.cus_id', '=', 'delivery_challan_details.Customer_Id')
			->where('delivery_challan_details.company_name', $select_company[0]->company_name)
			->orderby('Delivery_Date', 'DESC')
			->get();

		//return view('viewdc', compact('select'));
		return  response()->json($select);
	}
	public function getChallanDetails(Request $request) //gets Challan detail
	{
		$input = $request->all();
		$username = Auth::user()->username;
		$select_company = DB::table('user_security')
			->select('company_name')
			->where('username', $username)
			->get();
		$select = DB::table('delivery_challan_details')
			->where('delivery_challan_details.Challan_ID', $input['id'])
			->where('delivery_challan_details.Company_Name', $select_company[0]->company_name)
			->leftjoin(
				'delivery_item_details',
				'delivery_item_details.Challan_ID',
				'=',
				'delivery_challan_details.Challan_ID'
			)
			->leftjoin(
				'customer_detail',
				'customer_detail.cus_id',
				'=',
				'delivery_challan_details.Customer_Id'
			)
			->leftjoin(
				'org_details',
				'org_details.org_branch_id',
				'=',
				'delivery_challan_details.Seller_Branch_Id'
			)
			->select(
				'delivery_challan_details.*',
				'customer_detail.*',
				'delivery_item_details.*',
				'org_details.*'
			)
			->first();

		return  response()->json($select);
	}

	public function getChallanItemDetails(Request $request) //gets Challan order summary
	{
		$input = $request->all();
		$username = Auth::user()->username;
		$select_company = DB::table('user_security')
			->select('company_name')
			->where('username', $username)
			->get();

		$select = DB::table('delivery_item_details')
			->where('delivery_challan_details.company_name', $select_company[0]->company_name)
			->where('delivery_challan_details.Challan_ID', $input['id'])
			->leftjoin('delivery_challan_details', 'delivery_challan_details.Challan_ID', '=', 'delivery_item_details.Challan_ID')

			->whereRaw('Item_Type <> ""')
			->select(
				'delivery_item_details.Item_Type as type',
				'delivery_item_details.description as descriptions',
				'delivery_item_details.Challan_ID',
				'delivery_item_details.Item_Id as id',
				'delivery_item_details.Item_Name as name',
				'delivery_item_details.HSN_SAC as hsn',
				'delivery_item_details.GST as gst',
				'delivery_item_details.Unit_Price as price',
				'delivery_item_details.Quantity as quantity',
				'delivery_item_details.Discount as discount',
				'delivery_item_details.Item_Amount as item_total',
				'delivery_item_details.CGST_Percentage as cgst',
				'delivery_item_details.CGST_Amount as cgst_amount',
				'delivery_item_details.SGST_Percentage as sgst',
				'delivery_item_details.SGST_Amount as sgst_amount',
				'delivery_item_details.Tax_Amount as tax_amount',
				'delivery_item_details.Total_Bill_Amount as tot',
			)
			->get();
		return response()->json($select);
	}

	public function getChallanTotal(Request $request) //gets Challan item total and tax total
	{
		$input = $request->all();
		$username = Auth::user()->username;
		$select_company = DB::table('user_security')
			->select('company_name')
			->where('username', $username)
			->get();
		$select = DB::table('delivery_challan_details')
			->where('delivery_challan_details.Challan_ID', $input['id'])
			->where('delivery_challan_details.Company_Name', $select_company[0]->company_name)
			->leftjoin('delivery_item_details', 'delivery_item_details.Challan_ID', '=', 'delivery_challan_details.Challan_ID')
			->select([
				DB::raw("SUM(Item_Amount) as items_total"),
				DB::raw("SUM(CGST_Amount + SGST_Amount) as total_tax"),
			])
			->get();

		return response()->json($select);
	}

	public function DeleteInvoice(Request $request, $id)
	{
		$username = Auth::user()->username;

		$select_sql = DB::table('invoice_billing_details')
			->where('username', $username)
			->where('Invoice_ID',  $id)
			->count();

		if ($select_sql == 1) {
			$update_sql = DB::table('invoice_billing_details')
				->where('username', $username)
				->where('Invoice_ID',  $id)
				->update(['flag' => 0]);
			if ($update_sql == 1) {
				$sql = DB::table('cus_prod_details')
					->where('username', $username)
					->where('Invoice_ID',  $id)
					->update(['invoice_id' => '']);
				//return response()->json(['success' => true, 'message' => 'Invoice deleted'], 200);
				return redirect('viewinvoice');
			} else {
				return response()->json(['success' => true, 'message' => 'failed cus_prod_details'], 200);;
			}
		} else {
			return response()->json(['success' => true, 'message' => 'Invalid Value'], 200);
		}
	}
	public function DeleteChallan(Request $request, $id)
	{
		$username = Auth::user()->username;

		$select_sql = DB::table('delivery_challan_details')
			->where('username', $username)
			->where('Challan_ID',  $id)
			->count();

		if ($select_sql == 1) {
			$update_sql = DB::table('delivery_challan_details')
				->where('username', $username)
				->where('Challan_ID',  $id)
				->update(['flag' => 0]);
			if ($update_sql == 1) {
				$sql = DB::table('cus_prod_details')
					->where('username', $username)
					->where('challan_id',  $id)
					->update(['challan_id' => '']);
				//return response()->json(['success' => true, 'message' => 'Challan deleted'], 200);
				return redirect('viewdc');
			} else {
				return response()->json(['success' => true, 'message' => 'failed cus_prod_details'], 200);;
			}
		} else {
			return response()->json(['success' => true, 'message' => 'Invalid Value'], 200);
		}
	}
	//End of AJAX CALL

	function printInvoice(Request $request, $inv_id, $branch_id, $cus_id)
	{
		/* $data = $request->all();
		$invoice_details = $data['invoiceObj'];
		$branch_details = $data['branchObj'];
		$customer_details = $data['customerObj'];
		$total = $data['totalObj']; */

		//return response()->json($data);

		/* $pdf = PDF::loadView('invoiceprint');
		return $pdf->stream("invoiceprint.pdf", array("Attachment" => false)); */


		$username = Auth::user()->username;
		$select_company = DB::table('user_security')
			->select('company_name')
			->where('username', $username)
			->get();

		$org = DB::table('org_details')
			->join('invoice_billing_details', 'invoice_billing_details.Seller_Branch_Id', "=", 'org_details.org_branch_id')
			->where('org_status', '1')
			->where('org_details.org_branch_id', $branch_id)
			->where('invoice_billing_details.Seller_Branch_Id', $branch_id)
			->where('invoice_billing_details.Invoice_ID', $inv_id)
			//->where('company_name', $select_company[0]->company_name)
			->get();
		$cus = DB::table('customer_detail')
			->where('customer_detail.cus_id', $cus_id)
			->where('company_name', $select_company[0]->company_name)
			->get();

		$invoice_details = DB::table('invoice_billing_details')
			->where('invoice_billing_details.Invoice_ID', $inv_id)
			->where('invoice_billing_details.Company_Name', $select_company[0]->company_name)
			->leftjoin('invoice_item_details', 'invoice_item_details.Invoice_ID', '=', 'invoice_billing_details.Invoice_ID')
			->whereRaw('Item_Type <> ""')
			->select(
				'invoice_item_details.Item_Type as type',
				'invoice_item_details.description as descriptions',
				'invoice_item_details.Invoice_ID',
				'invoice_item_details.Item_Id',
				'invoice_item_details.Item_Name as name',
				'invoice_item_details.HSN_SAC as hsn',
				'invoice_item_details.GST as gst',
				'invoice_item_details.Unit_Price as price',
				'invoice_item_details.Quantity as quantity',
				'invoice_item_details.Discount as discount',
				'invoice_item_details.Item_Amount as item_total',
				'invoice_item_details.CGST_Percentage as cgst',
				'invoice_item_details.CGST_Amount as cgst_amount',
				'invoice_item_details.SGST_Percentage as sgst',
				'invoice_item_details.SGST_Amount as sgst_amount',
				'invoice_item_details.Tax_Amount as tax_amount',
			)->get();

		$total = DB::table('invoice_billing_details')
			->where('invoice_billing_details.Invoice_ID', $inv_id)
			->where('invoice_billing_details.Company_Name', $select_company[0]->company_name)
			->leftjoin('invoice_item_details', 'invoice_item_details.Invoice_ID', '=', 'invoice_billing_details.Invoice_ID')
			->whereRaw('Item_Type <> ""')
			->select([
				DB::raw("SUM(Item_Amount) as items_total"),
				DB::raw("SUM(sgst_amount + cgst_amount) as total_tax"),
			])->get();

		//return response()->json($cusdetails);
		$pdf = PDF::loadView('invoiceprint', compact('org', 'cus', 'invoice_details', 'total'));
		return $pdf->stream("invoiceprint.pdf", array("Attachment" => false));
	}
	function printChallan(Request $request, $id)
	{
		$username = Auth::user()->username;
		$select_company = DB::table('user_security')
			->select('company_name')
			->where('username', $username)
			->get();
		$challan = DB::table('delivery_challan_details')
			->where('delivery_challan_details.Challan_ID', $id)
			->where('delivery_challan_details.Company_Name', $select_company[0]->company_name)
			->leftjoin(
				'delivery_item_details',
				'delivery_item_details.Challan_ID',
				'=',
				'delivery_challan_details.Challan_ID'
			)
			->leftjoin(
				'customer_detail',
				'customer_detail.cus_id',
				'=',
				'delivery_challan_details.Customer_Id'
			)
			->leftjoin(
				'org_details',
				'org_details.org_branch_id',
				'=',
				'delivery_challan_details.Seller_Branch_Id'
			)
			->select(
				'delivery_challan_details.*',
				'customer_detail.*',
				'delivery_item_details.*',
				'org_details.*'
			)
			->first();

		$challan_items = DB::table('delivery_item_details')
			->where('delivery_challan_details.company_name', $select_company[0]->company_name)
			->where('delivery_challan_details.Challan_ID', $id)
			->leftjoin('delivery_challan_details', 'delivery_challan_details.Challan_ID', '=', 'delivery_item_details.Challan_ID')

			->whereRaw('Item_Type <> ""')
			->select(
				'delivery_item_details.Item_Type as type',
				'delivery_item_details.description as descriptions',
				'delivery_item_details.Challan_ID',
				'delivery_item_details.Item_Id as id',
				'delivery_item_details.Item_Name as name',
				'delivery_item_details.HSN_SAC as hsn',
				'delivery_item_details.GST as gst',
				'delivery_item_details.Unit_Price as price',
				'delivery_item_details.Quantity as quantity',
				'delivery_item_details.Discount as discount',
				'delivery_item_details.Item_Amount as item_total',
				'delivery_item_details.CGST_Percentage as cgst',
				'delivery_item_details.CGST_Amount as cgst_amount',
				'delivery_item_details.SGST_Percentage as sgst',
				'delivery_item_details.SGST_Amount as sgst_amount',
				'delivery_item_details.Tax_Amount as tax_amount',
				'delivery_item_details.Total_Bill_Amount as tot',
			)
			->get();

		$total = DB::table('delivery_challan_details')
			->where('delivery_challan_details.Challan_ID', $id)
			->where('delivery_challan_details.Company_Name', $select_company[0]->company_name)
			->leftjoin('delivery_item_details', 'delivery_item_details.Challan_ID', '=', 'delivery_challan_details.Challan_ID')
			->select([
				DB::raw("SUM(Item_Amount) as items_total"),
				DB::raw("SUM(CGST_Amount + SGST_Amount) as total_tax"),
			])
			->get();

		//return  response()->json($total);
		$pdf = PDF::loadView('dcprint', compact('challan', 'challan_items', 'total'));
		return $pdf->stream("dcprint.pdf", array("Attachment" => false));
	}
}
