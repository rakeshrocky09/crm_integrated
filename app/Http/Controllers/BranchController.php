<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class BranchController extends Controller
{
    public function getBranchs()
    {
        $title = 'View Organization';

        $select_company = DB::table('user_security')
            ->select('company_name')
            ->where('username', Auth::user()->username)
            ->get();

        $orgdetails = DB::table('org_details')
            ->where('org_status', '1')
            ->where('company_name', $select_company[0]->company_name)
            ->get();

        //return response()->json($orgdetails);die;
        return view('vieworganization', compact('title', 'orgdetails'));
    }

    //AJAX CALL FOR INVOICE
    public function getBranchDetails(Request $request) //Gets Sender and Receiver details
    {
        $input = $request->all();
        /* $select_company = DB::table('user_security')
            ->select('company_name')
            ->where('username', $input['username'])
            ->get(); */

        $orgdetails = DB::table('org_details')
            ->join('invoice_billing_details', 'invoice_billing_details.Seller_Branch_Id', "=", 'org_details.org_branch_id')
            ->where('org_status', '1')
            ->where('org_details.org_branch_id', $input['orgId'])
            ->where('invoice_billing_details.Seller_Branch_Id', $input['sellerBranchId'])
            ->where('invoice_billing_details.Invoice_ID', $input['invoiceId'])
            //->where('company_name', $select_company[0]->company_name)
            ->get();

        return json_encode($orgdetails);
    }
    public function getCustomerDetails(Request $request) //Gets customer details using customer id
    {
        $input = $request->all();
        $select_company = DB::table('user_security')
            ->select('company_name')
            ->where('username', $input['username'])
            ->get();

        $cusdetails = DB::table('customer_detail')
            ->where('customer_detail.cus_id', $input['cusId'])
            ->where('company_name', $select_company[0]->company_name)
            ->get();
        return json_encode($cusdetails);
    } //End of AJAX Call

    public function AddBranch(Request $request)
    {
        $validated = $request->validate([
            //'org_img' => 'required',
            'org_branch' => 'required',
            'parent_branch' => 'required',
            'reg_name' => 'required',
            'org_email' => 'required',
            'org_phone' => 'required',
            'pancard' => 'required',
            'gst_no' => 'required',
            'address' => 'required',
            'city' => 'required',
            'state' => 'required',
            'postal_code' => 'required',
            'ifsc' => 'required',
            'bank_name' => 'required',
            'ac_type' => 'required',
            'bank_branch' => 'required',
            'ac_no' => 'required',
            'ac_name' => 'required',
        ]);

        //$image = $request->file('org_img')->store('public/images/org');        

        $imageName = time() . '.' . $request->org_img->extension();
        $request->org_img->storeAs('public/images/org', $imageName);

        $img = 'org/' . $imageName;

        $org_branch = $request['org_branch'];
        $parent_branch = $request['parent_branch'];
        $reg_name = $request['reg_name'];
        $org_email = $request['org_email'];
        $org_phone = $request['org_phone'];
        $alt_phone = $request['alt_phone'];
        $pancard = $request['pancard'];
        $gst_no = $request['gst_no'];
        $address = $request['address'];
        $city = $request['city'];
        $state = $request['state'];
        $postal_code = $request['postal_code'];
        $ifsc = $request['ifsc'];
        $org_bank_name = $request['bank_name'];
        $ac_type = $request['ac_type'];
        $bank_branch = $request['bank_branch'];
        $ac_no = $request['ac_no'];
        $ac_name = $request['ac_name'];

        $username = Auth::user()->username;
        $status = '1';
        $branch_const = "BRAN";

        $select_company = DB::table('user_security')
            ->select('company_name')
            ->where('username', $username)
            ->get();

        $select_sql = DB::table('org_details')
            ->where('org_branch_name', $org_branch)
            ->where('username', $username)
            ->where('org_status', '=', '1')
            ->count();

        if ($select_sql == 0) {
            $insert = DB::table('org_details')
                ->insert([
                    'username' => $username,
                    'org_branch_const' => $branch_const,
                    'org_branch_name' => $org_branch,
                    'org_parent_name' => $parent_branch,
                    'org_reg_name' => $reg_name,
                    'org_emailid' => $org_email,
                    'org_address' => $address,
                    'org_city' => $city,
                    'org_state' => $state,
                    'org_pincode' => $postal_code,
                    'org_phone_no' => $org_phone,
                    'org_alter_phone' => $alt_phone,
                    'org_pancard' => $pancard,
                    'org_gstno' => $gst_no,
                    'org_bank_ifsc' => $ifsc,
                    'org_bank_name' => $org_bank_name,
                    'org_bank_branch' => $bank_branch,
                    'org_account_no' => $ac_no,
                    'org_acc_type' => $ac_type,
                    'org_holder_name' => $ac_name,
                    'org_status' => $status,
                    'company_name' => $select_company[0]->company_name,
                    'org_logo' => $img,
                ]);

            return redirect('vieworganization')->with('message', "Organization");
        } else {
            return redirect('vieworganization')->with('error', 'Organization');
        }
    }

    public function UpdateBranch(Request $request, $id)
    {

        /*  if ($request->hasFile('image') == '') {
            $loadimg = DB::table('org_details')
                ->where('org_branch_id', $id)
                ->select('org_logo')
                ->get();
            return $loadimg;
        } */
        $imageName = time() . '.' . $request->org_img->extension();
        $request->org_img->storeAs('public/images/org', $imageName);

        $img = 'org/' . $imageName;

        $org_branch = $request['edit_branchname'];
        $parent_branch = $request['edit_parent_branch'];
        $reg_name = $request['edit_regname'];
        $org_email = $request['edit_email'];
        $org_phone = $request['edit_phone'];
        $alt_phone = $request['edit_alterphone'];
        $pancard = $request['edit_pancard'];
        $gst_no = $request['edit_gst'];
        $address = $request['edit_address'];
        $city = $request['edit_city'];
        $state = $request['edit_state'];
        $postal_code = $request['edit_postal_code'];
        $ifsc = $request['edit_bank_ifsc'];
        $org_bank_name = $request['edit_bank_name'];
        $ac_type = $request['edit_ac_type'];
        $bank_branch = $request['edit_bank_branch'];
        $ac_no = $request['edit_ac_no'];
        $ac_name = $request['edit_acholder_name'];

        $username = Auth::user()->username;
        $company = DB::table('user_security')
            ->select('company_name')
            ->where('username', $username)
            ->get();

        $select_sql = DB::table('org_details')
            ->where('org_branch_id', $id)
            ->where('username', $username)
            ->where('company_name', $company[0]->company_name)
            ->get();

        if (count($select_sql) == 1) {
            $sql = DB::table('org_details')
                ->where('org_branch_id', $id)
                ->where('company_name', $company[0]->company_name)
                ->update([
                    'org_branch_name' => $org_branch,
                    'org_parent_name' => $parent_branch,
                    'org_reg_name' => $reg_name,
                    'org_emailid' => $org_email,
                    'org_address' => $address,
                    'org_city' => $city,
                    'org_state' => $state,
                    'org_pincode' => $postal_code,
                    'org_phone_no' => $org_phone,
                    'org_alter_phone' => $alt_phone,
                    'org_pancard' => $pancard,
                    'org_gstno' => $gst_no,
                    'org_bank_ifsc' => $ifsc,
                    'org_bank_name' => $org_bank_name,
                    'org_bank_branch' => $bank_branch,
                    'org_account_no' => $ac_no,
                    'org_acc_type' => $ac_type,
                    'org_holder_name' => $ac_name,
                    'org_logo' => $img,
                ]);

            return redirect('vieworganization')->with('update', "Organization");
        } else {
            return redirect('vieworganization')->with('error', 'Organization Update Failed Contact Admin');
        }
    }

    public function DeleteBranch(Request $request, $id)
    {
        $username = Auth::user()->username;

        $company = DB::table('user_security')
            ->select('company_name')
            ->where('username', $username)
            ->get();

        $select_sql = DB::table('org_details')
            ->where('org_branch_id', $id)
            ->where('company_name', $company[0]->company_name)
            ->count();

        if ($select_sql == 1) {
            $sql = DB::table('org_details')
                ->where('org_branch_id', $id)
                ->where('company_name', $company[0]->company_name)
                ->update(['org_status' => 0]);

            return redirect('vieworganization')->with('delete', "Organization");
        } else {
            return redirect('vieworganization')->with('error', 'Organization');
        }
    }

    public function getBranch(Request $request, $id)
    {
        $username = Auth::user()->username;
        $company = DB::table('user_security')
            ->select('company_name')
            ->where('username', $username)
            ->get();

        $sql = DB::table('org_details')
            ->where('org_status', 1)
            ->where('org_branch_id', $id)
            ->where('company_name', $company[0]->company_name)
            ->get();

        if (count($sql) >= 1) {
            return response()->json($sql);
        } else {
            return response()->json(['success' => true, 'message' => 'No Data Found'], 200);
        }
    }

    public function getAllBranchNames(Request $request)
    {
        $username = Auth::user()->username;
        $company = DB::table('user_security')
            ->select('company_name')
            ->where('username', $username)
            ->get();

        $sql = DB::table('org_details')
            ->where('org_status', '=', '1')
            //->where('org_branch_id', $input['id'])
            ->where('company_name', $company[0]->company_name)
            ->select('org_branch_name', 'org_branch_id')
            //->orderby('org_branch_id')
            ->get();

        if (count($sql) >= 1) {
            return response()->json($sql);
        } else {
            return response()->json(['success' => true, 'message' => 'No Data Found'], 200);
        }
    }

    public function getCustomerName(Request $request, $name)
    {
        $username = Auth::user()->username;
        $company = DB::table('user_security')
            ->select('company_name')
            ->where('username', $username)
            ->get();

        $sql = DB::table('customer_detail')
            ->where('cus_status', '=', '1')
            ->where('cus_branch_name', $name)
            ->where('company_name', $company[0]->company_name)
            ->get();

        if (count($sql) >= 1) {
            return response()->json($sql);
        } else {
            return response()->json(['success' => true, 'message' => 'No Data Found'], 200);
        }
    }

    public function getcustomerdetail(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make(
            $request->all(),
            [
                'branch' => 'required',
                'username' => 'required'
            ]
        );

        if ($validator->fails()) {
            $message = $validator->errors();
            return response()->json(['success' => false, 'message' => 'The given data was invalid.', 'errors' => $validator->errors()], 200);
        }

        $company = DB::table('user_security')
            ->select('company_name')
            ->where('username', $input['username'])
            ->get();

        $sql = DB::table('customer_detail')
            ->where('cus_status', '=', '1')
            ->where('cus_branch_name', $input['branch'])
            ->where('company_name', $company[0]->company_name)
            ->get();

        if (count($sql) >= 1) {
            return response()->json(['success' => true, 'Customer Details' => $sql], 200);
        } else {
            return response()->json(['success' => true, 'message' => 'No Data Found'], 200);
        }
    }
    public function getcustomerinfo(Request $request, $cusname)
    {
        $sql = DB::table('customer_detail')
            ->where('cus_status', '=', '1')
            ->where('customer_name', $cusname)
            ->get();

        if (count($sql) >= 1) {
            return response()->json($sql);
        } else {
            return response()->json(['success' => true, 'message' => 'No Data Found'], 200);
        }
    }

    public function getCustomerWithNum(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make(
            $request->all(),
            [
                'phoneno' => 'required',
                'username' => 'required'
            ]
        );

        if ($validator->fails()) {
            $message = $validator->errors();
            return response()->json(['success' => false, 'message' => 'The given data was invalid.', 'errors' => $validator->errors()], 200);
        }

        $company = DB::table('user_security')
            ->select('company_name')
            ->where('username', $input['username'])
            ->get();

        $sql = DB::table('customer_detail')
            ->where('cus_phone_no', $input['phoneno'])
            ->where('company_name', $company[0]->company_name)
            ->get();

        if (count($sql) >= 1) {
            return response()->json(['success' => true, 'Customer Details' => $sql], 200);
        } else {
            return response()->json(['success' => true, 'message' => 'No Data Found'], 200);
        }
    }
}
