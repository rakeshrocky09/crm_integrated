<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Validator;
use DB;

class UsersecurityController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth', ['except' => ['addUserDetails']]);
    }
    
    public function addUserDetails(Request $request)
	{
		$input = $request->all();
		$validator = Validator::make($request->all(), 
		[
			'username' => 'required',
			'companyname' => 'required'
		]);

		if ($validator->fails()) 
		{
            $message = $validator->errors();
            return response()->json(['success' => false, 'messages' => 'The given data was invalid.', 'errors' => $validator->errors()], 200);
        }

        if($input['username'] !=''){
        	$check_user = DB::table('user_security')->where('company_name',$input['companyname'])->where('username',$input['username'])->get();
        }else{
        	$check_user = [];
        }
        if(count($check_user) > 0)
		{
			return response()->json(['success' => false,'message'=>'User Already Exist'], 200);
		}
		else
		{
			$insertuser = DB::table('user_security')->insert(['company_name' => $input['companyname'],'username' => $input['username'],'password' => Hash::make($input['password']),'created_by' => 'admin','created_date'=>date('Y-m-d H:i:s')]);

			return response()->json(['success' => true,'message'=>'User Added Successfully'], 200);
		}
	}
}
